/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13235 $
 *
 * $Date: 2010-11-22 14:58:40 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.app;

import org.eclipse.ui.application.WorkbenchAdvisor;

import com.bredexsw.guidancer.app.core.GUIdancerWorkbenchAdvisor;
import com.bredexsw.guidancer.app.i18n.Messages;

/**
 * @author BREDEX GmbH
 */
public class Launcher extends org.eclipse.jubula.app.Launcher {
    @Override
    protected String getAppName() {
        return Messages.MainTitle;
    }

    @Override
    protected WorkbenchAdvisor getWorkbenchAdvisor() {
        return new GUIdancerWorkbenchAdvisor();
    }
}
