/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13235 $
 *
 * $Date: 2010-11-22 14:58:40 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.app.core;

import org.eclipse.jubula.app.core.JubulaWorkbenchAdvisor;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

/**
 * @author markus
 * @created Nov 22, 2010
 */
public class GUIdancerWorkbenchAdvisor extends JubulaWorkbenchAdvisor {
    /**
     * {@inheritDoc}
     */
    public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(
            IWorkbenchWindowConfigurer configurer) {
        return new GUIdancerWorkbenchWindowAdvisor(configurer);
    }
}
