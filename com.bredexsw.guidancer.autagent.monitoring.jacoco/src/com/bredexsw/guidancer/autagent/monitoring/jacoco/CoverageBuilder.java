/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.autagent.monitoring.jacoco;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICoverageVisitor;
import org.jacoco.core.analysis.ISourceFileCoverage;
import org.jacoco.core.internal.analysis.BundleCoverageImpl;
import org.jacoco.core.internal.analysis.SourceFileCoverageImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bredex CoverageBuilder which ignores duplicate class file names even if their
 * are different.
 *
 * @author BREDEX GmbH
 * @created Feb 15, 2011
 */
public class CoverageBuilder implements ICoverageVisitor {
    /** the logger */
    private static Logger log = LoggerFactory.getLogger(CoverageBuilder.class);
    /** class coverage **/
    private final Map<String, IClassCoverage> m_classes;
    /** sourcefile coverage */
    private final Map<String, ISourceFileCoverage> m_sourcefiles;
    /** pattern matchers */
    private final Set<Pattern> m_patterns;
    /** flag which indicates, allowing class file id collision is allowed or not */
    private Boolean m_allowClassFileIdCollision;
   

    /**
     * Create a new builder with pattern to filter classes
     * 
     * @param patterns
     *            Patterns to filter classes
     * @param allowClassFileIdCollision
     *            class file id collision is enabled
     */
    public CoverageBuilder(String patterns, Boolean allowClassFileIdCollision) {
        this.m_classes = new HashMap<String, IClassCoverage>();
        this.m_sourcefiles = new HashMap<String, ISourceFileCoverage>();
        this.m_allowClassFileIdCollision = allowClassFileIdCollision;
        this.m_patterns = new HashSet<Pattern>();
        processPatterns(patterns);
    }
    
    /**
     * Process the patterns for adding coverage
     * 
     * @param patterns
     *            patterns in concatenated string separated by semicolon
     */
    private void processPatterns(String patterns) {
        if (!StringUtils.isEmpty(patterns)) {
            String[] matcherPatterns = patterns
                    .split(StringConstants.SEMICOLON);
            for (String matcherPattern : matcherPatterns) {
                m_patterns.add(Pattern.compile(matcherPattern));
            }
        }
    }

    /**
     * Returns all class nodes currently contained in this builder.
     * 
     * @return all class nodes
     */
    public Collection<IClassCoverage> getClasses() {
        return Collections.unmodifiableCollection(m_classes.values());
    }

    /**
     * Returns all source file nodes currently contained in this builder.
     * 
     * @return all source file nodes
     */
    public Collection<ISourceFileCoverage> getSourceFiles() {
        return Collections.unmodifiableCollection(m_sourcefiles.values());
    }

    /**
     * Creates a bundle from all nodes currently contained in this bundle.
     * 
     * @param name
     *            Name of the bundle
     * @return bundle containing all classes and source files
     */
    public BundleCoverageImpl getBundle(final String name) {
        return new BundleCoverageImpl(name, m_classes.values(),
                m_sourcefiles.values());
    }

    /**
     * Check classes against package pattern and put them into the classes to
     * analyze map
     * 
     * @param coverage
     *            The Class coverage
     */
    public void visitCoverage(final IClassCoverage coverage) {

        // Only consider classes that actually contain code:
        if (coverage.getInstructionCounter().getTotalCount() > 0) {
            if (m_patterns.size() > 0) {
                for (Pattern pattern : m_patterns) {
                    if (StringConstants.EMPTY.equals(pattern.pattern())) {
                        addCoverage(coverage);
                    } else {
                        Matcher matcher = pattern
                                .matcher(coverage.getName().replace('/', '.'));
                        if (matcher.matches()) {
                            addCoverage(coverage);
                        }
                    }
                }
            } else {
                // If no pattern is given, do a full coverage
                addCoverage(coverage);
            }
        }
    }

    /**
     * @param coverage
     *            the coverage
     */
    private void addCoverage(final IClassCoverage coverage) {
        String coverageName = coverage.getName();
        final IClassCoverage dup = m_classes.put(coverageName, coverage);
        if (dup != null && dup.getId() != coverage.getId()) {
            if (m_allowClassFileIdCollision) {
                log.warn("CODE-COVERAGE: different class with same name found: " //$NON-NLS-1$
                        + coverageName);
            } else {
                throw new IllegalStateException(
                        "CODE-COVERAGE: different class with same name found: " //$NON-NLS-1$
                                + coverageName);
            }
        }
        final String source = coverage.getSourceFileName();
        if (source != null) {
            final ISourceFileCoverage sourceFile = getSourceFile(source,
                    coverage.getPackageName());
            ((SourceFileCoverageImpl) sourceFile).increment(coverage);
        }
    }

    /**
     * 
     * @param filename
     *            The filename
     * @param packagename
     *            The packagename
     * @return An {@link ISourceFileCoverage} instance
     */
    private ISourceFileCoverage getSourceFile(final String filename,
            final String packagename) {
        final String key = packagename + '/' + filename;
        ISourceFileCoverage sourcefile = m_sourcefiles.get(key);
        if (sourcefile == null) {
            sourcefile = new SourceFileCoverageImpl(filename, packagename);
            m_sourcefiles.put(key, sourcefile);
        }
        return sourcefile;
    }
}