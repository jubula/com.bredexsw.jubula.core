/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13293 $
 *
 * $Date: 2010-11-23 17:01:40 +0100 (Tue, 23 Nov 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.autagent.monitoring.jacoco;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.autagent.common.monitoring.AbstractMonitoring;
import org.eclipse.jubula.autagent.common.monitoring.MonitoringDataStore;
import org.eclipse.jubula.autagent.common.monitoring.MonitoringUtil;
import org.eclipse.jubula.tools.internal.constants.AutConfigConstants;
import org.eclipse.jubula.tools.internal.constants.MonitoringConstants;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.IMonitoringValue;
import org.eclipse.jubula.tools.internal.objects.MonitoringValue;
import org.eclipse.jubula.tools.internal.utils.NetUtil;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.ICoverageNode;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.data.ExecutionDataWriter;
import org.jacoco.core.data.SessionInfoStore;
import org.jacoco.core.internal.analysis.BundleCoverageImpl;
import org.jacoco.core.runtime.RemoteControlReader;
import org.jacoco.core.runtime.RemoteControlWriter;
import org.jacoco.report.DirectorySourceFileLocator;
import org.jacoco.report.IReportVisitor;
import org.jacoco.report.MultiSourceFileLocator;
import org.jacoco.report.ZipMultiReportOutput;
import org.jacoco.report.html.HTMLFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**   
 * This class uses the JaCoCo-API to calculate the code coverage value and 
 * to generate the HTML-Report
 * 
 * @author marc
 * @created 05.08.2010
 */
public class MonitoringImpl extends AbstractMonitoring {
    /** the logger */
    private static final Logger LOG = LoggerFactory
            .getLogger(MonitoringImpl.class);    
   
    /** Key for autConfigMap, this key was set in the extension point*/
    private static final String JAR_FILE_PATH_KEY  = "JAR_FILE_PATH"; //$NON-NLS-1$ 
    /** Key for autConfigMap, this key was set in the extension point*/
    private static final String AGENT_PORT_KEY = "MONITORING_AGENT_PORT"; //$NON-NLS-1$
    /** Key for autConfigMap, this key was set in the extension point*/
    private static final String PATTERN_KEY = "PATTERN"; //$NON-NLS-1$  
    /** the data store */    
    private MonitoringDataStore m_dataStore = 
        MonitoringDataStore.getInstance(); 
    /** all files that will be analyzed */
    private LinkedList<File> m_filesToAnalyze = new LinkedList<File>();    
    /** File extension that will be included into analysis */
    private String[] m_fileExtensions = 
        new String[] { "jar", "zip", "class", "java" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    /** CoverageNode */
    private IBundleCoverage m_bundleCoverage = null; 
    /** a data store */
    private ExecutionDataStore m_executionDataStore = new ExecutionDataStore();
    /** a data store */
    private SessionInfoStore m_sessionInfoStore = new SessionInfoStore();
    /** path of where the monitoring data should be stored and kept */
    private String m_permanentMonitoringDirectory;
    /**
     * {@inheritDoc}
     */
    public String createAgent() {
             
        String agentPort = String.valueOf(NetUtil.getFreePort());        
        boolean duplicate = MonitoringUtil.checkForDuplicateAutID(getAutId());
        if (!duplicate) {
            m_dataStore.putConfigValue(getAutId(), AGENT_PORT_KEY, agentPort);
        }         
        String jarFilePath = getMonitoringAttribute(JAR_FILE_PATH_KEY);
        String seperator = ","; //$NON-NLS-1$
        String outputMode = "output=tcpserver"; //$NON-NLS-1$
        String address = "address=" + getMonitoringAttribute(AutConfigConstants.AUT_AGENT_HOST); //$NON-NLS-1$
        String port = "port=" + agentPort; //$NON-NLS-1$
        String sessionId = "sessionid=" + getAutId(); //$NON-NLS-1$    
        File jacocoAgentJarDir = new File(getInstallDir(), jarFilePath);
        String jacocoAbsPath = "\"-javaagent:" //$NON-NLS-1$
            + jacocoAgentJarDir.getAbsolutePath() + "="; //$NON-NLS-1$         
        
        StringBuilder sb = new StringBuilder();   
        sb.append(jacocoAbsPath);
        sb.append(outputMode);
        sb.append(seperator);
        sb.append(port);
        sb.append(seperator);
        sb.append(address);
        sb.append(seperator);
        sb.append(sessionId);
        sb.append(StringConstants.QUOTE);         
        return sb.toString();
       
    } 
    
    /**
     * {@inheritDoc}
     */
    public Map<String, IMonitoringValue> getMonitoringData() {
        try {   
            
            loadExecutionData();
            mergeExecutionData();
           
            String workingDirPath = getMonitoringAttribute(
                    AutConfigConstants.WORKING_DIR);    
            String instFileAndDirPathes = StringUtils.defaultString(
                    getMonitoringAttribute(MonitoringConstants.INSTALL_DIR));

            initInstallFilesAndDirs(workingDirPath, instFileAndDirPathes);  
           
            CoverageBuilder coverageBuilder = new CoverageBuilder(
                    getMonitoringAttribute(PATTERN_KEY),
                    Boolean.parseBoolean(getMonitoringAttribute(
                            AutConfigConstants.ALLOW_CLASS_FILE_ID_COLLISION)));
            Analyzer analyzer = new Analyzer(m_executionDataStore,
                    coverageBuilder);

            for (File file : m_filesToAnalyze) {
                analyzer.analyzeAll(file);
            }
            m_filesToAnalyze.clear();
            LOG.info("JaCoCo analyzed " + coverageBuilder.getClasses().size() + " classes successfully"); //$NON-NLS-1$ //$NON-NLS-2$
            m_bundleCoverage = 
                new BundleCoverageImpl(getAutId(),    
                    coverageBuilder.getClasses(), 
                    coverageBuilder.getSourceFiles());
            
            return fillMapToSend(m_bundleCoverage);
        
        } catch (IOException e) {
            
            LOG.error("Error while analyzing class files", e); //$NON-NLS-1$  
            Map<String, IMonitoringValue> values = 
                    new HashMap<String, IMonitoringValue>();  
                
            values.put(MonitoringConstants.MONITORING_ERROR,
                    new MonitoringValue(e.getMessage(),
                            MonitoringConstants.STRING_VALUE,
                            MonitoringConstants.MONITORING_ERROR, true));
            return values;                     
        }
        
    }

    /**
     * Initialize the files and directories to analyze.
     * 
     * @param workingDirPath
     *            the current working directory
     * @param instFileAndDirPathes
     *            files and directories in concatenated form
     */
    private void initInstallFilesAndDirs(String workingDirPath,
            String instFileAndDirPathes) {
        
        if (instFileAndDirPathes == null) {
            return;
        }
        
        String[] pathes = instFileAndDirPathes.split(StringConstants.SEMICOLON);
        for (String path : pathes) {
            File dirToUse = new File(path);
            if (!dirToUse.isAbsolute() 
                    && !StringUtils.isEmpty(workingDirPath)) {
                dirToUse = new File(workingDirPath, path);
            }

            //scan directory for files to analyze
            scanDirectoy(dirToUse);
        }
    }
    /**
     * This method fills a map with the calculated monitoring values 
     * @param coverageNode A ICoverageNode instance
     * @return Returns a map with MonitoringValues
     */
    @SuppressWarnings("nls")
    private Map<String, IMonitoringValue> fillMapToSend(
            ICoverageNode coverageNode) {        
        
        Map<String, IMonitoringValue> values = 
            new HashMap<String, IMonitoringValue>();  
                                     
        values.put("Instruction Coverage Ratio", new MonitoringValue(
                checkNaN(String.valueOf(coverageNode.getInstructionCounter().
                        getCoveredRatio())), MonitoringConstants.PERCENT_VALUE, 
                "Instruction Coverage", true));        
        values.put("Covered Instruction Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getInstructionCounter().getCoveredCount()),
                MonitoringConstants.INTEGER_VALUE, "Instruction Coverage"));
        values.put("Total Instruction Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getInstructionCounter().getTotalCount()), 
                MonitoringConstants.INTEGER_VALUE, "Instruction Coverage"));
        values.put("Class Coverage Ratio", new MonitoringValue(
                checkNaN(String.valueOf(
                        coverageNode.getClassCounter().getCoveredRatio())), 
                MonitoringConstants.PERCENT_VALUE, "Class Coverage"));        
        values.put("Covered Classes Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getClassCounter().getCoveredCount()), 
                MonitoringConstants.INTEGER_VALUE, "Class Coverage"));        
        values.put("Total Classes Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getClassCounter().getTotalCount()), 
                MonitoringConstants.INTEGER_VALUE, "Class Coverage")); 
        values.put("Method Coverage Ratio", new MonitoringValue(
                checkNaN(String.valueOf(
                        coverageNode.getMethodCounter().getCoveredRatio())), 
                MonitoringConstants.PERCENT_VALUE, "Method Coverage"));        
        values.put("Covered Methods Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getMethodCounter().getCoveredCount()), 
                MonitoringConstants.INTEGER_VALUE, "Method Coverage"));        
        values.put("Total Methods Count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getMethodCounter().getTotalCount()), 
                MonitoringConstants.INTEGER_VALUE, "Method Coverage"));        
        values.put("Branch Coverage ratio", new MonitoringValue(
                checkNaN(String.valueOf(
                        coverageNode.getBranchCounter().getCoveredRatio())), 
                MonitoringConstants.PERCENT_VALUE, "Branch Coverage"));        
        values.put("Covered branches count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getBranchCounter().getCoveredCount()), 
                MonitoringConstants.INTEGER_VALUE, "Branch Coverage"));        
        values.put("Total branch count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getBranchCounter().getTotalCount()), 
                MonitoringConstants.INTEGER_VALUE, "Branch Coverage"));       
        values.put("Covered Complexity count", new MonitoringValue(
                String.valueOf(
                        coverageNode.getComplexityCounter().getCoveredCount()),
                MonitoringConstants.INTEGER_VALUE, "McCabe Complexity"));
        values.put("Total Complexity count", new MonitoringValue(
                String.valueOf(coverageNode.getComplexityCounter()
                        .getTotalCount()), 
                MonitoringConstants.INTEGER_VALUE, "McCabe Complexity"));
                
        return values;
    }

    /**
     * checks if the coverage ratio is "NaN". This happens when no ratio could
     * be calculated, because the covered count was 0. 
     * @param stringToCheck String to check for "NaN";
     * @return if "NaN" then 0.0 or the old value
     */
    private String checkNaN(String stringToCheck) {
        if (stringToCheck.equals("NaN")) { //$NON-NLS-1$
            LOG.info("JaCoCo could not calculated a particular coverage value"); //$NON-NLS-1$
            return "0.0"; //$NON-NLS-1$
        }
        return stringToCheck;
    }
    
    /** 
     *  Generates the JaCoCo HTML report  
     *  
     *  @param out The OutputStream to use for the Socket connection. 
     *  Everything what is written to this OutputStream will be streamed 
     *  back to the client and stored in the database. 
     */
    public void writeMonitoringReport(OutputStream out) {
        
        String configName = 
            getMonitoringAttribute(AutConfigConstants.AUT_CONFIG_NAME);        
        String reportName = getAutId() + " " + "(" + configName + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        String footer = "ITE - Functional Testing Code-Coverage Report for " + reportName; //$NON-NLS-1$       
        String workingDirPath = getMonitoringAttribute(
                AutConfigConstants.WORKING_DIR);
     
        //setting the ITE monitoring report footer text
        HTMLFormatter formatter = new HTMLFormatter();
        formatter.setFooterText(footer);        
        formatter.setLocale(Locale.getDefault());        
        ZipOutputStream zipStream = null;
        try {            
            zipStream = new ZipOutputStream(out);             
            IReportVisitor reportVisitor = formatter.createVisitor(
                    new ZipMultiReportOutput(zipStream));
            
            reportVisitor.visitInfo(m_sessionInfoStore.getInfos(), 
                    m_executionDataStore.getContents());
            
            MultiSourceFileLocator locator = new MultiSourceFileLocator(4);
            
            String srcDirectories = StringUtils.defaultString(
                    getMonitoringAttribute(MonitoringConstants.SOURCE_DIRS));
            
            if (!StringUtils.isEmpty(srcDirectories)) {
                String[] directories = srcDirectories
                        .split(StringConstants.SEMICOLON);
                for (String directory : directories) {
                    File dirToUse = new File(directory);
                    if (!dirToUse.isAbsolute()
                            && !StringUtils.isEmpty(workingDirPath)) {
                        dirToUse = new File(workingDirPath, directory);   
                    }
                    locator.add(new DirectorySourceFileLocator(dirToUse,
                            "utf-8", 4)); //$NON-NLS-1$
                }
            }
            
            reportVisitor.visitBundle(m_bundleCoverage, locator);           
            reportVisitor.visitEnd();                       
        } catch (IOException e) {
            LOG.error("Error while building report", e); //$NON-NLS-1$ 
        } finally {
            try {
                if (zipStream != null) {
                    zipStream.close();
                }                
            } catch (IOException e) {
                LOG.error("Error while closing zipOutStream", e); //$NON-NLS-1$                    
            }            
        }
    } 
    
    /**
     * This method connects to the JaCoCo agent and resets the collected
     * execution data.
     */
    public void resetMonitoringData() {
        
        String autHost = getMonitoringAttribute(
                AutConfigConstants.AUT_AGENT_HOST);
  
        int agentPort = Integer.valueOf(
                getMonitoringAttribute(AGENT_PORT_KEY));  
        Socket socket = null;
        try { 
            
            m_executionDataStore = new ExecutionDataStore(); 
            m_sessionInfoStore = new SessionInfoStore();
            socket = new Socket(InetAddress.getByName(autHost), agentPort);
            RemoteControlWriter writer = new RemoteControlWriter(socket
                    .getOutputStream());
            RemoteControlReader reader = new RemoteControlReader(socket
                    .getInputStream());
            reader.setSessionInfoVisitor(m_sessionInfoStore);
            reader.setExecutionDataVisitor(m_executionDataStore); 
            writer.visitDumpCommand(false, true);
            reader.read();  
        } catch (IOException e) {
            LOG.error("Error while resetting monitoring data", e); //$NON-NLS-1$           
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    LOG.error("Error while resetting monitoring data", e); //$NON-NLS-1$                    
                }
            }
        }
    }      
    /**
     * {@inheritDoc}
     */
    public void autRestartOccurred() {
        loadExecutionData();
    }
    /**
     * loads the executionDatafrom the agent
     */
    private void loadExecutionData() {

        String autHost = 
            getMonitoringAttribute(AutConfigConstants.AUT_AGENT_HOST);
        int agentPort = Integer.valueOf(getMonitoringAttribute(AGENT_PORT_KEY));
        Socket socket = null;
        OutputStream outputStream = null;
        
        try { 
            File monitoringDir = getMonitoringDirectory();
            // Remove monitoring data, only if it's created in the tmp directory.
            if (m_permanentMonitoringDirectory == null) {
                monitoringDir.deleteOnExit();
            }
            int fileCounter = 0;
            File monitoringExecFile = new File(
                    monitoringDir, getAutId() + fileCounter + ".exec"); //$NON-NLS-1$
            while (!monitoringExecFile.createNewFile()) {
                monitoringExecFile = new File(monitoringDir, getAutId() + fileCounter + ".exec"); //$NON-NLS-1$
                fileCounter++;
            }  
            outputStream = new FileOutputStream(monitoringExecFile);
            socket = new Socket(InetAddress.getByName(autHost), agentPort);
            RemoteControlWriter writer = new RemoteControlWriter(
                    socket.getOutputStream());
            RemoteControlReader reader = new RemoteControlReader(
                    socket.getInputStream());
            ExecutionDataWriter outputWriter = 
                    new ExecutionDataWriter(outputStream);
            reader.setSessionInfoVisitor(outputWriter);
            reader.setExecutionDataVisitor(outputWriter);
            writer.visitDumpCommand(true, false);           
            reader.read();                
        } catch (IOException e) { 
            LOG.error("Error while loading monitoring information from jacocoagent", e); //$NON-NLS-1$             
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                LOG.error("Error while loading monitoring information from jacocoagent", e); //$NON-NLS-1$ 
               
            }                           
        }                
    }       
    /**
     * 
     * Iterates over the saved exec files and loads its contents
     * 
     **/
    private void mergeExecutionData() {
        
        File[] execFileArray = findExecFiles();        
        for (int i = 0; i < execFileArray.length; i++) {            
            FileInputStream resourceStream = null;
            File file = null;
            try {                      
                file = execFileArray[i];                
                resourceStream = new FileInputStream(file);                
                ExecutionDataReader reader = 
                    new ExecutionDataReader(resourceStream);                
                reader.setSessionInfoVisitor(m_sessionInfoStore);
                reader.setExecutionDataVisitor(m_executionDataStore);
                reader.read();                 
            } catch (IOException e) {
                deleteExecFiles();
                LOG.error("Error while merging execution data. Maybe the *.exec files are corrupted", e); //$NON-NLS-1$
            } finally {
                try {
                    if (resourceStream != null) {
                        resourceStream.close();
                    }
                    if (file != null) {
                        if (m_permanentMonitoringDirectory == null) {
                            boolean success = file.delete();
                            if (!success) {
                                LOG.error(
                                        "Could not delete *.exec file after merging"); //$NON-NLS-1$
                            }
                        }
                    }
                } catch (IOException e) {
                    LOG.error("Error while merging execution data", e);  //$NON-NLS-1$
                }    
            }
        }
        
    } 
    /**
     * finds *.exec files
     * @return A File array with the found files.
     */
    private File[] findExecFiles() {        
        
        File tmpDir = getMonitoringDirectory();
        File[] execFileArray = tmpDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".exec"); //$NON-NLS-1$
            }
        });
        
        return execFileArray;
    }
    
    /**
     * deletes old *.exec files in case of an exception or test ending 
     */
    private void deleteExecFiles() {
        
        File[] filesToDelete = findExecFiles();
        for (int i = 0; i < filesToDelete.length; i++) {
            boolean success = filesToDelete[i].delete();             
            if (!success) {
                LOG.error("Could not delete file or file does not exists"); //$NON-NLS-1$
            }
        }
                
    }
    /**
     * Directory where the monitoring data is stored.
     * @return The directory where the monitoring data is stored
     */
    private File getMonitoringDirectory() {
        int agentPort = Integer.valueOf(getMonitoringAttribute(
                AutConfigConstants.AUT_AGENT_PORT));
        
        m_permanentMonitoringDirectory = getMonitoringAttribute(
                AutConfigConstants.EXTERNAL_MONITORING_REPORT_PATH);
        
        // If monitoring directory is not set, the tmp will be used.
        File monitoringDirectory = new File(
                m_permanentMonitoringDirectory != null
                        ? m_permanentMonitoringDirectory
                        : System.getProperty("java.io.tmpdir") //$NON-NLS-1$
                                + File.separator
                                + MonitoringConstants.TEMP_FOLDER_NAME
                                + agentPort);
        if (!monitoringDirectory.exists()) {
            boolean success = monitoringDirectory.mkdir();
            if (!success) {
                LOG.error("Could not create monitoring directory"); //$NON-NLS-1$
            }
        }        
        return monitoringDirectory;
    }
    
    /** The given directory will be scanned for files to analyze. Only files that
     *  could be read be the application will be collected. This is necessary 
     *  because it is possible that files can be blocked by other applications
     *  @param file The directory to scan     
     */
    private void scanDirectoy(File file) {
        
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] children = file.listFiles();
                for (int i = 0; i < children.length; i++) {
                    scanDirectoy(children[i]);
                }
            } else {          
                for (String extension : m_fileExtensions) {                
                    if (file.getName().toLowerCase().endsWith(extension)) {
                        m_filesToAnalyze.add(file);
                    }
                }
            } 
        } else {
            LOG.warn("Cannot scan path '" + file.getAbsolutePath() + "'. Path does not exist."); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * {@inheritDoc}
     */
    public void timeout() {
        LOG.error(this.getClass().getName() + ".timeout() called"); //$NON-NLS-1$

    }    
}