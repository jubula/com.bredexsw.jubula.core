/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved.
 *******************************************************************************/
package com.bredexsw.guidancer.client.monitoring.jacoco.i18n;

import org.eclipse.osgi.util.NLS;

/**
 * @author BREDEX GmbH
 * @created 03.03.2011
 */
public class Messages extends NLS {

    /**
     * Key for getting message for invalid package definition.
     */
    public static String InvalidPackagesDefinition;

    /**
     * Bundle name of messages.
     */
    private static final String BUNDLE_NAME = "com.bredexsw.guidancer.client.monitoring.jacoco.i18n.messages"; //$NON-NLS-1$

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    /**
     * Constructor
     */
    private Messages() {
        // hide
    }
}
