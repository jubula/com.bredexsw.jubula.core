/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13473 $
 *
 * $Date: 2010-12-02 18:00:11 +0100 (Thu, 02 Dec 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.client.monitoring.jacoco.validator;


import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

import com.bredexsw.guidancer.client.monitoring.jacoco.i18n.Messages;
import com.bredexsw.jubula.client.monitoring.validator.PatternValidator;
/** 
 * A validator to verify that the given pattern is valid for JaCoCo 
 * @author marc
 * @created 02.12.2010
 */
public class JaCoCoPatternValidator extends PatternValidator {      
  
    /**
     * {@inheritDoc}
     */
    public IStatus validate(Object value) {
        String packageNames = String.valueOf(value);
        if (StringUtils.isEmpty(packageNames)) {
            // Package name is not mandatory, so the empty package name is valid.
            return ValidationStatus.ok();
        }
        
        String[] matcherPatterns = packageNames
                .split(StringConstants.SEMICOLON);
        
        int countOfSemiColons = StringUtils.countMatches(packageNames,
                StringConstants.SEMICOLON);
        if (countOfSemiColons > matcherPatterns.length) {
            // Do not to allow empty strings separated by semicolons.
            return ValidationStatus
                    .error(Messages.InvalidPackagesDefinition);
        }
        
        for (String patternToValidate : matcherPatterns) {
            try {
                Pattern.compile(patternToValidate);
            } catch (PatternSyntaxException p) {
                return ValidationStatus
                        .error(Messages.InvalidPackagesDefinition);
            }
        }

        return ValidationStatus.ok();
    }
}
