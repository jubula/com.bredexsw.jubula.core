/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved.
 *******************************************************************************/
package com.bredexsw.guidancer.client.support.ui.i18n;

import org.eclipse.osgi.util.NLS;

/**
 * @author BREDEX GmbH
 * @created 03.03.2011
 */
public class Messages extends NLS {
    private static final String BUNDLE_NAME = "com.bredexsw.guidancer.client.support.ui.i18n.messages"; //$NON-NLS-1$
    
    public static String SupportRequestMailDialogAddConfigInfoLabel;
    public static String SupportRequestMailDialogAddAutStarterLogLabel;
    public static String SupportRequestMailDialogAddClientLogLabel;
    public static String SupportRequestMailDialogAddProjectsLabel;
    public static String SupportRequestMailDialogMessage;
    public static String SupportRequestMailDialogShellTitle;
    public static String SupportRequestMailDialogTitle;
    public static String SupportInfoPackageContentsGroupTitle;
    public static String SupportInfoPackageDestinationGroupTitle;
    public static String SupportInfoPackageDestinationLabel;
    public static String SupportInfoPackageDialogDestBrowseButtonText;
    public static String SupportInfoPackageFileLabel;
    public static String SupportInfoPackageOverwriteDialogMsg;
    public static String SupportInfoPackageOverwriteDialogTitle;
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    /**
     * Constructor
     */
    private Messages() {
        // hide
    }
}
