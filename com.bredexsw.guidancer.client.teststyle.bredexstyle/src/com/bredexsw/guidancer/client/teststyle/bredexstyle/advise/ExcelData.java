package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.jubula.client.core.model.ITestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 *
 */
public class ExcelData extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = Messages.CheckMessageExcelData;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }
    
    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof ITestCasePO)) {
            return false;
        }
        ITestCasePO testCase = (ITestCasePO) obj;
        return testCase.getDataFile() != null;
    }

}
