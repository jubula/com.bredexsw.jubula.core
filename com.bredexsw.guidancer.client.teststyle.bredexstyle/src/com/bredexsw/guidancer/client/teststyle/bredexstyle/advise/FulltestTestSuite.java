package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jubula.client.core.businessprocess.db.TestSuiteBP;
import org.eclipse.jubula.client.core.events.DataEventDispatcher;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.DataState;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.UpdateState;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.ITestSuitePO;
import org.eclipse.jubula.client.core.model.NodeMaker;
import org.eclipse.jubula.client.core.persistence.GeneralStorage;
import org.eclipse.jubula.client.core.persistence.NodePM;
import org.eclipse.jubula.client.core.persistence.PMException;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.client.teststyle.quickfix.Quickfix;
import org.eclipse.jubula.client.ui.rcp.controllers.PMExceptionHandler;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class FulltestTestSuite extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = 
        Messages.CheckMessageFulltestTestSuite;
    
    /** name */
    public static final String FULLTEST_PARAMETER = "name contains"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return NLS.bind(ERROR_MSG, getAttributeValue(FULLTEST_PARAMETER));
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IProjectPO)) {
            return false;
        }
        
        boolean exists = false;
        
        IProjectPO project = (IProjectPO)obj;
        for (ITestSuitePO p : TestSuiteBP.getListOfTestSuites(project)) {
            if (p.getName().contains(getAttributeValue(FULLTEST_PARAMETER))) {
                exists = true;
                break;
            }
        }
        
        return !exists;
    }
    
    @Override
    public Quickfix[] getQuickfix(Object obj) {
        return new Quickfix[] { new BrokenTestSuiteQuickfix() };
    }
    
    /** {@inheritDoc} */
    private class BrokenTestSuiteQuickfix extends Quickfix {
        
        /** {@inheritDoc} */
        public String getLabel() {
            return NLS.bind(Messages.QuickfixFulltest,
                    getAttributeValue(FULLTEST_PARAMETER));
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            try {
                ITestSuitePO testSuite = NodeMaker.createTestSuitePO(
                        getAttributeValue(FULLTEST_PARAMETER));
                BrokenTestSuite.setDefaultValuesToTestSuite(testSuite);
                INodePO parent = GeneralStorage.getInstance().getProject().
                        getExecObjCont();
                NodePM.addAndPersistChildNode(parent, testSuite, null);
                DataEventDispatcher.getInstance().fireDataChangedListener(
                        testSuite, DataState.Added, UpdateState.all);
            } catch (PMException e) {
                PMExceptionHandler.handlePMExceptionForMasterSession(e);
            } catch (Exception e) {
                PMExceptionHandler.handleProjectDeletedException();
            }
        }
    }
}
