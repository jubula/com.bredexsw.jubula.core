package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.jubula.client.core.model.ICategoryPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.ISpecTestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class NumberOfChildren extends BaseCheck {
    
    /** Error message */
    private static final String ERROR_MSG = 
        Messages.CheckMessageNumberOfChildren;

    /** name of the parameter which contains the quantity */
    private static final String QUANTITY = "quantity"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return NLS.bind(ERROR_MSG, getDefinedNumber());
    }

    @Override
    public boolean hasError(Object obj) {        
        boolean hasError = false;
        if (obj instanceof ICategoryPO) {
            ICategoryPO cat = (ICategoryPO) obj;
            if (cat.getNodeListSize() > getDefinedNumber()) {
                hasError = true;
            }
        } else if (obj instanceof IProjectPO) {
            IProjectPO project = (IProjectPO) obj;
            int children = getNumberOfChildrenInRoot(project);
            if (children > getDefinedNumber()) {
                hasError = true;
            }
        }
        return hasError;
    }
    
    /**
     * 
     * @param project Project for obtaining the root.
     * @return the children in the root
     */
    private int getNumberOfChildrenInRoot(IProjectPO project) {
        int children = 0;
        for (INodePO spec : project.getUnmodSpecList()) {
            if (spec instanceof ISpecTestCasePO) {
                children++;
            }
        }
        return children;
    }
    
    /**
     * @return The quantity defined by the attribute or the default if the
     * defined value is not a valid number.
     */
    private int getDefinedNumber() {
        return getIntegerAttributeValue(QUANTITY);
    }
}
