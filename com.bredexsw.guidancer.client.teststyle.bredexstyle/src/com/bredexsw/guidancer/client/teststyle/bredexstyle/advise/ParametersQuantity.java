package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.jubula.client.core.model.ITestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 * @created Nov 9, 2010
 * @version $Revision: 1.1 $
 */
public class ParametersQuantity extends BaseCheck {
    
    /** Error message */
    private static final String DESCRIPTION = 
        Messages.CheckMessageParametersQuantity;

    /** Name of the parameter which contains the quantity */
    private static final String QUANTITY = "quantity"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return NLS.bind(DESCRIPTION, getQuantity());
    }

    @Override
    public boolean hasError(Object obj) {
        boolean hasError = false;
        if (obj instanceof ITestCasePO) {
            ITestCasePO po = (ITestCasePO)obj;
            if (po.getParameterList().size() > getQuantity()) {
                hasError = true;
            }
        }
        return hasError;
    }

    /**
     * @return The quantity defined by the attribute or the default if the
     * defined value is not a valid number.
     */
    private int getQuantity() {
        return getIntegerAttributeValue(QUANTITY);
    }
}
