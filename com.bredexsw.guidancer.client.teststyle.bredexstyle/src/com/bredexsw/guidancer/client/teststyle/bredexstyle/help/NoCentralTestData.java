package com.bredexsw.guidancer.client.teststyle.bredexstyle.help;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jubula.client.core.businessprocess.TestDataCubeBP;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.ITestDataCategoryPO;
import org.eclipse.jubula.client.core.model.ITestDataCubePO;
import org.eclipse.jubula.client.core.persistence.GeneralStorage;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.client.teststyle.quickfix.Quickfix;
import org.eclipse.jubula.client.ui.rcp.handlers.open.AbstractOpenHandler;
import org.eclipse.ui.IEditorPart;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class NoCentralTestData extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = 
        Messages.CheckMessageNoCentraTestData;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IProjectPO)) {
            return false;
        }
        
        boolean hasError = false;
        IProjectPO project = (IProjectPO) obj;
        // Try to load the Test Suites of the project
        try {
            ITestDataCubePO[] lst =
                TestDataCubeBP.getAllTestDataCubesFor(project);
            hasError = lst.length == 0;
        } catch (NullPointerException exc) {
            // That means that there are none so do nothing
        }
        return hasError;
    }
    
    @Override
    public Quickfix[] getQuickfix(Object obj) {
        return new Quickfix[] { 
            new OpenCentralTestDataEditorQuickfix(),
        };
    }
    
    /** {@inheritDoc} */
    private static class OpenCentralTestDataEditorQuickfix extends Quickfix {
        
        /** {@inheritDoc} */
        public String getLabel() {
            return Messages.QuickfixOpenCTDEditor;
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            IProjectPO project = GeneralStorage.getInstance().getProject();
            if (project != null) {
                ITestDataCategoryPO centralTestData = 
                    project.getTestDataCubeCont();
                if (centralTestData != null) {
                    IEditorPart editor = 
                        AbstractOpenHandler.openEditor(centralTestData);
                    if (editor != null) {
                        editor.getSite().getPage().activate(editor);
                    }
                }
            }
        }
        
    }

}
