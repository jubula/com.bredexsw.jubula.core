package com.bredexsw.guidancer.client.teststyle.bredexstyle.help;

import java.util.Iterator;

import org.eclipse.jubula.client.core.model.ICapPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.teststyle.checks.DecoratingCheck;

/**
 * @author marcell
 */
public class NumberOfTestSteps extends DecoratingCheck {


    @Override
    public boolean decorate(Object obj) {
        return true;
    }
    
    @Override
    public String getSuffix(Object obj) {
        if (obj instanceof INodePO) {
            INodePO node = (INodePO)obj;
            return String.format(" - %d", getNumberOfChildren(node)); //$NON-NLS-1$
        }
        return null;
    }
    
    /**
     * @param node
     * The node which children should be counted.
     * @return The number of children of this node.
     */
    private int getNumberOfChildren(INodePO node) {
        int tmp = 0;
        Iterator<INodePO> iter = node.getNodeListIterator();
        while (iter.hasNext()) {
            Object next = iter.next();
            if (next instanceof ICapPO) {
                tmp++;                
            }
            if (next instanceof INodePO) {
                tmp += getNumberOfChildren((INodePO)next);
            }
        }
        return tmp;
    }

}
