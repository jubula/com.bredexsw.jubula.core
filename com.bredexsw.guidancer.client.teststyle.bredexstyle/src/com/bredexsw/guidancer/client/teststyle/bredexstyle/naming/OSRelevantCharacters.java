package com.bredexsw.guidancer.client.teststyle.bredexstyle.naming;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jubula.client.core.model.IPersistentObject;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class OSRelevantCharacters extends BaseCheck {
    /**
     * <code>INVALID_CHARS</code>
     */
    private static final List<CharSequence> INVALID_CHARS = 
        new LinkedList<CharSequence>();

    static {
        INVALID_CHARS.add(StringConstants.SPACE);
        INVALID_CHARS.add(StringConstants.RIGHT_INEQUALITY_SING);
        INVALID_CHARS.add(StringConstants.LEFT_INEQUALITY_SING);
        INVALID_CHARS.add(StringConstants.STAR);
        INVALID_CHARS.add(StringConstants.QUOTE);
        INVALID_CHARS.add(StringConstants.PIPE);
        INVALID_CHARS.add(StringConstants.QUESTION_MARK);
        INVALID_CHARS.add(StringConstants.SLASH);
        INVALID_CHARS.add(StringConstants.BACKSLASH);
        INVALID_CHARS.add(StringConstants.COLON);
    }

    @Override
    public String getDescription() {
        return Messages.CheckMessageOSRelevantCharacters;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IPersistentObject)) {
            return false;
        }
        IPersistentObject persObj = (IPersistentObject)obj;
        for (CharSequence cs : INVALID_CHARS) {
            if (persObj.getName().contains(cs)) {
                return true;
            }
        }
        return false;
    }
}
