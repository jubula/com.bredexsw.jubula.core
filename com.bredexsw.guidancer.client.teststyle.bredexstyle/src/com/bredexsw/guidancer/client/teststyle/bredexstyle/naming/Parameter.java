package com.bredexsw.guidancer.client.teststyle.bredexstyle.naming;

import org.eclipse.jubula.client.core.model.IParamDescriptionPO;
import org.eclipse.jubula.client.core.model.ITestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class Parameter extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = Messages.CheckMessageParameter;
    
    /** Regex which checks the name */
    public static final String REGEX = "[A-Z_0-9]*"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }
    
    @Override
    public boolean hasError(Object obj) {
        if (obj instanceof ITestCasePO) {
            ITestCasePO po = (ITestCasePO)obj;
            for (IParamDescriptionPO param : po.getParameterList()) {
                if (!param.getName().matches(REGEX)) {
                    return true;
                }
            }
        }
        return false;
    }
}
