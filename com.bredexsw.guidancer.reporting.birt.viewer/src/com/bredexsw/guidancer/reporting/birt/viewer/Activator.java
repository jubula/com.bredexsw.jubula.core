package com.bredexsw.guidancer.reporting.birt.viewer;

import org.eclipse.birt.report.viewer.utilities.WebViewer;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

    /** The plug-in ID */
    public static final String PLUGIN_ID = 
        "com.bredexsw.guidancer.reporting.birt.viewer"; //$NON-NLS-1$

    /** The shared instance */
    private static Activator plugin;

    /**
     * The constructor
     */
    public Activator() {
    // empty
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        WebViewer.startup();
        plugin = this;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

}
