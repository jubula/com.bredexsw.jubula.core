/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13473 $
 *
 * $Date: 2010-12-02 18:00:11 +0100 (Thu, 02 Dec 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.jubula.client.monitoring.validator;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.jubula.client.monitoring.i18n.Messages;

/**
 * A validator to verify that the given OS string is valid
 * 
 * @author BREDEX GmbH
 */
public class JREArchValidator implements IValidator {
    /**
     * default constructor
     */
    public JREArchValidator() {
        // default
    }

    /**
     * {@inheritDoc}
     */
    public IStatus validate(Object value) {
        final String jreArchString = String.valueOf(value);

        if ("32".equals(jreArchString) || "64".equals(jreArchString)) {
            return ValidationStatus.ok();
        }

        return ValidationStatus.error(NLS.bind(Messages.InvalidJREArch,
                jreArchString));
    }
}
