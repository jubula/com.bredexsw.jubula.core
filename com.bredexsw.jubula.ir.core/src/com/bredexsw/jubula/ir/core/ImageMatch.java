/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core;

import java.awt.Rectangle;

/**
 * Datatype for results of templateMatching
 * @author BREDEX GmbH
 *
 */
public class ImageMatch {

    //alternative names: matchresult templatematchresult
    /** position of match on screen */
    private Rectangle m_rect;
    
    /** similarity of match */
    private double m_similarity;
    
    /** creates new ImageResults object 
     * @param rect region to be added
     * @param similarity match percentage
     */
    public ImageMatch(Rectangle rect, double similarity) {
        m_rect = rect;
        m_similarity = similarity;
    }
    
    /**
     * get Rectangle
     * @return Rectangle
     */
    public Rectangle getRect() {
        return m_rect;
    }
    
    /**
     * get similarity
     * @return similarity
     */
    public double getSim() {
        return m_similarity;
    }    
}
