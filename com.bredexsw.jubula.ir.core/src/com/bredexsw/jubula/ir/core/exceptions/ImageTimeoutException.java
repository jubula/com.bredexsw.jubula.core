/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core.exceptions;

/**
 * @author BREDEX GmbH
 *
 */
public class ImageTimeoutException extends Exception {

    /*
     * FIXME write Method to try searching again, this time with
     * template as searchImage, searchImage as template
     */
    
    /**
     * 
     */
    private static final long serialVersionUID = 4703045982405683414L;
    
    /**

     */
    public ImageTimeoutException() {
        super("Timeout occurred in findRegions.");
       
    }

    /**
     * @param message The message
     */
    public ImageTimeoutException(String message) {
        super(message);
    }

}
