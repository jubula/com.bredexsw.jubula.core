/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core.exceptions;

/**
 * Exception thrown e.g. when template is higher/wider than search image
 * @author BREDEX GmbH
 *
 */
public class IncompatibleImageException extends Exception {

    /*
     * FIXME write Method to try searching again, this time with
     * template as searchImage, searchImage as template
     */
    
    /**
     * 
     */
    private static final long serialVersionUID = 4703045982405683414L;
    
    /**
     * Exception thrown e.g. when template is higher/wider than search image
     * outputs "Template is higher or wider than search image."
     */
    public IncompatibleImageException() {
        super("Images are incompatible");
       
    }

    /**
     * Exception thrown e.g. when template is higher/wider than search image
     * @param message The message
     */
    public IncompatibleImageException(String message) {
        super(message);
    }

}
