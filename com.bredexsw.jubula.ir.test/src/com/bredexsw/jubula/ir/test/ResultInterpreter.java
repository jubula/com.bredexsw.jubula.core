package com.bredexsw.jubula.ir.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * class used to interpret results from FinderTests
 * @author BREDEX GmbH
 *
 */
public class ResultInterpreter {

    /**
     * private constructor
     */
    private ResultInterpreter() {
        
    }
    /**
     * reads the times from the test result files
     * @param file file to read from
     * @return times from the image pairs
     */
    public static double[] getTimesFromFile(File file) {
        
        FileReader fileReader = null;
        double[] lines = new double[8];
        
        try {
            fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            
            reader.readLine();
            for (int counter = 0; counter < 8; counter++) {
                reader.readLine();
                lines[counter] = Double.valueOf(reader.readLine());
            }
            reader.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {            
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
              // Ignore issues during closing 
                }
            }
        }

        return lines;
    } 
    
    /**
     * executes the Interpreter and outputs the results in the console
     * @param args no arguments here, move along!
     */
    public static void main(String[] args) {
//        readTextFile("K:\\guidancer\\Workspace\\users\\OliverHoffmann\\testresults\\variableTests\\interesting\\testResultsRecognition33.txt");
        File folder = new File("K:\\guidancer\\Workspace\\users\\"
                + "OliverHoffmann\\testresults\\variableTests\\interesting\\");
        File[] listOfFiles = folder.listFiles();
        double[][] bestValues = new double[8][3];
        
        
        String[][] bestNames = new String[8][3];

        
        double[] currentValues;
        for (int i = 0; i < bestValues.length; i++) {
            bestValues[i][0] = 99999999;
            bestValues[i][1] = 99999999;
            bestValues[i][2] = 99999999;
        }
        for (int i = 0; i < listOfFiles.length; i++) {
            currentValues = getTimesFromFile(listOfFiles[i]);
            for (int j = 0; j < currentValues.length; j++) {
                if (currentValues[j] < bestValues[j][0]) {
                    bestValues[j][2] = bestValues[j][1];
                    bestValues[j][1] = bestValues[j][0];
                    bestValues[j][0] = currentValues[j];
                    bestNames[j][2] = bestNames[j][1];
                    bestNames[j][1] = bestNames[j][0];
                    bestNames[j][0] = listOfFiles[i].getName();
                } else if (currentValues[j] < bestValues[j][1]) {
                    bestValues[j][2] = bestValues[j][1];
                    bestValues[j][1] = currentValues[j];
                    bestNames[j][2] = bestNames[j][1];
                    bestNames[j][1] = listOfFiles[i].getName();
                } else if (currentValues[j] < bestValues[j][2]) {
                    bestValues[j][2] = currentValues[j];
                    bestNames[j][2] = listOfFiles[i].getName();
                }
            }
        }
        for (int i = 0; i < bestValues.length; i++) {
            System.out.println("1) " + (i + 1) + ": " + bestNames[i][0] 
                    + " with time: " + bestValues[i][0] + "s.");
            System.out.println("2) " + (i + 1) + ": " + bestNames[i][1]
                    + " with time: " + bestValues[i][1] + "s.");
            System.out.println("3) " + (i + 1) + ": " + bestNames[i][2]
                    + " with time: " + bestValues[i][2] + "s.");
        }
    }
}
