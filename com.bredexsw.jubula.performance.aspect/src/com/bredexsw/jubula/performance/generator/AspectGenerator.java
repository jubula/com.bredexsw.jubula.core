/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.performance.generator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Generates the aspect based on the methods declared in the .options file in
 * the performance feature
 * 
 * @author BREDEX GmbH
 *
 */
public class AspectGenerator {

    /** Aspect file header */
    private static String aspectHeader = "/**********************************"
            + "***"
            + "******************************************\n * Copyright (c) "
            + "2016 BREDEX GmbH.\n * All rights reserved. This program and "
            + "the accompanying materials\n * are made available under the "
            + "terms of the Eclipse Public License v1.0\n * which accompanies"
            + " this distribution, and is available at\n * http://www.eclipse."
            + "org/legal/epl-v10.html\n *\n * Contributors:\n *     BREDEX "
            + "GmbH - initial API and implementation and/or initial "
            + "documentation\n **********************************************"
            + "*********************************/\n"
            + "package com.bredexsw.jubula.performance.aspect;\n"
            + "import org.aspectj.lang.Signature;\n"
            + "import org.eclipse.core.runtime.PerformanceStats;\n\n"
            + "public aspect ClientPerformanceLogging {\n";
    
    /** object pointcut pattern  replace $METHOD_NAME and $PACKAGE_NAME*/
    private static String pointcut = "Object around(): execution("
            + "* $METHOD_NAME (..)) && "
            + "within($PACKAGE_NAME..*) {\n        "
            + "Signature sig = thisJoinPoint.getSignature();\n        "
            + "String PERF_METHOD = sig.getDeclaringTypeName() + \".\" + "
            + "sig.getName();\n        PerformanceStats stats = "
            + "PerformanceStats.getStats(PERF_METHOD, "
            + "thisJoinPoint.getTarget());\n        "
            + "stats.startRun();\n        "
            + "Object o = proceed();\n        "
            + "stats.endRun();\n        "
            + "return o;\n    }\n";
    
    /** working directory */
    private static String workingDir = System.getProperty("user.dir");
    
    /** private because util class */
    private AspectGenerator() {
    };
    
    /** Calls the generation 
     * @param args ignored
     * 
    **/
    public static void main(String[] args) {
        generate();
    }

    /**
     * Generates the aspect file
     */
    private static void generate() {
        Set<String> methods = readMethods();
        if (methods != null) {
            String dir = workingDir 
                    + "/src-gen/com/bredexsw/jubula/performance/aspect/";
            File directory = new File(dir);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            File aspectFile = new File(dir
                    + "ClientPerformanceLogging.aj");
            if (aspectFile.exists()) {
                aspectFile.delete();
            }
            try {
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(
                                new FileOutputStream(aspectFile)));
                aspectFile.createNewFile();     
                writer.write(aspectHeader);
                for (String fqM : methods) {
                    String methodName = fqM.substring(fqM.lastIndexOf(".") + 1);
                    String className = fqM.replace("." + methodName, "").trim();
                    className = className
                            .substring(className.lastIndexOf(".") + 1);
                    String packageName = fqM.substring(0,
                            fqM.indexOf("." + className));
                    
                    String pointcutValues = pointcut
                            .replace("$METHOD_NAME", methodName)
                            .replace("$PACKAGE_NAME", packageName);
                    
                    writer.write(pointcutValues);
                }
                writer.write("}\n");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return list of fully qualified method names, found in the .options file
     * 
     */
    private static Set<String> readMethods() {
        Stream<String> optionsFile = null;
        try {
            optionsFile = new BufferedReader(
                    new InputStreamReader(new FileInputStream(new File(
                            workingDir + "/../"
                            + "com.bredexsw.jubula.performance.feature/"
                            + ".options")))).lines();
            
            Set<String> methods = new TreeSet<>();
            optionsFile.forEach(new Consumer<String>() {

                @Override
                public void accept(String t) {
                    String[] splittet = t.split("=");
                    if (splittet.length == 2
                            && Pattern.matches("\\d+", splittet[1])) {
                        String fqMethod = splittet[0];
                        methods.add(fqMethod);
                    }
                }
            });
            return methods;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            optionsFile.close();
        }
        return null;
    }

}
