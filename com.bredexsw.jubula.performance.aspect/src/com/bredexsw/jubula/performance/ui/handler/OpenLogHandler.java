/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.performance.ui.handler;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jubula.client.ui.handlers.AbstractHandler;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.views.log.LogView;
import org.eclipse.ui.internal.views.log.Messages;

/**
 * Handles the importing of the performance log file
 * 
 * @author BREDEX GmbH
 *
 */
public class OpenLogHandler extends AbstractHandler {

    @Override
    protected Object executeImpl(ExecutionEvent event)
            throws ExecutionException {
        // get object which represents the workspace
        IWorkspace workspace = ResourcesPlugin.getWorkspace();

        String path = workspace.getRoot().getLocation().makeAbsolute()
                + "/.metadata/performance.log";
        LogView lView = ((LogView) HandlerUtil.getActivePart(event));

        File file = new Path(path).toFile();
        if (file.exists()) {
            lView.handleImportPath(path);
        } else {
            String msg = NLS.bind(Messages.LogView_FileCouldNotBeFound,
                    file.getName());
            MessageDialog.openError(lView.getViewSite().getShell(),
                    Messages.LogView_OpenFile, msg);
        }
        return null;
    }

}
