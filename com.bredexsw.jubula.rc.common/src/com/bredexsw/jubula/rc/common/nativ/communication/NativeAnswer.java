/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.common.nativ.communication;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;

import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * The native RC-Server answer object, contains the return value from the
 * invoked server method and a exception flag which says if a exception occurred
 * on the server side.
 * 
 */
public class NativeAnswer {
    /**
     * The Answer from the native RC-Server
     */
    private String m_answer;

    /**
     * indicates if an exception was thrown on the native RC side or not
     */
    private boolean m_exceptionFlag = false;

    /**
     * Exception object that represents the server exception.
     */
    private RemoteServerException m_serverexception;

    /**
     * The Constructor.
     */
    public NativeAnswer() {

    }

    /**
     * The Constructor.
     * 
     * @param serverAnswer
     *            the answer from the server
     */
    public NativeAnswer(String serverAnswer) {
        this.m_exceptionFlag = false;
        this.m_answer = serverAnswer;
    }

    /**
     * 
     * @param id
     *            the exception id
     * @param name
     *            the exception name
     * @param msg
     *            the exception message
     * @param method
     *            the method name
     * @param className
     *            the class name
     */
    public NativeAnswer(int id, String name, String msg, String method,
            String className) {
        this.m_exceptionFlag = true;
        this.m_serverexception = new RemoteServerException(id, name, msg,
                method, className);
    }

    /**
     * 
     * Returns the server answer as a int.
     * 
     * @return the int value
     */
    public int getInt() {
        return Integer.valueOf(m_answer).intValue();
    }

    /**
     * Returns the server answer as a String.
     * 
     * @return the string value
     */
    public String getString() {
        return m_answer;
    }

    /**
     * Returns the server answer as a Boolean.
     * 
     * @return the boolean value
     */
    public boolean getBoolean() {
        return new Boolean(m_answer).booleanValue();
    }

    /**
     * Converts the JSON native RC answer string to string[]
     * 
     * @return response string array
     */
    public String[] getStringArray() {
        JSONArray jsonarr = (JSONArray) JSONSerializer.toJSON(m_answer);
        String[] tmpArray = new String[jsonarr.size()];

        for (int i = 0; i < jsonarr.size(); i++) {
            tmpArray[i] = jsonarr.getString(i);
        }

        return tmpArray;
    }

    /**
     * Converts the JSON native RC answer string to string[]
     * 
     * @return response string array
     */
    public String[][] get2DStringArray() {
        JSONArray jsonarr = (JSONArray) JSONSerializer.toJSON(m_answer);
        String[][] tmpArray = new String[jsonarr.size()][];

        for (int i = 0; i < jsonarr.size(); i++) {
            JSONArray internalJsonArray = jsonarr.getJSONArray(i);
            tmpArray[i] = new String[internalJsonArray.size()];
            for (int j = 0; j < internalJsonArray.size(); j++) {
                tmpArray[i][j] = internalJsonArray.getString(j);
            }
        }

        return tmpArray;
    }

    /**
     * Converts the JSON native RC answer string to int[]
     * 
     * @return response string array
     */
    public int[] getIntArray() {
        JSONArray jsonarr = (JSONArray) JSONSerializer.toJSON(m_answer);
        int[] tmpArray = new int[jsonarr.size()];

        for (int i = 0; i < jsonarr.size(); i++) {
            String string = jsonarr.getString(i);
            tmpArray[i] = Integer.parseInt(string);
        }

        return tmpArray;
    }

    /**
     * Converts the JSON native RC answer string to a list of strings
     * 
     * @return response string array
     */
    public List<String> getStringList() {
        ArrayList<String> tmpList = new ArrayList<String>();

        if (!m_answer.equals("null")) { //$NON-NLS-1$ 
            JSONArray jsonarr = (JSONArray) JSONSerializer.toJSON(m_answer);

            for (int i = 0; i < jsonarr.size(); i++) {
                tmpList.add(jsonarr.getString(i));
            }
        }

        return tmpList;
    }

    /**
     * Return the ExceptionFlag which says if a exception occurred.
     * 
     * @return the exceptionFlag
     */
    public boolean getExceptionFlag() {
        return m_exceptionFlag;
    }

    /**
     * 
     * @return m_serverexception
     */
    public RemoteServerException getServerException() {
        return m_serverexception;
    }
}