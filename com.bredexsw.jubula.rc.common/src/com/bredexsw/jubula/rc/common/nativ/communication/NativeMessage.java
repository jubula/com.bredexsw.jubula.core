/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.common.nativ.communication;

import java.util.ArrayList;
import java.util.List;

/**
 * Object which is representing a message for / from the native RC Server.
 */
public class NativeMessage {
    /**
     * the name of the class to invoke the method on
     */
    private String m_className = null;
    /**
     * Name of the method which should be called on the native RC Server
     */
    private String m_methodName = null;
    /**
     * the locator of a component
     */
    private String m_locator = null;
    /**
     * List of parameters for the called method
     */
    private List<Object> m_param = new ArrayList<Object>();

    /**
     * Constructor
     * 
     * @param className
     *            the name of the class the given method will be invoked
     * @param methodName
     *            method name which will be invoked
     */
    public NativeMessage(String className, String methodName) {
        setClassName(className);
        setMethodName(methodName);
    }
    
    /**
     * Constructor
     * 
     * @param className
     *            the name of the class the given method will be invoked
     * @param methodName
     *            method name which will be invoked
     * @param locator
     *            locator of a component           
     */
    public NativeMessage(String className, String methodName, String locator) {
        setClassName(className);
        setMethodName(methodName);
        setLocator(locator);
    }
    
    /**
     * @return the method name
     */
    public String getMethodName() {
        return m_methodName;
    }
    
    /**
     * @return the method name
     */
    public String getLocator() {
        return m_locator;
    }

    /**
     * @param methodName
     *            the method name
     */
    private void setMethodName(String methodName) {
        m_methodName = methodName;
    }
    
    /**
     * 
     * @param locator the locator
     */
    private void setLocator(String locator) {
        m_locator = locator;   
    }  

    /**
     * @return the parameters
     */
    public List<Object> getParameters() {
        return m_param;
    }

    /**
     * @param parameters
     *            the parameters to add
     */
    public void addParameters(Object... parameters) {
        for (Object param : parameters) {
            m_param.add(param);
        }
    }
    
    /**
     * @param parameter
     *            the parameter to add
     */
    public void addParameter(Object parameter) {
        m_param.add(parameter);
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return m_className;
    }

    /**
     * @param className
     *            the className to set
     */
    private void setClassName(String className) {
        m_className = className;
    }
}