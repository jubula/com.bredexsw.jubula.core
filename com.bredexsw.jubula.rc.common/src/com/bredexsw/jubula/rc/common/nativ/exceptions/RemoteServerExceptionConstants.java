/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.common.nativ.exceptions;

/**
 * Contains the not exit worth exception IDs from the native RC server
 * exceptions.
 */
public class RemoteServerExceptionConstants {
    /**
     * Exception which indicates that a error occurred while performing a
     * test step action.
     */
    public static final int STEP_EXECUTION_EXCEPTION = 11;

    /**
     * Constructor.
     */
    private RemoteServerExceptionConstants() {

    }
}
