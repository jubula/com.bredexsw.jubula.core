#!/bin/bash -eu
# Publishes the contents of an archive file fo use in EPP package integration.
# The currect publishing process does not perform any kind of merge, so
# the previously published version is essentially made invisible (to
# be deleted or archived by hand after script execution).

# Usage: 
#   publish_remote.sh release_path archive_filename staging_path

# make sure exactly the right number of  args are supplied
# and print usage if not
if [ $# -ne 3 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Usage: $0 release_path archive_filename staging_path"
    exit 1
fi

RELEASE_PATH=$1
ARCHIVE=$2
STAGING_PATH=$3


if [ -e ${STAGING_PATH} ]; then
  OLD_STAGING_DIR=`mktemp -d staging_old.XXXXXXXXXX`
  mv ${STAGING_PATH} ${OLD_STAGING_DIR}
  echo "Old staged artifacts were moved to ${OLD_STAGING_DIR}. Remove or archive these artifacts as necessary."
fi

mkdir -p ${STAGING_PATH}
cd ${STAGING_PATH}

unzip ${ARCHIVE}
rm ${ARCHIVE}
chmod -R u=rwX,g=rwX,o=rX .

cd ..

if [ -e ${RELEASE_PATH} ]; then
  OLD_RELEASE_PATH=`mktemp -d release_old.XXXXXXXXXX`
  mv ${RELEASE_PATH} ${OLD_RELEASE_PATH}
  echo "Old published artifacts were moved to ${OLD_RELEASE_PATH}. Remove or archive these artifacts as necessary."
fi

mv ${STAGING_PATH} ${RELEASE_PATH}

echo "Publishing complete. Verify manually that the publication was successful."