/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.invokeexternalmethod;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author BREDEX GmbH
 * @created 11.10.2016
 */
public class TestMethods {
    
    /** Default constructor */
    private TestMethods() {
        //empty
    }

    /**
     * A method to be called with three parameters of different types
     * @param string first parameter
     * @param integer second parameter
     * @param bool third parameter
     */
    public static void invokeThreeParams(String string, Integer integer,
            Boolean bool) {
        // do nothing
    }
    
    /**
     * A method to be called with two parameters of different types
     * @param string first parameter
     * @param integer second parameter
     */
    public static void invokeTwoParams(String string, Integer integer) {
        // do nothing
    }
    
    /**
     * A method to be called with one parameter of different types
     * @param string first parameter
     */
    public static void invokeOneParam(String string) {
        // do nothing
    }
    
    /**
     * A method to be called with no parameters
     */
    public static void invokeNoParams() {
        // do nothing
    }
    
    
    /**
     * A method that returns a concatenated string of three parameters of
     * different types
     * @param string first parameter
     * @param integer second parameter
     * @param bool third parameter
     * @return concatenated string
     */
    public static String invokeThreeParamsAndStore(String string,
            Integer integer, Boolean bool) {
        return (string + integer + bool);
    }
    
    /**
     * A method that returns a concatenated string of two parameters of
     * different types
     * @param string first parameter
     * @param integer second parameter
     * @return concatenated string
     */
    public static String invokeTwoParamsAndStore(String string,
            Integer integer) {
        return (string + integer);
    }
    
    /**
     * A method that returns a concatenated string of one parameter
     * @param string first parameter
     * @return concatenated string
     */
    public static String invokeOneParamAndStore(String string) {
        if (string == null) {
            return "String was null";
        }
        return string;
    }
    
    /**
     * A method that returns a map with the parameters as key and value pairs
     * @param key1 the first key
     * @param value1 the first value
     * @param key2 the second key
     * @param value2 the second value
     * @return the map
     */
    public static Map<String, String> invokeTwoParamsAndStoreMap(String key1,
            String value1, String key2, String value2) {
        Map<String, String> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        return map;
    }
    
}
