/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 12678 $
 *
 * $Date: 2010-10-28 11:45:55 +0200 (Thu, 28 Oct 2010) $
 *
 * $Author: oussama $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2008 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.base.constants;

/**
 * @author berndk
 * @created 20.10.2008
 */
public interface ComponentNameConstants {
    
    /** the component id for the Buttons Testpage */
    public static final String MAINPAGE_SCROLLEDCOMP_TESTPAGE = "MainPage.ScrollableComps.Page"; //$NON-NLS-1$
    /** the component id for the Buttons Testpage */
    public static final String MAINPAGE_BUTTONS_TESTPAGE = "MainPage.Buttons.Page"; //$NON-NLS-1$
    /** the component id for the CheckBoxes Testpage */
    public static final String MAINPAGE_CHECKBOXES_TESTPAGE = "MainPage.CheckBoxes.Page"; //$NON-NLS-1$
    /** the component id for the RadioButtons Testpage */
    public static final String MAINPAGE_RADIOBUTTONS_TESTPAGE = "MainPage.RadioButtons.Page"; //$NON-NLS-1$
    /** the component id for the ComboBoxes Testpage */
    public static final String MAINPAGE_COMBOBOXES_TESTPAGE = "MainPage.ComboBoxes.Page"; //$NON-NLS-1$
    /** the component id for the TextFields Testpage */
    public static final String MAINPAGE_TEXTFIELDS_TESTPAGE = "MainPage.TextFields.Page"; //$NON-NLS-1$
    /** the component id for the TextAreas Testpage */
    public static final String MAINPAGE_TEXTAREAS_TESTPAGE = "MainPage.TextAreas.Page"; //$NON-NLS-1$
    /** the component id for the EditorPanes Testpage */
    public static final String MAINPAGE_EDITORPANES_TESTPAGE = "MainPage.EditorPanes.Page"; //$NON-NLS-1$
    /** the component id for the TextPanes Testpage */
    public static final String MAINPAGE_TEXTPANES_TESTPAGE = "MainPage.TextPanes.Page"; //$NON-NLS-1$
    /** the component id for the Labels Testpage */
    public static final String MAINPAGE_LABELS_TESTPAGE = "MainPage.Labels.Page"; //$NON-NLS-1$
    /** the component id for the Lists Testpage */
    public static final String MAINPAGE_LISTS_TESTPAGE = "MainPage.Lists.Page"; //$NON-NLS-1$
    /** the component id for the Menus Testpage */
    public static final String MAINPAGE_MENU_TESTPAGE = "MainPage.Menu.Page"; //$NON-NLS-1$
    /** the component id for the TabbedPanes Testpage */
    public static final String MAINPAGE_TABBEDPANES_TESTPAGE = "MainPage.TabbedPanes.Page"; //$NON-NLS-1$
    /** the component id for the Accordions Testpage */
    public static final String MAINPAGE_ACCORDIONS_TESTPAGE = "MainPage.Accordions.Page"; //$NON-NLS-1$
    /** the component id for the Charts Testpage */
    public static final String MAINPAGE_CHARTS_TESTPAGE = "MainPage.Charts.Page"; //$NON-NLS-1$
    /** the component id for the Tables Testpage */
    public static final String MAINPAGE_TABLES_TESTPAGE = "MainPage.Tables.Page"; //$NON-NLS-1$
    /** the component id for the Applications Testpage */
    public static final String MAINPAGE_APPLICATIONS_TESTPAGE = "MainPage.Applications.Page"; //$NON-NLS-1$
    /** the component id for the Panes Testpage */
    public static final String MAINPAGE_PANES_TESTPAGE = "MainPage.Panes.Page"; //$NON-NLS-1$
    /** the component id for the Containers Testpage */
    public static final String MAINPAGE_CONTAINERS_TESTPAGE = "MainPage.Containers.Page"; //$NON-NLS-1$
    /** the component id for the Trees Testpage */
    public static final String MAINPAGE_TREES_TESTPAGE = "MainPage.Trees.Page"; //$NON-NLS-1$
    /** the component id for the TreeTables Testpage */
    public static final String MAINPAGE_TREETABLES_TESTPAGE = "MainPage.TreeTables.Page"; //$NON-NLS-1$
    /** the component id for the PasswordFields Testpage */
    public static final String MAINPAGE_PASSWORDFIELDS_TESTPAGE = "MainPage.PasswordFields.Page"; //$NON-NLS-1$
    /** the component id for the Scale Testpage */
    public static final String MAINPAGE_SCALE_TESTPAGE = "MainPage.Scale.Page"; //$NON-NLS-1$    
    /** the component id for the Simple Extensions Testpage */
    public static final String MAINPAGE_SIMPLE_EXTENSION_TESTPAGE = "MainPage.SimpleExtension.Page"; //$NON-NLS-1$    
    /** the component id for the Slider Testpage */
    public static final String MAINPAGE_SLIDER_TESTPAGE = "MainPage.Slider.Page"; //$NON-NLS-1$
    /** the component id for the Spinner Testpage */
    public static final String MAINPAGE_SPINNER_TESTPAGE = "MainPage.Spinner.Page"; //$NON-NLS-1$
    /** the component id for the Toolbar Item Testpage */
    public static final String MAINPAGE_TOOLBAR_ITEM_TESTPAGE = "MainPage.ToolbarItem.Page"; //$NON-NLS-1$
    /** the component id for the ListViewe Testpage */
    public static final String MAINPAGE_LISTVIEW_TESTPAGE = "MainPage.ListView.Page"; //$NON-NLS-1$
    /** the component id for the ImageView Testpage */
    public static final String MAINPAGE_IMAGEVIEW_TESTPAGE = "MainPage.ImageView.Page"; //$NON-NLS-1$
    /** the component id for the ListViewe Testpage */
    public static final String MAINPAGE_CLABELS_TESTPAGE = "MainPage.CLabel.Page"; //$NON-NLS-1$
    /** the component id for the ListViewe Testpage */
    public static final String MAINPAGE_CTABFOLDERS_TESTPAGE = "MainPage.CTabFolder.Page"; //$NON-NLS-1$
    /** the component id for the JProgressBar Testpage */
    public static final String MAINPAGE_JPROGRESSBAR_TESTPAGE = "MainPage.JProgressBar.Page"; //$NON-NLS-1$
    /** the component id for the ProgressBar Testpage */
    public static final String MAINPAGE_PROGRESSBAR_TESTPAGE = "MainPage.ProgressBar.Page"; //$NON-NLS-1$
    /** the component id for the ToolTip Testpage */
    public static final String MAINPAGE_JFACE_TOOLTIP_TESTPAGE = "MainPage.Jface.ToolTip.Page"; //$NON-NLS-1$
    /** the component id for the table Testpage */
    public static final String MAINPAGE_JFACE_TABLE_TESTPAGE = "MainPage.Jface.Table.Page"; //$NON-NLS-1$
    /** the component id for the tree Testpage */
    public static final String MAINPAGE_JFACE_TREE_TESTPAGE = "MainPage.Jface.Tree.Page"; //$NON-NLS-1$
    /** the component id for the list Testpage */
    public static final String MAINPAGE_JFACE_LIST_TESTPAGE = "MainPage.Jface.List.Page"; //$NON-NLS-1$
    /** the component id for the combo Testpage */
    public static final String MAINPAGE_JFACE_COMBO_TESTPAGE = "MainPage.Jface.Combo.Page"; //$NON-NLS-1$
 
    /** component id for the CloseButton */
    public static final String TESTPAGES_CLOSEBUTTON = "TestPage.CloseButton"; //$NON-NLS-1$
    /** component id for the Label */
    public static final String TESTPAGES_CONTENTLABEL = "TestPage.ContentLabel"; //$NON-NLS-1$
    
    /** component id for Textfield01 */
    public static final String TESTPAGE_APPLICATION_TF01 = "TestPage.Application.TextField01"; //$NON-NLS-1$
    /** component id for Button01 */
    public static final String TESTPAGE_APPLICATION_EXIT_BTN = "TestPage.Application.Exit.Button"; //$NON-NLS-1$
    
    /** the component id for Button1 */
    public static final String TESTPAGE_BUTTONS_BTN01 = "TestPage.Buttons.Button01"; //$NON-NLS-1$
    /** the component id for Button2 */
    public static final String TESTPAGE_BUTTONS_BTN02 = "TestPage.Buttons.Button02"; //$NON-NLS-1$
    /** the component id for Button3 */
    public static final String TESTPAGE_BUTTONS_BTN03 = "TestPage.Buttons.Button03"; //$NON-NLS-1$
    /** the component id for Button4 */
    public static final String TESTPAGE_BUTTONS_BTN04 = "TestPage.Buttons.Button04"; //$NON-NLS-1$
    /** the component id for Button5 */
    public static final String TESTPAGE_BUTTONS_BTN05 = "TestPage.Buttons.Button05"; //$NON-NLS-1$
    
    /** the component id for CheckBox1 */
    public static final String TESTPAGE_CHECKBOXES_CBX01 = "TestPage.CheckBoxes.CheckBox01"; //$NON-NLS-1$
    /** the component id for CheckBox2 */
    public static final String TESTPAGE_CHECKBOXES_CBX02 = "TestPage.CheckBoxes.CheckBox02"; //$NON-NLS-1$
    
    /** the component id for CLabel1 */
    public static final String TESTPAGE_CLABELS_CLBL01 = "TestPage.CLabels.CLabel01"; //$NON-NLS-1$
    /** the component id for CLabel2 */
    public static final String TESTPAGE_CLABELS_CLBL02 = "TestPage.CLabels.CLabel02"; //$NON-NLS-1$
    /** the component id for CLabel2 */
    public static final String TESTPAGE_CLABELS_CLBL03 = "TestPage.CLabels.CLabel03"; //$NON-NLS-1$
    
    /** the component id for Panes */
    public static final String TESTPAGE_PANES = "TestPage.Panes"; //$NON-NLS-1$
    
    /** the component id for Containers */
    public static final String TESTPAGE_CONTAINERS = "TestPage.Containers"; //$NON-NLS-1$

    /** the component id for ImageViews */
    public static final String TESTPAGE_IMAGEVIEWS = "TestPage.ImageViews"; //$NON-NLS-1$
    /** the component id for ImageView1 */
    public static final String TESTPAGE_IMAGEVIEWS_IMAGEVIEW1 = "TestPage.ImageViews.ImageView01"; //$NON-NLS-1$
    /** the component id for ImageView2 */
    public static final String TESTPAGE_IMAGEVIEWS_IMAGEVIEW2 = "TestPage.ImageViews.ImageView02"; //$NON-NLS-1$
    /** the component id for ImageView3 */
    public static final String TESTPAGE_IMAGEVIEWS_IMAGEVIEW3 = "TestPage.ImageViews.ImageView03"; //$NON-NLS-1$
    
    /** the component id for ComboBox1 */
    public static final String TESTPAGE_COMBOBOXES_CMBX01 = "TestPage.ComboBoxes.ComboBox01"; //$NON-NLS-1$
    /** the component id for ComboBox2 */
    public static final String TESTPAGE_COMBOBOXES_CMBX02 = "TestPage.ComboBoxes.ComboBox02"; //$NON-NLS-1$
    /** the component id for ComboBox3 */
    public static final String TESTPAGE_COMBOBOXES_CMBX03 = "TestPage.ComboBoxes.ComboBox03"; //$NON-NLS-1$
    /** the component id for ComboBox4 */
    public static final String TESTPAGE_COMBOBOXES_CMBX04 = "TestPage.ComboBoxes.ComboBox04"; //$NON-NLS-1$
    /** the component id for ComboBox5 */
    public static final String TESTPAGE_COMBOBOXES_CMBX05 = "TestPage.ComboBoxes.ComboBox05"; //$NON-NLS-1$
    /** the component id for ComboBox6 */
    public static final String TESTPAGE_COMBOBOXES_CMBX06 = "TestPage.ComboBoxes.ComboBox06"; //$NON-NLS-1$
    
    /** the component id for CCombo1 */
    public static final String TESTPAGE_COMBOBOXES_CCOMBO1 = "TestPage.ComboBoxes.CCombo01"; //$NON-NLS-1$
    /** the component id for CCombo2 */
    public static final String TESTPAGE_COMBOBOXES_CCOMBO2 = "TestPage.ComboBoxes.CCombo02"; //$NON-NLS-1$
    /** the component id for CCombo3 */
    public static final String TESTPAGE_COMBOBOXES_CCOMBO3 = "TestPage.ComboBoxes.CCombo03"; //$NON-NLS-1$
    /** the component id for CCombo4 */
    public static final String TESTPAGE_COMBOBOXES_CCOMBO4 = "TestPage.ComboBoxes.CCombo04"; //$NON-NLS-1$
    
    /** the component id for CTabFolder1 */
    public static final String TESTPAGE_CTABFOLDERS_CTF01 = "TestPage.CTabFolders.CTabFolder01"; //$NON-NLS-1$
    /** the component id for CTabFolder2 */
    public static final String TESTPAGE_CTABFOLDERS_CTF02 = "TestPage.CTabFolders.CTabFolder02"; //$NON-NLS-1$
    /** the component id for CTabFolder3 */
    public static final String TESTPAGE_CTABFOLDERS_CTF03 = "TestPage.CTabFolders.CTabFolder03"; //$NON-NLS-1$
    /** the component id for CTabFolder4 */
    public static final String TESTPAGE_CTABFOLDERS_CTF04 = "TestPage.CTabFolders.CTabFolder04"; //$NON-NLS-1$
    /** the component id for CTabFolder5 */
    public static final String TESTPAGE_CTABFOLDERS_CTF05 = "TestPage.CTabFolders.CTabFolder05"; //$NON-NLS-1$
    
    /** the component id for EditorPane1 */
    public static final String TESTPAGE_EDITORPANES_EP01 = "TestPage.EditorPanes.EditorPane01"; //$NON-NLS-1$
    /** the component id for EditorPane2 */
    public static final String TESTPAGE_EDITORPANES_EP02 = "TestPage.EditorPanes.EditorPane02"; //$NON-NLS-1$
    /** the component id for EditorPane3 */
    public static final String TESTPAGE_EDITORPANES_EP03 = "TestPage.EditorPanes.EditorPane03"; //$NON-NLS-1$
    /** the component id for EditorPane4 */
    public static final String TESTPAGE_EDITORPANES_EP04 = "TestPage.EditorPanes.EditorPane04"; //$NON-NLS-1$
    /** the component id for EditorPane5 */
    public static final String TESTPAGE_EDITORPANES_EP05 = "TestPage.EditorPanes.EditorPane05"; //$NON-NLS-1$

    /** the component id for JProgressBar1 */
    public static final String TESTPAGE_JPROGRESSBAR_JPB01 = "TestPage.JProgressBar.JProgressBar01"; //$NON-NLS-1$
    /** the component id for JProgressBar2 */
    public static final String TESTPAGE_JPROGRESSBAR_JPB02 = "TestPage.JProgressBar.JProgressBar02"; //$NON-NLS-1$
    /** the component id for JProgressBar3 */
    public static final String TESTPAGE_JPROGRESSBAR_JPB03 = "TestPage.JProgressBar.JProgressBar03"; //$NON-NLS-1$    

    /** the component id for ProgressBar1 */
    public static final String TESTPAGE_PROGRESSBAR_PB01 = "TestPage.ProgressBar.ProgressBar01"; //$NON-NLS-1$    
    /** the component id for ProgressBar2 */
    public static final String TESTPAGE_PROGRESSBAR_PB02 = "TestPage.ProgressBar.ProgressBar02"; //$NON-NLS-1$
    /** the component id for ProgressBar3 */
    public static final String TESTPAGE_PROGRESSBAR_PB03 = "TestPage.ProgressBar.ProgressBar03"; //$NON-NLS-1$
    
    /** the component id for Label1 */
    public static final String TESTPAGE_LABELS_LBL01 = "TestPage.Labels.Label01"; //$NON-NLS-1$
    /** the component id for Label2 */
    public static final String TESTPAGE_LABELS_LBL02 = "TestPage.Labels.Label02"; //$NON-NLS-1$
    /** the component id for Label2 */
    public static final String TESTPAGE_LABELS_LBL03 = "TestPage.Labels.Label03"; //$NON-NLS-1$
    
    /** the component id for List1 */
    public static final String TESTPAGE_LISTS_LST01 = "TestPage.Lists.List01"; //$NON-NLS-1$
    /** the component id for List2 */
    public static final String TESTPAGE_LISTS_LST02 = "TestPage.Lists.List02"; //$NON-NLS-1$
    /** the component id for List3 */
    public static final String TESTPAGE_LISTS_LST03 = "TestPage.Lists.List03"; //$NON-NLS-1$
    /** the component id for List4 */
    public static final String TESTPAGE_LISTS_LST04 = "TestPage.Lists.List04"; //$NON-NLS-1$
    /** the component id for List5 */
    public static final String TESTPAGE_LISTS_LST05 = "TestPage.Lists.List05"; //$NON-NLS-1$
    /** the component id for List6 */
    public static final String TESTPAGE_LISTS_LST06 = "TestPage.Lists.List06"; //$NON-NLS-1$
    /** the component if for ScrollBar1 */
    public static final String TESTPAGE_LISTS_SB01 = "TestPage.Lists.ScrollBar01"; //$NON-NLS-1$
    /** the component if for ScrollBar2 */
    public static final String TESTPAGE_LISTS_SB02 = "TestPage.Lists.ScrollBar02"; //$NON-NLS-1$
    
    /** the component id for RadioButton1 */
    public static final String TESTPAGE_RADIOBUTTONS_RBTN01 = "TestPage.RadioButtons.RadioButton01"; //$NON-NLS-1$
    /** the component id for RadioButton2 */
    public static final String TESTPAGE_RADIOBUTTONS_RBTN02 = "TestPage.RadioButtons.RadioButton02"; //$NON-NLS-1$
    /** the component id for RadioButton3 */
    public static final String TESTPAGE_RADIOBUTTONS_RBTN03 = "TestPage.RadioButtons.RadioButton03"; //$NON-NLS-1$

    
    
    /** the component id for TabbedPane1 */
    public static final String TESTPAGE_TABBEDPANES_TBP01 = "TestPage.TabbedPanes.TabbedPane01"; //$NON-NLS-1$
    /** the component id for TabbedPane2 */
    public static final String TESTPAGE_TABBEDPANES_TBP02 = "TestPage.TabbedPanes.TabbedPane02"; //$NON-NLS-1$
    /** the component id for TabbedPane3 */
    public static final String TESTPAGE_TABBEDPANES_TBP03 = "TestPage.TabbedPanes.TabbedPane03"; //$NON-NLS-1$
    /** the component id for Panel1 */
    public static final String TESTPAGE_TABBEDPANES_PNL01 = "TestPage.TabbedPanes.Panel01"; //$NON-NLS-1$
    /** the component id for Panel2 */
    public static final String TESTPAGE_TABBEDPANES_PNL02 = "TestPage.TabbedPanes.Panel02"; //$NON-NLS-1$
    /** the component id for Panel3 */
    public static final String TESTPAGE_TABBEDPANES_PNL03 = "TestPage.TabbedPanes.Panel03"; //$NON-NLS-1$
    /** the component id for Panel4 */
    public static final String TESTPAGE_TABBEDPANES_PNL04 = "TestPage.TabbedPanes.Panel04"; //$NON-NLS-1$
    /** the component id for Panel5 */
    public static final String TESTPAGE_TABBEDPANES_PNL05 = "TestPage.TabbedPanes.Panel05"; //$NON-NLS-1$
    /** the component id for Panel6 */
    public static final String TESTPAGE_TABBEDPANES_PNL06 = "TestPage.TabbedPanes.Panel06"; //$NON-NLS-1$

    /** the component id for Accordion1 */
    public static final String TESTPAGE_ACCORDIONS_TBP01 = "TestPage.Accordions.TabbedPane01"; //$NON-NLS-1$
    /** the component id for Accordion2 */
    public static final String TESTPAGE_ACCORDIONS_TBP02 = "TestPage.Accordions.TabbedPane02"; //$NON-NLS-1$
    /** the component id for Accordion3 */
    public static final String TESTPAGE_ACCORDIONS_TBP03 = "TestPage.Accordions.TabbedPane03"; //$NON-NLS-1$
    /** the component id for Panel1 */
    public static final String TESTPAGE_ACCORDIONS_PNL01 = "TestPage.Accordions.Panel01"; //$NON-NLS-1$
    /** the component id for Panel2 */
    public static final String TESTPAGE_ACCORDIONS_PNL02 = "TestPage.Accordions.Panel02"; //$NON-NLS-1$
    /** the component id for Panel3 */
    public static final String TESTPAGE_ACCORDIONS_PNL03 = "TestPage.Accordions.Panel03"; //$NON-NLS-1$
    /** the component id for Panel4 */
    public static final String TESTPAGE_ACCORDIONS_PNL04 = "TestPage.Accordions.Panel04"; //$NON-NLS-1$
    /** the component id for Panel5 */
    public static final String TESTPAGE_ACCORDIONS_PNL05 = "TestPage.Accordions.Panel05"; //$NON-NLS-1$
    /** the component id for Panel6 */
    public static final String TESTPAGE_ACCORDIONS_PNL06 = "TestPage.Accordions.Panel06"; //$NON-NLS-1$

    /** the component id for Table */
    public static final String TESTPAGE_TABLES_TBL01 = "TestPage.Tables.Table01"; //$NON-NLS-1$
    /** the component id for Table with invisible header */
    public static final String TESTPAGE_TABLES_TBL02 = "TestPage.Tables.Table02"; //$NON-NLS-1$
    /** the component id for scrollable Table */
    public static final String TESTPAGE_TABLES_TBL03 = "TestPage.Tables.Table03"; //$NON-NLS-1$
    /** the component id for scrollable Table */
    public static final String TESTPAGE_TABLES_TBL04 = "TestPage.Tables.Table04"; //$NON-NLS-1$
    /** the component id for multiColumn CheckBox Table */
    public static final String TESTPAGE_TABLES_TBL05 = "TestPage.Tables.Table05"; //$NON-NLS-1$
    /** the component id for singleColumn CheckBox Table */
    public static final String TESTPAGE_TABLES_TBL06 = "TestPage.Tables.Table06"; //$NON-NLS-1$
    /** the component id for singleColumn CheckBox Table */
    public static final String TESTPAGE_TABLES_TBL07 = "TestPage.Tables.Table07"; //$NON-NLS-1$
    /** the component id for singleColumn CheckBox Table */
    public static final String TESTPAGE_TABLES_TBL08 = "TestPage.Tables.Table08"; //$NON-NLS-1$
    /** the component id for singleColumn CheckBox Table */
    public static final String TESTPAGE_TABLES_TBL09 = "TestPage.Tables.Table09"; //$NON-NLS-1$
    
    /** the component id for Listview */
    public static final String TESTPAGE_TITLE_LISTVIEW = "TESTPAGE.TITLE.LISTVIEW"; //$NON-NLS-1$
    /** the component id for Table with invisible header */
    public static final String TESTPAGE_LISTVIEW_LSTVIEW01 = "TESTPAGE.TITLE.LISTVIEW01"; //$NON-NLS-1$
    /** the component id for Listviwe02 */
    public static final String TESTPAGE_LISTVIEW_LSTVIEW02 = "TESTPAGE.TITLE.LISTVIEW02"; //$NON-NLS-1$
    /** the component id for Listviwe03 */
    public static final String TESTPAGE_LISTVIEW_LSTVIEW03 = "TESTPAGE.TITLE.LISTVIEW03"; //$NON-NLS-1$
    /** the component id for TextArea1 */
    public static final String TESTPAGE_TEXTAREAS_TA01 = "TestPage.TextAreas.TextArea01"; //$NON-NLS-1$
    /** the component id for TextArea2 */
    public static final String TESTPAGE_TEXTAREAS_TA02 = "TestPage.TextAreas.TextArea02"; //$NON-NLS-1$
    /** the component id for TextArea3 */
    public static final String TESTPAGE_TEXTAREAS_TA03 = "TestPage.TextAreas.TextArea03"; //$NON-NLS-1$
    /** the component id for TextArea4 */
    public static final String TESTPAGE_TEXTAREAS_TA04 = "TestPage.TextAreas.TextArea04"; //$NON-NLS-1$
    /** the component id for TextArea5 */
    public static final String TESTPAGE_TEXTAREAS_TA05 = "TestPage.TextAreas.TextArea05"; //$NON-NLS-1$
    
    /** the component id for TextField1 */
    public static final String TESTPAGE_TEXTFIELDS_TF01 = "TestPage.TextFields.TextField01"; //$NON-NLS-1$
    /** the component id for TextField2 */
    public static final String TESTPAGE_TEXTFIELDS_TF02 = "TestPage.TextFields.TextField02"; //$NON-NLS-1$
    /** the component id for TextField3 */
    public static final String TESTPAGE_TEXTFIELDS_TF03 = "TestPage.TextFields.TextField03"; //$NON-NLS-1$
    /** the component id for TextField4 */
    public static final String TESTPAGE_TEXTFIELDS_TF04 = "TestPage.TextFields.TextField04"; //$NON-NLS-1$
    /** the component id for TextField5 */
    public static final String TESTPAGE_TEXTFIELDS_TF05 = "TestPage.TextFields.TextField05"; //$NON-NLS-1$
    
    /** the component id for DatePicker01 */
    public static final String TESTPAGE_DATEPICKERS_DP01 = "TestPage.DatePickers.DatePicker01"; //$NON-NLS-1$
    /** the component id for DatePicker02 */
    public static final String TESTPAGE_DATEPICKERS_DP02 = "TestPage.DatePickers.DatePicker02"; //$NON-NLS-1$
    /** the component id for DatePicker03 */
    public static final String TESTPAGE_DATEPICKERS_DP03 = "TestPage.DatePickers.DatePicker03"; //$NON-NLS-1$
    
    /** The component id for the Shape Testpage */
    public static final String MAINPAGE_SHAPES_TESTPAGE = "MainPage.Shapes.Page"; //$NON-NLS-1$
    /** the component id for RECTANGLE01 */
    public static final String TESTPAGE_SHAPES_RECTANGLE01 = "TestPage.Shapes.Rectangle01"; //$NON-NLS-1$
    /** the component id for RECTANGLE02 */
    public static final String TESTPAGE_SHAPES_RECTANGLE02 = "TestPage.Shapes.Rectangle02"; //$NON-NLS-1$
    /** the component id for CIRCLE01 */
    public static final String TESTPAGE_SHAPES_CIRCLE01 = "TestPage.Shapes.Circle01"; //$NON-NLS-1$
    /** the component id for CIRCLE02 */
    public static final String TESTPAGE_SHAPES_CIRCLE02 = "TestPage.Shapes.Circle02"; //$NON-NLS-1$
    
    
    /** the component id for TextPane1 */
    public static final String TESTPAGE_TEXTPANES_TP01 = "TestPage.TextPanes.TextPane01"; //$NON-NLS-1$
    /** the component id for TextPane2 */
    public static final String TESTPAGE_TEXTPANES_TP02 = "TestPage.TextPanes.TextPane02"; //$NON-NLS-1$
    /** the component id for TextPane3 */
    public static final String TESTPAGE_TEXTPANES_TP03 = "TestPage.TextPanes.TextPane03"; //$NON-NLS-1$
    /** the component id for TextPane4 */
    public static final String TESTPAGE_TEXTPANES_TP04 = "TestPage.TextPanes.TextPane04"; //$NON-NLS-1$
    /** the component id for TextPane5 */
    public static final String TESTPAGE_TEXTPANES_TP05 = "TestPage.TextPanes.TextPane05"; //$NON-NLS-1$
    
    /** the component id for Tree1 */
    public static final String TESTPAGE_TREES_TRE01 = "TestPage.Trees.Tree01"; //$NON-NLS-1$
    /** the component id for Tree2 */
    public static final String TESTPAGE_TREES_TRE02 = "TestPage.Trees.Tree02"; //$NON-NLS-1$
    /** the component id for Tree3*/
    public static final String TESTPAGE_TREES_TRE03 = "TestPage.Trees.Tree03"; //$NON-NLS-1$
    /** the component id for Tree4*/
    public static final String TESTPAGE_TREES_TRE04 = "TestPage.Trees.Tree04"; //$NON-NLS-1$
    /** the component id for Tree5*/
    public static final String TESTPAGE_TREES_TRE05 = "TestPage.Trees.Tree05"; //$NON-NLS-1$
    /** the component id for Tree6*/
    public static final String TESTPAGE_TREES_TRE06 = "TestPage.Trees.Tree06"; //$NON-NLS-1$
    
    /** the component id for TreeTable1 */
    public static final String TESTPAGE_TREETABLES_TRT01 = "TestPage.TreeTables.TreeTable01"; //$NON-NLS-1$
    /** the component id for TreeTable2 */
    public static final String TESTPAGE_TREETABLES_TRT02 = "TestPage.TreeTables.TreeTable02"; //$NON-NLS-1$
    /** the component id for TreeTable3 */
    public static final String TESTPAGE_TREETABLES_TRT03 = "TestPage.TreeTables.TreeTable03"; //$NON-NLS-1$
    /** the component id for PasswordField1 */
    public static final String TESTPAGE_PASSWORDFIELDS_PWDF01 = "TestPage.PasswordFields.PasswordField01"; //$NON-NLS-1$
    /** the component id for PasswordField2 */
    public static final String TESTPAGE_PASSWORDFIELDS_PWDF02 = "TestPage.PasswordFields.PasswordField02"; //$NON-NLS-1$
    /** the component id for PasswordField3 */
    public static final String TESTPAGE_PASSWORDFIELDS_PWDF03 = "TestPage.PasswordFields.PasswordField03"; //$NON-NLS-1$
    /** the component id for PasswordField4 */
    public static final String TESTPAGE_PASSWORDFIELDS_PWDF04 = "TestPage.PasswordFields.PasswordField04"; //$NON-NLS-1$
    /** the component id for PasswordField5 */
    public static final String TESTPAGE_PASSWORDFIELDS_PWDF05 = "TestPage.PasswordFields.PasswordField05"; //$NON-NLS-1$
    
    /** the component id for Scale1 */
    public static final String TESTPAGE_SCALE_SC01 = "TestPage.Scale.Scale01"; //$NON-NLS-1$
    /** the component id for Scale2 */
    public static final String TESTPAGE_SCALE_SC02 = "TestPage.Scale.Scale02"; //$NON-NLS-1$
    /** the component id for Scale3 */
    public static final String TESTPAGE_SCALE_SC03 = "TestPage.Scale.Scale03"; //$NON-NLS-1$
    /** the component id for Scale4 */
    public static final String TESTPAGE_SCALE_SC04 = "TestPage.Scale.Scale04"; //$NON-NLS-1$
    
    /** the component id for Slider1 */
    public static final String TESTPAGE_SLIDER_SL01 = "TestPage.Slider.Slider01"; //$NON-NLS-1$
    /** the component id for Slider2 */
    public static final String TESTPAGE_SLIDER_SL02 = "TestPage.Slider.Slider02"; //$NON-NLS-1$
    /** the component id for Slider3 */
    public static final String TESTPAGE_SLIDER_SL03 = "TestPage.Slider.Slider03"; //$NON-NLS-1$
    /** the component id for Slider4 */
    public static final String TESTPAGE_SLIDER_SL04 = "TestPage.Slider.Slider04"; //$NON-NLS-1$
    /** the component id for Slider5 */
    public static final String TESTPAGE_SLIDER_SL05 = "TestPage.Slider.Slider05"; //$NON-NLS-1$
    
    /** the component id for Spinner1 */
    public static final String TESTPAGE_SPINNER_SP01 = "TestPage.Spinner.Spinner01"; //$NON-NLS-1$
    /** the component id for Spinner2 */
    public static final String TESTPAGE_SPINNER_SP02 = "TestPage.Spinner.Spinner02"; //$NON-NLS-1$
    /** the component id for Spinner3 */
    public static final String TESTPAGE_SPINNER_SP03 = "TestPage.Spinner.Spinner03"; //$NON-NLS-1$
    
    /** the component id for ComboBox for CSS styling */
    public static final String TESTPAGE_STYLED_COMBO = "TestPage.Styled.ComboBox"; //$NON-NLS-1$
    /** the component id for styled Button01 */
    public static final String TESTPAGE_STYLED_BTN01 = "TestPage.Styled.Button01"; //$NON-NLS-1$
    /** the component id for styled Button02 */
    public static final String TESTPAGE_STYLED_BTN02 = "TestPage.Styled.Button02"; //$NON-NLS-1$
    /** the component id for styled Button03 */
    public static final String TESTPAGE_STYLED_BTN03 = "TestPage.Styled.Button03"; //$NON-NLS-1$
    /** the component id for styled Button04 */
    public static final String TESTPAGE_STYLED_BTN04 = "TestPage.Styled.Button04"; //$NON-NLS-1$
    
    /** id for title Buttons */
    public static final String TESTPAGE_TITLE_BUTTONS = "Buttons"; //$NON-NLS-1$
    /** id for title CheckBoxes */
    public static final String TESTPAGE_TITLE_CHECKBOXES = "CheckBoxes"; //$NON-NLS-1$
    /** id for title Containers */
    public static final String TESTPAGE_TITLE_CONTAINERS = "Containers"; //$NON-NLS-1$
    /** id for title Panes */
    public static final String TESTPAGE_TITLE_PANES = "Panes"; //$NON-NLS-1$
    /** id for title RadioButtons */
    public static final String TESTPAGE_TITLE_RADIOBUTTONS = "RadioButtons"; //$NON-NLS-1$
    /** id for title ComboBoxes */
    public static final String TESTPAGE_TITLE_COMBOBOXES = "ComboBoxes"; //$NON-NLS-1$
    /** id for title Command */
    public static final String TESTPAGE_TITLE_COMMAND = "Command"; //$NON-NLS-1$
    /** id for title DatePickers */
    public static final String TESTPAGE_TITLE_DATEPICKERS = "DatePickers"; //$NON-NLS-1$
    /** id for title Shapes */
    public static final String TESTPAGE_TITLE_SHAPES = "Shapes"; //$NON-NLS-1$
    /** id for title Textfields */
    public static final String TESTPAGE_TITLE_TEXTFIELDS = "Textfields"; //$NON-NLS-1$
    /** id for title TextAreas */
    public static final String TESTPAGE_TITLE_TEXTAREAS = "TextAreas"; //$NON-NLS-1$
    /** id for title EditorPanes */
    public static final String TESTPAGE_TITLE_EDITORPANES = "EditorPanes"; //$NON-NLS-1$
    /** id for title TextPanes */
    public static final String TESTPAGE_TITLE_TEXTPANES = "TextPanes"; //$NON-NLS-1$
    /** id for title ImageViews */
    public static final String TESTPAGE_TITLE_IMAGEVIEWS = "ImageViews"; //$NON-NLS-1$
    /** id for title Labels */
    public static final String TESTPAGE_TITLE_LABELS = "Labels"; //$NON-NLS-1$
    /** id for title Lists */
    public static final String TESTPAGE_TITLE_LISTS = "Lists"; //$NON-NLS-1$
    /** id for title Menus */
    public static final String TESTPAGE_TITLE_MENUS = "Menus"; //$NON-NLS-1$
    /** id for title TabbedPanes */
    public static final String TESTPAGE_TITLE_TABBEDPANES = "TabbedPanes"; //$NON-NLS-1$
    /** id for title Accordions */
    public static final String TESTPAGE_TITLE_ACCORDIONS = "Accordions"; //$NON-NLS-1$
    /** id for title Charts */
    public static final String TESTPAGE_TITLE_CHARTS = "Charts"; //$NON-NLS-1$
    /** id for title Tables */
    public static final String TESTPAGE_TITLE_TABLES = "Tables"; //$NON-NLS-1$
    /** id for title Applications */
    public static final String TESTPAGE_TITLE_APPLICATION = "Applications"; //$NON-NLS-1$
    /** id for title Trees */
    public static final String TESTPAGE_TITLE_TREES = "Trees"; //$NON-NLS-1$
    /** id for title TreeTables */
    public static final String TESTPAGE_TITLE_TREETABLES = "TreeTables"; //$NON-NLS-1$
    /** id for title PasswordFields */
    public static final String TESTPAGE_TITLE_PASSWORDFIELDS = "PasswordFields"; //$NON-NLS-1$
    /** id for title Scale */
    public static final String TESTPAGE_TITLE_SCALE = "Scale"; //$NON-NLS-1$
    /** id for title Simple Extensions */
    public static final String TESTPAGE_TITLE_SIMPLE_EXTENSION = "SimpleExtension"; //$NON-NLS-1$
    /** id for title Slider */
    public static final String TESTPAGE_TITLE_SLIDER = "Slider"; //$NON-NLS-1$
    /** id for title Spinner */
    public static final String TESTPAGE_TITLE_SPINNER = "Spinner"; //$NON-NLS-1$
    /** id for title Toolbar Item */
    public static final String TESTPAGE_TITLE_TOOLBAR_ITEM = "ToolbarItem"; //$NON-NLS-1$
    /** id for title CLabels */
    public static final String TESTPAGE_TITLE_CLABELS = "CLabels"; //$NON-NLS-1$
    /** id for title CTabFolders*/
    public static final String TESTPAGE_TITLE_CTABFOLDERS = "CTabFolders"; //$NON-NLS-1$
    /** id for title JProgressBars*/
    public static final String TESTPAGE_TITLE_JPROGRESSBARS = "JProgressBars"; //$NON-NLS-1$
    /** id for title ProgressBars*/
    public static final String TESTPAGE_TITLE_PROGRESSBARS = "ProgressBars"; //$NON-NLS-1$
    /** id for title styled content */
    public static final String TESTPAGE_TITLE_STYLED_CONTENT = "StyledContent"; //$NON-NLS-1$
    /** id for title Hyperlink */
    public static final String TESTPAGE_TITLE_HYPERLINK = "Hyperlinks"; //$NON-NLS-1$
    
    /** id for DefaultToolTip Label */
    public static final String TESTPAGE_TOOLTIP_DEFAULT = "TestPage.ToolTip.DefaultTooltip"; //$NON-NLS-1$
    /** id for DefaultToolTip Label */
    public static final String TESTPAGE_TOOLTIP_DEFAULT_DELAY = "TestPage.ToolTip.DefaultTooltipDelay"; //$NON-NLS-1$
    /** id for custom ToolTip Label */
    public static final String TESTPAGE_TOOLTIP_CUSTOM = "TestPage.ToolTip.Custom.Label"; //$NON-NLS-1$
    /** id for Label in customToolTip */
    public static final String TESTPAGE_TOOLTIP_CUSTOM_INTERN = "TestPage.ToolTip.Custom.InToolTip.Label"; //$NON-NLS-1$
    /** id for Button in customToolTip */
    public static final String TESTPAGE_TOOLTIP_CUSTOM_BUTTON = "TestPage.ToolTip.Custom.InToolTip.Button"; //$NON-NLS-1$
    /** id for Label for a custom Listener */
    public static final String TESTPAGE_TOOLTIP_CUSTOM_LISTENER = "TestPage.ToolTip.Custom.ListenerLabel"; //$NON-NLS-1$

    /** the component id for SimpleExtensionComponents */
    public static final String TESTPAGE_SIMPLEEXT_C01 = "TestPage.SimpleExtensions.Component01"; //$NON-NLS-1$
    
}
