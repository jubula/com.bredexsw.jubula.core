package org.eclipse.jubula.qa.aut.caa.base.utils;

import static org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils.getString;

import org.eclipse.jubula.qa.aut.caa.base.utils.model.ListItem;

/**
 * Helper class for components. 
 * 
 */
public class ComponentUtils {
    /**
     * ComponentUtils
     */
    private ComponentUtils() {
    // empty
    }

    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getListItems() {
        return new String[] { getString("text1"), //$NON-NLS-1$
                getString("text2"), getString("text3"), //$NON-NLS-1$ //$NON-NLS-2$
                getString("text4"), getString("text5"), //$NON-NLS-1$ //$NON-NLS-2$
                getString("text6"), getString("text7") }; //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static Integer[] getNumberItems() {
        return new Integer[] { 
            new Integer(100000000), 
            new Integer(200000000), 
            new Integer(300000000),
            new Integer(400000000) };
    }
    
    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getListLongItems() {
        return new String[] { "Size: 100px", "VeryLongListItem", //$NON-NLS-1$ //$NON-NLS-2$
                              "VeryVeryLongListItem", "VeryVeryVeryLongListItem",  //$NON-NLS-1$//$NON-NLS-2$
                              "VeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryVeryVeryLongListItem" }; //$NON-NLS-1$
    }

    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getList() {

        String[] index = new String[200];
        for (int i = 0; i < index.length; i++) {
            index[i] = String.valueOf(i + 1);
        }
        return index;
    }
    
    /**
     * @return a array with {@link ListItem}
     */
    public static ListItem[] getListModelItems() {
        return new ListItem[] { 
            new ListItem(1, getString("text1")), //$NON-NLS-1$
            new ListItem(2, getString("text2")), //$NON-NLS-1$
            new ListItem(3, getString("text3")), //$NON-NLS-1$
            new ListItem(4, getString("text4")), //$NON-NLS-1$
            new ListItem(5, getString("text5")), //$NON-NLS-1$
            new ListItem(6, getString("text6")), //$NON-NLS-1$
            new ListItem(7, getString("text7")) //$NON-NLS-1$
        };
    }
    

}
