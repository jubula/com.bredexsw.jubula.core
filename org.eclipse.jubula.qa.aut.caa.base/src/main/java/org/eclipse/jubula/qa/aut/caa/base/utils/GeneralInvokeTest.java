package org.eclipse.jubula.qa.aut.caa.base.utils;

import java.util.Properties;
/**
 * Class containing general methods which will be invoke by the invoke actions
 *
 */
public class GeneralInvokeTest {
    
    /**
     * private constructor
     */
    protected GeneralInvokeTest() {
        // private
    }
    
    /**
     * Test method for the invoke actions
     * @param numOfProps number of properties which will be created
     * @return the created properties
     */
    public static Properties invokeReturnProps(int numOfProps) {
        Properties p = new Properties();
        for (int i = 0; i < numOfProps; i++) {
            p.setProperty("Test_Key" + i, "Test_Value" + i); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return p;
    }
    
    /**
     * Test method for invoke actions
     * @param time sleepy time
     * @throws InterruptedException
     */
    public static void invokeLongRunning(long time) {
        long begin = System.currentTimeMillis();
        while (System.currentTimeMillis() - begin > time) {
            long sleeptime = begin + time - System.currentTimeMillis();
            try {
                if (sleeptime > 0) {
                    Thread.sleep(sleeptime);
                }
            } catch (InterruptedException e) {
                // ignore 
            }
        }
        
    }
    
    /**
     * @param text the text
     * @param bool a boolean
     * @param integer an Integer
     * @param number a Double
     * @param simpleBoolean a simpleBoolean
     * @return the concatenation of all Strings
     */
    public static String invoke(String text, Boolean bool, Integer integer,
            Double number, boolean simpleBoolean) {
        return text + " " + bool + " " + integer + " " + number + " "  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            + simpleBoolean;
    }
    /**
     * @return {@link String} with value "no parameter"
     */
    public static String invoke() {
        return "no parameter"; //$NON-NLS-1$
    }
    
    /**
     * intended to use with graphics component
     * @param o the graphics component object (not used in method)
     * @return {@link String} with value "GraphicsComponent with no parameter"
     */
    public static String invoke(Object o) {
        return "GraphicsComponent with no parameter"; //$NON-NLS-1$
    }
    
    /**
     * intended to use with graphics component
     * @param o the graphics component object (not used in method)
     * @param text a text
     * @return {@link String} 
     */
    public static String invoke(Object o, String text) {
        return "GraphicsComponent with wit text: " + text; //$NON-NLS-1$
    }
    
    /**
     * Test method for the invoke actions
     * @param obj the component
     * @param text some text given 
     * @return the text of the component
     */
    public static String getClassOfComp(Object obj, String text) {
        return getClassOfComp(obj) +  " " + text; //$NON-NLS-1$
    }
    
    /**
     * Test method for the invoke actions
     * @param obj the component to get the text from
     * @return the text of the component
     */
    public static String getClassOfComp(Object obj) {
        return obj.getClass().getSimpleName();
    }
    /**
     * 
     * @param i an integer
     * @param b an boolean
     * @return a string with the values
     */
    public static String invokeSimple(int i, boolean b) {
        return "integer:" + i + " boolean:" + b; //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    /**
     * @param object instance of the class {@link SimpleInvoke}
     * @return to String of {@link SimpleInvoke}
     */
    public static String invokeOwnClass(SimpleInvoke object) {
        return object.toString();
    }
    
    /**
     * @return instance of the class {@link SimpleInvoke}
     */
    public static SimpleInvoke returnOwnClass() {
        return new SimpleInvoke(""); //$NON-NLS-1$
    }
    
    /**
     * @param integer a integer
     * @return the parameter
     */
    public static int returnInteger(int integer) {
        return integer;
    }
    /**
     * 
     * @return the string <code>"nonstatic"</code>
     */
    public String nonStaticMethod() {
        return "nonStatic"; //$NON-NLS-1$
    }
}
