package org.eclipse.jubula.qa.aut.caa.base.utils;
/**
 * class to use with invoke method
 * 
 */
public class SimpleInvoke {
    /** some text */
    private String m_text = null;
    /**
     * @param s some text
     */
    public SimpleInvoke(String s) {
        m_text = s;
    }
    /**
     * {@inheritDoc}
     */
    public String toString() {
        return "SimpleInvokeClass " + m_text; //$NON-NLS-1$
    }
}
