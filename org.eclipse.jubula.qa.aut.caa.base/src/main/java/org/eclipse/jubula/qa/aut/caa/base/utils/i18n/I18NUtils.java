package org.eclipse.jubula.qa.aut.caa.base.utils.i18n;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Helperclass for i18n.
 * 
 */
public class I18NUtils {

    /**
     * the stringBundle
     */
    private static ResourceBundle stringBundle;

    /**
     * the nameBundle
     */
    private static ResourceBundle nameBundle;

    /**
     * private constructor.
     */
    private I18NUtils() {
    // empty
    }

    /**
     * Returns the internationalized string for the given key.
     * 
     * @param key
     *            of i18n string
     * @return i18n string
     */
    public static String getString(String key) {
        String toReturn = key;
        if (stringBundle == null) {
            stringBundle = ResourceBundle
            .getBundle("org.eclipse.jubula.qa.aut.caa.base.utils.i18n.AUTStrings"); //$NON-NLS-1$
        }
        try {
            toReturn = stringBundle.getString(key);
        } catch (MissingResourceException e) {
            // ignore
        }
        return toReturn;
    }

    /**
     * Returns the internationalized component name for the given key.
     * 
     * @param key
     *            of i18n string
     * @return i18n string
     */
    public static String getName(String key) {
        String toReturn = key;
        if (nameBundle == null) {
            nameBundle = ResourceBundle
            .getBundle("org.eclipse.jubula.qa.aut.caa.base.utils.i18n.ComponentNames"); //$NON-NLS-1$
        }
        try {
            toReturn = nameBundle.getString(key);
        } catch (MissingResourceException e) {
            // ignore
        }
        return toReturn;
    }
    
    /**
     * Returns the internationalized Title for the TestPage
     * 
     * @param technicalName - key for the TestPage
     * @param clazz for the component to test
     * @return a String for TestPage Title
     */
    public static String getTestpageTitle(String technicalName, 
        Class<?> clazz) {
        return clazz.getName() + " - " + getString(technicalName); //$NON-NLS-1$
    }

}
