/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.jubula.qa.aut.caa.base.utils.model;

/**
 * a model class used for JList
 * @author BREDEX GmbH
 */
public class ListItem {
    /** an id */
    private int m_id;
    /** the value */
    private String m_value;
    
    
    /**
     * 
     * @param id an id
     * @param value a value
     */
    public ListItem(int id, String value) {
        m_id = id;
        m_value = value;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return m_id;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return m_value;
    }
    
    /**
     * {@inheritDoc}
     */
    public String toString() {
        return m_id + " " + m_value; //$NON-NLS-1$
    }
    
}
