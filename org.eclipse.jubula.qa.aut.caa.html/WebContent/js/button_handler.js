window.onload = function() {
    var button = document.getElementById("Button.button8");
    //button.disabled = true;
    button.style.visibility = "hidden";
    toogleEnable( button, 5000, false, true);
};

function toogleEnable( button, delay, disabled, enable ) {
  setTimeout(
    function() 
    {
      //button.disabled = disabled;
      button.style.visibility = disabled ? "hidden" : "visible";
      if(enable) {
	toogleEnable( button, delay, !disabled, false );
      };
    }, delay);
}