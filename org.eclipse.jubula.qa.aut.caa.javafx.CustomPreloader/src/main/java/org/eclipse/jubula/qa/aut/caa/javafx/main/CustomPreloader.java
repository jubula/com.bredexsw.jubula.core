package org.eclipse.jubula.qa.aut.caa.javafx.main;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Class for a simple Preloader with a progress bar.
 * 
 * @author Bredex GmbH
 */
public class CustomPreloader extends Preloader {

    /**
     * The ProgressBar
     */
    private ProgressBar m_bar;

    /**
     * The Stage
     */
    private Stage m_stage;

    /**
     * 
     * @return the Scene
     */
    private Scene createPreloaderScene() {
        m_bar = new ProgressBar();
        BorderPane p = new BorderPane();
        p.setCenter(m_bar);
        p.setBottom(new TextField("PRELOADER"));
        return new Scene(p, 300, 150);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.m_stage = stage;
        stage.setScene(createPreloaderScene());
        stage.show();
    }

    @Override
    public void handleProgressNotification(ProgressNotification pn) {
        m_bar.setProgress(pn.getProgress());
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification evt) {
        if (evt.getType() == StateChangeNotification.Type.BEFORE_START) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            m_stage.hide();
        }
    }

}
