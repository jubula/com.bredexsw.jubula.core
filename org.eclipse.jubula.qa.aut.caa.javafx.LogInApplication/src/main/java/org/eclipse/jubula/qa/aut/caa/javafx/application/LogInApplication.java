/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.eclipse.jubula.qa.aut.caa.javafx.application;

import org.eclipse.jubula.qa.aut.caa.javafx.preloader.Confirm;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * The Application for the Login-Preloader.
 * 
 * @author BREDEX GmbH
 */
public class LogInApplication extends Application implements Confirm {
    /**
     * The primary Stage
     */
    private Stage m_primaryStage;

    /**
     * True if the Login button in the Preloader was pressed.
     */
    private boolean m_flag = false;

    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Say 'Hello World'"); //$NON-NLS-1$
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!"); //$NON-NLS-1$
            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(btn);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!"); //$NON-NLS-1$
        primaryStage.setScene(scene);
        m_primaryStage = primaryStage;
        canStart();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     * 
     * @param args
     *            the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Checks if the Application can start now.
     */
    private void canStart() {
        if (m_flag) {
            Platform.runLater(new Runnable() {
                public void run() {
                    m_primaryStage.show();
                }
            });
        }
    }

    @Override
    public void confirm() {
        m_flag = true;
        canStart();
    }

}
