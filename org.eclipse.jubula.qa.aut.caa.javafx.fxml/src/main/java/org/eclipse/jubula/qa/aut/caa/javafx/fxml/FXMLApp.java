package org.eclipse.jubula.qa.aut.caa.javafx.fxml;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.TitledPane;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

/** Generic FXML loader application */
public class FXMLApp extends Application {
    /** the command line argument name */
    public static final String URL_ARG_NAME = "url"; //$NON-NLS-1$
    /** the FXML content component */
    private ScrollPane m_fxmlContent;
    /** the error message field */
    private TextInputControl m_errorMessage;

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane main = new BorderPane();

        Map<String, String> named = getParameters().getNamed();

        m_fxmlContent = new ScrollPane();

        m_errorMessage = new TextArea();
        final TitledPane err = new TitledPane("Error Log", m_errorMessage); //$NON-NLS-1$
        err.setExpanded(false);

        HBox top = new HBox();
        ObservableList<Node> topChildren = top.getChildren();
        final TextField urlField = new TextField();
        final String url = named.get(URL_ARG_NAME);
        urlField.setText(url);
        final Button load = new Button("Load FXML from URL:"); //$NON-NLS-1$
        load.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean errorOccurred = false;
                Node n = null;
                try {
                    n = (Node) FXMLLoader.load(new URL(urlField.getText()));
                } catch (IOException e) {
                    errorOccurred = true;
                    m_errorMessage.setText(e.getLocalizedMessage());
                }
                m_fxmlContent.setContent(n);

                err.setExpanded(errorOccurred);
                if (!errorOccurred) {
                    m_errorMessage.setText(null);
                }
            }
        });

        if (url != null) {
            load.getOnAction().handle(null);
        }

        HBox.setHgrow(urlField, Priority.ALWAYS);
        topChildren.addAll(load, urlField);

        main.setTop(top);
        main.setCenter(m_fxmlContent);
        main.setBottom(err);

        Scene scene = new Scene(main);

        addSystemDropSupport(urlField, load, scene);

        stage.setTitle("FXMLLoader - simply drop FXML-file on window!"); //$NON-NLS-1$
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param urlField
     *            the field
     * @param load
     *            the button to trigger
     * @param scene
     *            the scene to use
     */
    private void addSystemDropSupport(final TextField urlField,
        final Button load, Scene scene) {
        scene.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        scene.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    urlField.setText(db.getFiles().get(0).getAbsoluteFile()
                        .toURI().toString());
                    load.getOnAction().handle(null);
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
    }
}