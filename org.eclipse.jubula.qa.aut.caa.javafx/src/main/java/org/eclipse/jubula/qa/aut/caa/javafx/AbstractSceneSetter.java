package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

/**
 * Abstract class for initializing a test-pane creating a scene and setting it
 * on the primaryStage
 * 
 */
public abstract class AbstractSceneSetter implements EventHandler<ActionEvent> {

    /**
     * sets the new testing scene
     * 
     * @param event
     *            the event
     */
    public void handle(ActionEvent event) {
        TestPane testScene = getTestScene();
        String title = testScene.getSceneTitle();
        Scene scene = new Scene(testScene.getRootPane());
        StartScene.getInstance().setScene(scene, title);
    }

    /**
     * This method has to be overwritten by every class that initializes
     * components for testing. All specific components are added to the
     * container-class TestPane.
     * 
     * @return the TestPane with the specific components
     */
    protected abstract TestPane getTestScene();

}
