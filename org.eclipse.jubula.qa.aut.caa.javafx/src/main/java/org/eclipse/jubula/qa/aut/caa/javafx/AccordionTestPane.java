package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Creates {@link Accordion}s for use in CaA tests.
 * 
 */
public class AccordionTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane parentPane = new TestPane("title_accordions");
        parentPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_ACCORDIONS);

        addAccordion1(parentPane);
        
        addAccordion2(parentPane);
        
        addAccordion3(parentPane);
        
        return parentPane;
    }

    /**
     * Creates a new {@link Accordion} and adds it to the given pane. 
     * The created widget has the following notable properties:<ul>
     * <li>context menu</li>
     * <li>tabs on top</li>
     * <li>tabs cannot be closed</li>
     * <li>one disabled tab</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created widget should be added.
     */
    private void addAccordion1(TestPane parentPane) {
        Accordion widget = new Accordion();
        // don't set the prefHeight, as the JavaDoc for Accordion recommends 
        // against it
        widget.setPrefWidth(200);
        widget.setId(ComponentNameConstants.TESTPAGE_ACCORDIONS_TBP01);
        parentPane.addContextMenuToNode(widget);
        
        Label lbl1 = new Label(I18NUtils.getString("img_carrot"));
        TitledPane item1 = new TitledPane(I18NUtils.getString("tab1"), lbl1);
        
        Label lbl2 = new Label(I18NUtils.getString("img_banana"));
        TitledPane item2 = new TitledPane(I18NUtils.getString("tab2"), lbl2);
        
        TitledPane item3 = new TitledPane(I18NUtils.getString("tab3"), null);
        item3.setDisable(true);
        item1.setAnimated(false);
        item2.setAnimated(false);
        item3.setAnimated(false);
        widget.getPanes().addAll(item1, item2, item3);
        parentPane.addControls(widget);
        parentPane.addControlsToEventHandler(widget);
    }
    
    /**
     * Creates a new {@link Accordion} and adds it to the given pane. 
     * The created widget has the following notable properties:<ul>
     * <li>tabs on left</li>
     * <li>tabs cannot be closed</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created widget should be added.
     */
    private void addAccordion2(TestPane parentPane) {
        Accordion widget = new Accordion();
        // don't set the prefHeight, as the JavaDoc for Accordion recommends 
        // against it
        widget.setPrefWidth(200);
        widget.setId(ComponentNameConstants.TESTPAGE_ACCORDIONS_TBP02);

        Label lbl1 = new Label(I18NUtils.getString("img_carrot"));
        TitledPane item1 = new TitledPane(I18NUtils.getString("tab1"), lbl1);

        Label lbl2 = new Label(I18NUtils.getString("img_banana"));
        TitledPane item2 = new TitledPane(I18NUtils.getString("tab2"), lbl2);
        
        Label lbl3 = new Label(I18NUtils.getString("img_kiwi"));
        TitledPane item3 = new TitledPane(I18NUtils.getString("tab3"), lbl3);
        item1.setAnimated(false);
        item2.setAnimated(false);
        item3.setAnimated(false);
        widget.getPanes().addAll(item1, item2, item3);
        parentPane.addControls(widget);
        parentPane.addControlsToEventHandler(widget);
    }

    /**
     * Creates a new {@link Accordion} and adds it to the given pane. 
     * The created widget has the following notable properties:<ul>
     * <li>many tabs</li>
     * <li>tabs on top</li>
     * <li>tabs cannot be closed</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created widget should be added.
     */
    private void addAccordion3(TestPane parentPane) {
        Accordion widget = new Accordion();
        widget.setPrefWidth(200);
        widget.setPrefHeight(400);
        widget.setId(ComponentNameConstants.TESTPAGE_ACCORDIONS_TBP03);

        for (int i = 0; i < 50; i++) {
            Label label = new Label(I18NUtils.getString("img_carrot"));
            TitledPane tab = new TitledPane(String.valueOf(i), label);
            tab.setAnimated(false);
            widget.getPanes().add(tab);
        }
        
        ScrollPane scrollableContainer = new ScrollPane(widget);
        scrollableContainer.setMaxHeight(400);
        scrollableContainer.setFitToHeight(true);
        parentPane.addControls(scrollableContainer);
        parentPane.addControlsToEventHandler(widget);
    }
}
