package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes Application pane for testing.
 */
public class ApplicationTestPane extends AbstractSceneSetter {

    /**
     * @author BREDEX GmbH
     */
    private static class ProperAUTTerminator implements
            EventHandler<ActionEvent> {
        /** {@inheritDoc} */
        public void handle(ActionEvent arg0) {
            System.exit(0);
        }

    }

    /**
     * listener to properly terminate the AUT
     */
    private EventHandler<ActionEvent> m_terminateAUT =
            new ProperAUTTerminator();

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_application");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_APPLICATION);
        // Exit menu bar
        MenuBar menuBar = new MenuBar();
        menuBar.setId(JavaFXComponentNameConstants
                .TESTPAGE_APPLICATION_MENUBAR);
        Menu aut = new Menu(I18NUtils.getString("aut"));
        MenuItem item1 = new MenuItem(I18NUtils.getString("exit"));
        item1.addEventHandler(ActionEvent.ACTION, m_terminateAUT);
        aut.getItems().add(item1);
        menuBar.getMenus().add(aut);

        // text Field
        TextField textField = new TextField();
        textField.setId(ComponentNameConstants.TESTPAGE_APPLICATION_TF01);
        textField.setTooltip(new Tooltip(I18NUtils.getString("tooltip")));
        List<MenuItem> items = MenuBarTestPane.createContextMenu(testPane);
        MenuItem[] ar = new MenuItem[items.size()];
        items.toArray(ar);
        textField.setContextMenu(new ContextMenu(ar));

        // exit Button
        Button exitButton = new Button(I18NUtils.getName("exit"));
        exitButton.setId(ComponentNameConstants.TESTPAGE_APPLICATION_EXIT_BTN);
        exitButton.addEventHandler(ActionEvent.ACTION, m_terminateAUT);

        testPane.addControls(textField, exitButton);
        testPane.addMenuBar(menuBar);
        return testPane;
    }

}
