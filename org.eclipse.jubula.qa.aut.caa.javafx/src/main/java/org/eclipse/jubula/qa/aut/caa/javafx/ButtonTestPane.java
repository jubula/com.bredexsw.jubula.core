package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Reflection;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Initializes Buttons for testing.
 */
public class ButtonTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_buttons");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);
        
        // button 1
        Button btn1 = new Button(I18NUtils.getName("btn1"));
        btn1.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        btn1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn1.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(btn1);
        
        // button 2
        Button btn2 = new Button(I18NUtils.getName("btn2"));
        btn2.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        btn2.setDisable(true);
        btn2.setMaxWidth(Double.MAX_VALUE);

        // button 3
        Button btn3 = new Button(I18NUtils.getName("btn3"));
        btn3.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN03);
        btn3.setMaxWidth(Double.MAX_VALUE);
        btn3.setAlignment(Pos.CENTER_LEFT);
        Button graphicButton = new Button("Graphic Button");
        graphicButton.setId("Button.GraphicNode");
        graphicButton.setMaxWidth(120);
        btn3.setGraphic(graphicButton);
        Button butception = new Button("Graphic Button of Graphic Button");
        butception.setId("Button.GraphicNode.GraphicNode");
        butception.setMaxWidth(60);
        graphicButton.setGraphic(butception);
        
        // button 4
        Button btn4 = new Button(I18NUtils.getName("btn4"));
        btn4.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN04);
        btn4.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn4.setMaxWidth(Double.MAX_VALUE);
        btn4.setEffect(new Reflection());

        // button 5
        Button btn5 = new Button(I18NUtils.getName("btn5"));
        btn5.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN05);
        btn5.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn5.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(btn5);
        
        testPane.addControls(btn1, btn2, btn3, btn4);
        testPane.addControlsToEventHandler(btn1, btn2, btn3, btn4,
                graphicButton, butception);
        
        new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e1) {
                // ignore
            }
            Platform.runLater(() -> {
                testPane.addControls(btn5);
            });
            new Thread(() -> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e2) {
                    // ignore
                }
                Platform.runLater(() -> {
                    testPane.removeControls(btn5);
                });
            }).start();
        }).start();

        return testPane;
    }

}
