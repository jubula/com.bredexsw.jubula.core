package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
/**
 * Initializes CheckBoxes for testing.
 */
public class CheckBoxTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_checkBoxes");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_CHECKBOXES);
        
        // add components to test

        // checkbox 1
        CheckBox cbx1 = new CheckBox(I18NUtils.getName("cbx1")); //$NON-NLS-1$
        cbx1.setId(ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX01);
        cbx1.setTooltip(new Tooltip(I18NUtils.getString("tooltip"))); //$NON-NLS-1$
        testPane.addContextMenuToNode(cbx1);

        // checkbox 2
        CheckBox cbx2 = new CheckBox(I18NUtils.getName("cbx2")); //$NON-NLS-1$
        cbx2.setId(ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX02);
        cbx2.setDisable(true);
      
        testPane.addControls(cbx1, cbx2);
        testPane.addControlsToEventHandler(cbx1, cbx2);
        
        return testPane;
    }

}
