package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.Arrays;

import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes ComboBoxes for testing.
 */
public class ComboBoxTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_comboBoxes");

        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_COMBOBOXES);
        // ComboBox 1
        ComboBox<String> cb1 = new ComboBox<String>();
        cb1.setId(JavaFXComponentNameConstants.TESTPAGE_COMBOBOXES_CBOX01);
        tPane.addContextMenuToNode(cb1);
        EventHandler<MouseEvent> evH = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.getButton().equals(MouseButton.SECONDARY)) {
                    cb1.getContextMenu().show(cb1, event.getScreenX(),
                            event.getScreenY());
                    event.consume();
                }
            }
        };
        cb1.addEventFilter(MouseEvent.MOUSE_CLICKED, evH);
        cb1.addEventFilter(MouseEvent.MOUSE_PRESSED, evH);
        cb1.addEventFilter(MouseEvent.MOUSE_RELEASED, evH);
                

        // ComboBox 2
        ComboBox<String> cb2 = new ComboBox<String>();
        cb2.setId(JavaFXComponentNameConstants.TESTPAGE_COMBOBOXES_CBOX02);
        cb2.getItems().addAll(ComponentUtils.getListItems());

        // ComboBox 3
        ComboBox<String> cb3 = new ComboBox<String>();
        cb3.setId(JavaFXComponentNameConstants.TESTPAGE_COMBOBOXES_CBOX03);
        cb3.setDisable(true);

        // ComboBox 4
        ComboBox<Object> cb4 = new ComboBox<Object>();
        cb4.setId(JavaFXComponentNameConstants.TESTPAGE_COMBOBOXES_CBOX04);
        cb4.getItems().addAll(Arrays.asList(ComponentUtils.getListItems()));
        cb4.getItems().add(1, new Separator());

        // ComboBox 5
        ComboBox<String> cb5 = new ComboBox<String>();
        cb5.setId(JavaFXComponentNameConstants.TESTPAGE_COMBOBOXES_CBOX05);
        cb5.setTooltip(new Tooltip("Tooltip"));

        tPane.addControls(cb1, cb2, cb3, cb4, cb5);

        return tPane;
    }
}
