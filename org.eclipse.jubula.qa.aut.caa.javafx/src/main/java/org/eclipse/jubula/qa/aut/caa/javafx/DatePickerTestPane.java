package org.eclipse.jubula.qa.aut.caa.javafx;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import javafx.scene.control.DatePicker;
import javafx.util.StringConverter;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
/**
 * Initializes TextFields for testing.
 */
public class DatePickerTestPane extends AbstractSceneSetter {

    /**
     * class to format the time for the specific pattern {@link #PATTERN}
     *
     */
    private static class DateConverter  extends StringConverter<LocalDate> {
        /** the pattern*/
        private static final String PATTERN = "M/d/yyyy"; //$NON-NLS-1$
        /** the {@link DateTimeFormatter}*/
        private DateTimeFormatter m_dateFormatter = DateTimeFormatter
                .ofPattern(PATTERN);
        /**
         * @return the {@link #PATTERN}
         */
        public static String getPattern() {
            return PATTERN;
        }
        /**
         * {@inheritDoc}
         */
        public String toString(LocalDate date) {
            if (date != null) {
                return m_dateFormatter.format(date);
            } else {
                return ""; //$NON-NLS-1$
            }
        }

        /**
         * {@inheritDoc}
         */
        public LocalDate fromString(String string) {
            if (string != null && !string.isEmpty()) {
                return LocalDate.parse(string, m_dateFormatter);
            } else {
                return null;
            }
        }
    }
    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_datePickers"); //$NON-NLS-1$
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_DATEPICKERS);
        
        // Date Picker 1
        DatePicker dp1 = new DatePicker();
        dp1.setId(ComponentNameConstants.TESTPAGE_DATEPICKERS_DP01);
        setDatePattern(dp1);

        // Date Picker 1
        DatePicker dp2 = new DatePicker();
        dp2.setId(ComponentNameConstants.TESTPAGE_DATEPICKERS_DP02);
        dp2.setDisable(true);
        setDatePattern(dp2);
        
        // Date Picker 3
        DatePicker dp3 = new DatePicker(LocalDate.of(2009, Month.MAY, 23));
        dp3.setId(ComponentNameConstants.TESTPAGE_DATEPICKERS_DP03);
        dp3.setEditable(false);
        setDatePattern(dp3);
        testPane.addControls(dp1, dp2, dp3);
        testPane.addControlsToEventHandler(dp1, dp2, dp3);
        return testPane;
    }
    /**
     * sets the {@link DatePicker} to the format of {@link DateConverter#getPattern()}
     * @param datePicker the {@link DatePicker}
     */
    private void setDatePattern(DatePicker datePicker) {
        datePicker.setPromptText(DateConverter.getPattern());
        datePicker.setConverter(new DateConverter());
    }

}
