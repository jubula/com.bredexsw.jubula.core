package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Test page for Dialogs introduced with Java 8u40.
 *
 */
public class DialogTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_dialogs");
        testPane.setRootId(JavaFXComponentNameConstants.TESTPAGE_TITLE_DIALOGS);
        
        // button 1
        Button btn1 = new Button(I18NUtils.getName("btn1"));
        btn1.setId(JavaFXComponentNameConstants.TESTPAGE_DIALOGS_BTN01);
        btn1.setMaxWidth(Double.MAX_VALUE);
        btn1.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                Alert a = new Alert(AlertType.INFORMATION);
                a.setTitle(a.getClass().getName());
                a.show();
            }
        });
        
        // button 2
        Button btn2 = new Button(I18NUtils.getName("btn2"));
        btn2.setId(JavaFXComponentNameConstants.TESTPAGE_DIALOGS_BTN02);
        btn2.setMaxWidth(Double.MAX_VALUE);
        btn2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                ChoiceDialog<String> choice = new ChoiceDialog<String>(
                        I18NUtils.getName("val1"), I18NUtils.getName("val2"));
                choice.setTitle(choice.getClass().getName());
                choice.showAndWait().orElse(I18NUtils.getName("val3"));
            }
        });

        // button 2
        Button btn3 = new Button(I18NUtils.getName("btn3"));
        btn3.setId(JavaFXComponentNameConstants.TESTPAGE_DIALOGS_BTN03);
        btn3.setMaxWidth(Double.MAX_VALUE);
        btn3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                TextInputDialog tPutLog = new TextInputDialog(I18NUtils
                        .getName("val1"));
                tPutLog.initModality(Modality.NONE);
                tPutLog.setTitle(tPutLog.getClass().getName());
                tPutLog.show();
            }
        });

        testPane.addControls(btn1, btn2, btn3);
        return testPane;
    }

}
