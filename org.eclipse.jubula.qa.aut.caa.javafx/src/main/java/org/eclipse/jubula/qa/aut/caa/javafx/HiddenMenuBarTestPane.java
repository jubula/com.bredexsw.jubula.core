/*******************************************************************************
 * Copyright (c) 2018 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.jubula.qa.aut.caa.javafx;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;

/**
 * @author BREDEX GmbH
 *
 */
public class HiddenMenuBarTestPane extends AbstractSceneSetter {

    /** {@inheritDoc} */
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_menus");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_MENUS);
        

        MenuBar menuBar = new MenuBar();
        menuBar.setId(JavaFXComponentNameConstants.TESTPAGE_MENUS_MENUBAR);
        
        
        createMenu(tPane, menuBar);
        tPane.addMenuBar(menuBar);
        return tPane;
    }

    /**
     * @param tPane {@link TestPane}
     * @param menuBar the menu bar
     */
    private void createMenu(TestPane tPane, MenuBar menuBar) {
        
        Menu menu = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu"));
        Menu menu2 = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu2"));
        Menu menu3 = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu3"));
        Menu menu4 = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu4"));
        Menu menu5 = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu5"));
        Menu menu6 = MenuBarTestPane.createMenu(tPane,
                I18NUtils.getString("menu6"));
        menuBar.getMenus().addAll(menu, menu2, menu3, menu4, menu5,
                menu6);
        menu2.setVisible(false);
        menu3.setVisible(false);
        menu5.setVisible(false);
        
    }

}
