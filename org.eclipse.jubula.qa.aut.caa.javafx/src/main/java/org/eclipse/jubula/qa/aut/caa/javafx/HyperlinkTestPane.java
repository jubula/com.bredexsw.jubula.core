package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.Hyperlink;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Initializes Hyperlinks for testing.
 */
public class HyperlinkTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_hyperlink");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_HYPERLINK);

        // Hyperlink 1
        Hyperlink h1  = new Hyperlink(I18NUtils.getName("btn1"));
        h1.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        h1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        h1.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(h1);

        // Hyperlink 2
        Hyperlink h2 = new Hyperlink(I18NUtils.getName("btn2"));
        h2.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        h2.setDisable(true);
        h2.setMaxWidth(Double.MAX_VALUE);
        
        testPane.addControls(h1, h2);
        testPane.addControlsToEventHandler(h1, h2);
        
        return testPane;
    }
}
