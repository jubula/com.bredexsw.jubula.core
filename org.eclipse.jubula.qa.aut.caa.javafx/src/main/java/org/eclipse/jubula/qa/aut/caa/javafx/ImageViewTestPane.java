package org.eclipse.jubula.qa.aut.caa.javafx;

import java.net.URL;
import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Initializes ImageViews for testing.
 */
public class ImageViewTestPane extends AbstractSceneSetter {
    /** The testPane */
    private TestPane m_testPane;

    @Override
    protected TestPane getTestScene() {
        m_testPane = new TestPane("title_imageViews");
        m_testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_IMAGEVIEWS);

        URL url1 = this.getClass().getResource("/images/jubula1.png");
        final ImageView imageView1 = new ImageView(url1.toString());
        //double radius1 = imageView1.getImage().getHeight() * 0.5;
//        Circle shape1 = new Circle(radius1);
//        shape1.setCenterX(radius1);
//        shape1.setCenterY(radius1);
//        imageView1.setClip(shape1);
        imageView1.setId(ComponentNameConstants.TESTPAGE_IMAGEVIEWS_IMAGEVIEW1);
        imageView1.setFocusTraversable(true);
        List<MenuItem> items = MenuBarTestPane.createContextMenu(m_testPane);
        MenuItem[] ar = new MenuItem[items.size()];
        items.toArray(ar);
        ContextMenu cm = new ContextMenu(ar);
        imageView1.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        if (event.getButton() == MouseButton.SECONDARY) {
                            cm.hide();
                            cm.show(imageView1, event.getScreenX(),
                                    event.getScreenY());
                        }
                    }
                });

        m_testPane.addNode(imageView1);
        m_testPane.addNodesToEventHandler(imageView1);

        URL url2 = this.getClass().getResource("/images/jubula2.png");
        ImageView imageView2 = new ImageView(url2.toString());
        double radius2 = imageView2.getImage().getHeight() * 0.5;
        Circle shape2 = new Circle(radius2);
        shape2.setCenterX(radius2);
        shape2.setCenterY(radius2);
        imageView2.setClip(shape2);
        imageView2.setId(ComponentNameConstants.TESTPAGE_IMAGEVIEWS_IMAGEVIEW2);
        imageView2.setFocusTraversable(true);

        m_testPane.addNode(imageView2);
        m_testPane.addNodesToEventHandler(imageView2);

        URL url3 = this.getClass().getResource("/images/jubula3.png");
        ImageView imageView3 = new ImageView(url3.toString());
        double radius3 = imageView3.getImage().getHeight() * 0.5;
        Circle shape3 = new Circle(radius3);
        shape3.setCenterX(radius3);
        shape3.setCenterY(radius3);
        imageView3.setClip(shape3);
        imageView3.setId(ComponentNameConstants.TESTPAGE_IMAGEVIEWS_IMAGEVIEW3);
        imageView3.setDisable(true);
        imageView3.setFocusTraversable(true);

        m_testPane.addNode(imageView3);
        m_testPane.addNodesToEventHandler(imageView3);

        return m_testPane;
    }

}