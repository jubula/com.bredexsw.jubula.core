package org.eclipse.jubula.qa.aut.caa.javafx;


import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Initializes Labels for testing.
 */
public class LabelTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_labels");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_LABELS);
     // label 1
        Label lbl1 = new Label(I18NUtils.getName("lbl1"));
        lbl1.setId(ComponentNameConstants.TESTPAGE_LABELS_LBL01);
        lbl1.setTooltip(new Tooltip(I18NUtils.getString("tooltip"))); //$NON-NLS-1$
        tPane.addContextMenuToNode(lbl1);

        // label 2
        Label lbl2 = new Label(I18NUtils.getName("lbl2")); //$NON-NLS-1$
        lbl2.setId(ComponentNameConstants.TESTPAGE_LABELS_LBL02);
        lbl2.setDisable(true);

        // label 3
        final Label lbl3 = new Label(I18NUtils.getName("lbl3")); //$NON-NLS-1$
        lbl3.setId(ComponentNameConstants.TESTPAGE_LABELS_LBL03);
        
        EventHandler<MouseEvent> mhand = new EventHandler<MouseEvent>() {
            
            
            public void handle(MouseEvent e) {
                int clickCount = e.getClickCount();
                switch (clickCount) {
                    case 1:
                        lbl3.setText(I18NUtils.getName("oneClick"));
                        break;
                    case 2:
                        lbl3.setText(I18NUtils.getName("twoClick"));
                        break;
                    default:
                        lbl3.setText(e.getClickCount() + " Click");
                }

            }
        };
        lbl3.setOnMouseClicked(mhand);
        
        tPane.addControls(lbl1, lbl2, lbl3);
        return tPane;
    }

}
