package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.ToggleGroup;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes one MenuBar with menus for testing.
 */
public class MenuBarTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_menus");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_MENUS);
        // add components to test
        MenuBar menuBar = new MenuBar();
        menuBar.setId(JavaFXComponentNameConstants.TESTPAGE_MENUS_MENUBAR);

        Menu menu01 = createMenu(tPane, I18NUtils.getString("menu"));
        menu01.setId(JavaFXComponentNameConstants.TESTPAGE_MENUS_MENU01);

        menuBar.getMenus().add(menu01);

        Menu menu02 = createMenu(tPane, I18NUtils.getString("menu2"));
        menu02.setId(JavaFXComponentNameConstants.TESTPAGE_MENUS_MENU02);
        menuBar.getMenus().add(menu02);

        tPane.addMenuBar(menuBar);
        return tPane;
    }

    /**
     * Creates a menu, which can be added to a MenuBar
     * 
     * @param tPane
     *            necessary to set the Text
     * @param menuTitle
     *            the title for the menu
     * @return the menu with all items and handlers necessary for testing
     */
    static Menu createMenu(final TestPane tPane, String menuTitle) {
        // Main menu item 1
        MenuItem item1 = new MenuItem(I18NUtils.getString("menuitem_first"));
        // Main menu Item not visible
        MenuItem itemNotVisible = new MenuItem(
                I18NUtils.getString("menuitem_nonvisible"));
        itemNotVisible.setVisible(false);
        // First submenu
        Menu submenu = new Menu(I18NUtils.getString("menu_more"));
        // Second MenuItem
        MenuItem item2 = new MenuItem(I18NUtils.getString("menuitem_second"));
        // CheckBox menu Item
        CheckMenuItem cbxItem = new CheckMenuItem(
                I18NUtils.getString("menuitem_cbx"));
        // Radio menu item
        ToggleGroup tGroup = new ToggleGroup();
        RadioMenuItem rbxItem = new RadioMenuItem(
                I18NUtils.getString("menuitem_rbx"));
        rbxItem.setToggleGroup(tGroup);
        // Disabled menu item
        MenuItem item3 = new MenuItem(I18NUtils.getString("menuitem_third"));
        item3.setDisable(true);
        // Add items to first submenu
        submenu.getItems().addAll(item2, cbxItem, rbxItem,
                new SeparatorMenuItem(), item3);
        // Second submenu
        Menu submenu2 = new Menu(I18NUtils.getString("menu_more"));
        submenu.getItems().add(submenu2);
        // Menu item level 3
        MenuItem itemlvl3 = new MenuItem(I18NUtils.
                getString("menuitem_level3"));
        // Add items to second submenu
        submenu2.getItems().add(itemlvl3);
        // Third submenu
        Menu submenu3 = new Menu(I18NUtils.getString("menu_more"));
        submenu2.getItems().add(submenu3);
        // Menu item level 4
        MenuItem itemlvl4 = new MenuItem(I18NUtils.
                getString("menuitem_level4"));
        // Add items to third submenu
        submenu3.getItems().add(itemlvl4);
        // Add everything to main menu
        Menu mainMenu = new Menu(menuTitle);
        mainMenu.getItems().addAll(item1, itemNotVisible, submenu);
        ArrayList<MenuItem> allItems = new ArrayList<MenuItem>();
        allItems.addAll(mainMenu.getItems());
        allItems.addAll(submenu.getItems());
        allItems.addAll(submenu2.getItems());
        allItems.addAll(submenu3.getItems());
        for (MenuItem menuItem : allItems) {
            menuItem.addEventHandler(ActionEvent.ACTION,
                    new EventHandler<ActionEvent>() {

                        public void handle(ActionEvent event) {
                            MenuItem mItem = ((MenuItem) event.getSource());
                            tPane.setActionText(mItem.getText());
                        }
                    });
        }
        return mainMenu;
    }
    
    /**
     * Creates a a item list for a ContextMenu.
     * 
     * @param tPane
     *            necessary to set the Text
     * @return a list with MenuItems
     */
    static List<MenuItem> createContextMenu(final TestPane tPane) {
        // Main menu item 1
        MenuItem item1 = new MenuItem(I18NUtils.getString("menuitem_first"));
        // Main menu Item not visible
        MenuItem itemNotVisible = new MenuItem(
                I18NUtils.getString("menuitem_nonvisible"));
        itemNotVisible.setVisible(false);
        // First submenu
        Menu submenu = new Menu(I18NUtils.getString("menu_more"));
        // Second MenuItem
        MenuItem item2 = new MenuItem(I18NUtils.getString("menuitem_second"));
        // CheckBox menu Item
        CheckMenuItem cbxItem = new CheckMenuItem(
                I18NUtils.getString("menuitem_cbx"));
        // Radio menu item
        ToggleGroup tGroup = new ToggleGroup();
        RadioMenuItem rbxItem = new RadioMenuItem(
                I18NUtils.getString("menuitem_rbx"));
        rbxItem.setToggleGroup(tGroup);
        // Disabled menu item
        MenuItem item3 = new MenuItem(I18NUtils.getString("menuitem_third"));
        item3.setDisable(true);
        // Add items to first submenu
        submenu.getItems().addAll(item2, cbxItem, rbxItem,
                new SeparatorMenuItem(), item3);
        // Second submenu
        Menu submenu2 = new Menu(I18NUtils.getString("menu_more"));
        submenu.getItems().add(submenu2);
        // Menu item level 3
        MenuItem itemlvl3 = new MenuItem(I18NUtils.
                getString("menuitem_level3"));
        // Add items to second submenu
        submenu2.getItems().add(itemlvl3);
        // Third submenu
        Menu submenu3 = new Menu(I18NUtils.getString("menu_more"));
        submenu2.getItems().add(submenu3);
        // Menu item level 4
        MenuItem itemlvl4 = new MenuItem(I18NUtils.
                getString("menuitem_level4"));
        // Add items to third submenu
        submenu3.getItems().add(itemlvl4);
        List<MenuItem> rootItems = new ArrayList<MenuItem>();
        rootItems.add(item1);
        rootItems.add(itemNotVisible);
        rootItems.add(submenu);
        ArrayList<MenuItem> allItems = new ArrayList<MenuItem>();
        allItems.addAll(rootItems);
        allItems.addAll(submenu.getItems());
        allItems.addAll(submenu2.getItems());
        allItems.addAll(submenu3.getItems());
        for (MenuItem menuItem : allItems) {
            menuItem.addEventHandler(ActionEvent.ACTION,
                    new EventHandler<ActionEvent>() {

                        public void handle(ActionEvent event) {
                            MenuItem mItem = ((MenuItem) event.getSource());
                            tPane.setActionText(mItem.getText());
                        }
                    });
        }
        return rootItems;
    }

}
