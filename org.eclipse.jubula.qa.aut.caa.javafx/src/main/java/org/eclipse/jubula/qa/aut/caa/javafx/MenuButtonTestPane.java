package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Initializes MenuButtons for testing.
 */
public class MenuButtonTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_menuButtons");
        testPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_MENUBUTTONS);
        // button 1
        MenuButton btn1 = new MenuButton(I18NUtils.getName("btn1"));
        btn1.getItems().addAll(createMenu(testPane, "MenuButton1"));
        btn1.setId(JavaFXComponentNameConstants.TESTPAGE_MENUBUTTONS_BTN01);
        btn1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn1.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(btn1);
        
        // button 2
        MenuButton btn2 = new MenuButton(I18NUtils.getName("btn2"));
        btn2.getItems().addAll(createMenu(testPane, "MenuButton2"));
        btn2.setId(JavaFXComponentNameConstants.TESTPAGE_MENUBUTTONS_BTN02);
        btn2.setDisable(true);
        btn2.setMaxWidth(Double.MAX_VALUE);

        testPane.addControls(btn1, btn2);
        testPane.addControlsToEventHandler(btn1, btn2);

        return testPane;
    }
    
    /**
     * Creates a menu, which can be added to a MenuBar
     * 
     * @param tPane
     *            necessary to set the Text
     * @param menuTitle
     *            the title for the menu
     * @return the menu with all items and handlers necessary for testing
     */
    private static List<MenuItem> createMenu(final TestPane tPane,
            String menuTitle) {
        // Main menu item 1
        MenuItem item1 = new MenuItem(I18NUtils.getString("menuitem_first"));
        // Main menu Item not visible
        MenuItem itemNotVisible = new MenuItem(
                I18NUtils.getString("menuitem_nonvisible"));
        itemNotVisible.setVisible(false);
        // First submenu
        Menu submenu = new Menu(I18NUtils.getString("menu_more"));
        // Second MenuItem
        MenuItem item2 = new MenuItem(I18NUtils.getString("menuitem_second"));
        // CheckBox menu Item
        CheckMenuItem cbxItem = new CheckMenuItem(
                I18NUtils.getString("menuitem_cbx"));
        // Radio menu item
        ToggleGroup tGroup = new ToggleGroup();
        RadioMenuItem rbxItem = new RadioMenuItem(
                I18NUtils.getString("menuitem_rbx"));
        rbxItem.setToggleGroup(tGroup);
        // Disabled menu item
        MenuItem item3 = new MenuItem(I18NUtils.getString("menuitem_third"));
        item3.setDisable(true);
        // Add items to first submenu
        submenu.getItems().addAll(item2, cbxItem, rbxItem,
                new SeparatorMenuItem(), item3);
        // Second submenu
        Menu submenu2 = new Menu(I18NUtils.getString("menu_more"));
        submenu.getItems().add(submenu2);
        // Menu item level 3
        MenuItem itemlvl3 = new MenuItem(
                I18NUtils.getString("menuitem_level3"));
        // Add items to second submenu
        submenu2.getItems().add(itemlvl3);
        // Third submenu
        Menu submenu3 = new Menu(I18NUtils.getString("menu_more"));
        submenu2.getItems().add(submenu3);
        // Menu item level 4
        MenuItem itemlvl4 = new MenuItem(
                I18NUtils.getString("menuitem_level4"));
        // Add items to third submenu
        submenu3.getItems().add(itemlvl4);
        // Add everything to main menu
        ArrayList<MenuItem> mItems = new ArrayList<MenuItem>();
        mItems.add(item1);
        mItems.add(itemNotVisible);
        mItems.add(submenu);
        ArrayList<MenuItem> allItems = new ArrayList<MenuItem>();
        allItems.addAll(mItems);
        allItems.addAll(submenu.getItems());
        allItems.addAll(submenu2.getItems());
        allItems.addAll(submenu3.getItems());
        for (MenuItem menuItem : allItems) {
            menuItem.addEventHandler(ActionEvent.ACTION,
                    new EventHandler<ActionEvent>() {

                        public void handle(ActionEvent event) {
                            MenuItem mItem = ((MenuItem) event.getSource());
                            tPane.setActionText(mItem.getText());
                        }
                    });
        }
        return mItems;
    }

}
