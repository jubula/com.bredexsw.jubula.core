package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Because JavaFX supports multiple MenuBars, this is a class that initializes
 * two MenuBars. This Class Extends the Class for one MenuBar testing.
 */
public class MultipleMenuBarTestPane extends MenuBarTestPane {

    @Override
    protected TestPane getTestScene() {

        TestPane tPane = new TestPane("title_multipleMenus");
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_MULTIPLEMENUS);

        MenuBar mb1 = new MenuBar();
        mb1.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR01);
        Menu menu01 = this.createMenu(tPane, I18NUtils.getString("menu"));
        menu01.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR01_MENU01);
        mb1.getMenus().add(menu01);
        Menu menu02 = this.createMenu(tPane, I18NUtils.getString("menu2"));
        menu02.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR01_MENU02);
        mb1.getMenus().add(menu02);
        tPane.addMenuBar(mb1);

        MenuBar mb2 = new MenuBar();
        mb2.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR02);
        Menu menu03 = this.createMenu(tPane, I18NUtils.getString("menu"));
        menu03.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR02_MENU01);
        mb2.getMenus().add(menu03);
        Menu menu04 = this.createMenu(tPane, I18NUtils.getString("menu2"));
        menu04.setId(JavaFXComponentNameConstants.
                TESTPAGE_MULTIPLEMENUS_MENUBAR02_MENU02);
        mb2.getMenus().add(menu04);
        tPane.addControls(mb2);
   
        return tPane;
    }

}
