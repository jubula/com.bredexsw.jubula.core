/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.javafx;

import java.net.URL;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
/**
 * Test Pane for popup tests
 *
 */
public class PopupTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_popup");
        testPane.setRootId(JavaFXComponentNameConstants.TESTPAGE_TITLE_POPUP);
        
        // button 1
        Button btn1 = new Button(I18NUtils.getName("btn1"));
        btn1.setId(JavaFXComponentNameConstants.TESTPAGE_POPUP_BUTTONS_BTN01);
        btn1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn1.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(btn1);
        btn1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Popup p1 = new Popup();                
                // button 2
                Button b2 = new Button(I18NUtils.getName("btn2"));
                b2.setId(JavaFXComponentNameConstants.
                        TESTPAGE_POPUP_BUTTONS_BTN02);
                b2.setMaxWidth(Double.MAX_VALUE);
                
                
                p1.getContent().add(b2);
                b2.setOnAction(new EventHandler<ActionEvent>() {
                    
                    @Override
                    public void handle(ActionEvent event) {
                        p1.hide();
                    }
                });
                double sceneWidth = btn1.getScene().getWidth();
                double sceneHeight = btn1.getScene().getHeight();
                p1.show(btn1, sceneWidth / 2 * Math.random(),
                        sceneHeight / 2 * Math.random()
                                + btn1.getBoundsInParent().getMaxY() + 10);
            }
        });

        // button 3
        Button b3 = new Button(I18NUtils.getName("btn3"));
        b3.setId(JavaFXComponentNameConstants.
                TESTPAGE_POPUP_BUTTONS_BTN03);
        b3.setMaxWidth(Double.MAX_VALUE);
        b3.setAlignment(Pos.CENTER_LEFT);

        //Image View for second popup
        URL url1 = this.getClass().getResource("/images/jubula1.png");
        final ImageView imageView1 = new ImageView(url1.toString());
        imageView1.setId(JavaFXComponentNameConstants.
                TESTPAGE_POPUP_IMAGEVIEW01);
        imageView1.setFocusTraversable(true);
        b3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Popup p1 = new Popup();                
                p1.getContent().add(imageView1);
                imageView1.setOnMouseClicked(new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        p1.hide();
                    }
                });
                p1.show(b3.getScene().getWindow());
            }
        });
        testPane.addControls(btn1, b3);
        testPane.addControlsToEventHandler(btn1, b3);
        

        return testPane;
    }

}
