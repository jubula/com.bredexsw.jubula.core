package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Initializes RadioButtons for testing.
 */
public class RadioButtonTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_radioButtons");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_RADIOBUTTONS);

        ToggleGroup toggleGroup = new ToggleGroup();
        
        // button 1
        RadioButton rbtn1 = new RadioButton(I18NUtils.getName("rbtn1")); 
        rbtn1.setId(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN01);
        rbtn1.setSelected(false);
        rbtn1.setTooltip(new Tooltip(I18NUtils.getString("tooltip")));
        rbtn1.setToggleGroup(toggleGroup);
        tPane.addContextMenuToNode(rbtn1);

        // button 2
        RadioButton rbtn2 = new RadioButton(I18NUtils.getName("rbtn2"));
        rbtn2.setId(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN02);
        rbtn2.setSelected(true);
        rbtn2.setToggleGroup(toggleGroup);
        
        // button 3
        RadioButton rbtn3 = new RadioButton(I18NUtils.getName("rbtn3")); 
        rbtn3.setId(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN03);
        rbtn3.setDisable(true);
        rbtn3.setToggleGroup(toggleGroup);
        
        tPane.addControls(rbtn1, rbtn2, rbtn3);
        tPane.addControlsToEventHandler(rbtn1, rbtn2, rbtn3);
        
        return tPane;
    }

}
