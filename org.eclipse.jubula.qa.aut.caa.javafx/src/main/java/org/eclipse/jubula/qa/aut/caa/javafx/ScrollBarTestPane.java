package org.eclipse.jubula.qa.aut.caa.javafx;

import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

import javafx.geometry.Orientation;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Tooltip;
/**
 * Initializes ScrollBars for testing.
 */
public class ScrollBarTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_scrollBars");
        tPane.setRootId(JavaFXComponentNameConstants.TESTPAGE_TITLE_SCROLLBARS);
        // ScrollBar1
        ScrollBar sB1 = new ScrollBar();
        sB1.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLBARS_SCRBAR01);

        // ScrollBar2
        ScrollBar sB2 = new ScrollBar();
        sB2.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLBARS_SCRBAR02);
        sB2.setOrientation(Orientation.VERTICAL);

        // ScrollBar3
        ScrollBar sB3 = new ScrollBar();
        sB3.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLBARS_SCRBAR03);
        sB3.setOrientation(Orientation.VERTICAL);
        sB3.setDisable(true);

        // ScrollBar4
        ScrollBar sB4 = new ScrollBar();
        sB4.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLBARS_SCRBAR04);
        sB4.setTooltip(new Tooltip("Tooltip"));
        
     // ScrollBar5
        ScrollBar sB5 = new ScrollBar();
        sB5.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLBARS_SCRBAR05);
        sB5.setMin(sB5.getMax());

        tPane.addControls(sB1, sB2, sB3, sB4, sB5);

        return tPane;
    }

}
