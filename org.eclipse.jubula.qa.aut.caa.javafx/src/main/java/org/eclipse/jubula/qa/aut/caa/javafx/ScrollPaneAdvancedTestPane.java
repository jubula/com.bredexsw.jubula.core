package org.eclipse.jubula.qa.aut.caa.javafx;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;

import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes ScrollPanes for testing.
 */
public class ScrollPaneAdvancedTestPane extends AbstractSceneSetter {
    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_scrolledComp");
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_SCROLLPANES);

        try {
            ScrollPane pane = (ScrollPane) FXMLLoader.load(this.getClass()
                .getResource("/fxml/advancedScrollPane.fxml"));
            tPane.addNode(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tPane;
    }
}