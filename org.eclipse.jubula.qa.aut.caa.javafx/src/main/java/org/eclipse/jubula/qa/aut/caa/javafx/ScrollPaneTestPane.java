package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes ScrollPanes for testing.
 */
public class ScrollPaneTestPane extends AbstractSceneSetter {

    /** TextLog for actions on Components */
    private TextArea m_actionLog;

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_scrolledComp");
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_SCROLLPANES);

        m_actionLog = new TextArea();
        // ScrollPane1
        ScrollPane sP1 = new ScrollPane();
        sP1.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLPANES_SCRP01);
        sP1.setMaxSize(100, 100);

        // ScrollPane2
        ScrollPane sP2 = new ScrollPane();
        sP2.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLPANES_SCRP02);
        sP2.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        sP2.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        sP2.setMaxSize(100, 100);

        // ScrollPane3
        ScrollPane sP3 = new ScrollPane();
        sP3.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLPANES_SCRP03);
        sP3.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        sP3.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        sP3.setMaxSize(100, 100);
        sP3.setDisable(true);

        // ScrollPane4
        ScrollPane sP4 = new ScrollPane();
        sP4.setId(JavaFXComponentNameConstants.TESTPAGE_SCROLLPANES_SCRP04);
        sP4.setMaxSize(100, 50);

        createScrollPane(sP1, sP2, sP3, sP4);

        tPane.addControls(sP1, sP2, sP3, sP4, m_actionLog);

        return tPane;
    }

    /**
     * Adds components to the given panes, so that scrolling makes sense.
     * 
     * @param panes
     *            panes on which created panes will be added
     */
    private void createScrollPane(ScrollPane... panes) {
        EventHandler<MouseEvent> mHandler = new EventHandler<MouseEvent>() {

            
            public void handle(MouseEvent event) {

                m_actionLog.appendText(event.getSource().toString() + "\n");

            }

        };

        EventHandler<ActionEvent> aHandler = new EventHandler<ActionEvent>() {

            
            public void handle(ActionEvent event) {
                m_actionLog.appendText(event.getTarget().toString() + "\n");
            }

        };
        int i = 0;
        for (ScrollPane scrollPane : panes) {
            Button b = new Button("Button" + i);
            b.setId("Button" + i);
            b.setOnAction(aHandler);

            TextField tField = new TextField();
            tField.setId("TextField" + i);
            tField.setOnMouseClicked(mHandler);

            BorderPane bPane = new BorderPane();
            bPane.setCenter(b);
            bPane.setBottom(tField);
            scrollPane.setContent(bPane);
            i++;
        }
    }

}
