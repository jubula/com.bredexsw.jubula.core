package org.eclipse.jubula.qa.aut.caa.javafx;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * Initializes Shapes for testing.
 */
public class ShapeTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_shapes");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_SHAPES);
        
        HBox container = new HBox();
        
        // Rectangle 1
        Rectangle r1 = new Rectangle(400, 300, Paint.valueOf("#ddddff"));
        r1.setId(ComponentNameConstants.TESTPAGE_SHAPES_RECTANGLE01);
        Util.addDnDHandler(r1, testPane);
        
        // Rectangle 2
        Rectangle r2 = new Rectangle(400, 300, Paint.valueOf("#dddddd"));
        r2.setId(ComponentNameConstants.TESTPAGE_SHAPES_RECTANGLE02);
        r2.setDisable(true);
        Util.addDnDHandler(r2, testPane);

        // Circle 1
        Circle c1 = new Circle(20, Paint.valueOf("#4444aa"));
        c1.setId(ComponentNameConstants.TESTPAGE_SHAPES_CIRCLE01);
        Util.addDnDHandler(c1, testPane);
        
        // Circle 2
        Circle c2 = new Circle(20, Paint.valueOf("#444444"));
        c2.setId(ComponentNameConstants.TESTPAGE_SHAPES_CIRCLE02);
        c2.setDisable(true);
        Util.addDnDHandler(c2, testPane);
        
        container.getChildren().addAll(r1, r2, c1, c2);
        container.setSpacing(30);
        
        testPane.addNode(container);
        testPane.addNodesToEventHandler(r1, r2, c1, c2);
        return testPane;
    }

}
