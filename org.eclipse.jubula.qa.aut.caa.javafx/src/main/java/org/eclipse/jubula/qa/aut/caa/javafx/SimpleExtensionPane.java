package org.eclipse.jubula.qa.aut.caa.javafx;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.javafx.components.MyControl;

/**
 * Creates own components for use in CaA tests for simple extensions.
 * 
 */
public class SimpleExtensionPane extends AbstractSceneSetter {

    /** pane */
    private TestPane m_testpane;
    
    @Override
    protected TestPane getTestScene() {
        m_testpane = new TestPane("title_simpleExtension");
        m_testpane.setRootId(
                ComponentNameConstants.TESTPAGE_TITLE_SIMPLE_EXTENSION);

        MyControl myControl = new MyControl();
        myControl.setId(ComponentNameConstants.TESTPAGE_SIMPLEEXT_C01);
        m_testpane.addControls(myControl);
        m_testpane.addControlsToEventHandler(myControl);
        return m_testpane;
    }
}
