package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;
import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Creates Sliders for use in CaA tests.
 * 
 */
public class SliderTestPane extends AbstractSceneSetter {

    /** pane */
    private TestPane m_testpane;
    
    @Override
    protected TestPane getTestScene() {
        m_testpane = new TestPane("title_slider");
        m_testpane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_SLIDER);

        addSlider1(false);
        addSlider1(true);
        
        addSlider2();

        addSlider3();
        
        addSlider4();
        
        return m_testpane;
    }

    /** 
     * slider 1
     * @param disabled whether to disable slider
     */
    private void addSlider1(boolean disabled) {
        Slider slider = createHSliderBox(0.0, 130.0, 0.0, null);
        slider.setId(ComponentNameConstants.TESTPAGE_SLIDER_SL01
                + "_disable_" + disabled);
        slider.setMajorTickUnit(10f);
        slider.setBlockIncrement(20f);
        slider.setSnapToTicks(false);
        slider.setDisable(disabled);
        setTooltip(slider);
    }

    /** slider 2 */
    private void addSlider2() {
        Slider slider = createHSliderBox(0.0, 130.0, 0.0, null);
        slider.setId(ComponentNameConstants.TESTPAGE_SLIDER_SL02);
        slider.setMajorTickUnit(10f);
        slider.setBlockIncrement(20f);
        slider.setMinorTickCount(1);
        slider.setSnapToTicks(true);
        setTooltip(slider);
    }

    /** slider 3 */
    private void addSlider3() {
        Slider slider = createHSliderBox(0.0, 3.0, 0.0,
                new StringConverter<Double>() {
            
                private BidiMap<Double, String> m_map =
                        new TreeBidiMap<Double, String>();
                {
                    m_map.put(0.0, "Green");
                    m_map.put(1.0, "White");
                    m_map.put(2.0, "Orange");
                    m_map.put(3.0, "Black");
                }
                
                @Override
                public String toString(Double d) {
                    return m_map.get(d);
                }
                
                @Override
                public Double fromString(String string) {
                    return m_map.inverseBidiMap().get(string);
                }
            });
        slider.setId(ComponentNameConstants.TESTPAGE_SLIDER_SL03);
        slider.setMajorTickUnit(1f);
        slider.setBlockIncrement(1f);
        slider.setSnapToTicks(true);
        slider.setMinorTickCount(0);
        setTooltip(slider);
    }

    /** slider 4 */
    private void addSlider4() {
        Slider slider = createVSliderBox(0.0, 130.0, 0.0);
        slider.setId(ComponentNameConstants.TESTPAGE_SLIDER_SL04);
        slider.setMajorTickUnit(10f);
        slider.setBlockIncrement(20f);
        slider.setSnapToTicks(false);
        setTooltip(slider);
    }

    /**
     * sets the tooltip of a slider
     * @param slider the slider
     */
    private void setTooltip(Slider slider) {
        slider.setTooltip(new Tooltip(
                "BlockIncrement: " + slider.getBlockIncrement() + "\n"
                + "SnapToTicks: " + slider.isSnapToTicks()));
    }

    /**
     * Creates a horizontal slider with surrounding box
     * @param min min value
     * @param max max value
     * @param value initial value
     * @param formatter label formatter
     * @return the slider
     */
    private Slider createHSliderBox(double min,
            double max, double value, StringConverter<Double> formatter) {
        Label l = new Label("Value:");
        Slider slider = createSlider(min, max, value, l);
        slider.setPrefWidth(400);
        slider.setLabelFormatter(formatter);
        setTextOfLabel(l, slider);
        VBox hbox = new VBox();
        hbox.getChildren().addAll(l, slider);
        m_testpane.addNode(hbox);
        return slider;
    }

    /**
     * Creates a vertical slider with surrounding box
     * @param min min value
     * @param max max value
     * @param value initial value
     * @return the slider
     */
    private Slider createVSliderBox(double min,
            double max, double value) {
        Label l = new Label("Value:");
        Slider slider = createSlider(min, max, value, l);
        slider.setOrientation(Orientation.VERTICAL);
        slider.setPrefHeight(400);
        HBox hbox = new HBox();
        hbox.getChildren().addAll(slider, l);
        m_testpane.addNode(hbox);
        return slider;
    }

    /**
     * @param min min value
     * @param max max value
     * @param value initial value
     * @param l label to reflect value
     * @return the slider
     */
    private Slider createSlider(double min, double max, double value, Label l) {
        Slider slider = new Slider(min, max, value);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.valueProperty().addListener(
                (ChangeListener<Number>) (observable, oldValue, newValue) -> {
                setTextOfLabel(l, slider);
            });
        setTextOfLabel(l, slider);
        return slider;
    }

    /**
     * sets value of slider as text of label
     * @param l label
     * @param slider slider
     */
    private void setTextOfLabel(Label l, Slider slider) {
        StringConverter<Double> labelFormatter =
                slider.getLabelFormatter();
        if (labelFormatter == null) {
            l.setText("Value: " + slider.getValue());
        } else {
            l.setText("Value: " + labelFormatter.toString(
                    slider.getValue()));
        }
    }
}
