package org.eclipse.jubula.qa.aut.caa.javafx;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Initializes the first Scene with the Buttons for starting various test panes
 */
public class StartScene {
    
    /** Helper for design */
    private static final int NUMBER_OF_COLUMNS = 2;
    /** Instance **/
    private static StartScene instance;
    
    /** Helper for design */
    private static int column;
    /** Helper for design */
    private static int row;

    /** The title for the scene */
    private String m_sceneTitle;

    /** Pane for the buttons */
    private GridPane m_gridPane;
    
    /**The primary Stage **/
    private Stage m_primaryStage;

    /**
     * Constructor
     * @param applicationClass 
     */
    private StartScene(ApplicationClass applicationClass) {
        m_primaryStage = applicationClass.getPrimaryStage();
        m_sceneTitle = I18NUtils.getString("title_javaFXAUT");
        init();
    }
    /**
     * Creates a new Instance. 
     * @param applicationClass the class to get the primary Stage from
     */
    public static void createInstance(ApplicationClass applicationClass) {
        instance = new StartScene(applicationClass);
    }
    
    /**
     * Returns the instance
     * @return the instance
     */
    public static StartScene getInstance() {
        return instance;
    }
    
    /**
     * Changes the scene on the primary Stage to the start scene
     */
    public void showStartScene() {
        init();
    }

    /**
     * Initializes the start scene
     */
    private void init() {        
        column = 0;
        row = 0;
        BorderPane borderPane = new BorderPane();
        m_gridPane = new GridPane();
        m_gridPane.setPadding(new Insets(10, 10, 10, 10));
        m_gridPane.setHgap(10);
        m_gridPane.setVgap(10);
        m_gridPane.setAlignment(Pos.CENTER);
        addTestPagesToMain();
        borderPane.setCenter(m_gridPane);
        BorderPane.setAlignment(m_gridPane, Pos.CENTER);
        Scene startScene = new Scene(borderPane);
        setScene(startScene, m_sceneTitle);
    }
    
    /**
     * adds the buttons for navigation to the test pages
     */
    private void addTestPagesToMain() {
        addPagesFromAtoL();
        addPagesFromMtoZ();
    }
    
    /**
     * add pages from a to l
     */
    private void addPagesFromAtoL() {
        addControlElement(new AccordionTestPane(), "title_accordions",
                JavaFXComponentNameConstants.MAINPAGE_ACCORDIONS_TESTPAGE);
        addControlElement(new ApplicationTestPane(), "title_application",
                JavaFXComponentNameConstants.MAINPAGE_APPLICATIONS_TESTPAGE);
        addControlElement(new ButtonTestPane(), "title_buttons",
                ComponentNameConstants.MAINPAGE_BUTTONS_TESTPAGE);
        addControlElement(new ChartTestPane(), "title_charts",
                JavaFXComponentNameConstants.MAINPAGE_CHARTS_TESTPAGE);
        addControlElement(new CheckBoxTestPane(), "title_checkBoxes",
                ComponentNameConstants.MAINPAGE_CHECKBOXES_TESTPAGE);
        addControlElement(new ChoiceBoxTestPane(), "title_choiceBoxes",
                JavaFXComponentNameConstants.MAINPAGE_CHOICEBOXES_TESTPAGE);
        addControlElement(new ComboBoxTestPane(), "title_comboBoxes",
                JavaFXComponentNameConstants.MAINPAGE_COMBOBOXES_TESTPAGE);
        addControlElement(new ContainerTestPane(), "title_containers",
                JavaFXComponentNameConstants.MAINPAGE_CONTAINERS_TESTPAGE);
        addControlElement(new CustomListViewTestPane(), "title_customLists",
                JavaFXComponentNameConstants.MAINPAGE_CUSTOMLISTS_TESTPAGE);
        addControlElement(new DatePickerTestPane(), "title_datePickers",
                JavaFXComponentNameConstants.MAINPAGE_DATEPICKERS_TESTPAGE);
        addControlElement(new DialogTestPane(), "title_dialogs",
                JavaFXComponentNameConstants.MAINPAGE_DIALOGS_TESTPAGE);
        addControlElement(new HiddenMenuBarTestPane(), "title_hiddenMenus",
                JavaFXComponentNameConstants.MAINPAGE_HIDDENMENUS_TESTPAGE);
        addControlElement(new HyperlinkTestPane(), "title_hyperlink",
                JavaFXComponentNameConstants.MAINPAGE_HYPERLINK_TESTPAGE);
        addControlElement(new ImageViewTestPane(), "title_imageViews",
                JavaFXComponentNameConstants.MAINPAGE_IMAGEVIEW_TESTPAGE);
        addControlElement(new LabelTestPane(), "title_labels",
                JavaFXComponentNameConstants.MAINPAGE_LABELS_TESTPAGE);
        addControlElement(new ListViewTestPane(), "title_lists",
                JavaFXComponentNameConstants.MAINPAGE_LISTVIEW_TESTPAGE);
    }

    
    /**
     * add pages from m to z
     */
    private void addPagesFromMtoZ() {
        addControlElement(new MenuButtonTestPane(), "title_menuButtons",
                JavaFXComponentNameConstants.MAINPAGE_MENUBUTTON_TESTPAGE);
        addControlElement(new MenuBarTestPane(), "title_menus",
                ComponentNameConstants.MAINPAGE_MENU_TESTPAGE);
        addControlElement(new MultipleMenuBarTestPane(), "title_multipleMenus",
                JavaFXComponentNameConstants.
                MAINPAGE_MULTIPLEMENUBARS_TESTPAGE);
        addControlElement(new PaneTestPane(), "title_panes",
                JavaFXComponentNameConstants.MAINPAGE_PANES_TESTPAGE);
        addControlElement(new PasswordFieldTestPane(), "title_passwordField",
                JavaFXComponentNameConstants.MAINPAGE_PASSWORDFIELDS_TESTPAGE);
        addControlElement(new PopupTestPane(),
                "title_popup", JavaFXComponentNameConstants.
                MAINPAGE_POPUP_TESTPAGE);
        addControlElement(new RadioButtonTestPane(), "title_radioButtons",
                ComponentNameConstants.MAINPAGE_RADIOBUTTONS_TESTPAGE);
        addControlElement(new ScrollBarTestPane(), "title_scrollBars",
                JavaFXComponentNameConstants.MAINPAGE_SCROLLBARS_TESTPAGE);
        addControlElement(new ScrollPaneAdvancedTestPane(), 
            "title_scrolledComp", 
            JavaFXComponentNameConstants.MAINPAGE_SCROLLPANES_TESTPAGE);
        addControlElement(new ShapeTestPane(), "title_shapes", 
                JavaFXComponentNameConstants.MAINPAGE_SHAPES_TESTPAGE);
        addControlElement(new SliderTestPane(), "title_slider",
                JavaFXComponentNameConstants.MAINPAGE_SLIDER_TESTPAGE);
        addControlElement(new SimpleExtensionPane(), "title_simpleExtension",
                JavaFXComponentNameConstants
                .MAINPAGE_SIMPLE_EXTENSION_TESTPAGE);
        addControlElement(new StyledContentTestPane(), "title_styledContent",
                JavaFXComponentNameConstants.MAINPAGE_STYLEDCONTENT_TESTPAGE);
        addControlElement(new TabPaneTestPane(), "title_tabbedPanes",
                JavaFXComponentNameConstants.MAINPAGE_TABBEDPANES_TESTPAGE);
        addControlElement(new TableTestPane(), "title_tables",
                JavaFXComponentNameConstants.MAINPAGE_TABLES_TESTPAGE);
        addControlElement(new TextAreaTestPane(), "title_textAreas",
                ComponentNameConstants.MAINPAGE_TEXTAREAS_TESTPAGE);
        addControlElement(new TextFieldTestPane(), "title_textFields",
                ComponentNameConstants.MAINPAGE_TEXTFIELDS_TESTPAGE);
        addControlElement(new TextTestPane(), "title_texts",
                JavaFXComponentNameConstants.MAINPAGE_TEXTS_TESTPAGE);
        addControlElement(new ToggleButtonTestPane(), "title_toggleButtons",
                JavaFXComponentNameConstants.MAINPAGE_TOGGLEBUTTONS_TESTPAGE);
        addControlElement(new TreeTestPane(), "title_trees",
                JavaFXComponentNameConstants.MAINPAGE_TREES_TESTPAGE);
        addControlElement(new TreeTableTestPane(), "title_treeTables",
                JavaFXComponentNameConstants.MAINPAGE_TREETABLES_TESTPAGE);
        addControlElement(new TransformedTestPane(),
                "title_buttons_transformed", JavaFXComponentNameConstants.
                MAINPAGE_TRANSFORMED_BUTTONS_TESTPAGE);
        addControlElement(new TransformedNodesTestPane(),
                "title_widgets_transformed", JavaFXComponentNameConstants.
                MAINPAGE_TRANSFORMED_WIDGETS_TESTPAGE);
    }
    

    /**
     * Sets the given scene on the </code>primaryStage</code>, maximizes the
     * Stage and causes the stage to be displayed.
     * 
     * @param scene
     *            - the scene that will be set on the stage
     * @param title
     *            - the title for the stage
     */
    public void setScene(Scene scene, String title) {
        m_primaryStage.hide();
        m_primaryStage.setMaximized(false);
        m_primaryStage.setScene(scene);
        m_primaryStage.setTitle(title);
        m_primaryStage.setMaximized(true);
        m_primaryStage.show();
        m_primaryStage.toFront();
        m_primaryStage.requestFocus();
    }
   
    /**
     * Returns the primary Stage.
     * 
     * @return the primary Stage.
     */
    public Stage getPrimaryStage() {
        return m_primaryStage;
    }

    /**
     * Adds a button for the given component test class
     * 
     * @param controlE
     *            a component test class
     * @param title
     *            the text for the button
     * @param buttonId
     *            the component id for the button
     */
    private void addControlElement(AbstractSceneSetter controlE, String title,
            String buttonId) {
        Button b = new Button(I18NUtils.getString(title));
        b.setId(buttonId);
        b.setMaxWidth(Double.MAX_VALUE);
        b.setOnAction(controlE);
        m_gridPane.add(b, column, row);

        column = (column + 1) % NUMBER_OF_COLUMNS;
        if (column == 0) {
            row++;
        }
    }

}
