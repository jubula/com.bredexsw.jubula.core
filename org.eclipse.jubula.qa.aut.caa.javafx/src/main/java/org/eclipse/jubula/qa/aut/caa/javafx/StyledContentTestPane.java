package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Initializes Buttons for testing.
 */
public class StyledContentTestPane extends AbstractSceneSetter {
    
    /** combo box for css */
    private ComboBox<String> m_comboBox;
    /** button 1 */
    private Button m_btn1;
    /** button 2 */
    private Button m_btn2;
    /** button 3 */
    private Button m_btn3;
    /** button 4 */
    private Button m_btn4;
    /** button box */
    private VBox m_buttonBox;
    /** button 5 */
    private Button m_btn5;
    /** button 6 */
    private Button m_btn6;
    /** button 7 */
    private Button m_btn7;

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_styledContent");
        testPane.setRootId(
                ComponentNameConstants.TESTPAGE_TITLE_STYLED_CONTENT);
        
        // Combobox for choosing CSS-style
        m_comboBox = new ComboBox<String>();
        m_comboBox.setId(JavaFXComponentNameConstants.TESTPAGE_STYLED_COMBO);
        String[] styles = new String[]{"default", "CSS-1", "CSS-2"};
        m_comboBox.getItems().addAll(styles);
        
        m_comboBox.valueProperty().addListener(new ChangeListener<String>() {
                public void changed(
                        ObservableValue<? extends String> observable,
                        String oldValue, String newValue) {
                    Scene scene = m_comboBox.getScene();
                    ObservableList<String> stylesheets =
                            scene.getStylesheets();
                    stylesheets.clear();
                    if (newValue.equals(styles[1])) {
                        stylesheets.add("/stylesheets/style01.css");
                    } else if (newValue.equals(styles[2])) {
                        stylesheets.add("/stylesheets/style02.css");
                    }
                }
            });
        
        // button 1
        m_btn1 = new Button(I18NUtils.getName("btn1"));
        m_btn1.setId(ComponentNameConstants.TESTPAGE_STYLED_BTN01);
        m_btn1.setMaxWidth(Double.MAX_VALUE);
        m_btn1.getStyleClass().add("button1");
        
        // button 2
        m_btn2 = new Button(I18NUtils.getName("btn2"));
        m_btn2.setId(ComponentNameConstants.TESTPAGE_STYLED_BTN02);
        m_btn2.setDisable(true);
        m_btn2.setMaxWidth(Double.MAX_VALUE);
        
        // button 3
        m_btn3 = new Button(I18NUtils.getName("btn3"));
        m_btn3.setId(ComponentNameConstants.TESTPAGE_STYLED_BTN03);
        m_btn3.setMaxWidth(Double.MAX_VALUE);
        
        // button 4
        m_btn4 = new Button(I18NUtils.getName("btn4"));
        m_btn4.setId(ComponentNameConstants.TESTPAGE_STYLED_BTN04);
        m_btn4.setMaxWidth(Double.MAX_VALUE);
        m_btn4.getStyleClass().add("bordered");
        m_btn4.setStyle("-fx-background-color: #ffffffff;"
                + "-fx-text-fill: #222222ff;");
        
        testPane.addNode(m_comboBox, m_btn1, m_btn2, m_btn3, m_btn4);
        
        initTransparentButtons();

        testPane.addNode(m_buttonBox);
        testPane.addControlsToEventHandler(m_comboBox, m_btn1, m_btn2, m_btn3);

        return testPane;
    }

    /**
     * init transparent buttons
     */
    private void initTransparentButtons() {
        m_buttonBox = new VBox();
        m_buttonBox.setStyle("-fx-background-color: #00ff00;"
                + "-fx-padding: 5 5 5 5;");
        
        m_btn5 = new Button("Transparent Button 1");
        m_btn5.setMaxWidth(Double.MAX_VALUE);
        m_btn5.setStyle("-fx-background-color: #ff0000ff;");
        m_btn6 = new Button("Transparent Button 2");
        m_btn6.setMaxWidth(Double.MAX_VALUE);
        m_btn6.setStyle("-fx-background-color: #ff000088;");
        m_btn7 = new Button("Transparent Button 3");
        m_btn7.setMaxWidth(Double.MAX_VALUE);
        m_btn7.setStyle("-fx-background-color: #ff000033;");
        
        m_buttonBox.getChildren().add(m_btn5);
        m_buttonBox.getChildren().add(m_btn6);
        m_buttonBox.getChildren().add(m_btn7);
    }

}
