package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.geometry.Side;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Creates TabPanes for use in CaA tests.
 * 
 */
public class TabPaneTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane parentPane = new TestPane("title_tabbedPanes");
        parentPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_TABBEDPANES);

        addTabPane1(parentPane);
        
        addTabPane2(parentPane);
        
        addTabPane3(parentPane);

        return parentPane;
    }

    /**
     * Creates a new {@link TabPane} and adds it to the given pane. The created
     * widget has the following notable properties:<ul>
     * <li>context menu</li>
     * <li>tabs on top</li>
     * <li>tabs cannot be closed</li>
     * <li>one disabled tab</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created TabPane should be added.
     */
    private void addTabPane1(TestPane parentPane) {
        TabPane tabPane = new TabPane();
        tabPane.setPrefSize(200, 100);
        tabPane.setId(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP01);
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        parentPane.addContextMenuToNode(tabPane);
        
        Tab item1 = new Tab(I18NUtils.getString("tab1"));
        Label lbl1 = new Label(I18NUtils.getString("img_carrot"));
        item1.setContent(lbl1);
        
        Tab item2 = new Tab(I18NUtils.getString("tab2"));
        Label lbl2 = new Label(I18NUtils.getString("img_banana"));
        item2.setContent(lbl2);
        
        Tab item3 = new Tab(I18NUtils.getString("tab3"));
        item3.setDisable(true);
        
        tabPane.getTabs().addAll(item1, item2, item3);
        parentPane.addControls(tabPane);
    }
    
    /**
     * Creates a new {@link TabPane} and adds it to the given pane. The created
     * widget has the following notable properties:<ul>
     * <li>tabs on left</li>
     * <li>tabs cannot be closed</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created TabPane should be added.
     */
    private void addTabPane2(TestPane parentPane) {
        TabPane tabPane = new TabPane();
        tabPane.setPrefSize(200, 100);
        tabPane.setId(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP02);
        tabPane.setSide(Side.LEFT);
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        
        Tab item1 = new Tab(I18NUtils.getString("tab1"));
        Label lbl1 = new Label(I18NUtils.getString("img_carrot"));
        item1.setContent(lbl1);

        Tab item2 = new Tab(I18NUtils.getString("tab2"));
        Label lbl2 = new Label(I18NUtils.getString("img_banana"));
        item2.setContent(lbl2);
        
        Tab item3 = new Tab(I18NUtils.getString("tab3"));
        Label lbl3 = new Label(I18NUtils.getString("img_kiwi"));
        item3.setContent(lbl3);

        tabPane.getTabs().addAll(item1, item2, item3);
        parentPane.addControls(tabPane);
    }

    /**
     * Creates a new {@link TabPane} and adds it to the given pane. The created
     * widget has the following notable properties:<ul>
     * <li>many tabs</li>
     * <li>tabs on top</li>
     * <li>tabs cannot be closed</li>
     * </ul>
     * 
     * @param parentPane The pane to which the created TabPane should be added.
     */
    private void addTabPane3(TestPane parentPane) {
        TabPane tabPane = new TabPane();
        tabPane.setPrefSize(200, 100);
        tabPane.setId(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP03);
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        for (int i = 0; i < 50; i++) {
            Tab tab = new Tab(String.valueOf(i));
            Label label = new Label(I18NUtils.getString("img_carrot"));
            tab.setContent(label);
            tabPane.getTabs().add(tab);
        }
        
        parentPane.addControls(tabPane);
    }
}
