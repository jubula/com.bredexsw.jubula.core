package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;



/**
 * Creates 4 Tables
 * 
 * Table 1: enabled = true tooltip = true 
 * 
 * Table 2: enabled = true header = false
 * 
 * Table 3: enabled = true header = true editable = true
 * 
 * Table 4: generated table data
 * 
 */
public class TableTestPane extends AbstractSceneSetter {

    /** TableTypes **/
    private static enum TableType {
        /** Table1 **/
        TABLE_1,
        /** Table2 **/
        TABLE_2,
        /** Table3 **/
        TABLE_3;
    }
    
    /** TestPane **/
    private TestPane m_tPane;
    
    /**Context Menu**/
    private ContextMenu m_contextMenu;
    
    @Override
    protected TestPane getTestScene() {
        m_tPane = new TestPane("title_tables");
        m_tPane.setRootId(JavaFXComponentNameConstants.TESTPAGE_TITLE_TABLES);

        TableView<Item> tbl1 = createTable(TableType.TABLE_1);
        
        TableView<Item> tbl2 = createTable(TableType.TABLE_2);
        List<MenuItem> items = MenuBarTestPane.createContextMenu(m_tPane);
        MenuItem[] ar = new MenuItem[items.size()];
        items.toArray(ar);
        m_contextMenu = new ContextMenu(ar);
        tbl2.setContextMenu(m_contextMenu);

        TableView<Item> tbl3 = createTable(TableType.TABLE_3);
        
        TableView<CheckBoxItem> tbl5 = createTable5();
        
        TableView<CheckBoxItem> tbl8 = createTable8();
        
        TableView<CheckBoxItem> tbl9 = createTable9();
        
        TableView<Map> tbl4 = createTable4(500);
        
        TableView<Item> tbl7 = createTable(TableType.TABLE_3);
        initTable7(tbl7);
        
        //Just using a copy of table 7 and modifying the Cells
        TableView<Item> tbl10 = createTable(TableType.TABLE_3);
        initTable7(tbl10);
        configureTable10(tbl10);
        tbl10.setId(JavaFXComponentNameConstants.TESTPAGE_TABLES_TBL10);
        
        tbl1.getSelectionModel().setCellSelectionEnabled(true);
        tbl2.getSelectionModel().setCellSelectionEnabled(true);
        tbl3.getSelectionModel().setCellSelectionEnabled(true);
        tbl4.getSelectionModel().setCellSelectionEnabled(true);
        tbl5.getSelectionModel().setCellSelectionEnabled(true);
        tbl7.getSelectionModel().setCellSelectionEnabled(true);
        //This is how I count
        m_tPane.addControls(tbl1, tbl2, tbl3, tbl4, tbl5, tbl8,
                tbl9, tbl7, tbl10);
        return m_tPane;
    }

    /**
     * modifies the cell factory to represent a different approach of rendering
     * text in cells
     * 
     * @param tbl10 the table
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void configureTable10(TableView<Item> tbl10) {
        for (TableColumn tc : tbl10.getColumns().get(0).getColumns()) {
            tc.setCellFactory(
                    new Callback<TableColumn<Item, String>, 
                    TableCell<Item, String>>() {
                        public TableCell<Item, String> call(
                                TableColumn<Item, String> param) {
                            return new TableCell<Item, String>() {
                                @Override
                                protected void updateItem(String item,
                                        boolean empty) {
                                    Util.addDnDHandler(this, m_tPane);
                                    super.updateItem(item, empty);
                                    Text textShape = new Text(item);
                                    textShape.setFill(Color.RED);
                                    Text textShapeCopy = new Text(
                                            "\n" + item + " COPY") {
                                        private String m_jubTestProp = "test"
                                                + item;
                                    };
                                    TextFlow tflow = new TextFlow(textShape,
                                            textShapeCopy);
                                    this.setGraphic(tflow);
                                    setTooltip(new Tooltip(item));
                                }
                            };
                        }
                    });
        }
    }

    /**
     * initializes and modifies a given Table to represent the functionalities
     * which are associated with a table named 7. On a side note, this class is
     * a nightmare and I have only myself to blame.
     * 
     * @param tbl7 the table
     */
    private void initTable7(TableView<Item> tbl7) {
        tbl7.setId(JavaFXComponentNameConstants.TESTPAGE_TABLES_TBL07);
        
        TableColumn col1 = tbl7.getColumns().remove(0);
        TableColumn col2 = tbl7.getColumns().remove(0);
        TableColumn parentCol = new TableColumn<>("Parent Column");
        parentCol.getColumns().addAll(col1, col2);
        tbl7.getColumns().add(0, parentCol);
        tbl7.setTableMenuButtonVisible(true);
        col1.setSortable(true);
        parentCol.setResizable(false);
        tbl7.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tbl7.setMinSize(400, 300);
    }

    /**
     * Creates Table 5
     * @return Table 5
     */
    private TableView<CheckBoxItem> createTable5() {
        final TableView<CheckBoxItem> tbl5 = new TableView<CheckBoxItem>();
        tbl5.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL05); 
        initTable5(tbl5);
        tbl5.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tbl5.setMaxHeight(85);
        tbl5.setEditable(true);
        return tbl5;
    }

    /**
     * Creates Table 8
     * @return Table 8
     */
    private TableView<CheckBoxItem> createTable8() {
        final TableView<CheckBoxItem> tbl8 = new TableView<CheckBoxItem>();
        tbl8.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL08); 
        initTable8(tbl8);
        tbl8.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tbl8.setMaxHeight(85);
        tbl8.setEditable(true);
        return tbl8;
    }

    /**
     * Creates Table 9
     * @return Table 9
     */
    private TableView<CheckBoxItem> createTable9() {
        final TableView<CheckBoxItem> tbl9 = new TableView<CheckBoxItem>();
        tbl9.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL09); 
        initTable9(tbl9);
        tbl9.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tbl9.setMaxHeight(85);
        tbl9.setEditable(true);
        return tbl9;
    }
    
    /**
     * getHeaderTableData
     * 
     * @return TableData
     */
    private ObservableList<CheckBoxItem> getTable5Data() {
        return FXCollections.observableArrayList(
                (new CheckBoxItem(Boolean.valueOf(false), I18NUtils
                        .getString("val2"), Integer.toString(new Integer(3)),
                        Boolean.toString(new Boolean(true)))),
                new CheckBoxItem(Boolean.valueOf(false), I18NUtils
                        .getString("val5"), Integer.toString(new Integer(6)),
                        Boolean.toString(new Boolean(false))));
    }

    /**
     * initialize the given table in the style of table 5
     * @param tbl the table
     */
    private void initTable5(TableView<CheckBoxItem> tbl) {
        tbl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<CheckBoxItem, Boolean> col1 = 
                new TableColumn<CheckBoxItem, Boolean>(
                I18NUtils.getString("col1"));
        TableColumn<CheckBoxItem, String> col2 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col2"));
        TableColumn<CheckBoxItem, String> col3 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col3"));
        TableColumn<CheckBoxItem, String> col4 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col4"));
        col1.setSortable(false);
        col2.setSortable(false);
        col3.setSortable(false);
        col4.setSortable(false);

        col1.setCellValueFactory(
                new PropertyValueFactory<CheckBoxItem, Boolean>(
                "col1"));
        col2.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col2"));
        col3.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col3"));
        col4.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col4"));
        
        tbl.getItems().addAll(getTable5Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        col1.setCellFactory(CheckBoxTableCell.forTableColumn(col1));
        col1.setMinWidth(100);
        col1.setEditable(true);
        col2.setEditable(false);
        col3.setEditable(false);
        col4.setEditable(false);
    }
    
    /**
     * initialize the given table in the style of table 8
     * @param tbl the table
     */
    private void initTable8(TableView<CheckBoxItem> tbl) {
        tbl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<CheckBoxItem, String> col1 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col1"));
        TableColumn<CheckBoxItem, String> col2 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col2"));
        TableColumn<CheckBoxItem, Boolean> col3 = 
                new TableColumn<CheckBoxItem, Boolean>(
                I18NUtils.getString("col3"));
        TableColumn<CheckBoxItem, String> col4 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col4"));
        col1.setSortable(false);
        col2.setSortable(false);
        col3.setSortable(false);
        col4.setSortable(false);

        col1.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col1"));
        col2.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col2"));
        col3.setCellValueFactory(
                new PropertyValueFactory<CheckBoxItem, Boolean>(
                "col3"));
        col4.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col4"));
        
        tbl.getItems().addAll(getTable5Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        col3.setCellFactory(CheckBoxTableCell.forTableColumn(col3));
        col1.setMinWidth(100);
        col1.setEditable(false);
        col2.setEditable(false);
        col3.setEditable(true);
        col4.setEditable(false);
    }
    
    /**
     * initialize the given table in the style of table 8
     * @param tbl the table
     */
    private void initTable9(TableView<CheckBoxItem> tbl) {
        tbl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<CheckBoxItem, Boolean> col1 = 
                new TableColumn<CheckBoxItem, Boolean>(
                I18NUtils.getString("col1"));
        TableColumn<CheckBoxItem, String> col2 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col2"));
        TableColumn<CheckBoxItem, Boolean> col3 = 
                new TableColumn<CheckBoxItem, Boolean>(
                I18NUtils.getString("col3"));
        TableColumn<CheckBoxItem, String> col4 = 
                new TableColumn<CheckBoxItem, String>(
                I18NUtils.getString("col4"));
        col1.setSortable(false);
        col2.setSortable(false);
        col3.setSortable(false);
        col4.setSortable(false);

        col1.setCellValueFactory(
                new PropertyValueFactory<CheckBoxItem, Boolean>(
                "col1"));
        col2.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col2"));
        col3.setCellValueFactory(
                new PropertyValueFactory<CheckBoxItem, Boolean>(
                "col3"));
        col4.setCellValueFactory(new PropertyValueFactory<CheckBoxItem, String>(
                "col4"));
        
        tbl.getItems().addAll(getTable5Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        col1.setCellFactory(CheckBoxTableCell.forTableColumn(col1));
        col3.setCellFactory(CheckBoxTableCell.forTableColumn(col3));
        col1.setMinWidth(100);
        col1.setEditable(true);
        col2.setEditable(false);
        col3.setEditable(true);
        col4.setEditable(false);
    }

    /**
     * Create a table of the given Type
     * 
     * @param type
     *            the Table-Type
     * @return The Table
     */
    private TableView<Item> createTable(TableType type) {
        final TableView<Item> tbl = new TableView<Item>();
        tbl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<Item, String> col1 = new TableColumn<Item, String>(
                I18NUtils.getString("col1"));
        TableColumn<Item, String> col2 = new TableColumn<Item, String>(
                I18NUtils.getString("col2"));
        TableColumn<Item, String> col3 = new TableColumn<Item, String>(
                I18NUtils.getString("col3"));
        TableColumn<Item, String> col4 = new TableColumn<Item, String>(
                I18NUtils.getString("col4"));
        col1.setSortable(false);
        col2.setSortable(false);
        col3.setSortable(false);
        col4.setSortable(false);
        ArrayList<TableColumn<Item, String>>  columns = 
                new ArrayList<TableColumn<Item, String>>();
        columns.add(col1);
        columns.add(col2);
        columns.add(col3);
        columns.add(col4);
        switch (type) {
            case TABLE_1:
                initTable1(tbl, col1, col2, col3, col4);
                for (TableColumn<Item, String> tableColumn : columns) {
                    tableColumn.setCellFactory(
                            new Callback<TableColumn<Item, String>,
                            TableCell<Item, String>> () {
                            public TableCell<Item, String> call(
                                    TableColumn<Item, String> param) {
                                return new TableCell<Item, String>() {
                                    @Override
                                    protected void updateItem(
                                            String item, boolean empty) {
                                        Util.addDnDHandler(this,
                                                m_tPane);
                                        super.updateItem(item, empty);
                                        setText(item);
                                        setTooltip(new Tooltip(item));
                                    }                                   
                                };
                            }
                        });
                }
                break;
            case TABLE_2:
                initTable2(tbl, col1, col2, col3, col4);
                tbl.getSelectionModel().setSelectionMode(
                        SelectionMode.MULTIPLE);
                col1.setPrefWidth(72);
                col2.setPrefWidth(72);
                col3.setPrefWidth(72);
                col4.setPrefWidth(72);
                tbl.setFixedCellSize(17);
                tbl.setStyle("-fx-font-size: 10px;");
                tbl.setMaxHeight(80);
                tbl.setMaxWidth(320);
                break;
            case TABLE_3:
                initTable3(tbl, col1, col2, col3, col4);
                break;
            default:
                break;
        }
        return tbl;
    }
    /**
     * Create Table 4. Table 4 needs special handling.
     * @param width The width of the table
     * @return the Table
     */
    private TableView<Map> createTable4(double width) {
        final TableView<Map> tbl4 = new TableView<Map>();
        tbl4.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL04); 
        initTable4(tbl4);
        tbl4.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tbl4.setMaxWidth(width);
        return tbl4;
    }
    
    /**
     * Initializes Table 1
     * @param tbl the Table
     * @param col1 Column 1
     * @param col2 Column 2
     * @param col3 Column 3
     * @param col4 Column 4
     */
    @SuppressWarnings("unchecked")
    private void initTable1(TableView<Item> tbl,
            TableColumn<Item, String> col1,
            TableColumn<Item, String> col2,
            TableColumn<Item, String> col3,
            TableColumn<Item, String> col4) {
        //tbl.setTooltip(new Tooltip(I18NUtils.getString("tooltip")));
        tbl.setEditable(false);
        tbl.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL01);

        col1.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col1"));
        col2.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col2"));
        col3.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col3"));
        col4.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col4"));
        tbl.getItems().addAll(getTable1Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        col1.setPrefWidth(72);
        col2.setPrefWidth(72);
        col3.setPrefWidth(72);
        col4.setPrefWidth(72);
        tbl.setStyle("-fx-font-size: 10px;");
        tbl.setMaxHeight(100);
        tbl.setMaxWidth(320);
    }
    
    
    /**
     * Initializes Table 2
     * @param tbl the Table
     * @param col1 Column 1
     * @param col2 Column 2
     * @param col3 Column 3
     * @param col4 Column 4
     */
    private void initTable2(final TableView<Item> tbl,
            TableColumn<Item, String> col1,
            TableColumn<Item, String> col2,
            TableColumn<Item, String> col3,
            TableColumn<Item, String> col4) {
        tbl.setEditable(true);
        Callback<TableColumn<Item, String>, TableCell<Item, String>> call = 
                new Callback<TableColumn<Item, String>,
                TableCell<Item, String>>() {
            public TableCell<Item, String> call(
                    TableColumn<Item, String> param) {
                return new TextFieldTableCell<Item, String>(
                        new DefaultStringConverter()) {
                    @Override
                    public void startEdit() {
                        String cellText = getText();
                        super.startEdit();
                        TextField field = (TextField) getGraphic();
                        field.setContextMenu(m_contextMenu);
                        field.setPromptText(cellText);
                        if (itemProperty().get() != null) {
                            itemProperty().bind(field.textProperty());
                        }
                    }
                    @Override
                    public void cancelEdit() {
                        itemProperty().unbind();
                        super.cancelEdit();
                    }
                    @Override
                    public void commitEdit(String newValue) {
                        itemProperty().unbind();
                        super.commitEdit(newValue);
                    }
                    @Override
                    public void updateItem(String item, boolean empty) {
                        addDragSelectionToCell(this, m_tPane);
                        super.updateItem(item, empty);
                    }
                };
            }
        };
        tbl.getSelectionModel().setCellSelectionEnabled(true);
        col1.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col1"));
        col1.setCellFactory(call);
        col2.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col2"));
        col2.setCellFactory(call);
        col3.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col3"));
        col3.setCellFactory(call);
        col4.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col4"));
        col4.setCellFactory(call);
        tbl.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL02);
        col1.setResizable(false);
        col2.setResizable(false);
        col3.setResizable(false);
        col4.setResizable(false);
        tbl.getColumns().addAll(col1, col2, col3, col4);
        tbl.getItems().addAll(getTable2Data());
        tbl.widthProperty().addListener(new ChangeListener<Number>() {
            public void changed(
                    ObservableValue<? extends Number> observable,
                    Number oldValue, Number newValue) {
                Pane header = (Pane) tbl.lookup("TableHeaderRow");
                if (header != null && header.isVisible()) {
                    header.setMaxHeight(0);
                    header.setMinHeight(0);
                    header.setPrefHeight(0);
                    header.setVisible(false);
                    header.setManaged(false);
                }
            }
        });
    }
    
    /**
     * Adds the D&D handler to a given Cell. 
     * This is a workaround to allow a swing like multiple selection
     * 
     * @param cell
     *            the cell
     * @param tPane
     *            for convenience
     */
    private void addDragSelectionToCell(final TableCell<Item, String> cell,
            final TestPane tPane) {
        final SelectionModel<?> selModel = cell.getTableView().
                getSelectionModel();
        cell.setOnDragDetected(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                
                selModel.clearSelection();
                selModel.select(cell.getIndex());
                Dragboard db = cell.startDragAndDrop(
                        TransferMode.MOVE);
                HashMap<DataFormat, Object> map =
                        new HashMap<DataFormat, Object>();
                map.put(DataFormat.PLAIN_TEXT, cell.getItem());
                db.setContent(map);
            }

        });

        cell.setOnDragEntered(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                    
                event.acceptTransferModes(TransferMode.MOVE);
                if (cell.isSelected()) {
                    selModel.clearSelection(cell.getIndex());
                } else {
                    selModel.select(cell.getIndex());
                }
            }
        });
        
        cell.setOnDragExited(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
        });

        cell.setOnDragOver(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                event.acceptTransferModes(TransferMode.MOVE);

                if (cell.isSelected()) {
                    selModel.clearSelection(cell.getIndex());
                } else {
                    selModel.select(cell.getIndex());
                }
            }
        });
        
        cell.setOnDragDropped(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                event.setDropCompleted(true);
                if (cell.isSelected()) {
                    selModel.clearSelection(cell.getIndex());
                } else {
                    selModel.select(cell.getIndex());
                }
            }
        });
    }
    
    /**
     * Initializes Table 3
     * @param tbl the Table
     * @param col1 Column 1
     * @param col2 Column 2
     * @param col3 Column 3
     * @param col4 Column 4
     */
    private void initTable3(TableView<Item> tbl,
            TableColumn<Item, String> col1,
            TableColumn<Item, String> col2,
            TableColumn<Item, String> col3,
            TableColumn<Item, String> col4) {
        col1.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col1"));
        col1.setCellFactory(TextFieldTableCell.<Item>forTableColumn());
        col2.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col2"));
        col2.setCellFactory(TextFieldTableCell.<Item>forTableColumn());
        col3.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col3"));
        col3.setCellFactory(TextFieldTableCell.<Item>forTableColumn());
        col4.setCellValueFactory(new PropertyValueFactory<Item, String>(
                "col4"));
        col4.setCellFactory(TextFieldTableCell.<Item>forTableColumn());
        col1.setResizable(false);
        col2.setResizable(false);
        col3.setResizable(false);
        col4.setResizable(false);
        tbl.setEditable(true);
        tbl.setId(ComponentNameConstants.TESTPAGE_TABLES_TBL03);
        tbl.getItems().addAll(getTable1Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        tbl.setMaxHeight(70);
        tbl.setMaxWidth(100);
    }
    
    /**
     * Initializes Table 4
     * @param tbl the Table
     */
    private void initTable4(TableView<Map> tbl) {
        char colLetter = 'A';
        ObservableList<Map<String, String>> letterMapList =
                FXCollections.observableArrayList();              
        
        for (int i = 0; i < 21; i++) {
            Map<String, String> currMap = new HashMap<String, String>();
            letterMapList.add(currMap);
            char letter = 'A';
            for (int j = 0; j < 21; j++) {
                if (j == 20) {
                    currMap.put("right align", "rl" + (i + 1));
                } else {
                    currMap.put("" + letter, letter + String.valueOf(i + 1));
                    letter++;
                }
            }
            TableColumn<Map, String> col;
            if (i == 20) {
                col = new TableColumn<Map, String>(
                        "right align");
                
                col.setCellValueFactory(
                        new MapValueFactory<String>(
                                "right align"));
                col.setCellFactory(new Callback<TableColumn<Map, String>,
                        TableCell<Map, String>>() {

                    public TableCell call(TableColumn param) {
                        TableCell cell = new TableCell() {
                               
                                @Override
                                public void updateItem(Object item,
                                        boolean empty) {
                                    if (item != null) {
                                        setText(item.toString());
                                    }
                                }
                            };
                        cell.setAlignment(Pos.TOP_RIGHT);           
                        return cell;
                    }
                    
                });
            } else {
             
                col = new TableColumn<Map, String>(
                    "" + colLetter);
                col.setCellValueFactory(
                    new MapValueFactory<String>(
                            "" + colLetter));
                
                colLetter++;
            }
            col.setMinWidth("right align".length());
            col.setSortable(false);
            tbl.getColumns().add(col);
        }
        tbl.getItems().addAll(letterMapList);
        tbl.setMaxHeight(190);
    } 

    /**
     * getHeaderTableData
     * 
     * @return TableData
     */
    private ObservableList<Item> getTable1Data() {
        return FXCollections.observableArrayList(
                (new Item(I18NUtils.getString("val1"),
                        I18NUtils.getString("val2"),
                        Integer.toString(new Integer(3)),
                        Boolean.toString(new Boolean(true)))),
                new Item(I18NUtils.getString("val4"),
                        I18NUtils.getString("val5"),
                        Integer.toString(new Integer(6)),
                        Boolean.toString(new Boolean(false))),
                new Item(I18NUtils.getString("val1"),
                        I18NUtils.getString("val5"),
                        Integer.toString(new Integer(11)),
                        Boolean.toString(new Boolean(false))));
    }

    /**
     * getTableData
     * 
     * @return TableData
     */
    public static ObservableList<Item> getTable2Data() {
        return FXCollections.observableArrayList(
                new Item(I18NUtils.getString("text1"),
                        I18NUtils.getString("text7"),
                        I18NUtils.getString("text3"),
                        I18NUtils.getString("text5")),
                new Item(I18NUtils.getString("text2"),
                        I18NUtils.getString("text4"),
                        I18NUtils.getString("text5"),
                        I18NUtils.getString("text1")),
                new Item(I18NUtils.getString("text3"),
                        I18NUtils.getString("text1"),
                        I18NUtils.getString("text4"),
                        I18NUtils.getString("text7")),
                new Item(I18NUtils.getString("text2"),
                        I18NUtils.getString("text7"),
                        I18NUtils.getString("text3"),
                        I18NUtils.getString("text1")));

    }

    /**
     * A JavaFX Table requires a model.
     * 
     */
    public static class Item {
        /** Column1 **/
        private SimpleStringProperty m_col1;
        /** Column2 **/
        private SimpleStringProperty m_col2;
        /** Column3 **/
        private SimpleStringProperty m_col3;
        /** Column4 **/
        private SimpleStringProperty m_col4;

        /**
         * Constructor
         * 
         * @param col1
         *            value for Column 1
         * @param col2
         *            value for Column 2
         * @param col3
         *            value for Column 3
         * @param col4
         *            value for Column 4
         */
        public Item(String col1, String col2, String col3, String col4) {
            m_col1 = new SimpleStringProperty(col1);
            m_col2 = new SimpleStringProperty(col2);
            m_col3 = new SimpleStringProperty(col3);
            m_col4 = new SimpleStringProperty(col4);
        }

        /**
         * getter col1
         * 
         * @return col1
         */
        public String getCol1() {
            return m_col1.getValue();
        }

        /**
         * getter col2
         * 
         * @return col2
         */
        public String getCol2() {
            return m_col2.getValue();
        }

        /**
         * getter col3
         * 
         * @return col3
         */
        public String getCol3() {
            return m_col3.getValue();
        }

        /**
         * getter col4
         * 
         * @return col4
         */
        public String getCol4() {
            return m_col4.getValue();
        }
        
        /**
         * Return property by position
         * @param columnIdex column
         * @return property for editing
         */
        public SimpleStringProperty getProperty(int columnIdex) {
            switch (columnIdex) {
                case 0:
                    return m_col1;
                case 1:
                    return m_col2;
                case 2:
                    return m_col3;
                case 3:
                    return m_col4;
                default :
                    return m_col1;
            }
        }
    }
    
    /**
     * A JavaFX Table requires a model.
     * 
     */
    public class CheckBoxItem {
        /** Column1 **/
        private SimpleBooleanProperty m_col1;
        /** Column2 **/
        private SimpleStringProperty m_col2;
        /** Column3 **/
        private SimpleStringProperty m_col3;
        /** Column4 **/
        private SimpleStringProperty m_col4;

        /**
         * Constructor
         * 
         * @param col1
         *            value for Column 1
         * @param col2
         *            value for Column 2
         * @param col3
         *            value for Column 3
         * @param col4
         *            value for Column 4
         */
        public CheckBoxItem(Boolean col1, String col2,
                            String col3, String col4) {
            m_col1 = new SimpleBooleanProperty(col1);
            m_col2 = new SimpleStringProperty(col2);
            m_col3 = new SimpleStringProperty(col3);
            m_col4 = new SimpleStringProperty(col4);
        }

        /**
         * getter col1
         * 
         * @return col1
         */
        public Boolean getCol1() {
            return m_col1.getValue();
        }

        /**
         * getter col2
         * 
         * @return col2
         */
        public String getCol2() {
            return m_col2.getValue();
        }

        /**
         * getter col3
         * 
         * @return col3
         */
        public String getCol3() {
            return m_col3.getValue();
        }

        /**
         * getter col4
         * 
         * @return col4
         */
        public String getCol4() {
            return m_col4.getValue();
        }
    }

}
