package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
/**
 * Initializes TextAreas for testing.
 */
public class TextAreaTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_textAreas");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_TEXTAREAS);
        
     // textArea 1
        TextArea ta1 = new TextArea();
        ta1.setId(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA01);
        tPane.addContextMenuToNode(ta1);

        // textArea 2
        TextArea ta2 = new TextArea();
        ta2.setId(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA02);
        ta2.setDisable(true);

        // textArea 3
        TextArea ta3 = new TextArea();
        ta3.setId(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA03);
        ta3.setText(I18NUtils.getString("text1"));
        ta3.setTooltip(new Tooltip(I18NUtils.getString("tooltip")));

        // textArea 4
        TextArea ta4 = new TextArea();
        ta4.setId(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA04);
        ta4.setDisable(true);
        ta4.setText(I18NUtils.getString("text2"));

        // textArea 5
        TextArea ta5 = new TextArea();
        ta5.setId(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA05);
        ta5.setEditable(false);
        ta5.setText(I18NUtils.getString("text3"));

        tPane.addControls(ta1, ta2, ta3, ta4, ta5);
        tPane.addControlsToEventHandler(ta1, ta2, ta3, ta4, ta5);
        
        return tPane;
    }

}
