package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
/**
 * Initializes TextFields for testing.
 */
public class TextFieldTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_textFields");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_TEXTFIELDS);
        // textField 1
        TextField tfield1 = new TextField();
        tfield1.setId(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF01);
        tfield1.setTooltip(new Tooltip(I18NUtils.getString("tooltip")));
        testPane.addContextMenuToNode(tfield1);
        // textField 2
        TextField tfield2 = new TextField();
        tfield2.setId(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF02);
        tfield2.setDisable(true);
        // textField 3
        TextField tfield3 = new TextField();
        tfield3.setId(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF03);
        tfield3.setText(I18NUtils.getString("text1"));
        // textField 4
        TextField tfield4 = new TextField();
        tfield4.setId(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF04);
        tfield4.setDisable(true);
        tfield4.setText(I18NUtils.getString("text2"));
        // textField 5
        TextField tfield5 = new TextField();
        tfield5.setId(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF05);
        tfield5.setEditable(false);
        tfield5.setText(I18NUtils.getString("text3"));

        testPane.addControls(tfield1, tfield2, tfield3, tfield4, tfield5);
        testPane.addControlsToEventHandler(tfield1, tfield2, tfield3, tfield4,
                tfield5);
        return testPane;
    }

}
