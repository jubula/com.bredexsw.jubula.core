package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Initializes Texts for testing.
 */
public class TextTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_texts");
        tPane.setRootId(JavaFXComponentNameConstants.TESTPAGE_TITLE_TEXTS);
     // Text 1
        Text txt1 = new Text(I18NUtils.getName("lbl1"));
        txt1.setId(JavaFXComponentNameConstants.TESTPAGE_TEXT_TXT01);

        // Text 2
        Text txt2 = new Text(I18NUtils.getName("lbl2"));
        txt2.setId(JavaFXComponentNameConstants.TESTPAGE_TEXT_TXT02);
        txt2.setDisable(true);

        // label 3
        final Text txt3 = new Text(I18NUtils.getName("lbl3"));
        txt3.setId(JavaFXComponentNameConstants.TESTPAGE_TEXT_TXT03);
        
        EventHandler<MouseEvent> mhand = new EventHandler<MouseEvent>() {
            
            
            public void handle(MouseEvent e) {
                int clickCount = e.getClickCount();
                switch (clickCount) {
                    case 1:
                        txt3.setText(I18NUtils.getName("oneClick"));
                        break;
                    case 2:
                        txt3.setText(I18NUtils.getName("twoClick"));
                        break;
                    default:
                        txt3.setText(e.getClickCount() + " Click");
                }

            }
        };
        txt3.setOnMouseClicked(mhand);
        
        tPane.addTexts(txt1, txt2, txt3);
        return tPane;
    }

}
