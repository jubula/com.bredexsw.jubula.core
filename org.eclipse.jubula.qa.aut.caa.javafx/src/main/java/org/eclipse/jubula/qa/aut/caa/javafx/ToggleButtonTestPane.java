package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Initializes ToggleButtons for testing.
 */
public class ToggleButtonTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_toogleButtons");
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_TOGGLEBUTTONS);
        
        ToggleGroup tGroup = new ToggleGroup();
        
        //ToggleButton 1
        ToggleButton tb1 = new ToggleButton("tgBtn1");
        tb1.setId(JavaFXComponentNameConstants.
                TESTPAGE_TOGGLEBUTTONS_TOGLBUT01);
        tb1.setToggleGroup(tGroup);
        tPane.addContextMenuToNode(tb1);
      //ToggleButton 2
        ToggleButton tb2 = new ToggleButton("tgBtn2");
        tb2.setId(JavaFXComponentNameConstants.
                TESTPAGE_TOGGLEBUTTONS_TOGLBUT02);
        tb2.setSelected(true);
        tb2.setToggleGroup(tGroup);
        
      //ToggleButton 3
        ToggleButton tb3 = new ToggleButton("tgBtn3");
        tb3.setId(JavaFXComponentNameConstants.
                TESTPAGE_TOGGLEBUTTONS_TOGLBUT03);
        
      //ToggleButton 4
        ToggleButton tb4 = new ToggleButton("tgBtn3");
        tb4.setId(JavaFXComponentNameConstants.
                TESTPAGE_TOGGLEBUTTONS_TOGLBUT04);
        tb4.setDisable(true);
        tb4.setToggleGroup(tGroup);
      
        tPane.addControls(tb1, tb2, tb3, tb4);
        tPane.addControlsToEventHandler(tb1, tb2, tb3, tb4);

        return tPane;
    }

}
