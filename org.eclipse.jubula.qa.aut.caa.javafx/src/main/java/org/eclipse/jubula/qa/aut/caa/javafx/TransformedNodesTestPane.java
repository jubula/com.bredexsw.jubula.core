package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.CssMetaData;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.TableTestPane.Item;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Transformed Widgets TestPane
 * 
 * @author Bredex GmbH
 *
 */
public class TransformedNodesTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {

        TestPane testPane = new TestPane("title_widgets_transformed");
        /* Scaled rotated list */
        ListView<String> listView = new ListView<String>(createListModel());
        listView.setId(JavaFXComponentNameConstants.
                TESTPAGE_TRANSFORMED_WIDGETS_LIST);
        listView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE);
        listView.setRotate(45);
        listView.setScaleX(0.50);
        listView.setScaleY(0.50);
        listView.setTranslateX(200);
        List<CssMetaData<?, ?>> liste = listView.getCssMetaData();
        System.out.println(liste);
        testPane.addControls(listView);
        testPane.addContextMenuToNode(listView);

        /* Scaled rotated Table */
        TableView<Item> tbl = createTable();
        tbl.setRotate(135);
        tbl.setScaleX(0.75);
        tbl.setScaleY(1.5);
        tbl.setTranslateY(-50);
        testPane.addControls(tbl);
//        testPane.addContextMenuToNode(tbl);

        return testPane;
    }

    /**
     * 
     * @return a {@link TableView} with filled data
     */
    private TableView<Item> createTable() {
        TableView<Item> tbl = new TableView<Item>();
        tbl.setEditable(false);
        tbl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<Item, String> col1 = new TableColumn<Item, String>(
                I18NUtils.getString("col1"));
        TableColumn<Item, String> col2 = new TableColumn<Item, String>(
                I18NUtils.getString("col2"));
        TableColumn<Item, String> col3 = new TableColumn<Item, String>(
                I18NUtils.getString("col3"));
        TableColumn<Item, String> col4 = new TableColumn<Item, String>(
                I18NUtils.getString("col4"));
        col1.setSortable(false);
        col2.setSortable(false);
        col3.setSortable(false);
        col4.setSortable(false);
        col1.setCellValueFactory(
                new PropertyValueFactory<Item, String>("col1"));
        col2.setCellValueFactory(
                new PropertyValueFactory<Item, String>("col2"));
        col3.setCellValueFactory(
                new PropertyValueFactory<Item, String>("col3"));
        col4.setCellValueFactory(
                new PropertyValueFactory<Item, String>("col4"));
        tbl.getItems().addAll(TableTestPane.getTable2Data());
        tbl.getColumns().addAll(col1, col2, col3, col4);
        col1.setPrefWidth(72);
        col2.setPrefWidth(72);
        col3.setPrefWidth(72);
        col4.setPrefWidth(72);
        tbl.setStyle("-fx-font-size: 10px;");
        tbl.setMaxHeight(160);
        tbl.setMaxWidth(320);
        ArrayList<TableColumn<Item, String>> columns =
                new ArrayList<TableColumn<Item, String>>();
        columns.add(col1);
        columns.add(col2);
        columns.add(col3);
        columns.add(col4);
        tbl.getSelectionModel().setCellSelectionEnabled(true);
        tbl.setId(JavaFXComponentNameConstants.
                TESTPAGE_TRANSFORMED_WIDGETS_TABLE);
        return tbl;
    }

    /**
     * @return the default list model
     */
    private ObservableList<String> createListModel() {
        return FXCollections.observableArrayList(I18NUtils.getString("text1"),
                I18NUtils.getString("text2"), I18NUtils.getString("text3"),
                I18NUtils.getString("text4"), I18NUtils.getString("text5"),
                I18NUtils.getString("text6"), I18NUtils.getString("text7"));
    }

}
