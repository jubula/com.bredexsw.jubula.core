package org.eclipse.jubula.qa.aut.caa.javafx;


import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.shape.Circle;
import javafx.scene.transform.Shear;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Transformed Buttons TestPane
 * @author Bredex GmbH
 *
 */
public class TransformedTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_buttons_transformed");
        testPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_BUTTONS_TRANSFORMED);
        // button 1
        Button btn1 = new Button(I18NUtils.getName("btn1"));
        btn1.setId(JavaFXComponentNameConstants.
                TESTPAGE_BUTTONS_TRANSFORMED_BTN01);
        btn1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn1.setRotate(45);
        testPane.addContextMenuToNode(btn1);
        
        // button 2
        Button btn2 = new Button(I18NUtils.getName("btn2"));
        btn2.setId(JavaFXComponentNameConstants.
                TESTPAGE_BUTTONS_TRANSFORMED_BTN02);
        btn2.setTranslateX(400);
        btn2.setTranslateY(100);
        double radius2 = 30;
        Circle shape2 = new Circle(radius2);
        btn2.setClip(shape2);
        shape2.setCenterX(radius2);
        //btn2.setTranslateZ(90);

        
     // button 3
        Button btn3 = new Button(I18NUtils.getName("btn3"));
        btn3.setId(JavaFXComponentNameConstants.
                TESTPAGE_BUTTONS_TRANSFORMED_BTN03);
        btn3.setScaleX(0.9);
        btn3.setScaleY(0.5);
        btn3.getTransforms().add(new Shear(-0.4, 0.4));
        //btn3.setScaleZ(0.5);
     // button 4
        Button btn4 = new Button(I18NUtils.getName("btn4"));
        btn4.setId(JavaFXComponentNameConstants.
                TESTPAGE_BUTTONS_TRANSFORMED_BTN04);
        btn4.setScaleX(1.5);
        btn4.setScaleY(0.8);
//        btn4.setScaleZ(0.9);
        btn4.setTranslateX(90);
        btn4.setTranslateY(90);
        //btn4.setTranslateZ(90);
        btn4.setRotate(135);

        testPane.addNode(btn1, btn2, btn3, btn4);
        testPane.addControlsToEventHandler(btn1, btn2, btn3, btn4);

        return testPane;
    }

}
