package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Creates two TreeTableViews, the second one is editable
 *
 */
public class TreeTableTestPane extends AbstractSceneSetter {
    /** The Test Pane **/
    private final TestPane m_tPane = new TestPane("title_treeTables");

    @Override
    protected TestPane getTestScene() {
        m_tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_TREETABLES);
        //Create TreeTableView1
        TreeTableView<TTModel> tbw1 = new TreeTableView<>();
        createTableView1(tbw1);
        tbw1.setId(ComponentNameConstants.TESTPAGE_TREETABLES_TRT01);
        tbw1.setEditable(false);
        tbw1.setMaxHeight(230);
        tbw1.setPrefWidth(452);
        for (TreeTableColumn<TTModel, ?> c: tbw1.getColumns()) {
            c.setPrefWidth(150);
            c.setResizable(false);
        }
        tbw1.setTableMenuButtonVisible(true);
        tbw1.getSelectionModel().setCellSelectionEnabled(true);
        //Create TreeTableView2
        TreeTableView<TTModel> tbw2 = new TreeTableView<>();
        createTableView2(tbw2);
        tbw2.setId(ComponentNameConstants.TESTPAGE_TREETABLES_TRT02);
        tbw2.setEditable(true);
        tbw2.setMaxWidth(150);
        tbw2.setMaxHeight(100);
        for (TreeTableColumn<TTModel, ?> c: tbw2.getColumns()) {
            c.setPrefWidth(150);
            c.setResizable(false);
        }
        //Create TreeTableView3
        TreeTableView<TTModel> tbw3 = new TreeTableView<>();
        createTableView1(tbw3);
        tbw3.setId(JavaFXComponentNameConstants.TESTPAGE_TREETABLES_TRT03);
        tbw3.setEditable(false);
        tbw3.setDisable(true);
        tbw3.setMaxHeight(150);
        tbw3.setPrefWidth(452);
        for (TreeTableColumn<TTModel, ?> c: tbw3.getColumns()) {
            c.setPrefWidth(150);
            c.setResizable(false);
        }
        tbw3.setTableMenuButtonVisible(true);
        tbw3.getSelectionModel().setCellSelectionEnabled(true);
        
        //TreeTable with visible root
        TreeTableView<TTModel> tbw4 = new TreeTableView<>();
        createTableView2(tbw4);
        tbw4.setId(JavaFXComponentNameConstants.TESTPAGE_TREETABLES_TRT04);
        tbw4.setEditable(true);
        tbw4.setMaxWidth(150);
        tbw4.setMaxHeight(100);
        for (TreeTableColumn<TTModel, ?> c: tbw4.getColumns()) {
            c.setPrefWidth(150);
            c.setResizable(false);
        }
        tbw4.setShowRoot(true);
        
        TreeTableView<TTModel> tbw5 = new TreeTableView<>();
        tbw5.setId(JavaFXComponentNameConstants.TESTPAGE_TREETABLES_TRT05);
        tbw5.setMaxWidth(150);
        tbw5.setMaxHeight(100);
        
        TreeTableView<TTModel> tbw6 = createTableWithNestedHeader();
        
        m_tPane.addControls(tbw1, tbw2, tbw3, tbw4, tbw5, tbw6);
        m_tPane.addContextMenuToNode(tbw1);
        return m_tPane;
    }

    /**
     * 
     * @return a {@link TreeTableView} with a nested header
     */
    private TreeTableView<TTModel> createTableWithNestedHeader() {
        //Create TreeTableView1
        TreeTableView<TTModel> tbw6 = new TreeTableView<>();
        createTableView1(tbw6);
        tbw6.setId(JavaFXComponentNameConstants.TESTPAGE_TREETABLES_TRT06);
        tbw6.setEditable(false);
        TreeTableColumn<TTModel, ?> col1 = tbw6.getColumns().remove(1);
        TreeTableColumn<TTModel, ?> col2 = tbw6.getColumns().remove(1);
        TreeTableColumn<TTModel, ?> parentCol =
                new TreeTableColumn("Parent Column");
        parentCol.getColumns().addAll(col1, col2);
        tbw6.getColumns().add(1, parentCol);
        tbw6.setPrefHeight(300);
        tbw6.setPrefWidth(325);
        for (TreeTableColumn<TTModel, ?> c: tbw6.getColumns()) {
            c.setPrefWidth(150);
            c.setResizable(false);
        }
        return tbw6;
    }

    /**
     * Creates TreeTableView1
     * @param tbw TTView1
     */
    private void createTableView1(TreeTableView<TTModel> tbw) {
        TreeTableColumn<TTModel, String> column1 = new TreeTableColumn<>(
                I18NUtils.getString("col1"));
        column1.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column1;
            }
        });
        TreeTableColumn<TTModel, String> column2 = new TreeTableColumn<>(
                I18NUtils.getString("col2"));
        column2.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column2;
            }
        });
        TreeTableColumn<TTModel, String> column3 = new TreeTableColumn<>(
                I18NUtils.getString("col3"));
        column3.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column3;
            }
        });
        tbw.setRoot(new TreeItem<TreeTableTestPane.TTModel>(new TTModel("",
                "", "")));
        tbw.setShowRoot(false);
        tbw.getRoot().getChildren()
                .addAll(getRow1(), getRow2(), getRow3(), getRow4());
        tbw.getColumns().addAll(column1, column2, column3);
        // Code für Drag & Drop
        ArrayList<TreeTableColumn<TTModel, String>> columnsWithType = 
                new ArrayList<>();
        columnsWithType.add(column1);
        columnsWithType.add(column2);
        columnsWithType.add(column3);
        for (TreeTableColumn<TTModel, String> column : columnsWithType) {
            column.setCellFactory(new Callback<TreeTableColumn<TTModel, String>,
                    TreeTableCell<TTModel, String>>() {
                @Override
                public TreeTableCell<TTModel, String> call(
                        TreeTableColumn<TTModel, String> param) {
                    return new TreeTableCell<TTModel, String>() {
                        @Override
                        protected void updateItem(final String item,
                                boolean empty) {
                            addDragHandlerToCell(this, m_tPane);
                            super.updateItem(item, empty);
                            setText(item);
                            setTooltip(new Tooltip(item));
                        }
                    };
                }
            });
        }
    }
    
    /**
     * Adds the D&D handler to a given Cell
     * 
     * @param cell
     *            the cell
     * @param tPane
     *            for convenience
     */
    private void addDragHandlerToCell(final TreeTableCell<TTModel, String> cell,
            final TestPane tPane) {

        cell.setOnDragDetected(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                Dragboard db = cell.startDragAndDrop(
                        TransferMode.COPY_OR_MOVE);
                HashMap<DataFormat, Object> map =
                        new HashMap<DataFormat, Object>();
                map.put(DataFormat.PLAIN_TEXT, cell.getItem());
                db.setContent(map);
            }

        });

        cell.setOnDragEntered(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragExited(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });

        cell.setOnDragOver(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragDropped(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                String text = (String) db.getContent(DataFormat.PLAIN_TEXT);
                tPane.setActionText("Drag source: " + text + " Drop target: "
                        + cell.getItem() + " [index=" + cell.getIndex() + "]");
                event.setDropCompleted(true);
            }
        });
    }
    
    /**
     * Creates TreeTableView2
     * @param tbw TTView2
     */
    private void createTableView2(TreeTableView<TTModel> tbw) {
        TreeTableColumn<TTModel, String> column1 = new TreeTableColumn<>(
                I18NUtils.getString("col1"));
        column1.setCellFactory(TextFieldTreeTableCell
                .<TTModel> forTreeTableColumn());
        column1.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column1;
            }
        });
        TreeTableColumn<TTModel, String> column2 = new TreeTableColumn<>(
                I18NUtils.getString("col2"));
        column2.setCellFactory(TextFieldTreeTableCell
                .<TTModel> forTreeTableColumn());
        column2.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column2;
            }
        });
        TreeTableColumn<TTModel, String> column3 = new TreeTableColumn<>(
                I18NUtils.getString("col3"));
        column3.setCellFactory(TextFieldTreeTableCell
                .<TTModel> forTreeTableColumn());
        column3.setCellValueFactory(new Callback<TreeTableColumn.
                CellDataFeatures<TTModel, String>, ObservableValue<String>>() {

            @Override
            public SimpleStringProperty call(
                    CellDataFeatures<TTModel, String> param) {
                return param.getValue().getValue().m_column3;
            }
        });
        tbw.setRoot(new TreeItem<TreeTableTestPane.TTModel>(new TTModel("", "",
                "")));
        tbw.setShowRoot(false);
        tbw.getRoot().getChildren()
                .addAll(getRow1(), getRow2(), getRow3(), getRow4());
        tbw.getColumns().addAll(column1, column2, column3);
    }

    /**
     * Creates Row 4
     * @return Row 4
     */
    private TreeItem<TTModel> getRow4() {
        TreeItem<TTModel> itemRow = new TreeItem<>(new TTModel(
                I18NUtils.getString("val2"), I18NUtils.getString("text6"),
                I18NUtils.getString("img_banana")));
        TreeItem<TTModel> subItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_first"),
                I18NUtils.getString("text7"), I18NUtils.getString("tooltip")));
        TreeItem<TTModel> subItem2 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_second"),
                I18NUtils.getString("text2"),
                I18NUtils.getString("title_lists")));
        TreeItem<TTModel> subItem3 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_third"),
                I18NUtils.getString("text5"),
                I18NUtils.getString("title_trees")));
        TreeItem<TTModel> subItem4 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_fourth"),
                I18NUtils.getString("text4"),
                I18NUtils.getString("title_menus")));
        TreeItem<TTModel> subsubItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("val6"), I18NUtils.getString("text3"),
                I18NUtils.getString("tab1")));
        itemRow.getChildren().setAll(subItem1, subItem2, subItem3, subItem4);
        subItem1.getChildren().add(subsubItem1);
        return itemRow;
    }

    /**
     * Creates Row 3
     * @return Row 3
     */
    private TreeItem<TTModel> getRow3() {
        TreeItem<TTModel> itemRow = new TreeItem<>(new TTModel(
                I18NUtils.getString("val3"), I18NUtils.getString("text3"),
                I18NUtils.getString("img_kiwi")));
        TreeItem<TTModel> subItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_first"),
                I18NUtils.getString("text6"),
                I18NUtils.getString("title_tables")));
        TreeItem<TTModel> subItem2 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_second"),
                I18NUtils.getString("text2"),
                I18NUtils.getString("title_labels")));
        TreeItem<TTModel> subItem3 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_third"), I18NUtils.getString("tab1"),
                I18NUtils.getString("title_buttons")));
        TreeItem<TTModel> subItem4 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_fourth"),
                I18NUtils.getString("tab2"),
                I18NUtils.getString("title_application")));
        TreeItem<TTModel> subsubItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("val5"), I18NUtils.getString("tab3"),
                I18NUtils.getString("img_kiwi")));
        itemRow.getChildren().setAll(subItem1, subItem2, subItem3, subItem4);
        subItem1.getChildren().add(subsubItem1);
        return itemRow;
    }

    /**
     * Creates Row 2
     * @return Row 2
     */
    private TreeItem<TTModel> getRow2() {
        TreeItem<TTModel> itemRow = new TreeItem<>(new TTModel(
                I18NUtils.getString("val2"), I18NUtils.getString("text6"),
                I18NUtils.getString("img_banana")));
        TreeItem<TTModel> subItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_first"),
                I18NUtils.getString("text7"), I18NUtils.getString("tooltip")));
        TreeItem<TTModel> subItem2 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_second"),
                I18NUtils.getString("text2"),
                I18NUtils.getString("title_lists")));
        TreeItem<TTModel> subItem3 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_third"),
                I18NUtils.getString("text5"),
                I18NUtils.getString("title_trees")));
        TreeItem<TTModel> subItem4 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_fourth"),
                I18NUtils.getString("text4"),
                I18NUtils.getString("title_menus")));
        TreeItem<TTModel> subsubItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("val6"), I18NUtils.getString("text3"),
                I18NUtils.getString("tab1")));
        itemRow.getChildren().setAll(subItem1, subItem2, subItem3, subItem4);
        subItem1.getChildren().add(subsubItem1);
        return itemRow;
    }

    /**
     * Creates Row 1
     * @return Row 1
     */
    private TreeItem<TTModel> getRow1() {
        TreeItem<TTModel> itemRow = new TreeItem<>(new TTModel(
                I18NUtils.getString("val1"), I18NUtils.getString("text1"),
                I18NUtils.getString("tab1")));
        TreeItem<TTModel> subItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_first"),
                I18NUtils.getString("text4"), I18NUtils.getString("menu")));
        TreeItem<TTModel> subItem2 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_second"),
                I18NUtils.getString("text1"), I18NUtils.getString("menubar")));
        TreeItem<TTModel> subItem3 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_third"),
                I18NUtils.getString("text2"),
                I18NUtils.getString("menu_more")));
        TreeItem<TTModel> subItem4 = new TreeItem<>(new TTModel(
                I18NUtils.getString("item_fourth"),
                I18NUtils.getString("text7"), I18NUtils.getString("menu")));
        TreeItem<TTModel> subsubItem1 = new TreeItem<>(new TTModel(
                I18NUtils.getString("text1"), I18NUtils.getString("text1"),
                I18NUtils.getString("text1")));
        itemRow.getChildren().setAll(subItem1, subItem2, subItem3, subItem4);
        subItem1.getChildren().add(subsubItem1);
        return itemRow;
    }

    /**
     * The Model for the TreeTableViews
     */
    private class TTModel {
        /**
         * Property for Column 1
         */
        private SimpleStringProperty m_column1;
        /**
         * Property for Column 2
         */
        private SimpleStringProperty m_column2;
        /**
         * Property for Column 3
         */
        private SimpleStringProperty m_column3;

        /**
         * Creates on Item with the give values
         * @param column1 value for this column
         * @param column2 value for this column
         * @param column3 value for this column
         */
        public TTModel(String column1, String column2, String column3) {
            this.m_column1 = new SimpleStringProperty(column1);
            this.m_column2 = new SimpleStringProperty(column2);
            this.m_column3 = new SimpleStringProperty(column3);
        }

    }

}
