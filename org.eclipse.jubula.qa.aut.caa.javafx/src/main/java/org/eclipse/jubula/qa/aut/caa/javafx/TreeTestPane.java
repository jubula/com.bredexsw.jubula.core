package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.HashMap;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeItem.TreeModificationEvent;
import javafx.scene.control.TreeView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;

/**
 * TreeView 1 tooltip TreeView 2 tooltip disabled TreeView 3 TreeView 4 ==
 * iniTree infinite binary tree TreeView 5 different TreeItem structure
 * 
 */
public class TreeTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        final TestPane tPane = new TestPane("title_trees");
        tPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_TREES);

        // der Tree wird erstellt
        final TreeView<String> tree1 = new TreeView<String>(createTreeModel());
        tree1.setId(ComponentNameConstants.TESTPAGE_TREES_TRE01);
        tree1.getRoot().setExpanded(true);
        tree1.setPrefSize(150, 150);
        tPane.addContextMenuToNode(tree1);
        // Code für Drag & Drop
        tree1.setCellFactory(new Callback<TreeView<String>,
                TreeCell<String>>() {
            public TreeCell<String> call(TreeView<String> param) {
                return new TreeCell<String>() {
                    @Override
                    protected void updateItem(final String item,
                            boolean empty) {
                        addDragHandlerToCell(this, tPane);
                        super.updateItem(item, empty);
                        setText(item);
                        setTooltip(new Tooltip(item));
                    }
                };
            }
        });
        // ein zweiter Tree wird erstellt
        TreeView<String> tree2 = new TreeView<String>(createTreeModel());
        tree2.setId(ComponentNameConstants.TESTPAGE_TREES_TRE02);
        tree2.setTooltip(new Tooltip("Tooltip")); //$NON-NLS-1$
        tree2.setDisable(true);
        tree2.getRoot().setExpanded(true);
        // ein dritter Tree wird erstellt
        TreeView<String> tree3 = new TreeView<String>(createTreeModel());
        tree3.setId(ComponentNameConstants.TESTPAGE_TREES_TRE03);
        tree3.getRoot().setExpanded(true);
        tree3.setPrefHeight(80);
        tree3.setMaxWidth(80);
        // tree with custom TreeModel and non-TreeNodes
        final InfiniteBinaryTree iniModel = new InfiniteBinaryTree();
        TreeView<Integer> iniTree = new TreeView<Integer>(iniModel.getRoot());
        TreeItem<Integer> r = iniTree.getRoot();
        TreeItem<Integer> c1 = new TreeItem<Integer>(new Integer(2));
        TreeItem<Integer> c2 = new TreeItem<Integer>(new Integer(3));
        r.getChildren().addAll(c1, c2);
        r.addEventHandler(TreeItem.branchExpandedEvent(), new EventHandler() {
            public void handle(Event event) {
                handleBinaryTreeExpand(iniModel, event);
            }
        });
        iniTree.setId("CustomTreeModel"); //$NON-NLS-1$
        // ein fuenfter Tree wird erstellt
        TreeView<String> tree5 = new TreeView<String>(createExtraTreeNode());
        tree5.setId(ComponentNameConstants.TESTPAGE_TREES_TRE05);
        tree5.getRoot().setExpanded(true);
        tree5.setPrefHeight(80);
        tree5.setMaxWidth(80);
        
        final TreeView<String> tree6 = new TreeView<String>();
        tree6.setId(ComponentNameConstants.TESTPAGE_TREES_TRE06);
        tree6.setPrefSize(150, 150);
        
        tPane.addControls(tree1, tree2, tree3, iniTree, tree5, tree6);
        return tPane;
    }

    /**
     * Adds the D&D handler to a given Cell
     * 
     * @param cell
     *            the cell
     * @param tPane
     *            for convenience
     */
    private void addDragHandlerToCell(final TreeCell<String> cell,
            final TestPane tPane) {

        cell.setOnDragDetected(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                Dragboard db = cell.startDragAndDrop(
                        TransferMode.COPY_OR_MOVE);
                HashMap<DataFormat, Object> map =
                        new HashMap<DataFormat, Object>();
                map.put(DataFormat.PLAIN_TEXT, cell.getItem());
                db.setContent(map);
            }

        });

        cell.setOnDragEntered(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragExited(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });

        cell.setOnDragOver(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragDropped(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                String text = (String) db.getContent(DataFormat.PLAIN_TEXT);
                tPane.setActionText("Drag source: " + text + " Drop target: "
                        + cell.getItem() + " [index=" + cell.getIndex() + "]");
                event.setDropCompleted(true);
            }
        });
    }

    /**
     * Taken from
     * http://www.developer.com/java/ent/article.php/939061/Simple-TreeModel
     * -Example-Infinite-Binary-Tree.htm and modified.
     * 
     * InfiniteBinaryTree
     * 
     * @author Daniel Green Superliminal Software
     * 
     *         A little example program to show the power of the TreeModel
     *         interface. Running it displays a binary tree with numbers on each
     *         node. Every positive number can be found somewhere in the tree.
     *         See if you can find the node labeled "1000". Hint: The base 2
     *         representation of any node value can be read out from the path
     *         leading to that node where opening the first child of a node
     *         represents a 0 and opening the second child represents a 1.
     *         Enjoy!
     */
    private static class InfiniteBinaryTree {

        /**
         * Returns the root of the Infinite Tree
         * 
         * @return root TreeItem
         */
        public TreeItem<Integer> getRoot() {
            return new TreeItem<Integer>(new Integer(1));
        }

        /**
         * Get one child for a given TreeItem
         * 
         * @param parent
         *            the TreeItem to get the children for
         * @param index
         *            index of the parent to calculate the value for the child,
         *            for the second child pass index = 1 else 0.
         * @return One child TreeItem
         */
        public TreeItem<Integer> nxtLevel(TreeItem<Integer> parent, int index) {
            // the magic formula
            TreeItem<Integer> child = new TreeItem<Integer>(new Integer(2
                    * valueOf(parent) + index));
            TreeItem<Integer> nxtChild1 = new TreeItem<Integer>(new Integer(
                    2 * valueOf(child) + 0));
            TreeItem<Integer> nxtChild2 = new TreeItem<Integer>(new Integer(
                    2 * valueOf(child) + 1));
            child.getChildren().setAll(nxtChild1, nxtChild2);
            return child;
        }

        /**
         * helper function
         * 
         * @param obj
         *            Object to evaluate.
         * @return the value of the object within the context of this model.
         */
        private int valueOf(TreeItem<Integer> obj) {
            return obj.getValue().intValue();
        }
    }

    /**
     * 
     * @return the default TreeNode that can be used as a model for test trees.
     */
    private static TreeItem<String> createTreeModel() {
        // Struktur der Trees wird festgelegt:
        // die Namen der einzelnen Nodes sind in der Klasse I18NUtils festgelegt
        TreeItem<String> rootItem = new TreeItem<String>(
                I18NUtils.getString("text1")); //$NON-NLS-1$

        TreeItem<String> item1 = new TreeItem<String>(
                I18NUtils.getString("text2")); //$NON-NLS-1$
        TreeItem<String> item2 = new TreeItem<String>(
                I18NUtils.getString("text3")); //$NON-NLS-1$
        TreeItem<String> item21 = new TreeItem<String>(
                I18NUtils.getString("text4")); //$NON-NLS-1$
        // die erste Verzweigung wird noch weiter verzweigt (tree211, tree212)
        TreeItem<String> item211 = new TreeItem<String>(
                I18NUtils.getString("text7")); //$NON-NLS-1$
        TreeItem<String> item212 = new TreeItem<String>(
                I18NUtils.getString("text2")); //$NON-NLS-1$
        TreeItem<String> item22 = new TreeItem<String>(
                I18NUtils.getString("text5")); //$NON-NLS-1$
        TreeItem<String> item3 = new TreeItem<String>(
                I18NUtils.getString("text6")); //$NON-NLS-1$
        TreeItem<String> item31 = new TreeItem<String>(
                I18NUtils.getString("text1")); //$NON-NLS-1$
        TreeItem<String> item4 = new TreeItem<String>(
                I18NUtils.getString("text7")); //$NON-NLS-1$

        rootItem.getChildren().addAll(item1, item2, item3, item4);
        item2.getChildren().addAll(item21, item22);
        item21.getChildren().addAll(item211, item212);
        item3.getChildren().add(item31);
        return rootItem;
    }

    /**
     * @return a root TreeItem with extra entrys
     */
    private static TreeItem<String> createExtraTreeNode() {
        TreeItem<String> root = new TreeItem<String>(
                I18NUtils.getString("text1")); //$NON-NLS-1$

        TreeItem<String> tree1 = new TreeItem<String>(
                I18NUtils.getString("long_item_name")); //$NON-NLS-1$
        TreeItem<String> tree2 = new TreeItem<String>(
                I18NUtils.getString("long_item_name")); //$NON-NLS-1$
        TreeItem<String> tree3 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree4 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$

        TreeItem<String> tree11 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree12 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree21 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree22 = new TreeItem<String>(
                I18NUtils.getString("long_item_name")); //$NON-NLS-1$
        TreeItem<String> tree31 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree32 = new TreeItem<String>(
                I18NUtils.getString("short_item_name")); //$NON-NLS-1$
        TreeItem<String> tree41 = new TreeItem<String>(
                I18NUtils.getString("long_item_name")); //$NON-NLS-1$
        TreeItem<String> tree42 = new TreeItem<String>(
                I18NUtils.getString("long_item_name")); //$NON-NLS-1$

        root.getChildren().addAll(tree1, tree2, tree3, tree4);

        tree1.getChildren().addAll(tree11, tree12);
        tree2.getChildren().addAll(tree21, tree22);
        tree3.getChildren().addAll(tree31, tree32);
        tree4.getChildren().addAll(tree41, tree42);

        return root;
    }
    
    /**
     * Handles the expanding of the binary tree and adds a random delay
     * @param iniModel the model
     * @param event the event
     */
    private void handleBinaryTreeExpand(
            final InfiniteBinaryTree iniModel, Event event) {
        try {
            Thread.sleep((long) (1000 * Math.random()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (((TreeModificationEvent) event).wasExpanded()) {
            TreeItem<Integer> parent = ((TreeModificationEvent) event)
                    .getTreeItem();
            TreeItem<Integer> child1 = parent.getChildren().get(0);
            child1.getChildren().setAll(iniModel.nxtLevel(child1, 0),
                    iniModel.nxtLevel(child1, 1));
            TreeItem<Integer> child2 = parent.getChildren().get(1);
            child2.getChildren().setAll(iniModel.nxtLevel(child2, 0),
                    iniModel.nxtLevel(child2, 1));
        }
    }
}
