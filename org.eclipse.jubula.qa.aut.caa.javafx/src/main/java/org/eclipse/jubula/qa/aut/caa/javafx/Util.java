package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.HashMap;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.IndexedCell;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 * Util class for test panes
 */
public class Util {
    
    /**
     * Constructor
     */
    private Util() {
        // private
    }
    
    /**
     * Adds the D&D handler to a given node
     * 
     * @param node
     *            the node
     * @param tPane
     *            for convenience
     */
    public static void addDnDHandler(final Node node,
            final TestPane tPane) {

        node.setOnDragDetected(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                Dragboard db = node.startDragAndDrop(
                        TransferMode.COPY_OR_MOVE);
                db.setDragView(node.snapshot(null, null));
                HashMap<DataFormat, Object> map =
                        new HashMap<DataFormat, Object>();
                if (node instanceof IndexedCell) {
                    IndexedCell<?> cell = (IndexedCell<?>) node;
                    map.put(DataFormat.PLAIN_TEXT, 
                            cell.getItem() + " [index="
                                    + cell.getIndex() + "]\n");
                } else {
                    map.put(DataFormat.PLAIN_TEXT, 
                            " [node=" + node.getId() + "]\n");
                }
                db.setContent(map);
            }

        });

        node.setOnDragEntered(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        node.setOnDragExited(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });

        node.setOnDragOver(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        node.setOnDragDropped(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                String text = (String) db.getContent(DataFormat.PLAIN_TEXT);
                if (node instanceof IndexedCell) {
                    IndexedCell<?> cell = (IndexedCell<?>) node;
                    tPane.setActionText("Drag source: " + text
                            + " Drop target: " + cell.getItem()
                            + " [index=" + cell.getIndex() + "]");
                } else {
                    tPane.setActionText("Drag source: " + text
                            + " Drop target: " + " [node=" + node.getId()
                            + "]");
                }
                event.setDropCompleted(true);
            }
        });
    }
}
