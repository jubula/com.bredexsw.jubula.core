/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.jubula.qa.aut.caa.javafx.components;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;

/**
 * @author BREDEX GmbH
 *
 */
public class MyControlSkin extends SkinBase<MyControl> {
    
    /** available colors */
    private List<Paint> m_colors = new ArrayList<Paint>();
    
    /** color index */
    private int m_idx = 0;

    /**
     * @param control the control
     */
    protected MyControlSkin(MyControl control) {
        super(control);
        m_colors.add(Color.web("red"));
        m_colors.add(Color.web("orange"));
        m_colors.add(Color.web("yellow"));
        m_colors.add(Color.web("green"));
        m_colors.add(Color.web("blue"));
        m_colors.add(Color.web("black"));
        fillNode();
    }

    /** fills the skin node */
    private void fillNode() {
        BorderPane background = new BorderPane();
        background.setBottom(new Label("I am an instance of the '"
                + getSkinnable().getClass().getName() + "' class."));
        final Circle circle = new Circle(200, Color.web("white", 0));
        changeColor(circle);
        circle.setStrokeType(StrokeType.OUTSIDE);
        circle.setStrokeWidth(20f);
        background.setOnMouseClicked(e -> {
            changeColor(circle);
        });
        background.setCenter(circle);
        getChildren().clear();
        getChildren().add(background);
    }

    /**
     * @param circle the circle
     */
    private void changeColor(Circle circle) {
        if (m_idx == m_colors.size()) {
            m_idx = 0;
        }
        circle.setStroke(m_colors.get(m_idx++));
    }

    /** {@inheritDoc} */
    public void dispose() {
        // nothing
    }

}
