/*******************************************************************************
 * Copyright (c) 2015 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rcp.gef.identifier;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jubula.rc.rcp.e3.gef.identifier.IEditPartIdentifier;
/**
 * @author BREDEX GmbH
 */
public class IdentifierAdapterFactory implements IAdapterFactory {
    /** classes for which this factory provides adapters */
    private static final Class<?>[] ADAPTABLE_TYPES = 
            new Class[] { AbstractGraphicalEditPart.class };
    
    /**
     * {@inheritDoc}
     */
    public Object getAdapter(Object adaptableObject, Class adapterType) {
        if (adapterType == IEditPartIdentifier.class) {
            if (adaptableObject instanceof AbstractGraphicalEditPart) {
                return new ShapeEditPartIdentifier(
                        (AbstractGraphicalEditPart) adaptableObject);
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public Class<?>[] getAdapterList() {
        return ADAPTABLE_TYPES;
    }

}
