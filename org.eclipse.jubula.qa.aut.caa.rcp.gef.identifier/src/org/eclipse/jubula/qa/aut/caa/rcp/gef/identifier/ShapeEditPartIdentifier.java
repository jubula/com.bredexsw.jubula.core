package org.eclipse.jubula.qa.aut.caa.rcp.gef.identifier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jubula.rc.rcp.e3.gef.identifier.ClassCountEditPartIdentifier;
import org.eclipse.jubula.rc.rcp.e3.gef.identifier.IDirectionalEditPartAnchorIdentifier;
import org.eclipse.jubula.rc.rcp.e3.gef.identifier.IEditPartIdentifier;
/**
 * 
 * @author BREDEX GmbH
 */
public class ShapeEditPartIdentifier implements IEditPartIdentifier,
    IDirectionalEditPartAnchorIdentifier {

    /** the edit part */
    private AbstractGraphicalEditPart m_editPart;

    /** delegate for providing the identifier string */
    private ClassCountEditPartIdentifier m_delegate;
    
    /**
     * 
     * @param editPart the editPart to give an identifier
     */
    public ShapeEditPartIdentifier(AbstractGraphicalEditPart editPart) {
        m_editPart = editPart;
        m_delegate = new ClassCountEditPartIdentifier(editPart);
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, ConnectionAnchor> getIncomingConnectionAnchors() {
        Map<String, ConnectionAnchor> anchorMap = 
                new HashMap<String, ConnectionAnchor>();
        List<?> list = m_editPart.getTargetConnections();
        int index = 1;
        if (m_editPart instanceof NodeEditPart) {
            NodeEditPart nodeEditPart = (NodeEditPart) m_editPart;
            for (Object object : list) {
                if (object instanceof ConnectionEditPart) {
                    ConnectionEditPart connection = (ConnectionEditPart) object;
                    ConnectionAnchor anchor = nodeEditPart
                            .getTargetConnectionAnchor(connection);
                    anchorMap.put("target_" + index++, anchor);
                }
            }
        }
        return anchorMap;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, ConnectionAnchor> getOutgoingConnectionAnchors() {
        Map<String, ConnectionAnchor> anchorMap = 
                new HashMap<String, ConnectionAnchor>();
        List<?> list = m_editPart.getSourceConnections();
        int index = 1;
        if (m_editPart instanceof NodeEditPart) {
            NodeEditPart nodeEditPart = (NodeEditPart) m_editPart;
            for (Object object : list) {
                if (object instanceof ConnectionEditPart) {
                    ConnectionEditPart connection = (ConnectionEditPart) object;
                    ConnectionAnchor anchor = nodeEditPart
                            .getSourceConnectionAnchor(connection);
                    anchorMap.put("source_" + index++, anchor);
                }
            }
        }
        return anchorMap;
    }

    /**
     * {@inheritDoc}
     */
    public String getIdentifier() {
        return m_delegate.getIdentifier();
    }

    /**
     *  {@inheritDoc}
     */
    public Map<String, ConnectionAnchor> getConnectionAnchors() {
        Map<String, ConnectionAnchor> anchorMap = 
                new HashMap<String, ConnectionAnchor>();
        anchorMap.putAll(getIncomingConnectionAnchors());
        anchorMap.putAll(getOutgoingConnectionAnchors());
        return anchorMap;
    }

}
