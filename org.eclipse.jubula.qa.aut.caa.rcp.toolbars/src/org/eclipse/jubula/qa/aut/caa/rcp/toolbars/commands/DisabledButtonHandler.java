package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands;

/**
 * Handler for disabled buttons.
 */
public class DisabledButtonHandler extends ButtonHandler {

    /**
     * @return false.
     */
    public boolean isEnabled() {
        return false;
    }


}
