package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands;

import org.eclipse.core.commands.ExecutionEvent;

/**
 * Handler for pull down buttons to show a meaningful description.
 */
public class PullDownHandler extends ButtonHandler {

    /**
     * @param event The event.
     * @return "PullDownButton".
     */
    protected String getStatusText(ExecutionEvent event) {
        return "PullDownButton";
    }

}
