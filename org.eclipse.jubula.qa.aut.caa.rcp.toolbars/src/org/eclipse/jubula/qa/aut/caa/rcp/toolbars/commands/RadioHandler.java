package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;

/**
 * Handler for radio buttons to show the selected button in the status bar.
 */
public class RadioHandler extends ButtonHandler {

    /**
     * @param event The event.
     * @return null
     * @throws ExecutionException An exception...
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
//        if (HandlerUtil.matchesRadioState(event)) {
            //return null; // already in updated state
        String currentState = event.getParameter(RadioState.PARAMETER_ID);
        HandlerUtil.updateRadioState(event.getCommand(), currentState);
//        }
        updateStatusText(event);
        return null;
    }

    /**
     * @param event The event.
     * @return A description of the radio button state.
     */
    protected String getStatusText(ExecutionEvent event) {
        String currentState = event.getParameter(RadioState.PARAMETER_ID);
        return currentState;
    }

}
