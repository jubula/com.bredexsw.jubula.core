package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Handler for toggle buttons to show the state in the status bar.
 */
public class ToggleHandler extends ButtonHandler {

    /** True, if the button is checked, otherwise false. */
    private boolean m_isChecked;

    /**
     * @param event The event.
     * @return null
     * @throws ExecutionException An exception...
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        Command command = event.getCommand();
        m_isChecked = !HandlerUtil.toggleCommandState(command);
        updateStatusText(event);
        return null;
    }

    /**
     * @param event The event.
     * @return A description of the radio button state.
     */
    protected String getStatusText(ExecutionEvent event) {
        return super.getStatusText(event) + ": " + m_isChecked;
    }

}
