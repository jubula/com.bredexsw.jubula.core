package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.perspective;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Definition of the tool bar perspective.
 */
public class Perspective implements IPerspectiveFactory {

    /** The percentage of the view. */
    private static final float VIEW_PERCENTAGE = 0.5f;

    /**
     * @param layout The given layout.
     */
    public void createInitialLayout(final IPageLayout layout) {
        layout.setEditorAreaVisible(false);
        layout.addView(
                "org.eclipse.jubula.qa.aut.caa.rcp.toolbars.view",
                IPageLayout.LEFT,
                VIEW_PERCENTAGE,
                layout.getEditorArea());
    }

}
