/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 *
 * @author tobias
 * @created 31.08.2011
 * @version $Revision: 1.1 $
 */
public abstract class AbstractApplicationWindow extends ApplicationWindow {

    /**
     * @param parentShell parent Shell
     *
     */
    public AbstractApplicationWindow(Shell parentShell) {
        super(parentShell);
    }
    
    /**
     * 
     */
    public void openWindow() {
        setBlockOnOpen(true);
        getParentShell().setVisible(false);
        open();
        getParentShell().setVisible(true);
//        Display.getCurrent().dispose();
    }
    
    /**
     * {@inheritDoc}
     */
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        
        shell.setText(getWindowTitle());
        shell.setSize(300, 300);
    }
    
    /**
     * {@inheritDoc}
     */
    protected Control getContents() {
        return null;
    }
    
    /**
     * Returns the shell to open.
     * 
     * @param parent the Composite
     * @return Control to open
     */
    protected Control createContents(Composite parent) {
//        RowLayoutFactory.swtDefaults().create();
//        RowDataFactory.swtDefaults().create();
        
        SashForm sash = new SashForm(parent, SWT.VERTICAL);
        sash.SASH_WIDTH = 2;
        
        Button close = new Button(sash, SWT.PUSH | SWT.WRAP);
        
        close.setLayoutData(
                GridDataFactory.swtDefaults().grab(true, true).create());
        close.setText("Close"); //$NON-NLS-1$
        close.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
//                getParentShell().setVisible(true);
                close();
            }
        });

        
        ScrolledComposite sc = new ScrolledComposite(sash, SWT.H_SCROLL
            | SWT.V_SCROLL);

        Composite child = new Composite(sc, SWT.NONE);
        child.setLayout(new FillLayout(SWT.VERTICAL));
        
        makeContent(child);
        
        sc.setContent(child);
        sc.setExpandHorizontal(true);
        sc.setExpandVertical(true);
        
        sash.setWeights(new int[] { 1, 5});
        
        return sash;
    }
    
    /**
     * @param parent parent Composite
     * @return parent
     */
    protected abstract Composite makeContent(Composite parent);
    
    /**
     * @return window Title
     */
    protected abstract String getWindowTitle();
    
}
