/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels.ListViewerEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 *
 * @author tobias
 * @created 05.09.2011
 * @version $Revision: 1.1 $
 */
public class ListViewerOpener extends AbstractApplicationWindow {

    /** m_filter */
    private NumberFilter m_filter = new NumberFilter();
    
    /**
     * @param parentShell parent Shell
     */
    public ListViewerOpener(Shell parentShell) {
        super(parentShell);
    }

    /**
     * {@inheritDoc}
     */
    protected Composite makeContent(Composite parent) {
     // Add a checkbox to toggle filter
        parent.setLayout(new RowLayout(SWT.VERTICAL));
        Button filterNumbers = new Button(parent, SWT.CHECK);
        filterNumbers.setText("&Show entrys with numbers"); //$NON-NLS-1$
        
        final ListViewer lv = new ListViewer(parent);
        lv.setContentProvider(new EntryContentProvider());
        lv.setLabelProvider(new EntryLabelProvider());
        lv.setInput(MockModels.DEFAULT_LIST_MODEL);
        
        filterNumbers.addSelectionListener(new SelectionAdapter() {
            @SuppressWarnings("synthetic-access")
            public void widgetSelected(SelectionEvent event) {
                if (((Button)event.widget).getSelection()) {
                    lv.addFilter(m_filter);
                } else {
                    lv.removeFilter(m_filter);
                }
            }
        });
        return parent;
    }

    /**
     * {@inheritDoc}
     */
    protected String getWindowTitle() {
        return ListViewer.class.getCanonicalName();
    }

    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    class NumberFilter extends ViewerFilter {
        /**
         * {@inheritDoc}
         */
        public boolean select(Viewer arg0, Object arg1, Object arg2) {
            return ((ListViewerEntry)arg2).isNumber();
        }
    }
    
    
    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    class EntryLabelProvider implements ILabelProvider {

        /**
         * {@inheritDoc}
         */
        public Image getImage(Object arg0) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        public String getText(Object arg0) {
            return ((MockModels.ListViewerEntry)arg0).getName();
        }

        /**
         * {@inheritDoc}
         */
        public void addListener(ILabelProviderListener arg0) {
          // Throw it away
        }

        /**
         * Disposes any resources
         */
        public void dispose() {
          // Nothing to dispose
        }

        /**
         * {@inheritDoc}
         */
        public boolean isLabelProperty(Object arg0, String arg1) {
            return false;
        }

        /**
         * {@inheritDoc}
         */
        public void removeListener(ILabelProviderListener arg0) {
          // Ignore
        }
    }
    
    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    class EntryContentProvider implements IStructuredContentProvider {

        /**
         * {@inheritDoc}
         */
        @SuppressWarnings("unchecked")
        public Object[] getElements(Object arg0) {
            return ((List<MockModels.ListViewerEntry>)arg0).toArray();
        }

        /**
         * Disposes any created resources
         */
        public void dispose() {
            // Do nothing
        }

        /**
         * {@inheritDoc}
         */
        public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
          // Do nothing
        }
    }
}
