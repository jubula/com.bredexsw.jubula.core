/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * 
 *
 * @author tobias
 * @created 31.08.2011
 * @version $Revision: 1.1 $
 */
public class TableViewerOpener extends AbstractApplicationWindow {

    /**
     * @param parentShell parent Shell to set visible
     */
    public TableViewerOpener(Shell parentShell) {
        super(parentShell);
    }

    /**
     * {@inheritDoc}
     */
    protected Composite makeContent(Composite parent) {
        TableViewer tableViewer = new TableViewer(parent, SWT.MULTI);
        Table table = tableViewer.getTable();
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        
        char anfang = 'A'; 
        for (int i = 0; i < 20; i++) {
            TableColumn column1 = new TableColumn(table, SWT.NONE);
            column1.setText(String.valueOf(anfang));
            column1.setWidth(61);
            anfang++;
        }
        for (int i = 1; i < 21; i++) {
            anfang = 'A';
            TableItem item1 = new TableItem(table, SWT.NONE);
            for (int j = 0; j < 20; j++) {
                item1.setText(j, anfang + String.valueOf(i));
                anfang++;
            }
        }

        addTableRightAlignColumn(table);
        
        return parent;
    }

    /**
     * @param table for a new column
     */
    private void addTableRightAlignColumn(Table table) {
        int index = table.getColumnCount();
        TableColumn column = new TableColumn (table, SWT.RIGHT, index);
        column.setText (I18NUtils.getString("rightAlignColName")); //$NON-NLS-1$
        TableItem [] items = table.getItems ();
        String rl = I18NUtils.getString("textRightAlign"); //$NON-NLS-1$
        for (int i = 0; i < items.length; i++) {
            items[i].setText(index, rl + (i + 1));
        }
        column.pack();
    }
    
    /**
     * {@inheritDoc}
     */
    protected String getWindowTitle() {
        return TableViewer.class.getCanonicalName();
    }

}
