package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.jface.window.ToolTip;
import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 * @author BREDEX GmbH
 */
@SuppressWarnings("nls")
public class ToolTipOpener extends AbstractApplicationWindow {

    /**
     * 
     * @param parentShell
     *            parent
     */
    public ToolTipOpener(Shell parentShell) {
        super(parentShell);
    }

    /**
     * {@inheritDoc}
     */
    protected Composite makeContent(Composite parent) {
        // default ToolTip
        Label defaultToolTip = new Label(parent, SWT.NONE);
        CompUtils.setComponentName(defaultToolTip,
                ComponentNameConstants.TESTPAGE_TOOLTIP_DEFAULT);
        defaultToolTip.setText(I18NUtils.getName("defaultToolTipLabel"));
        defaultToolTip.pack();
        DefaultToolTip toolTip = new DefaultToolTip(defaultToolTip);
        toolTip.setText(I18NUtils.getString("defaultToolTip"));

        // default ToolTip
        Label defaultToolTipDelay = new Label(parent, SWT.NONE);
        CompUtils.setComponentName(defaultToolTipDelay,
                ComponentNameConstants.TESTPAGE_TOOLTIP_DEFAULT);
        defaultToolTipDelay .setText(I18NUtils
                .getName("defaultToolTipDelayLabel"));
        defaultToolTipDelay .pack();
        DefaultToolTip toolTipDelay  = new DefaultToolTip(defaultToolTipDelay);
        toolTipDelay .setText(I18NUtils.getString("defaultToolTipDelay"));
        toolTipDelay.setPopupDelay(4000);
        toolTipDelay.setHideDelay(400000);
        
        
        // customToolTip
        Label customToolTipLabel = new Label(parent, SWT.NONE);
        CompUtils.setComponentName(customToolTipLabel,
                ComponentNameConstants.TESTPAGE_TOOLTIP_CUSTOM);
        
        customToolTipLabel.setText(I18NUtils.getName("customToolTipLabel"));
        customToolTipLabel.pack();
        new ToolTip(customToolTipLabel) {

            @Override
            protected Composite createToolTipContentArea(Event event,
                    Composite parent) {
                Label label = new Label(parent, SWT.NONE);
                CompUtils.setComponentName(label,
                        ComponentNameConstants.TESTPAGE_TOOLTIP_CUSTOM_INTERN);
                label.setText(I18NUtils.getName("customToolTip"));
                Button button = new Button(parent, SWT.NONE);
                button.setText(I18NUtils.getName("btn1"));
                CompUtils.setComponentName(button, 
                        ComponentNameConstants.TESTPAGE_TOOLTIP_CUSTOM_BUTTON);
                return parent;
            }
        };
        
        Label noDefaultListener = new Label(parent, SWT.NONE);
        noDefaultListener.setText(I18NUtils.getName("clickToolTipLabel"));
        CompUtils.setComponentName(noDefaultListener,
                ComponentNameConstants.TESTPAGE_TOOLTIP_CUSTOM_LISTENER);
        final DefaultToolTip manualToolTip = 
                new DefaultToolTip(noDefaultListener, SWT.NONE, true);
        manualToolTip.setText(I18NUtils.getString("clickToolTip"));
        noDefaultListener.addMouseListener(new MouseListener() {
            
            public void mouseUp(MouseEvent e) {
                // not used
            }
            
            public void mouseDown(MouseEvent e) {
                manualToolTip.show(new Point(e.x, e.y));
            }
            
            public void mouseDoubleClick(MouseEvent e) {
                // not used
            }
        });
        return parent;
    }

    /**
     * {@inheritDoc}
     */
    protected String getWindowTitle() {
        return this.getClass().getCanonicalName();
    }

}
