/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * @author tobias
 * @created 14.09.2011
 * @version $Revision: 1.1 $
 */
public class MockModels {

    /** default tree */
    public static final MockModels.Node DEFAULT_TREE_MODEL;
    
    /** default combo list*/
    public static final MockModels.ComboViewerEntry[] DEFAULT_COMBO_LIST_MODEL;
    
    /** m_entrys */
    public static final List<MockModels.ListViewerEntry> DEFAULT_LIST_MODEL;
    
    static {
        Node n = new Node("node"); //$NON-NLS-1$
        Node node0 = new Node(I18NUtils.getString("text1")); //$NON-NLS-1$
        n.addChildren(node0);
        node0.addChildren(new Node(I18NUtils.getString("text2"))); //$NON-NLS-1$
        Node node2 = new Node(I18NUtils.getString("text3")); //$NON-NLS-1$
        node0.addChildren(node2);
        Node node21 = new Node(I18NUtils.getString("text4")); //$NON-NLS-1$
        node21.addChildren(new Node(I18NUtils.getString("text7"))); //$NON-NLS-1$
        node21.addChildren(new Node(I18NUtils.getString("text2"))); //$NON-NLS-1$
        node2.addChildren(node21);
        node2.addChildren(new Node(I18NUtils.getString("text5"))); //$NON-NLS-1$
        Node node3 = new Node(I18NUtils.getString("text6")); //$NON-NLS-1$
        node0.addChildren(node3);
        node3.addChildren(new Node(I18NUtils.getString("text1"))); //$NON-NLS-1$
        node0.addChildren(new Node(I18NUtils.getString("text7"))); //$NON-NLS-1$
        
        DEFAULT_TREE_MODEL = n;
        
        DEFAULT_COMBO_LIST_MODEL = new ComboViewerEntry[] { 
            new ComboViewerEntry(I18NUtils.getString("text1")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text2")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text3")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text4")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text5")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text6")), //$NON-NLS-1$
            new ComboViewerEntry(I18NUtils.getString("text7")) }; //$NON-NLS-1$
        
        DEFAULT_LIST_MODEL = new ArrayList<ListViewerEntry>();
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text1"), //$NON-NLS-1$
                false));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text2"), //$NON-NLS-1$
                false));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text3"), //$NON-NLS-1$
                false));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text4"), //$NON-NLS-1$
                false));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text5"), //$NON-NLS-1$
                true));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text6"), //$NON-NLS-1$
                false));
        DEFAULT_LIST_MODEL.add(new ListViewerEntry(I18NUtils.getString("text7"), //$NON-NLS-1$
                true));
    }
    
    /**
     * @author tobias
     * @created 15.09.2011
     * @version $Revision: 1.1 $
     */
    public static class Node {


        /** name for node */
        private final String m_name;

        /** list of children*/
        private final List<Node> m_children;

        /**
         * @param name name 
         */
        public Node(String name) {
            m_name = name;
            m_children = new ArrayList<MockModels.Node>();
        }

        /**
         * @param n node
         */
        public void addChildren(Node n) {
            getChildren().add(n);
        }

        /**
         * @return the name
         */
        public String getName() {
            return m_name;
        }

        /**
         * @return the children
         */
        public List<Node> getChildren() {
            return m_children;
        }

    }
    
    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    public static class ComboViewerEntry {
        /** m_name */
        private String m_name;

        /**
         * @param name
         *            name
         */
        public ComboViewerEntry(String name) {
            m_name = name;

        }

        /**
         * @return name
         */
        public String getName() {
            return m_name;
        }

        /**
         * @param name
         *            name
         */
        public void setName(String name) {
            m_name = name;
        }
    }
    
    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    public static class ListViewerEntry {
        /** m_name */
        private String m_name;

        /** m_number */
        private boolean m_number;

        /**
         * @param name  name
         * @param number number
         */
        public ListViewerEntry(String name, boolean number) {
            this.m_name = name;
            this.m_number = number;
        }

        /**
         * @return boolean
         */
        public boolean isNumber() {
            return m_number;
        }

        /**
         * @return String
         */
        public String getName() {
            return m_name;
        }
    }
    
    /**
     * private constructor for checkstye
     */
    private MockModels() {
        //empty
    }
}
