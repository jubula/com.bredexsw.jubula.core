package org.eclipse.jubula.qa.aut.caa.rp.jface.view;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.ToolTip;
import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.rp.jface.jface.ComboViewerOpener;
import org.eclipse.jubula.qa.aut.caa.rp.jface.jface.ListViewerOpener;
import org.eclipse.jubula.qa.aut.caa.rp.jface.jface.TableViewerOpener;
import org.eclipse.jubula.qa.aut.caa.rp.jface.jface.ToolTipOpener;
import org.eclipse.jubula.qa.aut.caa.rp.jface.jface.TreeViewerOpener;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.part.ViewPart;

/**
 * 
 *
 * @author tobias
 * @created 25.08.2011
 * @version $Revision: 1.1 $
 */
public class View extends ViewPart {

    /**
     * <code>ID</code>
     */
    public static final String ID = "org.eclipse.jubula.qa.aut.caa.rp.jface.view"; //$NON-NLS-1$

    /**
     * This is a callback that will allow us to create the viewer and initialize
     * it.
     * @param parent composite
     */
    public void createPartControl(Composite parent) {
        
        parent.setLayout(new RowLayout(SWT.VERTICAL));
        
        addTableButton(parent, getSite().getShell());
        addTreeButton(parent, getSite().getShell());
        addListButton(parent, getSite().getShell());
        addComboButton(parent, getSite().getShell());
        addToolTipButton(parent, getSite().getShell());
    }


    /**
     * @param c composite
     * @param shell main shell
     */
    private void addTableButton(Composite c, final Shell shell) {
        Button btnApp = new Button(c, SWT.PUSH);
        btnApp.setText(TableViewer.class.getSimpleName());
        CompUtils.setComponentName(btnApp,
                ComponentNameConstants.MAINPAGE_JFACE_TABLE_TESTPAGE);
        btnApp.setLayoutData(new RowData(120, 25));
        btnApp.setLocation(110, 130);
        btnApp.addListener(SWT.Selection, new Listener() {
            
            public void handleEvent(Event e) {
                new TableViewerOpener(shell).openWindow();
            }
        });
    }
    
    /**
     * @param c composite
     * @param shell main shell
     */
    private void addTreeButton(Composite c, final Shell shell) {
        Button btnApp = new Button(c, SWT.PUSH);
        btnApp.setText(TreeViewer.class.getSimpleName());
        CompUtils.setComponentName(btnApp,
                ComponentNameConstants.MAINPAGE_JFACE_TREE_TESTPAGE);
        btnApp.setLayoutData(new RowData(120, 25));
        btnApp.addListener(SWT.Selection, new Listener() {
            
            public void handleEvent(Event e) {
                new TreeViewerOpener(shell).openWindow();
            }
        });
    }
    
    /**
     * @param c composite
     * @param shell main shell
     */
    private void addListButton(Composite c, final Shell shell) {
        Button btnApp = new Button(c, SWT.PUSH);
        btnApp.setText(ListViewer.class.getSimpleName());
        CompUtils.setComponentName(btnApp,
                ComponentNameConstants.MAINPAGE_JFACE_LIST_TESTPAGE);
        btnApp.setLayoutData(new RowData(120, 25));
        btnApp.addListener(SWT.Selection, new Listener() {
            
            public void handleEvent(Event e) {
                new ListViewerOpener(shell).openWindow();
            }
        });
    }
    
    /**
     * @param c composite
     * @param shell main shell
     */
    private void addComboButton(Composite c, final Shell shell) {
        Button btnApp = new Button(c, SWT.PUSH);
        btnApp.setText(ComboViewer.class.getSimpleName());
        CompUtils.setComponentName(btnApp,
                ComponentNameConstants.MAINPAGE_JFACE_COMBO_TESTPAGE);
        btnApp.setLayoutData(new RowData(120, 25));
        btnApp.addListener(SWT.Selection, new Listener() {
            
            public void handleEvent(Event e) {
                new ComboViewerOpener(shell).openWindow();
            }
        });
    }

    /**
     * 
     * @param parent parent composite
     * @param shell main shell
     */
    private void addToolTipButton(Composite parent, final Shell shell) {
        Button btnApp = new Button(parent, SWT.PUSH);
        btnApp.setText(ToolTip.class.getSimpleName());
        CompUtils.setComponentName(btnApp,
                ComponentNameConstants.MAINPAGE_JFACE_TOOLTIP_TESTPAGE);
        btnApp.setLayoutData(new RowData(120, 25));
        btnApp.addListener(SWT.Selection, new Listener() {
            
            public void handleEvent(Event e) {
                new ToolTipOpener(shell).openWindow();
            }
        });
    }
    
    /**
     * Passing the focus request to the viewer's control.
     */
    public void setFocus() {
        //empty
    }

}