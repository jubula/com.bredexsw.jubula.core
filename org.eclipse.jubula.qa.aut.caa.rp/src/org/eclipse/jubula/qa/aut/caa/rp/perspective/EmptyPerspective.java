/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.perspective;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * 
 *
 * @author tobias
 * @created Feb 11, 2011
 * @version $Revision: 1.1 $
 */
public class EmptyPerspective implements IPerspectiveFactory {

    /**
     * {@inheritDoc}
     */
    public void createInitialLayout(IPageLayout layout) {
        layout.setEditorAreaVisible(false);
    }

}
