package org.eclipse.jubula.qa.aut.caa;

import org.eclipse.jubula.qa.aut.caa.swing.StartFrame;

/**
 * Mainclass to start the Swing AUT.
 */
public class StartSwingAUT {

    /**
     * 
     */
    private static StartFrame startFrame;
    /**
     * private Constructor
     */
    private StartSwingAUT() {
        // empty
    }
    


    /** CaA_AUT Open
     * 
     */
    public static void autStart() {
        startFrame.setVisible(true);
    }

    /**CaA_AUT Close
     * 
     */
    public static void autClose() {
        startFrame.dispose();

    }
    /**
     * @param args
     *            args
     */
    public static void main(String[] args) {
        startFrame = new StartFrame();
        autStart();
    }
}
