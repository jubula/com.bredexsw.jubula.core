package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import org.eclipse.jubula.qa.aut.caa.StartSwingAUT;

/**
 * AbstractFrameOpener
 * 
 * @author markus
 * @created 19.03.2008
 */
public abstract class AbstractFrameOpener implements ActionListener {
   
    /**
     * Opens the JFrame;
     * 
     * @param arg0
     *            arg0
     */
    public void actionPerformed(ActionEvent arg0) {
       // StartSwingAUT start = new StartSwingAUT();
        final AbstractTestFrame frame = getFrame();
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        StartSwingAUT.autClose();
    }

    /**
     * Returns the JFrame to open.
     * 
     * @return frame to open
     */
    protected abstract AbstractTestFrame getFrame();

}
