package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame
 * 
 */
public class ApplicationFrameOpener extends AbstractFrameOpener {
    /**
     * @author BREDEX GmbH
     */
    private static class ProperAUTTerminator implements ActionListener {
        /** {@inheritDoc} */
        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }
    
    /**
     * listener to properly terminate the AUT
     */
    private ActionListener m_terminateAUT = new ProperAUTTerminator();
    
    /** {@inheritDoc} */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_application"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_APPLICATION);

        // exit menu
        JMenuBar exitMenuBar = new JMenuBar();
        exitMenuBar.add(createAUTExitMenu());
        
        // textField 1
        JTextField tf1 = new JTextField();
        tf1.setName(ComponentNameConstants.TESTPAGE_APPLICATION_TF01);
        tf1.setColumns(15);
        tf1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils
                .addContextListenerToComponent(tf1, frame.getlabel());

        // exit button
        JButton exitBtn = new JButton(I18NUtils.getName("exit")); //$NON-NLS-1$
        exitBtn.setName(ComponentNameConstants.TESTPAGE_APPLICATION_EXIT_BTN);
        exitBtn.addActionListener(m_terminateAUT);
        
        frame.getPnlContent().add(tf1, 
                SwingComponentUtils.getConstraints(0, 0));
        frame.getPnlContent().add(exitBtn, 
                SwingComponentUtils.getConstraints(0, 1));
        
        
        frame.setJMenuBar(exitMenuBar);

        return frame;
    }

    /**
     * @return the menu to properly exit the AUT
     */
    private JMenu createAUTExitMenu() {
        JMenu menu = new JMenu(I18NUtils.getString("aut"));
        JMenuItem exit = new JMenuItem(I18NUtils.getString("exit")); //$NON-NLS-1$
        exit.addActionListener(m_terminateAUT);
        menu.add(exit);
        return menu;
    }

}
