package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JButtons to test.
 * 
 * JButton 1: enabled = true tooltip = true contextmenu = true
 * 
 * JButton 2: enabled = false
 * 
 */
public class ButtonFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_buttons"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);
        // add components to test
        
        // button 1
        final JButton btn1 = new JButton(I18NUtils.getName("btn1")); //$NON-NLS-1$
        btn1.setName(ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        btn1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(btn1,
                frame.getlabel());
        frame.getPnlContent().add(btn1,
                SwingComponentUtils.getConstraints(0, 0));
        
        // button 2
        JButton btn2 = new JButton(I18NUtils.getName("btn2")); //$NON-NLS-1$
        btn2.setName(ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        btn2.setEnabled(false);
        frame.getPnlContent().add(btn2,
                SwingComponentUtils.getConstraints(0, 1));

        // button 3
        final JButton btn3 = new JButton(I18NUtils.getName("btn3")); //$NON-NLS-1$
        btn3.setName(ComponentNameConstants.TESTPAGE_BUTTONS_BTN03);
        btn3.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(btn3,
                frame.getlabel());
        btn3.setVisible(false);
        
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(90, 33));
        panel.add(btn3);
        
        frame.getPnlContent().add(panel,
                SwingComponentUtils.getConstraints(0, 2));
        
        Thread t1 = new Thread() {
            public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // ignore
                    }
                btn3.setVisible(true);
                Thread t2 = new Thread() {
                    public void run() {
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            // ignore
                        }
                        btn3.setVisible(false);
                    }
                };
                t2.start();
            }
        };
        t1.start();
        
        return frame;
    }

}
