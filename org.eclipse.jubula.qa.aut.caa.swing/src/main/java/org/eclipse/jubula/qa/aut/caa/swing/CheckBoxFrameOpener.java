package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.JCheckBox;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JCheckBoxes to test.
 * 
 * JCheckBox 1: enabled = true tooltip = true contextmenu = true
 * 
 * JCheckBox 2: enabled = false
 */
public class CheckBoxFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_checkBoxes"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_CHECKBOXES);
        // add components to test

        // checkbox 1
        JCheckBox cbx1 = new JCheckBox(I18NUtils.getName("cbx1")); //$NON-NLS-1$
        cbx1.setName(ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX01);
        cbx1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(cbx1,
                frame.getlabel());
        frame.getPnlContent().add(cbx1,
                SwingComponentUtils.getConstraints(0, 0));

        // checkbox 2
        JCheckBox cbx2 = new JCheckBox(I18NUtils.getName("cbx2")); //$NON-NLS-1$
        cbx2.setName(ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX02);
        cbx2.setEnabled(false);
        frame.getPnlContent().add(cbx2,
                SwingComponentUtils.getConstraints(0, 1));

        return frame;
    }

}
