package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.ListCellRenderer;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JComboBoxes to test.
 * 
 * JComboBox 1: enabled = true editable=true tooltip = true contextmenu = true
 * (does not work, maybe a PopupMenuListener is better?)
 * 
 * JComboBox 2: enabled = false
 * 
 * JComboBox 3: enabled = true editable = false
 */
public class ComboBoxFrameOpener extends AbstractFrameOpener {
    /**
     * @author markus
     * @created Sep 20, 2010
     */
    protected class JSpinnerListCellRenderer extends JSpinner implements
        ListCellRenderer {
        /**
         * {@inheritDoc}
         */
        public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
            setValue(value);
            return this;
        }
    }

    /**
     * @author markus
     * @created Sep 20, 2010
     */
    protected class Testable1JSpinnerListCellRenderer extends
        JSpinnerListCellRenderer {
        /**
         * @return the testable text
         */
        public String getTestableText() {
            return "getTestableText() " + getValue().toString(); //$NON-NLS-1$
        }
    }

    /**
     * @author markus
     * @created Sep 20, 2010
     */
    protected class Testable2JSpinnerListCellRenderer extends
        JSpinnerListCellRenderer {
        /**
         * @return the testable text
         */
        public String getText() {
            return "getText() " + getValue().toString(); //$NON-NLS-1$
        }
    }
    
    /**
     * @author markus
     * @created Sep 20, 2010
     */
    protected class DerivedTestable1JSpinnerListCellRenderer extends
        Testable1JSpinnerListCellRenderer {
        // empty
    }

    /**
     * @author markus
     * @created Sep 20, 2010
     */
    protected class DerivedTestable2JSpinnerListCellRenderer extends
        Testable2JSpinnerListCellRenderer {
        // empty
    }
    
    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_comboBoxes"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_COMBOBOXES);
        // add components to test

        // comboBox 1
        final JComboBox cmbx1 = new JComboBox(ComponentUtils.getListItems());
        cmbx1.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX01);
        cmbx1.setEditable(true);
        cmbx1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        MouseAdapter handler = new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getButton() == MouseEvent.BUTTON3) {
                    SwingComponentUtils.getPopup().show(evt.getComponent(),
                            evt.getX(), evt.getY());
                }
            }
        };
        Component c[] = cmbx1.getComponents();
        for (int i = 0; i < c.length; i++) {
            c[i].addMouseListener(handler);
        }
        SwingComponentUtils.addContextListenerToComponent(cmbx1,
                frame.getlabel());
        frame.getPnlContent().add(cmbx1,
                SwingComponentUtils.getConstraints(0, 0));

        // comboBox 2
        JComboBox cmbx2 = new JComboBox(ComponentUtils.getListItems());
        cmbx2.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX02);
        cmbx2.setEnabled(false);
        frame.getPnlContent().add(cmbx2,
                SwingComponentUtils.getConstraints(0, 1));

        // comboBox 3
        JComboBox cmbx3 = new JComboBox(ComponentUtils.getListItems());
        cmbx3.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX03);
        frame.getPnlContent().add(cmbx3,
                SwingComponentUtils.getConstraints(0, 2));

        frame.getPnlContent().add(createCBX4(),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 3));

        frame.getPnlContent().add(
                createCBX5to9(new JSpinnerListCellRenderer()),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 4));

        frame.getPnlContent().add(
                createCBX5to9(new Testable1JSpinnerListCellRenderer()),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 5));

        frame.getPnlContent().add(
                createCBX5to9(new Testable2JSpinnerListCellRenderer()),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 6));

        frame.getPnlContent().add(
                createCBX5to9(new DerivedTestable1JSpinnerListCellRenderer()),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 7));

        frame.getPnlContent().add(
                createCBX5to9(new DerivedTestable2JSpinnerListCellRenderer()),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 8));
        frame.getPnlContent().add(createCBX10(),
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 9));
        
        return frame;
    }

    /**
     * @return the 4th combobox
     *  s. ticket #3013
     */
    private Component createCBX4() {
        JComboBox cmbx4 = new JComboBox(ComponentUtils.getListLongItems());
        cmbx4.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX04);
        Dimension d = cmbx4.getPreferredSize();
        d.setSize(100, d.getHeight());
        cmbx4.setPreferredSize(d);
        return cmbx4;
    }
    
    /**
     * @param rendererToUse the renderer to use for these combo boxes
     * @return the 5th combobox
     *  s. ticket #2774
     */
    private Component createCBX5to9(ListCellRenderer rendererToUse) {
        JComboBox cmbx5 = new JComboBox(ComponentUtils.getNumberItems());
        cmbx5.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX05
                + rendererToUse.getClass().getName());
        cmbx5.setRenderer(rendererToUse);
        Dimension d = cmbx5.getPreferredSize();
        d.setSize(150, d.getHeight());
        cmbx5.setPreferredSize(d);
        return cmbx5;
    }
    
    /**
     * 
     * @return the 10th {@link JComboBox}
     */
    private Component createCBX10() {
        final JComboBox cmbx = new JComboBox(
                ComponentUtils.getListModelItems());
        cmbx.setName(ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX06);
        Dimension d = cmbx.getPreferredSize();
        d.setSize(150, d.getHeight());
        cmbx.setPreferredSize(d);
        return cmbx;
    }
}
