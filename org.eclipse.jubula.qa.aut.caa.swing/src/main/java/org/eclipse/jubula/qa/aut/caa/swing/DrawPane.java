/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JComponent;
/**
 * CUstom component. Draw Pane. Guess what it does.
 *
 */
public class DrawPane extends JComponent {

    /** The image */
    private Image m_image;
    /** graphics */
    private Graphics2D m_graphics2D;
    /** coordinates for drag */
    private int m_currentX, m_currentY, m_oldX, m_oldY;

    /**
     * Create Component and atach listeners
     */
    public DrawPane() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                m_oldX = e.getX();
                m_oldY = e.getY();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                m_currentX = e.getX();
                m_currentY = e.getY();
                if (m_graphics2D != null) {
                    m_graphics2D.drawLine(m_oldX, m_oldY, m_currentX,
                            m_currentY);
                }
                repaint();
                m_oldX = m_currentX;
                m_oldY = m_currentY;
            }
        });
        addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (m_graphics2D != null) {
                    m_graphics2D.drawRect(e.getX(), e.getY(), 1, 1);
                }
                repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // NOTHING
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // NOTHING
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // NOTHING
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // NOTHING
            }
        });
    }

    /**
     * Paint the component.
     * 
     * @param g
     *            Graphics
     */
    public void paintComponent(Graphics g) {
        if (m_image == null) {
            m_image = createImage(200, 200);
            m_graphics2D = (Graphics2D) m_image.getGraphics();
            m_graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            clear();
        }
        g.drawImage(m_image, 0, 0, null);
    }

    /**
     * clear the painting
     */
    public void clear() {
        m_graphics2D.setPaint(Color.white);
        m_graphics2D.fillRect(0, 0, getSize().width, getSize().height);
        m_graphics2D.setPaint(Color.black);
        repaint();
    }
}