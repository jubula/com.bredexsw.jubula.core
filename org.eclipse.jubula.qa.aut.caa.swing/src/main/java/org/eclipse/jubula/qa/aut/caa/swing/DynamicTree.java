package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * A lazy created tree (loads additional nodes when expanded
 * 
 * @author janw
 *
 */
public class DynamicTree extends JTree {

    /**
     * Array to store the nodes in
     */
    private static DefaultMutableTreeNode nodes[] = {
        new DefaultMutableTreeNode(I18NUtils.getString("text1"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text2"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text3"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text6"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text7"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text4"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text5"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text1"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text7"), true), //$NON-NLS-1$
        new DefaultMutableTreeNode(I18NUtils.getString("text2"), true)}; //$NON-NLS-1$


    /**
     * Treemodel to provide Lazy Loading (Tree 6)
     * @author janw
     */
    public static class DynamicTreeModel implements TreeModel {
        
        /**
         * constructor adding four nodes to root node
         */
        public DynamicTreeModel() {
            nodes[0].add(nodes[1]);
            nodes[0].add(nodes[2]);
            nodes[0].add(nodes[3]);
            nodes[0].add(nodes[4]);
        }
        
        /** {@inheritDoc} */
        public Object getRoot()  {
            return nodes[0];
        }

        /** {@inheritDoc} */
        public int getChildCount(Object parent)  { 
            return ((DefaultMutableTreeNode)parent).getChildCount();
        }

        /** {@inheritDoc} */
        public Object getChild(Object parent, int index) {
            return ((DefaultMutableTreeNode)parent).getChildAt(index);
        }

        /** {@inheritDoc} */
        public int getIndexOfChild(Object parent, Object child) {
            return ((DefaultMutableTreeNode)parent).
                    getIndex((DefaultMutableTreeNode)child);
        }

        /** {@inheritDoc} */
        public boolean isLeaf(Object node) {
            DefaultMutableTreeNode ilNode = (DefaultMutableTreeNode) node;
            return !(ilNode.equals(nodes[0]) || ilNode.equals(nodes[2]) 
                    || ilNode.equals(nodes[3]) || ilNode.equals(nodes[5]));
        }

        /** {@inheritDoc} */
        public void valueForPathChanged(TreePath path, Object newValue) {
        }

        /** {@inheritDoc} */
        public void addTreeModelListener(TreeModelListener l) {
        }

        /** {@inheritDoc} */
        public void removeTreeModelListener(TreeModelListener l) {
        }
    }
    
    /**
     * Constructor
     */
    public DynamicTree() {
        super(new DynamicTreeModel());
        setName(ComponentNameConstants.TESTPAGE_TREES_TRE06);
        setToggleClickCount(1);
        
        // On expansion new nodes get added to the expanded node
        this.addTreeWillExpandListener(new TreeWillExpandListener() {
            public void treeWillExpand(TreeExpansionEvent event)
                throws ExpandVetoException {
                DefaultMutableTreeNode selectedNode =
                        (DefaultMutableTreeNode)event.getPath().
                            getLastPathComponent();
                if (selectedNode.getChildCount() == 0) {
                    if (selectedNode.equals(nodes[2])) {
                        selectedNode.add(nodes[5]);
                        selectedNode.add(nodes[6]);
                    } else if (selectedNode.equals(nodes[3])) {
                        selectedNode.add(nodes[7]);
                    } else if (selectedNode.equals(nodes[5])) {
                        selectedNode.add(nodes[8]);
                        selectedNode.add(nodes[9]);
                    }
                }
            }
            public void treeWillCollapse(TreeExpansionEvent event)
                throws ExpandVetoException {
            }
        });
    }
}
