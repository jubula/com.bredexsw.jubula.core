package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JEditorPane;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JEditorPanes to test.
 * 
 * JEditorPane 1: enabled = true tooltip = true contextmenu = true
 * 
 * JEditorPane 2: enabled = false text = false
 * 
 * JEditorPane 3: enabled = true editable = true text = true
 * 
 * JEditorPane 4: enabled = false text = true
 * 
 * JEditorPane 5: editable = false text = true
 * 
 */
public class EditorPaneFrameOpener extends AbstractFrameOpener {
    /**
     * the preferred size for editor panes
     */
    private static final Dimension PREFERRED_SIZE = new Dimension(105, 25);

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_editorPanes"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_EDITORPANES);
        // add components to test

        // editorPane 1
        JEditorPane ep1 = new JEditorPane();
        ep1.setName(ComponentNameConstants.TESTPAGE_EDITORPANES_EP01);
        ep1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils
                .addContextListenerToComponent(ep1, frame.getlabel());
        ep1.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(ep1, SwingComponentUtils.getConstraints(0, 0));

        // editorPane 2
        JEditorPane ep2 = new JEditorPane();
        ep2.setName(ComponentNameConstants.TESTPAGE_EDITORPANES_EP02);
        ep2.setEnabled(false);
        ep2.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(ep2, SwingComponentUtils.getConstraints(0, 1));

        // editorPane 3
        JEditorPane ep3 = new JEditorPane();
        ep3.setName(ComponentNameConstants.TESTPAGE_EDITORPANES_EP03);
        ep3.setText(I18NUtils.getString("text1")); //$NON-NLS-1$
        ep3.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(ep3, SwingComponentUtils.getConstraints(0, 2));

        // editorPane 4
        JEditorPane ep4 = new JEditorPane();
        ep4.setName(ComponentNameConstants.TESTPAGE_EDITORPANES_EP04);
        ep4.setEnabled(false);
        ep4.setText(I18NUtils.getString("text2")); //$NON-NLS-1$
        ep4.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(ep4, SwingComponentUtils.getConstraints(0, 3));

        // editorPane 5
        JEditorPane ep5 = new JEditorPane();
        ep5.setName(ComponentNameConstants.TESTPAGE_EDITORPANES_EP05);
        ep5.setEditable(false);
        ep5.setText(I18NUtils.getString("text3")); //$NON-NLS-1$
        ep5.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(ep5, SwingComponentUtils.getConstraints(0, 4));

        return frame;
    }

}
