package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;
import org.eclipse.jubula.qa.aut.caa.utils.ListTransferHandler;


/**
 * Opener for JFrame with JLists to test.
 * 
 * JList 1: enabled = true tooltip = true contextmenu = true selectionmode =
 * single selection
 * 
 * JList 2: enabled = false
 * 
 * JList 3: selectionmode = single interval selection scrollbar = true
 * 
 * JList 4: selectionmode = multiple interval selection
 * 
 */
public class ListFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_lists"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_LISTS);
        final JLabel frameLabel = frame.getlabel();
        // add components to test
        
        createFirstList(frame, frameLabel);
        
        // list 2
        JList lst2 = new JList(ComponentUtils.getListItems());
        lst2.setName(ComponentNameConstants.TESTPAGE_LISTS_LST02);
        lst2.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        lst2.setEnabled(false);
        frame.getPnlContent().add(lst2,
                SwingComponentUtils.getConstraints(1, 0));
        
        // list 3
        JList lst3 = new JList(ComponentUtils.getListItems());
        lst3.setName(ComponentNameConstants.TESTPAGE_LISTS_LST03);
        lst3.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lst3.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        lst3.setVisibleRowCount(-1);
        JScrollPane listScroller = new JScrollPane(lst3);
        listScroller.setPreferredSize(new Dimension(70, 70));
        listScroller.setName(ComponentNameConstants.TESTPAGE_LISTS_SB01);
        frame.getPnlContent().add(lst3,
                SwingComponentUtils.getConstraints(2, 0));
        
        // list 4
        JList lst4 = new JList(ComponentUtils.getListItems());
        lst4.setName(ComponentNameConstants.TESTPAGE_LISTS_LST04);
        lst4.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        lst4.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        lst4.setVisibleRowCount(-1);
        frame.getPnlContent().add(lst4,
                SwingComponentUtils.getConstraints(3, 0));
        
        // list 5
        JList lst5 = new JList(ComponentUtils.getList());
        lst5.setName(ComponentNameConstants.TESTPAGE_LISTS_LST05);
        lst5.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        lst5.setLayoutOrientation(JList.VERTICAL);
        lst5.setVisibleRowCount(7);
        lst5.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                frameLabel.setText(e.getClickCount() + " Click");
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mousePressed(MouseEvent e) {
            }
            public void mouseReleased(MouseEvent e) {
            }
        });
        
        
        JScrollPane listScroller2 = new JScrollPane(lst5);
        listScroller2.setPreferredSize(new Dimension(50, 130));
        listScroller2.setName(ComponentNameConstants.TESTPAGE_LISTS_SB02);
        frame.getPnlContent().add(listScroller2,
            SwingComponentUtils.getConstraints(0, 1));
        
        JList lst6 = new JList(ComponentUtils.getListModelItems());
        lst6.setName(ComponentNameConstants.TESTPAGE_LISTS_LST06);
        lst6.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        lst6.setLayoutOrientation(JList.VERTICAL);
        lst6.setVisibleRowCount(7);
        frame.getPnlContent().add(lst6,
                SwingComponentUtils.getConstraints(1, 1));
        
        return frame;
    }
    
    /**
     * 
     * @param frame the frame
     * @param frameLabel the label showin ui actions
     */
    private void createFirstList(AbstractTestFrame frame, 
            final JLabel frameLabel) {
        // list 1
        JList lst1 = new JList(ComponentUtils.getListItems());
        lst1.setName(ComponentNameConstants.TESTPAGE_LISTS_LST01);
        lst1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        lst1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lst1.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        SwingComponentUtils.addContextListenerToComponent(lst1,
                frame.getlabel());
        lst1.setVisibleRowCount(-1);
        frame.getPnlContent().add(lst1,
                SwingComponentUtils.getConstraints(0, 0));
        lst1.setDragEnabled(true);
        lst1.setDropMode(DropMode.ON);
        lst1.setTransferHandler(new ListTransferHandler(lst1, frameLabel));
    }
   

}
