package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JPasswordField;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;

/**
 * Opener for JFrame with JPasswordFields to test.
 * 
 * JPasswordField 1: enabled = true; tooltip = true; contextmenu = true
 * 
 * JPasswordField 2: enabled = false; text = false
 * 
 * JPasswordField 3: enabled = true; editable = true; text = true
 * 
 * JPasswordField 4: enabled = false; text = true
 * 
 * JPasswordField 5: editable = false; text = true
 */
public class PasswordFieldFrameOpener extends AbstractFrameOpener {
    
    /**
     * the preferred size for password fields
     */
    private static final Dimension PREFERRED_SIZE = new Dimension(115, 20);

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_passwordField");
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_PASSWORDFIELDS);
        //add components to test
        
        // passwordField 1
        JPasswordField pwdf1 = new JPasswordField(10);
        pwdf1.setName(ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pwdf1.setPreferredSize(PREFERRED_SIZE);
        pwdf1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils.addContextListenerToComponent(pwdf1,
                frame.getlabel());
        frame.getPnlContent().add(pwdf1,
                SwingComponentUtils.getConstraints(0, 0));

        // passwordField 2
        JPasswordField pwdf2 = new JPasswordField(10);
        pwdf2.setName(ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF02);
        pwdf2.setPreferredSize(PREFERRED_SIZE);
        pwdf2.setEnabled(false);
        frame.getPnlContent().add(pwdf2,
                SwingComponentUtils.getConstraints(0, 1));
        
        //passwordField 3
        JPasswordField pwdf3 = new JPasswordField("text1", 10);
        pwdf3.setName(ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF03);
        pwdf3.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent().add(pwdf3,
                SwingComponentUtils.getConstraints(0, 2));
        
        //passwordField 4
        JPasswordField pwdf4 = new JPasswordField("text2", 10);
        pwdf4.setName(ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF04);
        pwdf4.setPreferredSize(PREFERRED_SIZE);
        pwdf4.setEnabled(false);
        frame.getPnlContent().add(pwdf4,
                SwingComponentUtils.getConstraints(0, 3));
        
        //passwordField 5
        JPasswordField pwdf5 = new JPasswordField("text3", 10);
        pwdf5.setName(ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF05);
        pwdf5.setPreferredSize(PREFERRED_SIZE);
        pwdf5.setEditable(false);
        frame.getPnlContent().add(pwdf5,
                SwingComponentUtils.getConstraints(0, 4));
        
        return frame;
    }
}
