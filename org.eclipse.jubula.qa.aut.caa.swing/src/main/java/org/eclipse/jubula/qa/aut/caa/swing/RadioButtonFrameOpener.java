package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JRadioButtons to test.
 * 
 * JRadioButton 1: enabled = true tooltip = true contextmenu = true
 * 
 * JRadioButton 2: selected = true
 * 
 * JRadioButton 3: enabled = false
 */
public class RadioButtonFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_radioButtons"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_RADIOBUTTONS);
        // add components to test

        ButtonGroup buttonGroup = new ButtonGroup();
        
        // button 1
        JRadioButton rbtn1 = new JRadioButton(I18NUtils.getName("rbtn1")); //$NON-NLS-1$
        rbtn1.setName(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN01);
        rbtn1.setSelected(false);
        rbtn1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(rbtn1,
                frame.getlabel());
        frame.getPnlContent().add(rbtn1,
                SwingComponentUtils.getConstraints(0, 0));
        buttonGroup.add(rbtn1);

        // button 2
        JRadioButton rbtn2 = new JRadioButton(I18NUtils.getName("rbtn2")); //$NON-NLS-1$
        rbtn2.setName(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN02);
        rbtn2.setSelected(true);
        frame.getPnlContent().add(rbtn2,
                SwingComponentUtils.getConstraints(0, 1));
        buttonGroup.add(rbtn2);
        
        // button 3
        JRadioButton rbtn3 = new JRadioButton(I18NUtils.getName("rbtn3")); //$NON-NLS-1$
        rbtn3.setName(ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN03);
        rbtn3.setEnabled(false);
        frame.getPnlContent().add(rbtn3,
                SwingComponentUtils.getConstraints(0, 2));

        return frame;
    }

}
