/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;
/**
 * Test Page for simple extension components
 *
 */
public class SimpleExtensionFrameOpener extends AbstractFrameOpener {

    @Override
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_simpleExtension"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_SIMPLE_EXTENSION);
        // add components to test
        
        // Custom component
        DrawPane custom = new DrawPane();
        custom.setPreferredSize(new Dimension(200, 200));
        custom.setName(ComponentNameConstants.TESTPAGE_SIMPLEEXT_C01);
        frame.getPnlContent().add(custom,
                SwingComponentUtils.getConstraints(0, 0));
        return frame;
    }

}
