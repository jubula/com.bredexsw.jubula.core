package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;

/**
 * slider
 * 
 * @author ann
 * 
 */
public class SliderFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_slider");
        frame.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL01);
        JPanel pnlContent = frame.getPnlContent();
        // add components to test

        // slider1
        JLabel l1 = new JLabel();
        JSlider sl1 = createSlider(SwingConstants.HORIZONTAL, 0, 130, 0, l1);
        sl1.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL01);
        sl1.setMajorTickSpacing(10);
        sl1.setMinorTickSpacing(2);
        sl1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils
                .addContextListenerToComponent(sl1, frame.getlabel());
        pnlContent.add(l1, SwingComponentUtils.getConstraints(0, 0));
        pnlContent.add(sl1, SwingComponentUtils.getConstraints(0, 1));

        // slider2
        JLabel l2 = new JLabel();
        JSlider sl2 = createSlider(SwingConstants.HORIZONTAL, 0, 130, 0, l2);
        sl2.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL02);
        sl2.setMajorTickSpacing(10);
        sl2.setMinorTickSpacing(2);
        sl2.setEnabled(false);
        SwingComponentUtils
                .addContextListenerToComponent(sl2, frame.getlabel());
        pnlContent.add(l2, SwingComponentUtils.getConstraints(0, 2));
        pnlContent.add(sl2, SwingComponentUtils.getConstraints(0, 3));
        
        // slider3
        JLabel l3 = new JLabel();
        JSlider sl3 = createSlider(SwingConstants.HORIZONTAL, 0, 130, 0, l3);
        sl3.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL03);
        sl3.setMajorTickSpacing(10);
        sl3.setMinorTickSpacing(5);
        sl3.setSnapToTicks(true);
        SwingComponentUtils
                .addContextListenerToComponent(sl3, frame.getlabel());
        pnlContent.add(l3, SwingComponentUtils.getConstraints(0, 4));
        pnlContent.add(sl3, SwingComponentUtils.getConstraints(0, 5));

        // slider4
        JLabel l4 = new JLabel();
        JSlider sl4 = createSlider(SwingConstants.HORIZONTAL, 0, 3, 0, l4);
        sl4.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL04);
        sl4.setMajorTickSpacing(1);
        sl4.setSnapToTicks(true);
        Dictionary<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();
        labels.put(0, new JLabel("Green"));
        labels.put(1, new JLabel("White"));
        labels.put(2, new JLabel("Orange"));
        labels.put(3, new JLabel("Black"));
        sl4.setLabelTable(labels);
        setTextOfLabel(l4, sl4);
        SwingComponentUtils
                .addContextListenerToComponent(sl4, frame.getlabel());
        pnlContent.add(l4, SwingComponentUtils.getConstraints(0, 6));
        pnlContent.add(sl4, SwingComponentUtils.getConstraints(0, 7));
        
        // slider5
        JLabel l5 = new JLabel("Value:");
        JSlider sl5 = createSlider(SwingConstants.VERTICAL, 0, 130, 0, l5);
        sl5.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL05);
        sl5.setMajorTickSpacing(10);
        sl5.setMinorTickSpacing(2);
        sl5.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils
                .addContextListenerToComponent(sl5, frame.getlabel());
        pnlContent.add(sl5, SwingComponentUtils.getConstraints(0, 8));
        pnlContent.add(l5, SwingComponentUtils.getConstraints(1, 8));
        
        return frame;
    }

    /**
     * @param orientation orientation
     * @param min min value
     * @param max max value
     * @param value initial value
     * @param l label
     * @return slider
     */
    private JSlider createSlider(int orientation, int min, int max, int value,
            final JLabel l) {
        final JSlider slider = new JSlider(orientation, min, max,
                value);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        setTextOfLabel(l, slider);
        if (orientation == SwingConstants.HORIZONTAL) {
            slider.setPreferredSize(new Dimension(400, 50));
        } else {
            slider.setPreferredSize(new Dimension(50, 400));
        }
        slider.addChangeListener(new ChangeListener() {
            
            public void stateChanged(ChangeEvent e) {
                setTextOfLabel(l, slider);
            }

        });
        return slider;
    }

    /**
     * sets value of slider as text of label
     * @param l label
     * @param slider slider
     */
    private void setTextOfLabel(final JLabel l, final JSlider slider) {
        @SuppressWarnings("unchecked")
        Dictionary<Integer, JLabel> labelTable = slider.getLabelTable();
        if (labelTable != null) {
            JLabel jLabel = labelTable.get(slider.getValue());
            l.setText("Value: " + ((jLabel != null)
                    ? jLabel.getText() : slider.getValue()));
        } else {
            l.setText("Value: " + slider.getValue());
        }
    }
}
