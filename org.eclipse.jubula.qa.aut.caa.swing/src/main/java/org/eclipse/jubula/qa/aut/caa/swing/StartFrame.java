package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * StartFrame of the SwingAUT. The startFrame contains buttons to open other
 * frames with the components to test.
 * 
 */
public class StartFrame extends JFrame {

    /**
     * constructor
     * 
     */
    public StartFrame() {
        init();
    }

    /**
     * Initializes the startFrame.
     * 
     */
    private void init() {
        this.setTitle(I18NUtils.getString("title_swingAUT")); //$NON-NLS-1$
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setName(I18NUtils.getString("title_swingAUT")); //$NON-NLS-1$

        JPanel pnlButtons = new JPanel();
        pnlButtons.setName("pnlButtons"); //$NON-NLS-1$
        pnlButtons.setLayout(new GridBagLayout());

        addButtonButton(pnlButtons);
        addCheckBoxesButton(pnlButtons);
        addRadioButton(pnlButtons);
        addComboBoxButton(pnlButtons);
        addTextFieldButton(pnlButtons);
        addTextAreaButton(pnlButtons);
        addEditorPaneButton(pnlButtons);
        addTextPaneButton(pnlButtons);
        addLabelButton(pnlButtons);
        addListButton(pnlButtons);
        addMenuButton(pnlButtons);
        addTabbedPaneButton(pnlButtons);
        addTableButton(pnlButtons);
        addApplicationButton(pnlButtons);
        addTreeButton(pnlButtons);
        addScrolledButton(pnlButtons);
        addPasswordFieldButton(pnlButtons);
        addSliderButton(pnlButtons);
        addJProgressBarButton(pnlButtons);
        addSimpleExtensionButton(pnlButtons);
        
        this.getContentPane().add(pnlButtons);
        this.pack();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    
    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addButtonButton(JPanel pnlButtons) {
        // test buttons
        JButton btnButtons = new JButton(I18NUtils.getString("title_buttons")); //$NON-NLS-1$
        btnButtons.setName(ComponentNameConstants.MAINPAGE_BUTTONS_TESTPAGE);
        btnButtons.addActionListener(new ButtonFrameOpener());
        pnlButtons.add(btnButtons, SwingComponentUtils.getConstraints(0, 0));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addSimpleExtensionButton(JPanel pnlButtons) {
        // test buttons
        JButton btnButtons = new JButton(
                I18NUtils.getString("title_simpleExtension")); //$NON-NLS-1$
        btnButtons.setName(
                ComponentNameConstants.MAINPAGE_SIMPLE_EXTENSION_TESTPAGE);
        btnButtons.addActionListener(new SimpleExtensionFrameOpener());
        pnlButtons.add(btnButtons, SwingComponentUtils.getConstraints(1, 9));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addCheckBoxesButton(JPanel pnlButtons) {
        // test checkBoxes
        JButton btnCheckBoxes = new JButton(I18NUtils
                .getString("title_checkBoxes")); //$NON-NLS-1$
        btnCheckBoxes
                .setName(ComponentNameConstants.MAINPAGE_CHECKBOXES_TESTPAGE);
        btnCheckBoxes.addActionListener(new CheckBoxFrameOpener());
        pnlButtons.add(btnCheckBoxes, SwingComponentUtils.getConstraints(0, 1));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addRadioButton(JPanel pnlButtons) {
        // test radioButtons
        JButton btnRadioButtons = new JButton(I18NUtils
                .getString("title_radioButtons")); //$NON-NLS-1$
        btnRadioButtons
                .setName(ComponentNameConstants.MAINPAGE_RADIOBUTTONS_TESTPAGE);
        btnRadioButtons.addActionListener(new RadioButtonFrameOpener());
        pnlButtons.add(btnRadioButtons, SwingComponentUtils
                .getConstraints(0, 2));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addComboBoxButton(JPanel pnlButtons) {
        // test comboBoxes
        JButton btnComboBoxes = new JButton(I18NUtils
                .getString("title_comboBoxes")); //$NON-NLS-1$
        btnComboBoxes
                .setName(ComponentNameConstants.MAINPAGE_COMBOBOXES_TESTPAGE);
        btnComboBoxes.addActionListener(new ComboBoxFrameOpener());
        pnlButtons.add(btnComboBoxes, SwingComponentUtils.getConstraints(0, 3));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTextFieldButton(JPanel pnlButtons) {
        // test textFields
        JButton btnTextFields = new JButton(I18NUtils
                .getString("title_textFields")); //$NON-NLS-1$
        btnTextFields
                .setName(ComponentNameConstants.MAINPAGE_TEXTFIELDS_TESTPAGE);
        btnTextFields.addActionListener(new TextFieldFrameOpener());
        pnlButtons.add(btnTextFields, SwingComponentUtils.getConstraints(0, 4));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTextAreaButton(JPanel pnlButtons) {
        // test textAreas
        JButton btnTextAreas = new JButton(I18NUtils
                .getString("title_textAreas")); //$NON-NLS-1$
        btnTextAreas
                .setName(ComponentNameConstants.MAINPAGE_TEXTAREAS_TESTPAGE);
        btnTextAreas.addActionListener(new TextAreaFrameOpener());
        pnlButtons.add(btnTextAreas, SwingComponentUtils.getConstraints(0, 5));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addEditorPaneButton(JPanel pnlButtons) {
        // test editorPanes
        JButton btnEditorPanes = new JButton(I18NUtils
                .getString("title_editorPanes")); //$NON-NLS-1$
        btnEditorPanes
                .setName(ComponentNameConstants.MAINPAGE_EDITORPANES_TESTPAGE);
        btnEditorPanes.addActionListener(new EditorPaneFrameOpener());
        pnlButtons
                .add(btnEditorPanes, SwingComponentUtils.getConstraints(0, 6));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTextPaneButton(JPanel pnlButtons) {
        // test textPanes
        JButton btnTextPanes = new JButton(I18NUtils
                .getString("title_textPanes")); //$NON-NLS-1$
        btnTextPanes
                .setName(ComponentNameConstants.MAINPAGE_TEXTPANES_TESTPAGE);
        btnTextPanes.addActionListener(new TextPaneFrameOpener());
        pnlButtons.add(btnTextPanes, SwingComponentUtils.getConstraints(0, 7));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addLabelButton(JPanel pnlButtons) {
        // test labels
        JButton btnLabels = new JButton(I18NUtils.getString("title_labels")); //$NON-NLS-1$
        btnLabels.setName(ComponentNameConstants.MAINPAGE_LABELS_TESTPAGE);
        btnLabels.addActionListener(new LabelFrameOpener());
        pnlButtons.add(btnLabels, SwingComponentUtils.getConstraints(0, 8));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addListButton(JPanel pnlButtons) {
        // test lists
        JButton btnLists = new JButton(I18NUtils.getString("title_lists")); //$NON-NLS-1$
        btnLists.setName(ComponentNameConstants.MAINPAGE_LISTS_TESTPAGE);
        btnLists.addActionListener(new ListFrameOpener());
        pnlButtons.add(btnLists, SwingComponentUtils.getConstraints(0, 9));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addMenuButton(JPanel pnlButtons) {
        // test menus
        JButton btnMenus = new JButton(I18NUtils.getString("title_menus")); //$NON-NLS-1$
        btnMenus.setName(ComponentNameConstants.MAINPAGE_MENU_TESTPAGE);
        btnMenus.addActionListener(new MenuFrameOpener());
        pnlButtons.add(btnMenus, SwingComponentUtils.getConstraints(1, 0));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTabbedPaneButton(JPanel pnlButtons) {
        // test tabbedPanes
        JButton btnTabbedPanes = new JButton(I18NUtils
                .getString("title_tabbedPanes")); //$NON-NLS-1$
        btnTabbedPanes
                .setName(ComponentNameConstants.MAINPAGE_TABBEDPANES_TESTPAGE);
        btnTabbedPanes.addActionListener(new TabbedPaneFrameOpener());
        pnlButtons
                .add(btnTabbedPanes, SwingComponentUtils.getConstraints(1, 1));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTableButton(JPanel pnlButtons) {
        // test tables
        JButton btnTables = new JButton(I18NUtils.getString("title_tables")); //$NON-NLS-1$
        btnTables.setName(ComponentNameConstants.MAINPAGE_TABLES_TESTPAGE);
        btnTables.addActionListener(new TableFrameOpener());
        pnlButtons.add(btnTables, SwingComponentUtils.getConstraints(1, 2));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addApplicationButton(JPanel pnlButtons) {
        // test buttons
        JButton btnButtons = new JButton(I18NUtils
                .getString("title_application")); //$NON-NLS-1$
        btnButtons
                .setName(ComponentNameConstants.MAINPAGE_APPLICATIONS_TESTPAGE);
        btnButtons.addActionListener(new ApplicationFrameOpener());
        pnlButtons.add(btnButtons, SwingComponentUtils.getConstraints(1, 3));
    }

    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addTreeButton(JPanel pnlButtons) {
        // test trees
        JButton btnButtons = new JButton(I18NUtils.getString("title_trees")); //$NON-NLS-1$
        btnButtons.setName(ComponentNameConstants.MAINPAGE_TREES_TESTPAGE);
        btnButtons.addActionListener(new TreeFrameOpener());
        pnlButtons.add(btnButtons, SwingComponentUtils.getConstraints(1, 4));
    }

    /**
     * @param pnlButtons
     *              pnlButtons
     */
    private void addScrolledButton(JPanel pnlButtons) {
        //test scrolledLabel
        JButton btnButtons = new JButton(I18NUtils
                .getString("title_scrolledComp"));
        btnButtons.setName(ComponentNameConstants
                .MAINPAGE_SCROLLEDCOMP_TESTPAGE);
        btnButtons.addActionListener(new ScrolledLabelFrameOpener());
        pnlButtons.add(btnButtons, SwingComponentUtils.getConstraints(1, 5));
    }
        
    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addPasswordFieldButton(JPanel pnlButtons) {
        // test passwordFields
        JButton btnPasswordField = new JButton(
                I18NUtils.getString("title_passwordField")); //$NON-NLS-1$
        btnPasswordField.setName(
                ComponentNameConstants.MAINPAGE_PASSWORDFIELDS_TESTPAGE);
        btnPasswordField.addActionListener(new PasswordFieldFrameOpener());
        pnlButtons.add(btnPasswordField,
                SwingComponentUtils.getConstraints(1, 6));        
    }
    
    /**
     * @param pnlButtons
     *            pblButtons
     */
    private void addSliderButton(JPanel pnlButtons) {
        // test slider
        JButton btnSlider = new JButton(I18NUtils.getString("title_slider")); //$NON-NLS-1$
        btnSlider.setName(ComponentNameConstants.MAINPAGE_SLIDER_TESTPAGE);
        btnSlider.addActionListener(new SliderFrameOpener());
        pnlButtons.add(btnSlider, SwingComponentUtils.getConstraints(1, 7));
    }
    
    /**
     * @param pnlButtons
     *            pnlButtons
     */
    private void addJProgressBarButton(JPanel pnlButtons) {
        // test JProgressBar
        JButton btnJProgressBar = new JButton(
                I18NUtils.getString("title_jProgressBar")); //$NON-NLS-1$
        btnJProgressBar
                .setName(ComponentNameConstants.MAINPAGE_JPROGRESSBAR_TESTPAGE);
        btnJProgressBar.addActionListener(new JProgressBarFrameOpener());
        pnlButtons.add(btnJProgressBar,
                SwingComponentUtils.getConstraints(1, 8));
    }

}
