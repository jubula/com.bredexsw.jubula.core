package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JTabbedPane to test.
 * 
 * JTabbedpane 1: enabled = true /false tooltip = true mnemonic = true
 * tabPlacement = top
 * 
 * JTabbedPane 2: tabPlacement = left
 * 
 */
public class TabbedPaneFrameOpener extends AbstractFrameOpener {
    /**
     * <code>icon1</code>
     */
    private ImageIcon m_icon1 = SwingComponentUtils.getIcon("carrot"); //$NON-NLS-1$
    /**
     * <code>icon2</code>
     */
    private ImageIcon m_icon2 = SwingComponentUtils.getIcon("banana"); //$NON-NLS-1$
    /**
     * <code>icon3</code>
     */
    private ImageIcon m_icon3 = SwingComponentUtils.getIcon("kiwi"); //$NON-NLS-1$
    
    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_tabbedPanes"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TABBEDPANES);
        // add components to test
        // tabbedPane 1
        JTabbedPane tbp1 = new JTabbedPane();
        tbp1.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP01);
        tbp1.setTabPlacement(SwingConstants.TOP);
        tbp1.setPreferredSize(new Dimension(200, 100));
        SwingComponentUtils.addContextListenerToComponent(tbp1,
                frame.getlabel());
        
        JPanel pnl1 = new JPanel();
        pnl1.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL01);
        pnl1.add(new JLabel(I18NUtils.getString("img_carrot"), m_icon1, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp1.addTab(I18NUtils.getString("tab1"), pnl1); //$NON-NLS-1$
        tbp1.setToolTipTextAt(0, I18NUtils.getString("tooltip")); //$NON-NLS-1$
        tbp1.setMnemonicAt(0, KeyEvent.VK_1);
        
        JPanel pnl2 = new JPanel();
        pnl2.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL02);
        pnl2.add(new JLabel(I18NUtils.getString("img_banana"), m_icon2, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp1.addTab(I18NUtils.getString("tab2"), pnl2); //$NON-NLS-1$
        tbp1.setToolTipTextAt(0, I18NUtils.getString("tooltip")); //$NON-NLS-1$
        tbp1.setMnemonicAt(1, KeyEvent.VK_2);
        
        JPanel pnl3 = new JPanel();
        pnl3.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL03);
        pnl3.add(new JLabel(I18NUtils.getString("img_kiwi"), m_icon3, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp1.addTab(I18NUtils.getString("tab3"), pnl3); //$NON-NLS-1$
        tbp1.setToolTipTextAt(0, I18NUtils.getString("tooltip")); //$NON-NLS-1$
        tbp1.setMnemonicAt(2, KeyEvent.VK_3);
        tbp1.setEnabledAt(2, false);
        frame.getPnlContent().add(tbp1,
                SwingComponentUtils.getConstraints(0, 0));
        // tabbedPane 2
        frame.getPnlContent().add(createSecondTabbedPane(),
                SwingComponentUtils.getConstraints(0, 1));
        // tabbedPane 3
        frame.getPnlContent().add(createThirdTabbedPane(),
                SwingComponentUtils.getConstraints(0, 2));

        return frame;
    }

    /**
     * @return the second tabbed pane
     */
    private Component createSecondTabbedPane() {
        JTabbedPane tbp2 = new JTabbedPane();
        tbp2.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP02);
        tbp2.setTabPlacement(SwingConstants.LEFT);
        tbp2.setPreferredSize(new Dimension(200, 100));

        JPanel pnl4 = new JPanel();
        pnl4.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL04);
        pnl4.add(new JLabel(I18NUtils.getString("img_carrot"), m_icon1, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp2.addTab(I18NUtils.getString("tab1"), pnl4); //$NON-NLS-1$

        JPanel pnl5 = new JPanel();
        pnl5.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL05);
        pnl5.add(new JLabel(I18NUtils.getString("img_banana"), m_icon2, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp2.addTab(I18NUtils.getString("tab2"), pnl5); //$NON-NLS-1$

        JPanel pnl6 = new JPanel();
        pnl6.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_PNL06);
        pnl6.add(new JLabel(I18NUtils.getString("img_kiwi"), m_icon3, //$NON-NLS-1$
                SwingConstants.CENTER));
        tbp2.addTab(I18NUtils.getString("tab3"), pnl6); //$NON-NLS-1$
        return tbp2;
    }

    /**
     * @return the third tabbed pane
     */
    private Component createThirdTabbedPane() {
        JTabbedPane tbp = new JTabbedPane();
        tbp.setName(ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP03);
        tbp.setTabPlacement(SwingConstants.TOP);
        tbp.setPreferredSize(new Dimension(200, 100));
        tbp.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        for (int i = 0; i < 50; i++) {
            JPanel pnl = new JPanel();
            pnl.add(new JLabel(I18NUtils.getString("img_carrot"), m_icon1, //$NON-NLS-1$
                    SwingConstants.CENTER));
            tbp.addTab(i + "", pnl); //$NON-NLS-1$
        }
        return tbp;
    }
}
