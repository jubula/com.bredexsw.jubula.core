/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 11734 $
 *
 * $Date: 2010-08-05 16:17:42 +0200 (Thu, 05 Aug 2010) $
 *
 * $Author: zeb $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2008 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.DropMode;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;
import org.eclipse.jubula.qa.aut.caa.utils.TreeTransferHandler;


/**
 * @author berndk
 * @created 13.10.2008
 */
public class TreeFrameOpener extends AbstractFrameOpener {

    /**
     * Taken from http://www.developer.com/java/ent/article.php/939061/Simple-TreeModel-Example-Infinite-Binary-Tree.htm
     * and modified to pass Checkstyle.
     * 
     * InfiniteBinaryTree
     * @author Daniel Green
     * Superliminal Software
     *
     * A little example program to show the power of the TreeModel interface.
     * Running it displays a binary tree with numbers on each node.
     * Every positive number can be found somewhere in the tree.
     * See if you can find the node labeled "1000".
     * Hint: The base 2 representation of any node value can be read out from
     * the path leading to that node where opening the first child of a node
     * represents a 0 and opening the second child represents a 1.
     * Enjoy!
     */
    private static class InfiniteBinaryTree implements TreeModel {
        /**
         * {@inheritDoc}
         */
        public Object getRoot()  {
            return new Integer(1); // start at node number 1
        }

        /**
         * {@inheritDoc}
         */
        public int getChildCount(Object parent)  {
            return 2; // because its a binary tree
        }

        /**
         * {@inheritDoc}
         */
        public Object getChild(Object parent, int index) {
            // the magic formula
            return new Integer(2 * valueOf(parent) + index);
        }

        /**
         * {@inheritDoc}
         */
        public int getIndexOfChild(Object parent, Object child) {
            // odd number == first child, even == second
            return valueOf(child) % 2;
        }

        /**
         * {@inheritDoc}
         */
        public boolean isLeaf(Object node) {
            return false; // an infinite number of internal nodes and no leaves!
        }

        // stubbed out methods not needed for display though for any real
        // application at least the listener methods should be implemented.
        /**
         * {@inheritDoc}
         */
        public void addTreeModelListener(
                javax.swing.event.TreeModelListener l) {
            // stub
        }
        /**
         * {@inheritDoc}
         */
        public void removeTreeModelListener(
                javax.swing.event.TreeModelListener l) {
            // stub
        }
        /**
         * {@inheritDoc}
         */
        public void valueForPathChanged(TreePath path, Object newValue) {
            // stub
        }

        /**
         * helper function
         * @param obj Object to evaluate.
         * @return the value of the object within the context of this model.
         */
        private int valueOf(Object obj)  {
            return ((Integer)obj).intValue();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_trees"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TREES);
        // add component to test
        TreeNode parent = createTreeModel();

        // der Tree wird erstellt
        JTree tree = new JTree(parent);
        tree.setName(ComponentNameConstants.TESTPAGE_TREES_TRE01);
        tree.setToolTipText("my Tooltip"); //$NON-NLS-1$
        tree.setShowsRootHandles(true);
        JScrollPane sp = new JScrollPane(tree,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        SwingComponentUtils.addContextListenerToComponent(tree,
                frame.getlabel());
        sp.setPreferredSize(new Dimension(150, 150));
        frame.getPnlContent().add(sp, SwingComponentUtils.getConstraints(0, 0));
        tree.setDragEnabled(true);
        tree.setDropMode(DropMode.ON);
        tree.setTransferHandler(new TreeTransferHandler(tree, 
                frame.getlabel()));
        // ein zweiter Tree wird erstellt
        JTree treetwo = new JTree(parent.getChildAt(1));
        treetwo.setName(ComponentNameConstants.TESTPAGE_TREES_TRE02);
        treetwo.setToolTipText("Tooltip"); //$NON-NLS-1$
        treetwo.setEnabled(false);
        treetwo.setShowsRootHandles(true);
        frame.getPnlContent().add(treetwo,
                SwingComponentUtils.getConstraints(0, 1));
        
        // ein dritter Tree wird erstellt
        JTree treeNr3 = new JTree(parent);
        treeNr3.setName(ComponentNameConstants.TESTPAGE_TREES_TRE04);
        treeNr3.setMinimumSize(new Dimension(100, 100));
        JScrollPane sp2 = new JScrollPane(treeNr3);
        sp2.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp2.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sp2.setPreferredSize(new Dimension(80, 80));
        frame.getPnlContent().add(sp2, SwingComponentUtils.
                getConstraintsWithoutHorizontalFill(0, 2));
        
        // tree with custom TreeModel and non-TreeNodes
        JTree customTree = new JTree(new InfiniteBinaryTree());
        customTree.setShowsRootHandles(true);
        customTree.setName("CustomTreeModel"); //$NON-NLS-1$
        customTree.setMinimumSize(new Dimension(100, 100));
        JScrollPane customTreeScrollPane = new JScrollPane(customTree,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        frame.getPnlContent().add(customTreeScrollPane,
                SwingComponentUtils.getConstraints(0, 3));
     
        // ein fuenfter Tree wird erstellt
        JTree treeNr5 = new JTree(createExtraTreeNode());
        treeNr5.setName(ComponentNameConstants.TESTPAGE_TREES_TRE05);
        treeNr5.setMinimumSize(new Dimension(100, 100));
        JScrollPane sp3 = new JScrollPane(treeNr5);
        sp3.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp3.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sp3.setPreferredSize(new Dimension(80, 80));
        frame.getPnlContent().add(sp3, SwingComponentUtils.
                getConstraintsWithoutHorizontalFill(0, 4));
        
        createTree6(frame);
       
        return frame;
    }
    
    /**
     * Method to create some kind of lazy-loading tree
     * @param frame The on which the tree wil be created
     */
    private void createTree6(AbstractTestFrame frame) {
        
        DynamicTree treeNr6 = new DynamicTree();
        JScrollPane sp4 = new JScrollPane(treeNr6,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp4.setPreferredSize(new Dimension(200, 200));
        SwingComponentUtils.addContextListenerToComponent(treeNr6,
                frame.getlabel());
        frame.getPnlContent().add(sp4,
                SwingComponentUtils.getConstraints(1, 0));
    }

    /**
     * 
     * @return the default TreeNode that can be used as a model for test trees.
     */
    private static TreeNode createTreeModel() {
        // Struktur der Trees wird festgelegt:
        // die Namen der einzelnen Nodes sind in der Klasse I18NUtils festgelegt
        DefaultMutableTreeNode parent = new DefaultMutableTreeNode(I18NUtils
                .getString("text1"), true); //$NON-NLS-1$

        DefaultMutableTreeNode tree1 = new DefaultMutableTreeNode(I18NUtils
                .getString("text2")); //$NON-NLS-1$
        DefaultMutableTreeNode tree2 = new DefaultMutableTreeNode(I18NUtils
                .getString("text3")); //$NON-NLS-1$
        DefaultMutableTreeNode tree21 = new DefaultMutableTreeNode(I18NUtils
                .getString("text4")); //$NON-NLS-1$
        // die erste Verzweigung wird noch weiter verzweigt (tree211, tree212)
        DefaultMutableTreeNode tree211 = new DefaultMutableTreeNode(I18NUtils
                .getString("text7")); //$NON-NLS-1$
        DefaultMutableTreeNode tree212 = new DefaultMutableTreeNode(I18NUtils
                .getString("text2")); //$NON-NLS-1$
        DefaultMutableTreeNode tree22 = new DefaultMutableTreeNode(I18NUtils
                .getString("text5")); //$NON-NLS-1$
        DefaultMutableTreeNode tree3 = new DefaultMutableTreeNode(I18NUtils
                .getString("text6")); //$NON-NLS-1$
        DefaultMutableTreeNode tree31 = new DefaultMutableTreeNode(I18NUtils
                .getString("text1")); //$NON-NLS-1$
        DefaultMutableTreeNode tree4 = new DefaultMutableTreeNode(I18NUtils
                .getString("text7")); //$NON-NLS-1$

        parent.add(tree1);
        parent.add(tree2);
        tree2.add(tree21);
        tree21.add(tree211);
        tree21.add(tree212);
        tree2.add(tree22);
        parent.add(tree3);
        tree3.add(tree31);
        parent.add(tree4);
        
        return parent;
    }
    
    /**
     * @return a TreeNodes with extra entrys
     */
    private static TreeNode createExtraTreeNode() {
        DefaultMutableTreeNode parent = new DefaultMutableTreeNode(I18NUtils
                .getString("text1"), true); //$NON-NLS-1$
        
        DefaultMutableTreeNode tree1 = new DefaultMutableTreeNode(I18NUtils
                .getString("long_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree2 = new DefaultMutableTreeNode(I18NUtils
                .getString("long_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree3 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree4 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        
        DefaultMutableTreeNode tree11 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree12 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree21 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree22 = new DefaultMutableTreeNode(I18NUtils
                .getString("long_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree31 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree32 = new DefaultMutableTreeNode(I18NUtils
                .getString("short_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree41 = new DefaultMutableTreeNode(I18NUtils
                .getString("long_item_name")); //$NON-NLS-1$
        DefaultMutableTreeNode tree42 = new DefaultMutableTreeNode(I18NUtils
                .getString("long_item_name")); //$NON-NLS-1$
        
        parent.add(tree1);
        parent.add(tree2);
        parent.add(tree3);
        parent.add(tree4);
        
        tree1.add(tree11);
        tree1.add(tree12);
        tree2.add(tree21);
        tree2.add(tree22);
        tree3.add(tree31);
        tree3.add(tree32);
        tree4.add(tree41);
        tree4.add(tree42);
        
        return parent;
    }
}
