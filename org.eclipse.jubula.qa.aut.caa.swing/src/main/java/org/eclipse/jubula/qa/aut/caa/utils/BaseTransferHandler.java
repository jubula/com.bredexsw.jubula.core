package org.eclipse.jubula.qa.aut.caa.utils;

import javax.swing.JComponent;
import javax.swing.TransferHandler;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class BaseTransferHandler extends TransferHandler {
    
    /** */
    private static final long serialVersionUID = 1L;

    /** the component to check */
    private JComponent m_component;
    
    /**
     * @param component JComponent to check if it contains drop position
     */
    public BaseTransferHandler(JComponent component) {
        m_component = component;
    }
    /**
     * {@inheritDoc}
     */
    public boolean canImport(TransferSupport support) {
        DropLocation dl = support.getDropLocation();
        if (dl != null) {
            return m_component.contains(dl.getDropPoint());
        }
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public int getSourceActions(JComponent c) {
        return TransferHandler.COPY_OR_MOVE;
    }
    /**
     * 
     * @param nameFrom name from source
     * @param intFrom int from source
     * @param nameTo name from target
     * @param intTo int from target
     * @return appropriate String
     */
    public String createDragAndDropText(String nameFrom, int intFrom,
            String nameTo, int intTo) {
        StringBuilder builder = new StringBuilder();
        builder.append("Drag source: ").append(nameFrom);
        builder.append(" [index= ").append(intFrom).append("] ");
        builder.append("Drop target: ").append(nameTo);
        builder.append(" [index= ").append(intTo).append("] ");
        return builder.toString();  
    }
}
