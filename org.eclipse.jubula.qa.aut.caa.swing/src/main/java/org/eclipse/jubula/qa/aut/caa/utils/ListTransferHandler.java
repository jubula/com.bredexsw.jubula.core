package org.eclipse.jubula.qa.aut.caa.utils;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.TransferHandler;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class ListTransferHandler extends BaseTransferHandler {

    /**  */
    private static final long serialVersionUID = 1L;
    /** the list */
    private JList m_list;
    /** the JLabel reflecting ui actions */
    private JLabel m_label;
    
    /**
     * 
     * @param list the JList
     * @param label the JLabel reflecting ui actions
     */
    public ListTransferHandler(JList list, JLabel label) {
        super(list);
        this.m_list = list;
        this.m_label = label;
    }

    /**
     * {@inheritDoc}
     */
    public boolean importData(TransferHandler.TransferSupport support) {
        canImport(support);
        String text = "";
        if (support.getDropLocation() 
                instanceof javax.swing.JList.DropLocation) {
            javax.swing.JList.DropLocation dl = 
                    (javax.swing.JList.DropLocation) support
                    .getDropLocation();
            text = createDragAndDropText(m_list.getSelectedValue().toString(),
                    m_list.getSelectedIndex(),
                    m_list.getModel().getElementAt(dl.getIndex()).toString(),
                    dl.getIndex());

        }
        m_label.setText(text);
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    protected Transferable createTransferable(JComponent c) {
        return new StringSelection(m_list.getSelectedValue().toString());
    }

}
