package org.eclipse.jubula.qa.aut.caa.utils;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;


/**
 * Helper class for swing components.
 */
public class SwingComponentUtils {

    /**
     * popup
     */
    private static JPopupMenu popup;

    /**
     * private Constructor
     */
    private SwingComponentUtils() {
        // empty
    }

    /**
     * Returns GridBagConstraints to add a component.
     * 
     * @param x
     *            position
     * @param y
     *            position
     * @return GridBagConstraint to use
     */
    public static GridBagConstraints getConstraints(int x, int y) {
        GridBagConstraints constr = new GridBagConstraints();
        constr.gridx = x;
        constr.gridy = y;
        constr.insets = new Insets(10, 10, 10, 10);
        constr.fill = GridBagConstraints.HORIZONTAL;
        return constr;
    }
    
    /**
     * Returns GridBagConstraints to add a component without the h-fill option.
     * 
     * @param x
     *            position
     * @param y
     *            position
     * @return GridBagConstraint to use
     */
    public static GridBagConstraints getConstraintsWithoutHorizontalFill(int x,
            int y) {
        GridBagConstraints constr = getConstraints(x, y);
        constr.fill = GridBagConstraints.NONE;
        return constr;
    }

    /**
     * Returns an imageIcon.
     * 
     * @param name
     *            name
     * @return imageIcon imageIcon
     */
    public static ImageIcon getIcon(String name) {
        return new ImageIcon("de/bredex/guidancer/aut/images/" + name + ".jpg", //$NON-NLS-1$ //$NON-NLS-2$
                I18NUtils.getString("img_" + name)); //$NON-NLS-1$
    }

    /**
     * addContextListenerToComponent
     * @param output JLabel
     * @param comp
     *            Component
     */
    public static void addContextListenerToComponent(Component comp,
            JLabel output) {
        setPopup(getPopupMenu(output));

        comp.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    getPopup().show(e.getComponent(), e.getX(), e.getY());
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    getPopup().show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
    }

    /**
     * Returns a popupMenu to use as context menu.
     * @param output 
     * @return popup
     */
    private static JPopupMenu getPopupMenu(final JLabel output) {
        setPopup(new JPopupMenu());
        JMenuItem item1 = new JMenuItem(I18NUtils.getString("menuitem_first")); //$NON-NLS-1$
        item1.setName(I18NUtils.getName("mi1")); //$NON-NLS-1$
        JMenuItem nonVisibleItem = new JMenuItem(I18NUtils.getString("menuitem_nonvisible")); //$NON-NLS-1$
        nonVisibleItem.setVisible(false);
        JMenu menu = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        menu.setName(I18NUtils.getName("mn2")); //$NON-NLS-1$
        JMenuItem item2 = new JMenuItem(I18NUtils.getString("menuitem_second")); //$NON-NLS-1$
        item2.setName(I18NUtils.getName("mi2")); //$NON-NLS-1$
        JCheckBoxMenuItem cbxItem = new JCheckBoxMenuItem(I18NUtils
                .getString("menuitem_cbx")); //$NON-NLS-1$
        cbxItem.setName(I18NUtils.getName("mi3")); //$NON-NLS-1$
        JRadioButtonMenuItem rbxItem = new JRadioButtonMenuItem(I18NUtils
                .getString("menuitem_rbx")); //$NON-NLS-1$
        rbxItem.setName(I18NUtils.getName("mi4")); //$NON-NLS-1$
        JMenuItem item3 = new JMenuItem(I18NUtils.getString("menuitem_third")); //$NON-NLS-1$
        item3.setName(I18NUtils.getName("mi5")); //$NON-NLS-1$
        item3.setEnabled(false);
        JMenu menu2 = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        menu.setName(I18NUtils.getName("mn3")); //$NON-NLS-1$
        JMenuItem item4 = new JMenuItem(I18NUtils.getString("menuitem_level3")); //$NON-NLS-1$
        item4.setName(I18NUtils.getName("mi6")); //$NON-NLS-1$
        JMenu menu3 = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        menu.setName(I18NUtils.getName("mn4")); //$NON-NLS-1$
        JMenuItem item5 = new JMenuItem(I18NUtils.getString("menuitem_level4")); //$NON-NLS-1$
        menu.setName(I18NUtils.getName("mi7")); //$NON-NLS-1$
        menu.add(item2);
        menu.add(cbxItem);
        menu.add(rbxItem);
        menu.addSeparator();
        menu.add(item3);
        menu.add(menu2);
        menu2.add(item4);
        menu2.add(menu3);
        menu3.add(item5);
        getPopup().add(item1);
        getPopup().add(nonVisibleItem);
        getPopup().add(menu);
        getPopup().setName(I18NUtils.getName("popup")); //$NON-NLS-1$
        
        if (output != null) {
            ActionListener listener = new ActionListener() {
                
                public void actionPerformed(ActionEvent event) {
                    output.setText(event.getActionCommand());
                    
                }
            };
            item1.addActionListener(listener);
            item2.addActionListener(listener);
            item3.addActionListener(listener);
            cbxItem.addActionListener(listener);
            rbxItem.addActionListener(listener);
            item4.addActionListener(listener);
            item5.addActionListener(listener);
        }
        
        return getPopup();
    }

    /**
     * @param pop the popup to set
     */
    public static void setPopup(JPopupMenu pop) {
        SwingComponentUtils.popup = pop;
    }

    /**
     * @return the popup
     */
    public static JPopupMenu getPopup() {
        return popup;
    }
       
}
