package org.eclipse.jubula.qa.aut.caa.utils;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.TransferHandler;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class TableTransferHandler extends BaseTransferHandler {

    /**  */
    private static final long serialVersionUID = 1L;
    /** the table */
    private JTable m_table;
    /** the JLabel reflecting ui actions */
    private JLabel m_label;
    
    /**
     * @param table the JTable
     * @param label the JLabel reflecting ui actions
     */
    public TableTransferHandler(JTable table, JLabel label) {
        super(table);
        this.m_table = table;
        this.m_label = label;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean importData(TransferHandler.TransferSupport support) {
        canImport(support);
        String text = "From: " + m_table.getValueAt(m_table.getSelectedRow(),
                m_table.getSelectedColumn()) + " - ";
        if (support.getDropLocation() 
                instanceof javax.swing.JTable.DropLocation) {
            javax.swing.JTable.DropLocation dl = 
                    (javax.swing.JTable.DropLocation) support
                    .getDropLocation();
            text += "To "
                    + m_table.getValueAt(dl.getRow(), dl.getColumn());

        }
        m_label.setText(text);
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    protected Transferable createTransferable(JComponent c) {
        return new StringSelection(
                m_table.getValueAt(m_table.getSelectedRow(),
                m_table.getSelectedColumn()).toString());
    }
}
