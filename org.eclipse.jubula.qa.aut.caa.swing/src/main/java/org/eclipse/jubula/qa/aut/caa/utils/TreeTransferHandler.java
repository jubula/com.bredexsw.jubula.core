package org.eclipse.jubula.qa.aut.caa.utils;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class TreeTransferHandler extends BaseTransferHandler {

    /**  */
    private static final long serialVersionUID = 1L;
    /** the tree */
    private JTree m_tree;
    /** the JLabel reflecting ui actions */
    private JLabel m_label;
    
    /**
     * 
     * @param tree the JTree
     * @param label the JLabel reflecting ui actions
     */
    public TreeTransferHandler(JTree tree, JLabel label) {
        super(tree);
        this.m_tree = tree;
        this.m_label = label;
    }

    /**
     * {@inheritDoc}
     */
    public boolean importData(TransferHandler.TransferSupport support) {
        canImport(support);
        String text = "";
        if (support.getDropLocation() 
                instanceof javax.swing.JTree.DropLocation) {
            javax.swing.JTree.DropLocation dl = 
                    (javax.swing.JTree.DropLocation) support
                    .getDropLocation();
            TreePath path = dl.getPath();
            text = createDragAndDropText(m_tree.getSelectionPath()
                    .getLastPathComponent().toString(),
                    m_tree.getSelectionModel().getLeadSelectionRow(),
                    path.getLastPathComponent().toString(),
                    m_tree.getRowForPath(path));
        }
        m_label.setText(text);
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    protected Transferable createTransferable(JComponent c) {
        return new StringSelection(
                m_tree.getSelectionPath().getLastPathComponent().toString());
    }
}
