package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Composite;

/**
 * ButtonListener to open a shell with CLabels to test.
 * 
 * @author janw
 *
 */
public class CLabelShellOpener extends AbstractShellOpener {

    /**
     * 
     * @param owner owner
     */
    public CLabelShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getPageTitle() {
        return I18NUtils.getString("title_clabel"); //$NON-NLS-1$
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fillContent(Composite parent) {
        
        CompUtils.setComponentName(parent,
            ComponentNameConstants.TESTPAGE_TITLE_CLABELS);
    
        // CLabel 1
        CLabel clbl1 = new CLabel(parent, SWT.LEFT);
        clbl1.setText(I18NUtils.getName("clbl1"));
        CompUtils.setComponentName(clbl1,
                ComponentNameConstants.TESTPAGE_CLABELS_CLBL01);
        clbl1.setSize(70, 25);
        clbl1.setLocation(10, 10);
        clbl1.setToolTipText(I18NUtils.getString("tooltip"));
        clbl1.setMenu(getPopupMenu(getCurrentPageShell()));
        
        // label 2
        CLabel clbl2 = new CLabel(parent, SWT.LEFT);
        clbl2.setText(I18NUtils.getName("clbl2"));
        CompUtils.setComponentName(clbl2,
                ComponentNameConstants.TESTPAGE_CLABELS_CLBL02);
        clbl2.setSize(70, 25);
        clbl2.setLocation(10, 50);
        clbl2.setEnabled(false);
        
        // label 3
        final CLabel clbl3 = new CLabel(parent, SWT.LEFT);
        clbl3.setText(I18NUtils.getName("clbl3"));
        CompUtils.setComponentName(clbl3,
                ComponentNameConstants.TESTPAGE_CLABELS_CLBL03);
        clbl3.setSize(70, 25);
        clbl3.setLocation(10, 90);
        clbl3.addMouseListener(new MouseAdapter() {
            public void mouseDoubleClick(MouseEvent e) {
                
                clbl3.setText(I18NUtils.getName("twoClick"));
                
            }
            
            public void mouseDown(MouseEvent e) {
                clbl3.setText(I18NUtils.getName("oneClick"));
            }
        });
    }
}
