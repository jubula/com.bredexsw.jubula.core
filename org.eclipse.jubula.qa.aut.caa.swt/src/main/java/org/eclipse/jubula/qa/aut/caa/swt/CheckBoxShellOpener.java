package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * ButtonListener to open a shell with CheckBoxes to test.
 * 
 * CheckBox 1: enabled = true tooltip = true contextmenu = true
 * 
 * CheckBox 2: enabled = false
 * 
 */
public class CheckBoxShellOpener extends AbstractShellOpener {

    /**
     * CheckBoxShellOpener
     * 
     * @param owner
     *            owner
     */
    public CheckBoxShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent, 
                ComponentNameConstants.TESTPAGE_TITLE_CHECKBOXES);

        // button 1
        Button btn1 = new Button(parent, SWT.CHECK);
        btn1.setText(I18NUtils.getName("cbx1"));
        CompUtils.setComponentName(btn1, 
                ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX01);
        btn1.setSize(85, 25);
        btn1.setLocation(10, 10);
        btn1.setToolTipText(I18NUtils.getString("tooltip"));
        btn1.setMenu(getPopupMenu(getCurrentPageShell()));

        // button 2
        Button btn2 = new Button(parent, SWT.CHECK);
        btn2.setText(I18NUtils.getName("cbx2"));
        CompUtils.setComponentName(btn2,
                ComponentNameConstants.TESTPAGE_CHECKBOXES_CBX02);
        btn2.setSize(85, 25);
        btn2.setLocation(10, 50);
        btn2.setEnabled(false);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_checkBoxes"); //$NON-NLS-1$
    }
}
