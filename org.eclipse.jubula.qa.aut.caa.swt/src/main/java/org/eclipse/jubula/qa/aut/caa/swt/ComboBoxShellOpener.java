package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

/**
 * ButtonListener to open a shell with Combos to test.
 * 
 * Combo 1: enabled = true editable=true tooltip = true contextmenu = true
 * 
 * Combo 2: enabled = false
 * 
 * Combo 3: enabled = true editable = false (read_only = true)
 * 
 */
public class ComboBoxShellOpener extends AbstractShellOpener {

    /**
     * @param owner
     *            owner
     */
    public ComboBoxShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        
        final int height = 25;
        final int width = 85;
        final int yLocationIncrement = 40;
        final int locationX = 10;
        int locationY = 10;

        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_COMBOBOXES);
        
        // comboBox 1
        Combo cmbx1 = new Combo(parent, SWT.DROP_DOWN);
        CompUtils.setComponentName(cmbx1,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX01);
        cmbx1.setSize(width, height);
        cmbx1.setLocation(locationX, locationY);
        cmbx1.setItems(ComponentUtils.getListItems());
        cmbx1.select(0);
        cmbx1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        cmbx1.setMenu(getPopupMenu(getCurrentPageShell()));

        // comboBox 2
        locationY += yLocationIncrement;
        Combo cmbx2 = new Combo(parent, SWT.DROP_DOWN);
        CompUtils.setComponentName(cmbx2,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX02);
        cmbx2.setEnabled(false);
        cmbx2.setSize(width, height);
        cmbx2.setLocation(locationX, locationY);
        cmbx2.setItems(ComponentUtils.getListItems());
        cmbx2.select(0);

        // comboBox 3
        locationY += yLocationIncrement;
        Combo cmbx3 = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
        CompUtils.setComponentName(cmbx3,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX03);
        cmbx3.setSize(width, height);
        cmbx3.setLocation(locationX, locationY);
        cmbx3.setItems(ComponentUtils.getListItems());
        cmbx3.select(0);
        
        // comboBox 4
        locationY += yLocationIncrement;
        Combo cmbx4 = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
        CompUtils.setComponentName(cmbx4,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CMBX04);
        cmbx4.setSize(width, height);
        cmbx4.setLocation(locationX, locationY);
        cmbx4.setItems(ComponentUtils.getListLongItems());
        cmbx4.select(0);

        createCCombos(parent, getCurrentPageShell(), width, height, 
                locationX, locationY, yLocationIncrement);
    }

    /**
     * Creates a set of {@link CCombo} components. The components are combined
     * into a group in order to make it easier to tell from screen captures
     * what kind of component was being tested.
     * 
     * @param parent the composite to which the group will be added
     * @param contentShell the Shell which gets the composite
     * @param width Standard width for each combo box.
     * @param height Standard height to use for each combo box.
     * @param locationX X-coordinate to use for positioning the combo boxes.
     * @param groupLocationY Y-coordinate to use for positioning the combo boxes.
     * @param yLocationIncrement The vertical distance between the upper-left
     *                           corner of each combo box.   
     */
    private void createCCombos(Composite parent, Shell contentShell,
            int width, int height, int locationX,
            int groupLocationY, int yLocationIncrement) {

        int locationY = 25;

        Group comboGroup = new Group(parent, SWT.SHADOW_ETCHED_IN);
        comboGroup.setText("CCombo");
        comboGroup.setLocation(locationX, groupLocationY + yLocationIncrement);
        
        // CCombo 1
        CCombo ccombo1 = new CCombo(comboGroup, SWT.NONE);
        CompUtils.setComponentName(ccombo1,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CCOMBO1);
        ccombo1.setSize(width, height);
        ccombo1.setLocation(locationX, locationY);
        ccombo1.setItems(ComponentUtils.getListItems());
        ccombo1.select(0);
        ccombo1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        ccombo1.setMenu(getPopupMenu(contentShell));

        // CCombo 2
        locationY += yLocationIncrement;
        CCombo ccombo2 = new CCombo(comboGroup, SWT.NONE);
        CompUtils.setComponentName(ccombo2,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CCOMBO2);
        ccombo2.setEnabled(false);
        ccombo2.setSize(width, height);
        ccombo2.setLocation(locationX, locationY);
        ccombo2.setItems(ComponentUtils.getListItems());
        ccombo2.select(0);

        // CCombo 3
        locationY += yLocationIncrement;
        CCombo ccombo3 = new CCombo(comboGroup, SWT.READ_ONLY);
        CompUtils.setComponentName(ccombo3,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CCOMBO3);
        ccombo3.setSize(width, height);
        ccombo3.setLocation(locationX, locationY);
        ccombo3.setItems(ComponentUtils.getListItems());
        ccombo3.select(0);
        
        // CCombo 4
        locationY += yLocationIncrement;
        CCombo ccombo4 = new CCombo(comboGroup, SWT.READ_ONLY);
        CompUtils.setComponentName(ccombo4,
                        ComponentNameConstants.TESTPAGE_COMBOBOXES_CCOMBO4);
        ccombo4.setSize(width, height);
        ccombo4.setLocation(locationX, locationY);
        ccombo4.setItems(ComponentUtils.getListLongItems());
        ccombo4.select(0);

        comboGroup.setSize(
                width + (locationX * 2) + 50, 
                locationY + height + 10);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_comboBoxes"); //$NON-NLS-1$
    }
}
