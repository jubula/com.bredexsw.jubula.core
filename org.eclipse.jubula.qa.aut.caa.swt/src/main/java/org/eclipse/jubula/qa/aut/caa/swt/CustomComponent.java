
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
/**
 * Custom Component taken from: http://stackoverflow.com/questions/1779727/create-a-custom-button-with-swt
 *
 */
public class CustomComponent extends Composite {

    
    /** mouse **/
    private int m_mouse = 0;
    /** hit */
    private boolean m_hit = false;

    /**
     * constructor
     * @param parent the parent 
     * @param style the style
     */
    public CustomComponent(Composite parent, int style) {
        super(parent, style);
        this.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent e) {
                    switch (m_mouse) {
                    case 0:
                        // Default state
                        e.gc.drawString("Normal", 5, 5);
                        break;
                    case 1:
                        // Mouse over
                        e.gc.drawString("Mouse over", 5, 5);
                        break;
                    case 2:
                        // Mouse down
                        e.gc.drawString("Hit", 5, 5);
                        break;
                    default:
                        break;
                    }
            }
        });
        this.addMouseMoveListener(new MouseMoveListener() {
            public void mouseMove(MouseEvent e) {
                if (!m_hit) {
                    return;
                }
                m_mouse = 2;
                if (e.x < 0 || e.y < 0 || e.x > getBounds().width
                        || e.y > getBounds().height) {
                    m_mouse = 0;
                }
                redraw();
            }
        });
        this.addMouseTrackListener(new MouseTrackAdapter() {
            public void mouseEnter(MouseEvent e) {
                m_mouse = 1;
                redraw();
            }

            public void mouseExit(MouseEvent e) {
                m_mouse = 0;
                redraw();
            }
        });
        this.addMouseListener(new MouseAdapter() {
            public void mouseDown(MouseEvent e) {
                m_hit = true;
                m_mouse = 2;
                redraw();
            }

            public void mouseUp(MouseEvent e) {
                m_hit = false;
                m_mouse = 1;
                if (e.x < 0 || e.y < 0 || e.x > getBounds().width
                        || e.y > getBounds().height) {
                    m_mouse = 0;
                }
                redraw();
                if (m_mouse == 1) {
                    notifyListeners(SWT.Selection, new Event());
                }
            }
        });
        this.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.keyCode == '\r' || e.character == ' ') {
                    Event event = new Event();
                    notifyListeners(SWT.Selection, event);
                }
            }
        });
    }

}
