package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;

/**
 * ButtonListener to open a shell with Lists to test.
 * 
 * List 1: enabled = true tooltip = true contextmenu = true selectionmode =
 * single selection
 * 
 * List 2: enabled = false
 * 
 * List 3: selectionmode = multi intervall selection scrollbar = true
 * 
 * List 4: 200 entries, vertical scrollbar, clickCount monitor
 * 
 * not supported: multiselection only with shift
 * 
 */
public class ListShellOpener extends AbstractShellOpener {

    /**
     * ListShellOpener
     * 
     * @param owner
     *            owner
     */
    public ListShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent, 
                ComponentNameConstants.TESTPAGE_TITLE_LISTS);
        
        // list 1
        List lst1 = new List(parent, SWT.SINGLE);
        CompUtils.setComponentName(lst1, 
                ComponentNameConstants.TESTPAGE_LISTS_LST01);
        lst1.setToolTipText(I18NUtils.getString("tooltip"));
        lst1.setMenu(getPopupMenu(getCurrentPageShell()));
        lst1.setBounds(10, 10, 60, 90);
        lst1.setItems(ComponentUtils.getListItems());
        addDragAndDrop(lst1);
        // list 2
        List lst2 = new List(parent, SWT.SINGLE);
        CompUtils.setComponentName(lst2, 
                ComponentNameConstants.TESTPAGE_LISTS_LST02);
        lst2.setEnabled(false);
        lst2.setBounds(90, 10, 60, 90);
        lst2.setItems(ComponentUtils.getListItems());

        // list 3
        List lst3 = new List(parent, SWT.MULTI | SWT.H_SCROLL);
        CompUtils.setComponentName(lst3, 
                ComponentNameConstants.TESTPAGE_LISTS_LST03);
        lst3.setBounds(170, 10, 60, 90);
        lst3.setItems(ComponentUtils.getListItems());

        // list 4
        List lst4 = new List(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
        CompUtils.setComponentName(lst4, 
                ComponentNameConstants.TESTPAGE_LISTS_LST04);
        lst4.setBounds(250, 10, 60, 90);
        lst4.setItems(ComponentUtils.getList());
        lst4.addMouseListener(new MouseAdapter() {
            public void mouseDoubleClick(MouseEvent e) {
                getLabel().setText(I18NUtils.getName("2 Click"));
            }
            public void mouseDown(MouseEvent e) {
                getLabel().setText("1 Click");
            }
        });
    }
    
    /**
     * Adds drag and drop functionality to Lists
     * @param lst the list
     */
    private void addDragAndDrop(final List lst) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance()};
        DragSource source = new DragSource(lst, 
                DND.DROP_COPY | DND.DROP_MOVE);
        source.setTransfer(types);
        source.addDragListener(new DragSourceListener() {
            private String m_source = "";

            public void dragStart(DragSourceEvent event) {
                String[] selection = lst.getSelection();
                if (selection.length > 0) {
                    event.doit = true;
                    m_source = selection[0];
                } else {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {
                if (m_source != null) {
                    event.data = "Drag source: " + m_source;
                } 
            }

            public void dragFinished(DragSourceEvent event) {
                //nothing
            }
        });
        DropTarget target = new DropTarget(lst,
                DND.DROP_COPY | DND.DROP_MOVE);
        target.setTransfer(types);
        target.addDropListener(new DropTargetListener() {
            public void dropAccept(DropTargetEvent event) {
                //nothing
            }
            public void drop(DropTargetEvent event) {
                if (event.data == null) {
                    event.detail = DND.DROP_NONE;
                    return;
                }
                String text = event.data.toString() + "  ";
                if (event.data instanceof String) {
                    Point mouse = lst.toControl(event.x, event.y);
                    int itemTop = 0;
                    for (int i = 0; i < lst.getItemCount(); i++) {
                        if (mouse.y >= itemTop
                                && mouse.y <= itemTop + lst.getItemHeight()) {
                            int index = lst.getTopIndex() + i;
                            text += "Drop target: " + lst.getItem(index);
                        }
                        itemTop += lst.getItemHeight();
                    }
                }
                getLabel().setText(text);
                getLabel().pack();
            }
            
            public void dragOver(DropTargetEvent event) {
                event.feedback |= DND.FEEDBACK_SELECT;
                
            }
            
            public void dragOperationChanged(DropTargetEvent arg0) {
                //nothing
            }
            
            public void dragLeave(DropTargetEvent arg0) {
                //nothing
            }
            
            public void dragEnter(DropTargetEvent arg0) {
                //nothing
            }
        });
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_lists"); //$NON-NLS-1$
    }
}
