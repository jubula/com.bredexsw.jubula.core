package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * ButtonListener to open a shell with password text to test.
 * 
 * password Text 1: enabled = true; tooltip = true; contextmenu = true
 * 
 * password Text 2: enabled = false; text = false
 * 
 * password Text 3: enabled = true; editable = true; text = true
 * 
 * password Text 4: enabled = false; text = true
 * 
 * password Text 5: editable = false; text = true
 */
public class PasswordFieldShellOpener extends AbstractShellOpener {

    /**
     * PasswordFieldShellOpener
     * @param owner owner
     */
    public PasswordFieldShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_PASSWORDFIELDS);

        //passwordField 1
        Text pwdf1 = new Text(parent, SWT.BORDER | SWT.PASSWORD);
        CompUtils.setComponentName(pwdf1,
                ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pwdf1.setSize(70, 25);
        pwdf1.setLocation(10, 10);
        pwdf1.setToolTipText(I18NUtils.getString("tooltip"));
        pwdf1.setMenu(getPopupMenu(getCurrentPageShell()));
        
        //passwordField 2
        Text pwdf2 = new Text(parent, SWT.BORDER | SWT.PASSWORD);
        CompUtils.setComponentName(pwdf2,
                ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF02);
        pwdf2.setSize(70, 25);
        pwdf2.setLocation(10, 50);
        pwdf2.setEnabled(false);
        
        //passwordField 3
        Text pwdf3 = new Text(parent, SWT.BORDER | SWT.PASSWORD);
        CompUtils.setComponentName(pwdf3,
                ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF03);
        pwdf3.setSize(70, 25);
        pwdf3.setLocation(10, 90);
        pwdf3.setText(I18NUtils.getString("text1"));
        
        //passwordField 4
        Text pwdf4 = new Text(parent, SWT.BORDER | SWT.PASSWORD);
        CompUtils.setComponentName(pwdf4,
                ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF04);
        pwdf4.setSize(70, 25);
        pwdf4.setLocation(10, 130);
        pwdf4.setText(I18NUtils.getString("text2"));
        pwdf4.setEnabled(false);
        
        //passwordField 5
        Text pwdf5 = new Text(parent, SWT.BORDER | SWT.PASSWORD);
        CompUtils.setComponentName(pwdf5,
                ComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF05);
        pwdf5.setSize(70, 25);
        pwdf5.setLocation(10, 170);
        pwdf5.setText(I18NUtils.getString("text3"));
        pwdf5.setEditable(false);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_passwordField"); //$NON-NLS-1$
    }
}