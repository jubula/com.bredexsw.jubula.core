package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * ButtonListener to open a shell with RadioButtons to test.
 * 
 * RadioButton 1:
 * selected = false
 * tooltip = true
 * contextmenu = true
 * enabled = false
 * 
 * RadioButton 2:
 * enabled = true
 * selected = true
 * 
 * RadioButton 3:
 * enabled = false
 *
 */
public class RadioButtonShellOpener extends AbstractShellOpener {
    
    /**
     * RadioButtonShellOpener
     * @param owner owner
     */
    public RadioButtonShellOpener(Composite owner) {
        super(owner);
    }
  
    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_RADIOBUTTONS);
        
        // button 1
        Button btn1 = new Button(parent, SWT.RADIO);
        btn1.setText(I18NUtils.getName("rbtn1"));
        CompUtils.setComponentName(btn1,
                ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN01);
        btn1.setSize(85, 25);
        btn1.setLocation(10, 10);
        btn1.setToolTipText(I18NUtils.getString("tooltip"));
        btn1.setMenu(getPopupMenu(getCurrentPageShell()));
        btn1.setSelection(false);
        
        // button 2
        Button btn2 = new Button(parent, SWT.RADIO);
        btn2.setText(I18NUtils.getName("rbtn2"));
        CompUtils.setComponentName(btn2,
                ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN02);
        btn2.setSize(85, 25);
        btn2.setLocation(10, 50);
        btn2.setSelection(true);
                
        // button 3
        Button btn3 = new Button(parent, SWT.RADIO);
        btn3.setText(I18NUtils.getName("rbtn3"));
        CompUtils.setComponentName(btn3,
                ComponentNameConstants.TESTPAGE_RADIOBUTTONS_RBTN03);
        btn3.setSize(85, 25);
        btn3.setLocation(10, 90);
        btn3.setEnabled(false);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_radioButtons"); //$NON-NLS-1$
    }
}
