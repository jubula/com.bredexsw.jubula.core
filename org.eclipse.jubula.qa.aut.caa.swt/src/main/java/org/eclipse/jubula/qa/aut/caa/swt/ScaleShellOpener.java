package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Scale;

/**
 * ButtonListener to open a shell with scale to test.
 * 
 * scale 1: scale 2: scale 3: scale 4:
 */
public class ScaleShellOpener extends AbstractShellOpener {

    /**
     * scaleShellOpener
     * 
     * @param owner
     *            owner
     */
    public ScaleShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_SCALE);

        // scale 1
        Scale sc1 = new Scale(parent, SWT.HORIZONTAL);
        CompUtils.setComponentName(sc1,
                ComponentNameConstants.TESTPAGE_SCALE_SC01);
        sc1.setLocation(10, 10);
        sc1.setToolTipText(I18NUtils.getString("tooltip"));
        sc1.setMenu(getPopupMenu(getCurrentPageShell()));
        sc1.setBounds(10, 10, 150, 25);

        // scale 2
        Scale sc2 = new Scale(parent, SWT.HORIZONTAL);
        CompUtils.setComponentName(sc2,
                ComponentNameConstants.TESTPAGE_SCALE_SC02);
        sc2.setLocation(10, 50);
        sc2.setBounds(10, 50, 150, 25);
        sc2.setEnabled(false);

        // scale 3
        Scale sc3 = new Scale(parent, SWT.VERTICAL);
        CompUtils.setComponentName(sc3,
                ComponentNameConstants.TESTPAGE_SCALE_SC03);
        sc3.setBounds(10, 90, 25, 150);
        sc3.setToolTipText(I18NUtils.getString("tooltip"));
        sc3.setMenu(getPopupMenu(getCurrentPageShell()));

        // scale 4
        Scale sc4 = new Scale(parent, SWT.VERTICAL);
        CompUtils.setComponentName(sc4,
                ComponentNameConstants.TESTPAGE_SCALE_SC04);
        sc4.setBounds(50, 90, 25, 150);
        sc4.setEnabled(false);
    }
    
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_scale"); //$NON-NLS-1$
    }
}
