/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import java.sql.Time;
import java.util.Date;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

/**
 * 
 *
 * @author tobias
 * @created 05.08.2011
 * @version $Revision: 1.1 $
 */
public class ScrolledCompShellOpener extends AbstractShellOpener {

    /** Textfield for logs */
    private Text m_logtext;
    
    /**
     * ButtonShellOpener
     * 
     * @param owner
     *            owner
     */
    public ScrolledCompShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent, 
                ComponentNameConstants.MAINPAGE_SCROLLEDCOMP_TESTPAGE);
        
        ScrolledComposite scrollComp1 = new ScrolledComposite(parent, SWT.BORDER
            | SWT.H_SCROLL | SWT.V_SCROLL);
        scrollComp1.setLocation(10, 10);
        scrollComp1.setSize(122, 150);
        Composite comp1 = createComposite(scrollComp1, 1);
        scrollComp1.setContent(comp1);
        scrollComp1.setOrigin(0, 0);

        ScrolledComposite scrollComp2 = new ScrolledComposite(parent, SWT.BORDER
            | SWT.H_SCROLL | SWT.V_SCROLL);
        scrollComp2.setLocation(137, 10);
        scrollComp2.setSize(122, 150);
        Composite comp2 = createComposite(scrollComp2, 2);
        scrollComp2.setContent(comp2);
        scrollComp2.setOrigin(400, 400);
        
        ScrolledComposite scrollComp3 = new ScrolledComposite(parent, SWT.BORDER
            | SWT.H_SCROLL | SWT.V_SCROLL);
        scrollComp3.setLocation(264, 10);
        scrollComp3.setSize(122, 150);
        
        ScrolledComposite scrollComp31 = new ScrolledComposite(scrollComp3, 
                SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        scrollComp31.setSize(122, 150);
        Composite comp3 = createComposite(scrollComp31, 3);
        scrollComp31.setContent(comp3);
        scrollComp31.setOrigin(400, 400);
        scrollComp3.setContent(scrollComp31);
        
        m_logtext = new Text(parent, SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
        m_logtext.setLocation(10, 250);
        m_logtext.setSize(365, 110);
    }
    
    /**
     * @param scrollcomp parent composite
     * @param number scrollcomp
     * @return composite
     */
    private Composite createComposite(ScrolledComposite scrollcomp,
            int number) {
        Composite comp = new Composite(scrollcomp, SWT.LEFT);
        comp.setSize(410, 400);
        
        Button button = new Button(comp, SWT.PUSH);
        button.setText("Button" + number);
        button.setSize(400, 25);
        button.setLocation(5, 5);
        addActionListener(button, button.getText());
        
        Text text = new Text(comp, SWT.BORDER);
        text.setSize(400, 25);
        text.setLocation(5, 35);
        addActionListener(text, "Text" + number);
        
        return comp;
    }

    /**
     * @param widget clickable widget
     * @param name nam of widget
     */
    private void addActionListener(Widget widget, final String name) {
        
        widget.addListener(SWT.MouseDown, new Listener() {
            
            public void handleEvent(Event arg0) {
                Date today = new Date();
                Time time = new Time(today.getTime());
                m_logtext.insert(time + " " + name + "\n");
            }
        });
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_scrolledComp"); //$NON-NLS-1$
    }
}
