/*******************************************************************************
 * Copyright (c) 2016 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
/**
 * Simple Extension Test
 *
 */
public class SimpleExtensionShellOpener extends AbstractShellOpener {

    /**
     * boa-constrictor
     * @param owner who really owns something?
     */
    public SimpleExtensionShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * 
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_SIMPLE_EXTENSION);

        // comp 1
        CustomComponent cust = new CustomComponent(parent, SWT.PUSH);
        CompUtils.setComponentName(cust,
                ComponentNameConstants.TESTPAGE_SIMPLEEXT_C01);
        cust.setSize(70, 25);
        cust.setLocation(10, 10);

    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_simpleExtension"); //$NON-NLS-1$
    }
}
