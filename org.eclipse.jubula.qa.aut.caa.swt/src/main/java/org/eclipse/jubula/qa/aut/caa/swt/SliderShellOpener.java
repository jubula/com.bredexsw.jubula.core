package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Slider;

/**
 * ButtonListener to open a shell with slider to test.
 * 
 * slider 1: slider 2: slider 3: slider 4:
 */
public class SliderShellOpener extends AbstractShellOpener {

    /**
     * SliderShellOpener
     * 
     * @param owner
     *            owner
     */
    public SliderShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_SLIDER);

        // slider 1
        Slider sl1 = new Slider(parent, SWT.HORIZONTAL);
        CompUtils.setComponentName(sl1,
                ComponentNameConstants.TESTPAGE_SLIDER_SL01);
        // sl1.setLocation(10, 10);
        sl1.setToolTipText(I18NUtils.getString("tooltip"));
        sl1.setMenu(getPopupMenu(getCurrentPageShell()));
        sl1.setBounds(10, 10, 150, 25);
        sl1.setMaximum(130);

        // slider 2
        Slider sl2 = new Slider(parent, SWT.HORIZONTAL);
        CompUtils.setComponentName(sl2,
                ComponentNameConstants.TESTPAGE_SLIDER_SL02);
        sl2.setLocation(10, 50);
        sl2.setBounds(10, 50, 150, 25);
        sl2.setEnabled(false);

        // slider 3
        Slider sl3 = new Slider(parent, SWT.VERTICAL);
        CompUtils.setComponentName(sl3,
                ComponentNameConstants.TESTPAGE_SLIDER_SL03);
        sl3.setBounds(10, 90, 25, 150);
        sl3.setToolTipText(I18NUtils.getString("tooltip"));
        sl3.setMenu(getPopupMenu(getCurrentPageShell()));
        sl3.setMaximum(130);
        
        // slider 4
        Slider sl4 = new Slider(parent, SWT.VERTICAL);
        CompUtils.setComponentName(sl4,
                ComponentNameConstants.TESTPAGE_SLIDER_SL04);
        sl4.setBounds(50, 90, 25, 150);
        sl4.setEnabled(false);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_slider"); //$NON-NLS-1$
    }
}
