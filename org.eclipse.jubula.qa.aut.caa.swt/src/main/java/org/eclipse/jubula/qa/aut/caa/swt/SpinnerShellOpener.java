package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;

/**
 * ButtonListener to open a shell with sinner to test.
 * 
 * sinner 1: sinner 2: sinner 3: sinner 4:
 */
public class SpinnerShellOpener extends AbstractShellOpener {

    /**
     * SpinnerShellOpener
     * 
     * @param owner
     *            owner
     */
    public SpinnerShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_SPINNER);

        // spinner 1
        Spinner sp1 = new Spinner(parent, SWT.NONE);
        CompUtils.setComponentName(sp1,
                ComponentNameConstants.TESTPAGE_SPINNER_SP01);
        sp1.setLocation(10, 10);
        sp1.setToolTipText(I18NUtils.getString("tooltip"));
        sp1.setMenu(getPopupMenu(getCurrentPageShell()));
        sp1.setBounds(10, 10, 50, 25);

        // spinner 2
        Spinner sp2 = new Spinner(parent, SWT.NONE);
        CompUtils.setComponentName(sp2,
                ComponentNameConstants.TESTPAGE_SPINNER_SP02);
        sp2.setLocation(10, 50);
        sp2.setBounds(10, 50, 50, 25);
        sp2.setEnabled(false);

        // spinner 3
        Spinner sp3 = new Spinner(parent, SWT.READ_ONLY);
        CompUtils.setComponentName(sp3,
                ComponentNameConstants.TESTPAGE_SPINNER_SP03);
        sp3.setBounds(10, 90, 50, 25);
        sp3.setToolTipText(I18NUtils.getString("tooltip"));
        sp3.setMenu(getPopupMenu(getCurrentPageShell()));

    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_spinner"); //$NON-NLS-1$
    }
}
