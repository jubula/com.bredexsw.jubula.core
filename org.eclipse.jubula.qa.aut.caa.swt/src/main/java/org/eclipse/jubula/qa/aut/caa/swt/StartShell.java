package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * StartShell
 * 
 * TextPane, EditorPane and TextArea are not supported in SWT
 * 
 * @author markus
 * @created 19.03.2008
 */
public class StartShell {

    /**
     * constructor
     */
    private StartShell() {
    // empty
    }

    /**
     * getShell
     * 
     * @param display
     *            display
     * @return Shell
     */
    public static Shell getShell(Display display) {
        Shell shell = new Shell(display);
        shell.setText(I18NUtils.getString("title_swtAUT"));
        CompUtils.setComponentName(shell,
                I18NUtils.getString("title_swtAUT"));
        shell.setMaximized(true);
        shell.setLayout(new FillLayout());
        
        createContent(shell);
        return shell;
    }
    
    /**
     * @param composite the composite
     * @return the composite
     */
    public static Composite createContent(Composite composite) {
        ScrolledComposite scrollComposite = new ScrolledComposite(composite, 
                SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CENTER);
        Composite comp = new Composite(scrollComposite, SWT.NONE);
        
        GridLayout gridLayout = new GridLayout(2, false);
        comp.setLayout(gridLayout);
        gridLayout.horizontalSpacing = 15;
        gridLayout.verticalSpacing = 15;
        gridLayout.marginLeft = 25;
        gridLayout.marginTop = 25;
        
        createMainPageButtons(comp, new ButtonShellOpener(comp),
            ComponentNameConstants.MAINPAGE_BUTTONS_TESTPAGE);
        createMainPageButtons(comp, new CheckBoxShellOpener(comp),
            ComponentNameConstants.MAINPAGE_CHECKBOXES_TESTPAGE);
        createMainPageButtons(comp, new RadioButtonShellOpener(comp),
            ComponentNameConstants.MAINPAGE_RADIOBUTTONS_TESTPAGE);
        createMainPageButtons(comp, new ComboBoxShellOpener(comp),
            ComponentNameConstants.MAINPAGE_COMBOBOXES_TESTPAGE); 
        createMainPageButtons(comp, new TextFieldShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TEXTFIELDS_TESTPAGE);
        createMainPageButtons(comp, new LabelShellOpener(comp),
            ComponentNameConstants.MAINPAGE_LABELS_TESTPAGE);
        createMainPageButtons(comp, new ListShellOpener(comp),
            ComponentNameConstants.MAINPAGE_LISTS_TESTPAGE);
        createMainPageButtons(comp, new MenuShellOpener(comp),
            ComponentNameConstants.MAINPAGE_MENU_TESTPAGE);     
        createMainPageButtons(comp, new ApplicationShellOpener(comp),
            ComponentNameConstants.MAINPAGE_APPLICATIONS_TESTPAGE);
        createMainPageButtons(comp, new TreeShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TREES_TESTPAGE);
        createMainPageButtons(comp, new TableShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TABLES_TESTPAGE);
        createMainPageButtons(comp, new TabbedPaneShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TABBEDPANES_TESTPAGE);       
        createMainPageButtons(comp, new TextAreaShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TEXTAREAS_TESTPAGE);
        createMainPageButtons(comp, new EditorPaneShellOpener(comp),
            ComponentNameConstants.MAINPAGE_EDITORPANES_TESTPAGE);
        createMainPageButtons(comp, new TextPaneShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TEXTPANES_TESTPAGE);
        createMainPageButtons(comp, new TreeTableShellOpener(comp), 
            ComponentNameConstants.MAINPAGE_TREETABLES_TESTPAGE);
        createMainPageButtons(comp, new ScrolledCompShellOpener(comp),
            ComponentNameConstants.MAINPAGE_SCROLLEDCOMP_TESTPAGE);
        createMainPageButtons(comp, new PasswordFieldShellOpener(comp),
            ComponentNameConstants.MAINPAGE_PASSWORDFIELDS_TESTPAGE);
        createMainPageButtons(comp, new SliderShellOpener(comp),
            ComponentNameConstants.MAINPAGE_SLIDER_TESTPAGE);
        createMainPageButtons(comp, new ToolbarItemShellOpener(comp),
            ComponentNameConstants.MAINPAGE_TOOLBAR_ITEM_TESTPAGE);      
        createMainPageButtons(comp, new CLabelShellOpener(comp),
            ComponentNameConstants.MAINPAGE_CLABELS_TESTPAGE);
        createMainPageButtons(comp, new CTabFolderShellOpener(comp),
            ComponentNameConstants.MAINPAGE_CTABFOLDERS_TESTPAGE);        
        createMainPageButtons(comp, new ScaleShellOpener(comp),
                ComponentNameConstants.MAINPAGE_SCALE_TESTPAGE);
        createMainPageButtons(comp, new SpinnerShellOpener(comp),
                ComponentNameConstants.MAINPAGE_SCALE_TESTPAGE);
        createMainPageButtons(comp, new ProgressBarShellOpener(comp),
                ComponentNameConstants.MAINPAGE_PROGRESSBAR_TESTPAGE);        
        createMainPageButtons(comp, new SimpleExtensionShellOpener(comp),
                ComponentNameConstants.MAINPAGE_SIMPLE_EXTENSION_TESTPAGE);

        comp.setSize(comp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        scrollComposite.setContent(comp);
        
        return comp;
    }
        
    /**
     * @param c the Composite
     * @param opener the Page to open when Button is clicked
     * @param compName the Identifier for the Button
     */
    private static void createMainPageButtons(Composite c,
            AbstractShellOpener opener, String compName) {
        
        Button testPageButton = new Button(c, SWT.PUSH);
        testPageButton.setText(opener.getPageTitle());
        testPageButton.setLayoutData(new GridData(85, 25));
        testPageButton.addSelectionListener(opener);
        
        CompUtils.setComponentName(testPageButton, compName);
    }
}