/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 9793 $
 *
 * $Date: 2009-12-14 13:40:36 +0100 (Mon, 14 Dec 2009) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

/**
 * Tabfolder1: enabled=true; 3 Tabitems
 * Tabfolder2: enabled=false; 3 Tabitems
 * 
 * not supported in SWT: disable a single Tabitem
 *                       Tabitems on the left side
 * 
 * @author berndk
 * @created 14.01.2009
 */
public class TabbedPaneShellOpener extends AbstractShellOpener {

    /**
     * TabbedPaneShellOpener
     * 
     * @param owner
     *            owner
     */
    public TabbedPaneShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                        ComponentNameConstants.TESTPAGE_TITLE_TABBEDPANES);

        // Tabbed Pane 1
        Menu tabFolderContextMenu = getPopupMenu(getCurrentPageShell());
        TabFolder tabfolder1 = new TabFolder(parent, SWT.LEFT);
        tabfolder1.setLocation(10, 10);
        tabfolder1.setSize(200, 100);
        CompUtils.setComponentName(tabfolder1,
                ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP01);
        tabfolder1.setVisible(true);
        tabfolder1.setEnabled(true);
        tabfolder1.setMenu(tabFolderContextMenu);
        
        TabItem item1 = new TabItem(tabfolder1, SWT.LEFT);
        item1.setText(I18NUtils.getString("tab1"));
        Label lbl1 = new Label(tabfolder1, SWT.CENTER);
        lbl1.setText(I18NUtils.getString("img_carrot"));
        lbl1.setMenu(tabFolderContextMenu);
        item1.setControl(lbl1);

        TabItem item2 = new TabItem(tabfolder1, SWT.LEFT);
        item2.setText(I18NUtils.getString("tab2"));
        Label lbl2 = new Label(tabfolder1, SWT.CENTER);
        lbl2.setText(I18NUtils.getString("img_banana"));
        lbl2.setMenu(tabFolderContextMenu);
        item2.setControl(lbl2);
        
        TabItem item3 = new TabItem(tabfolder1, SWT.TOP);
        item3.setText(I18NUtils.getString("tab3"));
        Label lbl3 = new Label(tabfolder1, SWT.CENTER);
        lbl3.setEnabled(false);
        lbl3.setText(I18NUtils.getString("img_kiwi"));
        lbl3.setMenu(tabFolderContextMenu);
        item3.setControl(lbl3);
        
        
        // Tabbed Pane 2
        TabFolder tabfolder2 = new TabFolder(parent, SWT.LEFT);
        tabfolder2.setLocation(10, 120);
        tabfolder2.setSize(200, 100);
        CompUtils.setComponentName(tabfolder2,
                ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP02);
        tabfolder2.setVisible(true);
        tabfolder2.setEnabled(false);
        
        TabItem item21 = new TabItem(tabfolder2, SWT.RIGHT);
        item21.setText(I18NUtils.getString("tab1"));
        Label lbl21 = new Label(tabfolder2, SWT.CENTER);
        lbl21.setText(I18NUtils.getString("img_carrot"));
        item21.setControl(lbl21);

        TabItem item22 = new TabItem(tabfolder2, SWT.LEFT);
        item22.setText(I18NUtils.getString("tab2"));
        Label lbl22 = new Label(tabfolder2, SWT.CENTER);
        lbl22.setText(I18NUtils.getString("img_banana"));
        item22.setControl(lbl22);
        
        TabItem item23 = new TabItem(tabfolder2, SWT.LEFT);
        item23.setText(I18NUtils.getString("tab3"));
        Label lbl23 = new Label(tabfolder2, SWT.CENTER);
        lbl23.setText(I18NUtils.getString("img_kiwi"));
        item23.setControl(lbl23);
 
        createThirdTabbedPane(parent);
    }

    /**
     * @param c the parent to use
     */
    private void createThirdTabbedPane(Composite c) {
        TabFolder tabfolder = new TabFolder(c, SWT.LEFT);
        tabfolder.setLocation(10, 230);
        tabfolder.setSize(200, 100);
        CompUtils.setComponentName(tabfolder,
                ComponentNameConstants.TESTPAGE_TABBEDPANES_TBP03);
        tabfolder.setVisible(true);
        
        for (int i = 0; i < 50; i++) {
            TabItem item = new TabItem(tabfolder, SWT.RIGHT);
            item.setText(i + ""); //$NON-NLS-1$
            Label lbl = new Label(tabfolder, SWT.CENTER);
            lbl.setText(I18NUtils.getString("img_carrot"));
            item.setControl(lbl);
        }
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_tabbedPanes"); //$NON-NLS-1$
    }
}
