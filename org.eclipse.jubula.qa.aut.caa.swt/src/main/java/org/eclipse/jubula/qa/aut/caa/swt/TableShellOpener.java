/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 11727 $
 *
 * $Date: 2010-08-04 18:27:28 +0200 (Wed, 04 Aug 2010) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

/**
 * @author berndk
 * @created 14.01.2009
 */
public class TableShellOpener extends AbstractShellOpener {

    /**
     * TableShellOpener
     * 
     * 3 Tables:
     * 
     * Table1: 2 rows and 4 coloumns;
     *         multi selection mode = true; 
     *         setHeaderVisible =true;
     *         editable = false;
     *         
     * Table2: 3 rows and 4 columns;
     *         setHeaderVisible = false;
     *         editable = true;
     * 
     * Table3: 2 rows and 3 columns;
     *         with scrollbar and style SWT.FULL_SELECTION;
     *   
     * @param owner
     *            owner
     */
    public TableShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TABLES);
        createHeaderTable(parent, getCurrentPageShell(), SWT.NONE);
        createNoHeaderTable(parent);
        createScrollTable(parent);
        createBigScrollableTable(parent);
        createHeaderTable(parent, getCurrentPageShell(), SWT.CHECK);
        createSingleColumnCheckboxTable(parent);
    }

    /**
     * @param parent parent Component
     */
    private void createSingleColumnCheckboxTable(final Composite parent) {
        final Table tableCheckBoxSC = new Table(parent, SWT.CHECK
                | SWT.H_SCROLL | SWT.BORDER);        
        // table 6
        tableCheckBoxSC.setLayoutData(new GridData(GridData.FILL_BOTH));
        tableCheckBoxSC.setSize(100, 130);
        tableCheckBoxSC.setLocation(300, 350);
        tableCheckBoxSC.setHeaderVisible(true);
        tableCheckBoxSC.setLinesVisible(true);
        CompUtils.setComponentName(tableCheckBoxSC, 
                ComponentNameConstants.TESTPAGE_TABLES_TBL06);
        TableItem item1 = new TableItem(tableCheckBoxSC, SWT.NONE);
        item1.setText(I18NUtils.getString("text1")); //$NON-NLS-1$
        TableItem item2 = new TableItem(tableCheckBoxSC, SWT.NONE);
        item2.setText(I18NUtils.getString("text2")); //$NON-NLS-1$
        TableItem item3 = new TableItem(tableCheckBoxSC, SWT.NONE);
        item3.setText(I18NUtils.getString("text3")); //$NON-NLS-1$
        TableItem item4 = new TableItem(tableCheckBoxSC, SWT.NONE);
        item4.setText(I18NUtils.getString("text4")); //$NON-NLS-1$
    }

    /**
     * @param parent parent Component
     */
    private void createNoHeaderTable(final Composite parent) {
        // table 2
        final Table tableNoHeader = new Table(parent, SWT.BORDER | SWT.MULTI);
        tableNoHeader.setLayoutData(new GridData(GridData.FILL_BOTH));
        tableNoHeader.setSize(250, 80);
        tableNoHeader.setLocation(10, 120);
        tableNoHeader.setHeaderVisible(false);
        tableNoHeader.setLinesVisible(true);
        TableColumn columnNoHeader1 = new TableColumn(tableNoHeader, SWT.NONE);
        columnNoHeader1.setWidth(61);
        TableColumn columnNoHeader2 = new TableColumn(tableNoHeader, SWT.NONE);
        columnNoHeader2.setWidth(61);
        TableColumn columnNoHeader3 = new TableColumn(tableNoHeader, SWT.NONE);
        columnNoHeader3.setWidth(61);
        TableColumn columnNoHeader4 = new TableColumn(tableNoHeader, SWT.NONE);
        columnNoHeader4.setWidth(61);

        TableItem firstRow = new TableItem(tableNoHeader, SWT.NONE);
        firstRow.setText(new String[] { I18NUtils.getString("text1"), //$NON-NLS-1$
                I18NUtils.getString("text7"), I18NUtils.getString("text3"), //$NON-NLS-1$ //$NON-NLS-2$
                I18NUtils.getString("text5") }); //$NON-NLS-1$
        TableItem secondRow = new TableItem(tableNoHeader, SWT.NONE);
        secondRow.setText(new String[] { I18NUtils.getString("text2"), //$NON-NLS-1$
                I18NUtils.getString("text4"), I18NUtils.getString("text5"), //$NON-NLS-1$ //$NON-NLS-2$
                I18NUtils.getString("text1") }); //$NON-NLS-1$
        TableItem thirdRow = new TableItem(tableNoHeader, SWT.NONE);
        thirdRow.setText(new String[] { I18NUtils.getString("text3"), //$NON-NLS-1$
                I18NUtils.getString("text1"), I18NUtils.getString("text4"), //$NON-NLS-1$ //$NON-NLS-2$
                I18NUtils.getString("text7") }); //$NON-NLS-1$
        addTableEditor(tableNoHeader);
        tableNoHeader.getItem(0).setBackground(1, new Color(
                getCurrentPageShell().getDisplay(),
                200, 100, 0));
    }

    /**
     * @param parent parent Component
     * @param contentShell parent shell
     * @param checkBoxFlag additional Style-Parameter (only use SWT.NONE or SWT.CHECK)
     */
    private void createHeaderTable(Composite parent, Shell contentShell,
            int checkBoxFlag) {
        // table 1 and 5
        final Table table1 = new Table(parent, SWT.BORDER | SWT.MULTI
                | SWT.FULL_SELECTION | checkBoxFlag);
        Menu tableContextMenu = getPopupMenu(contentShell);
        table1.setLayoutData(new GridData(GridData.FILL_BOTH));
        if (checkBoxFlag == SWT.CHECK) {
            table1.setLocation(300, 230);
            CompUtils.setComponentName(table1,
                    ComponentNameConstants.TESTPAGE_TABLES_TBL05);
        } else {
            table1.setLocation(10, 10);
            CompUtils.setComponentName(table1,
                    ComponentNameConstants.TESTPAGE_TABLES_TBL01);
        }
        table1.setSize(250, 100);
        table1.setHeaderVisible(true);
        table1.setLinesVisible(true);
        table1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        createTableContents(table1);
        TableCursor cursor = new TableCursor(table1, SWT.NONE);
        cursor.setMenu(tableContextMenu);
        table1.setMenu(tableContextMenu);
    }

    /**
     * 
     * @param table table
     */
    private void createTableContents(final Table table) {
        TableColumn column1 = new TableColumn(table, SWT.NONE);
        column1.setText(I18NUtils.getString("col1")); //$NON-NLS-1$
        column1.setWidth(61);
        TableColumn column2 = new TableColumn(table, SWT.NONE);
        column2.setText(I18NUtils.getString("col2")); //$NON-NLS-1$
        column2.setWidth(61);
        TableColumn column3 = new TableColumn(table, SWT.NONE);
        column3.setText(I18NUtils.getString("col3")); //$NON-NLS-1$
        column3.setWidth(61);
        TableColumn column4 = new TableColumn(table, SWT.NONE);
        column4.setText(I18NUtils.getString("col4")); //$NON-NLS-1$
        column4.setWidth(61);
        TableItem item1 = new TableItem(table, SWT.NONE);
        item1.setText(new String[] { I18NUtils.getString("val1"), //$NON-NLS-1$
                I18NUtils.getString("val2"), "3", "true" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        TableItem item2 = new TableItem(table, SWT.NONE);
        item2.setText(new String[] { I18NUtils.getString("val4"), //$NON-NLS-1$
                I18NUtils.getString("val5"), "6", "false" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    
    /**
     * 
     * @param parent parent Component
     */
    private void createScrollTable(Composite parent) {
        // table 3
        final Table tableScroll = new Table(parent, SWT.BORDER | SWT.H_SCROLL 
                | SWT.V_SCROLL | SWT.FULL_SELECTION);
        tableScroll.setLayoutData(new GridData(GridData.FILL_BOTH));
        tableScroll.setSize(95, 65);
        tableScroll.setLocation(10, 220);
        tableScroll.setHeaderVisible(true);
        tableScroll.setLinesVisible(true);
        CompUtils.setComponentName(tableScroll, 
                ComponentNameConstants.TESTPAGE_TABLES_TBL03);
        createTableContents(tableScroll);
    }
    
    /**
     * @param parent parent Component
     */
    private void createBigScrollableTable(Composite parent) {
        Table table = new Table(parent, SWT.BORDER | SWT.H_SCROLL 
                | SWT.V_SCROLL | SWT.FULL_SELECTION);
        table.setSize(200, 200);
        table.setLocation(300, 10);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        
        char anfang = 'A'; 
        for (int i = 0; i < 20; i++) {
            TableColumn column = new TableColumn(table, SWT.NONE);
            column.setText(String.valueOf(anfang));
            column.setWidth(61);
            anfang++;
        }
        for (int i = 1; i < 21; i++) {
            anfang = 'A';
            TableItem item = new TableItem(table, SWT.NONE);
            for (int j = 0; j < 20; j++) {
                item.setText(j, anfang + String.valueOf(i));
                anfang++;
            }
        }
        addTableRightAlignColumn(table);
    }
    
    /**
     * @param table for a new column
     */
    private void addTableRightAlignColumn(Table table) {
        int index = table.getColumnCount();
        TableColumn column = new TableColumn (table, SWT.RIGHT, index);
        column.setText (I18NUtils.getString("rightAlignColName")); //$NON-NLS-1$
        TableItem [] items = table.getItems ();
        String rl = I18NUtils.getString("textRightAlign"); //$NON-NLS-1$
        for (int i = 0; i < items.length; i++) {
            items[i].setText(index, rl + (i + 1));
        }
        column.pack();
    }
    
    /**
     * @param table table
     */
    private void addTableEditor(final Table table) {
        final TableEditor editor = new TableEditor(table);
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        table.addListener(SWT.MouseDown, new Listener() {
            public void handleEvent(Event event) {
                Rectangle clientArea = table.getClientArea();
                Point pt = new Point(event.x, event.y);
                int index = table.getTopIndex();
                while (index < table.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = table.getItem(index);
                    for (int i = 0; i < table.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            final int column = i;
                            final Text text = new Text(table, SWT.NONE);
                            Listener textListener = new Listener() {
                                public void handleEvent(final Event e) {
                                    switch (e.type) {
                                        case SWT.FocusOut:
                                            item.setText(column, text
                                                            .getText());
                                            text.dispose();
                                            break;
                                        case SWT.Traverse:
                                            switch (e.detail) {
                                                case SWT.TRAVERSE_RETURN:
                                                    // FALL THROUGH
                                                case SWT.TRAVERSE_ESCAPE:
                                                    text.dispose();
                                                    e.doit = false;
                                                default:
                                                    break;
                                            }
                                            break;
                                        case SWT.Modify:
                                            Text text = (Text)editor.
                                                getEditor();
                                            editor.getItem().setText(column, 
                                                    text.getText());
                                        default:
                                            break;
                                    }
                                }
                            };
                            text.addListener(SWT.FocusOut, textListener);
                            text.addListener(SWT.Traverse, textListener);
                            text.addListener(SWT.Modify, textListener);
                            editor.setEditor(text, item, i);
                            text.setText(item.getText(i));
                            text.selectAll();
                            text.setFocus();
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        });
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_tables"); //$NON-NLS-1$
    }
    
}
