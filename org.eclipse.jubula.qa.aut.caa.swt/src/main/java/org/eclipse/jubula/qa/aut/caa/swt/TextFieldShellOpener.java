package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * ButtonListener to open a shell with Texts to test.
 * 
 * Text 1: 
 * enabled = true 
 * tooltip = true 
 * contextmenu = true
 * 
 * Text 2: 
 * enabled = false 
 * text = false
 * 
 * Text 3: 
 * enabled = true 
 * editable = true 
 * text = true
 * 
 * Text 4:
 * enabled = false 
 * text = true
 * 
 * Text 5: 
 * editable = false 
 * text = true
 * 
 */
public class TextFieldShellOpener extends AbstractShellOpener {

    /**
     * TextFieldShellOpener
     * 
     * @param owner
     *            owner
     */
    public TextFieldShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TEXTFIELDS);

        // textField 1
        Text tf1 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf1,
                ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF01);
        tf1.setSize(70, 25);
        tf1.setLocation(10, 10);
        tf1.setToolTipText(I18NUtils.getString("tooltip"));
        tf1.setMenu(getPopupMenu(getCurrentPageShell()));

        // textField 2
        Text tf2 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf2, 
                ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF02);
        tf2.setEnabled(false);
        tf2.setSize(70, 25);
        tf2.setLocation(10, 50);

        // textField 3
        Text tf3 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf3,
                ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF03);
        tf3.setText(I18NUtils.getString("text1"));
        tf3.setSize(70, 25);
        tf3.setLocation(10, 90);

        // textField 4
        Text tf4 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf4,
                ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF04);
        tf4.setText(I18NUtils.getString("text2"));
        tf4.setEnabled(false);
        tf4.setSize(70, 25);
        tf4.setLocation(10, 130);

        // textField 5
        Text tf5 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf5, 
                ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF05);
        tf5.setText(I18NUtils.getString("text3"));
        tf5.setEditable(false);
        tf5.setSize(70, 25);
        tf5.setLocation(10, 170);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_textFields"); //$NON-NLS-1$
    }
}
