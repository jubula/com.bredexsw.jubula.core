/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 11727 $
 *
 * $Date: 2010-08-04 18:27:28 +0200 (Wed, 04 Aug 2010) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * @author berndk
 * @created 14.01.2009
 */

/**
 * ButtonListener to open a shell with Trees to test.
 * 
 * Tree 1: enabled = true  selectionmode = multi intervall selection
 * 
 * Tree 2: enabled = false 
 *
 * Tree 3: enabled = true  selectionmode = multi intervall selection
 *         checked = true
 *
 * Tree 4: selectionmode = SWT.FULL_SELECTION
 */
public class TreeShellOpener extends AbstractShellOpener {

    /**
     * array to store the TreeItems for DynamicTree in
     */
    private static TreeItem items[] = new TreeItem[10];
    
    
    /**
     * TreeShellOpener
     * @param owner owner
     */
    public TreeShellOpener(Composite owner) {
        super(owner);
    }
  
    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TREES);
        
        Shell shell = getCurrentPageShell();
        createTree(parent, shell);
        
        createDisabledTree(parent);
        
        createCheckBoxTree(parent);
        
        createFullSelectionTree(parent);
        
        createExceptionTree(parent);
        
        createDynamicTree(parent, shell);
    }

    /**
     * Creates a Semi-dynamic tree loading tree items on demand
     * First item after expanding a node has to be static to get the "+" rendered,
     * following nodes with same parent are dynamically added
     * @param parent parent of the component
     * @param contentShell the Shell
     */
    private void createDynamicTree(Composite parent, Shell contentShell) {
        // tree6
        Tree tree6 = new Tree(parent, 
                SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
        tree6.setSize(180, 140);
        tree6.setLocation(140, 160);
        CompUtils.setComponentName(tree6,
                ComponentNameConstants.TESTPAGE_TREES_TRE06);

        tree6.setMenu(getPopupMenu(contentShell));
        
        TreeItem treeItem0 = new TreeItem(tree6, SWT.NONE);
        treeItem0.setText(I18NUtils.getString("text1"));
        items[0] = treeItem0;

        TreeItem treeItem1 = new TreeItem(treeItem0, SWT.NONE);
        treeItem1.setText(I18NUtils.getString("text2"));
        items[1] = treeItem1;
        TreeItem treeItem2 = new TreeItem(treeItem0, SWT.NONE);
        treeItem2.setText(I18NUtils.getString("text3"));
        items[2] = treeItem2;
        TreeItem treeItem3 = new TreeItem(treeItem0, SWT.NONE);
        treeItem3.setText(I18NUtils.getString("text6"));
        items[3] = treeItem3;
        TreeItem treeItem4 = new TreeItem(treeItem0, SWT.NONE);
        treeItem4.setText(I18NUtils.getString("text7"));
        items[4] = treeItem4;
        
        TreeItem treeItem21 = new TreeItem(items[2], SWT.NONE);
        treeItem21.setText(I18NUtils.getString("text4"));
        items[5] = treeItem21;
        TreeItem treeItem31 = new TreeItem(items[3], SWT.NONE);
        treeItem31.setText(I18NUtils.getString("text1"));
        items[7] = treeItem31;
        
        tree6.addListener(SWT.Expand, new Listener() {
            public void handleEvent(Event event) {
                TreeItem selectedItem = (TreeItem)event.item;
                if (selectedItem.getItemCount() == 1) {
                    if (selectedItem.equals(items[2])) {
                        TreeItem treeItem22 = new TreeItem(items[2], SWT.NONE);
                        treeItem22.setText(I18NUtils.getString("text5"));
                        items[6] = treeItem22;
                        TreeItem treeItem211 = new TreeItem(items[5], SWT.NONE);
                        treeItem211.setText(I18NUtils.getString("text7"));
                        items[8] = treeItem211;
                    } else if (selectedItem.equals(items[5])) {
                        TreeItem treeItem212 = new TreeItem(items[5], SWT.NONE);
                        treeItem212.setText(I18NUtils.getString("text2"));
                        items[9] = treeItem212;
                    }
                }
            }
        });
    }

    /**
     * @param parent adds a disabled tree to the composite
     */
    private void createDisabledTree(Composite parent) {
        // tree2
        Tree tree2 = new Tree(parent, SWT.BORDER);
        tree2.setSize(120, 30);
        tree2.setEnabled(false);
        tree2.setLocation(10, 120);
        CompUtils.setComponentName(tree2,
                ComponentNameConstants.TESTPAGE_TREES_TRE02);
        
        TreeItem tree2Item0 = new TreeItem(tree2, SWT.NONE);
        tree2Item0.setText(I18NUtils.getString("text3"));
        TreeItem tree2Item1 = new TreeItem(tree2Item0, SWT.NONE);
        tree2Item1.setText(I18NUtils.getString("text4"));

        TreeItem tree2Item11 = new TreeItem(tree2Item1, SWT.NONE);
        tree2Item11.setText(I18NUtils.getString("text7"));
        TreeItem tree2Item12 = new TreeItem(tree2Item1, SWT.NONE);
        tree2Item12.setText(I18NUtils.getString("text2"));
        
        TreeItem tree2Item2 = new TreeItem(tree2Item0, SWT.NONE);
        tree2Item2.setText(I18NUtils.getString("text5"));
    }

    /**
     * @param parent adds the main tree to the composite
     * @param contentShell the parent Shell
     */
    private void createTree(Composite parent, Shell contentShell) {
        // tree1
        Tree tree1 = new Tree(parent, 
                SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
        Menu tableContextMenu = getPopupMenu(contentShell);
        tree1.setSize(120, 100);
        tree1.setLocation(10, 10);
        CompUtils.setComponentName(tree1,
                ComponentNameConstants.TESTPAGE_TREES_TRE01);   
        
        createTreeItems(tree1);
        tree1.setMenu(tableContextMenu);
        addDragAndDrop(tree1);
    }

    /**
     * adds drag and drop functionality to a tree
     * @param tree the tree
     */
    private void addDragAndDrop(final Tree tree) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance()};
        DragSource source = new DragSource(tree, 
                DND.DROP_COPY | DND.DROP_MOVE);

        source.addDragListener(new DragSourceListener() {

            private TreeItem m_source = null;

            public void dragStart(DragSourceEvent event) {
                TreeItem[] selection = tree.getSelection();
                if (selection.length > 0) {
                    event.doit = true;
                    m_source = selection[0];
                } else {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {
                event.data = m_source.getText();
                //nothing
            }
            public void dragFinished(DragSourceEvent event) {
                //nothing
            }
        });
        
        source.setTransfer(types);
        DropTarget target = new DropTarget(tree,
                DND.DROP_COPY | DND.DROP_MOVE);
        target.setTransfer(types);
        
        target.addDropListener(new DropTargetListener() {

            public void dropAccept(DropTargetEvent event) {
                //nothing
            }

            public void drop(DropTargetEvent event) {
                if (event.data == null) {
                    event.detail = DND.DROP_NONE;
                    return;
                }
                String text = "Drag source: " + event.data.toString() + " - ";
                if (event.item instanceof TreeItem) {
                    text += "Drop target: " + ((TreeItem)event.item).getText();
                    
                }
                getLabel().setText(text);
                getLabel().pack();
            }

            public void dragOver(DropTargetEvent event) {
                event.feedback |= DND.FEEDBACK_SELECT;
            }

            public void dragOperationChanged(DropTargetEvent arg0) {
                //nothing
            }

            public void dragLeave(DropTargetEvent arg0) {
                //nothing
            }

            public void dragEnter(DropTargetEvent arg0) {
                //nothing
            }
        });
    }

    /**
     * 
     * @param tree tree where the items are created
     */
    private void createTreeItems(Tree tree) {
        TreeItem treeItem0 = new TreeItem(tree, SWT.NONE);
        treeItem0.setText(I18NUtils.getString("text1"));
        
        TreeItem treeItem1 = new TreeItem(treeItem0, SWT.NONE);
        treeItem1.setText(I18NUtils.getString("text2"));
        
        TreeItem treeItem2 = new TreeItem(treeItem0, SWT.NONE);
        treeItem2.setText(I18NUtils.getString("text3"));
        
        TreeItem treeItem21 = new TreeItem(treeItem2, SWT.NONE);
        treeItem21.setText(I18NUtils.getString("text4"));

        TreeItem treeItem211 = new TreeItem(treeItem21, SWT.NONE);
        treeItem211.setText(I18NUtils.getString("text7"));
        TreeItem treeItem212 = new TreeItem(treeItem21, SWT.NONE);
        treeItem212.setText(I18NUtils.getString("text2"));
        
        TreeItem treeItem22 = new TreeItem(treeItem2, SWT.NONE);
        treeItem22.setText(I18NUtils.getString("text5"));
        
        TreeItem treeItem3 = new TreeItem(treeItem0, SWT.NONE);
        treeItem3.setText(I18NUtils.getString("text6"));
        TreeItem treeItem31 = new TreeItem(treeItem3, SWT.NONE);
        treeItem31.setText(I18NUtils.getString("text1"));
        
        TreeItem treeItem4 = new TreeItem(treeItem0, SWT.NONE);
        treeItem4.setText(I18NUtils.getString("text7"));
    }

    /**
     * @param parent adds the CheckBoxTree to the composite
     */
    private void createCheckBoxTree(Composite parent) {
        // tree3
        Tree tree3 = new Tree(parent, SWT.BORDER | SWT.MULTI | SWT.CHECK 
                | SWT.V_SCROLL | SWT.H_SCROLL);
        tree3.setSize(120, 120);
        tree3.setLocation(10, 160);
        CompUtils.setComponentName(tree3, 
                ComponentNameConstants.TESTPAGE_TREES_TRE03);
        
        TreeItem tree3Item0 = new TreeItem(tree3, SWT.NONE);
        tree3Item0.setText(I18NUtils.getString("text1"));
        tree3Item0.setChecked(true);
        TreeItem tree3Item1 = new TreeItem(tree3, SWT.NONE);
        tree3Item1.setText(I18NUtils.getString("text6"));
        TreeItem tree3Item00 = new TreeItem(tree3Item0, SWT.NONE);
        tree3Item00.setText(I18NUtils.getString("text3"));
        TreeItem tree3Item01 = new TreeItem(tree3Item0, SWT.NONE);
        tree3Item01.setText(I18NUtils.getString("text5"));
        TreeItem tree3Item000 = new TreeItem(tree3Item00, SWT.NONE);
        tree3Item000.setText(I18NUtils.getString("text2"));
        TreeItem tree3Item001 = new TreeItem(tree3Item00, SWT.NONE);
        tree3Item001.setText(I18NUtils.getString("text7"));
        tree3Item001.setChecked(true);
        TreeItem tree3Item010 = new TreeItem(tree3Item01, SWT.NONE);
        tree3Item010.setText(I18NUtils.getString("text1"));
    }
    
    /**
     * @param parent adds the FullSelectionTree to the composite
     */
    private void createFullSelectionTree(Composite parent) {
        // tree 4
        Tree treeSelect = new Tree(parent, SWT.BORDER | SWT.FULL_SELECTION
                | SWT.V_SCROLL | SWT.H_SCROLL);
        treeSelect.setSize(65, 80);
        treeSelect.setLocation(140, 10);
        CompUtils.setComponentName(treeSelect, 
                ComponentNameConstants.TESTPAGE_TREES_TRE04);
        createTreeItems(treeSelect);
    }
    
    /**
     * @param parent adds a Tree with exception for long item /parent names
     */
    private void createExceptionTree(Composite parent) {
        //tree 5
        Tree tree = new Tree(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL 
               | SWT.CHECK | SWT.H_SCROLL);
        tree.setSize(120, 120);
        tree.setLocation(215, 10);
        CompUtils.setComponentName(tree, 
                ComponentNameConstants.TESTPAGE_TREES_TRE05);
        
        TreeItem treeItem = new TreeItem(tree, SWT.NONE);
        treeItem.setText(I18NUtils.getString("text1"));
        
        TreeItem treeItem0 = new TreeItem(treeItem, SWT.NONE);
        treeItem0.setText(I18NUtils.getString("long_item_name"));
        
        TreeItem treeItem01 = new TreeItem(treeItem0, SWT.NONE);
        treeItem01.setText(I18NUtils.getString("short_item_name"));
        TreeItem treeItem02 = new TreeItem(treeItem0, SWT.NONE);
        treeItem02.setText(I18NUtils.getString("short_item_name"));
        
        TreeItem treeItem1 = new TreeItem(treeItem, SWT.NONE);
        treeItem1.setText(I18NUtils.getString("long_item_name"));

        TreeItem treeItem11 = new TreeItem(treeItem1, SWT.NONE);
        treeItem11.setText(I18NUtils.getString("short_item_name"));
        TreeItem treeItem12 = new TreeItem(treeItem1, SWT.NONE);
        treeItem12.setText(I18NUtils.getString("long_item_name"));
        
        TreeItem treeItem2 = new TreeItem(treeItem, SWT.NONE);
        treeItem2.setText(I18NUtils.getString("short_item_name"));
        
        TreeItem treeItem21 = new TreeItem(treeItem2, SWT.NONE);
        treeItem21.setText(I18NUtils.getString("short_item_name"));
        TreeItem treeItem22 = new TreeItem(treeItem2, SWT.NONE);
        treeItem22.setText(I18NUtils.getString("long_item_name"));
        
        TreeItem treeItem3 = new TreeItem(treeItem, SWT.NONE);
        treeItem3.setText(I18NUtils.getString("short_item_name"));
        
        TreeItem treeItem31 = new TreeItem(treeItem3, SWT.NONE);
        treeItem31.setText(I18NUtils.getString("long_item_name"));
        TreeItem treeItem32 = new TreeItem(treeItem3, SWT.NONE);
        treeItem32.setText(I18NUtils.getString("long_item_name"));
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_trees"); //$NON-NLS-1$
    }
}
