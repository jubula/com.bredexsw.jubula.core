/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 11727 $
 *
 * $Date: 2010-08-04 18:27:28 +0200 (Wed, 04 Aug 2010) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;



import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

/**
 * TreeTable with 3 Columns
 * 
 * @author berndk
 * @created Jul 2, 2009
 */
public class TreeTableShellOpener extends AbstractShellOpener {

    /**
     * @param owner
     *            owner
     */
    public TreeTableShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TREETABLES);
        createTreeTable(parent);
        createFullSelectionTreeTable(parent);
        createSelectionTreeTable(parent);
    }

    /**
     * @param parent parent Composite
     */
    private void createTreeTable(final Composite parent) {
        final Tree tableTree = new Tree(parent, SWT.BORDER);
        tableTree.setSize(360, 200);
        tableTree.setLocation(10, 10);
        CompUtils.setComponentName(tableTree,
                ComponentNameConstants.TESTPAGE_TREETABLES_TRT01);
        tableTree.setHeaderVisible(true);
        createTreeTableColumns(tableTree);
        createTreeTableItems(tableTree);
        tableTree.getItem(3).getItem(2).setBackground(1,
                new Color(parent.getDisplay(), 200, 100, 0));
        // fix for issue http://bugzilla.bredex.de/153
        tableTree.setSelection(new TreeItem[] { tableTree.getItem(0) });
        addDragAndDrop(tableTree);
    }

    /**
     * 
     * @param tableTree Columns for the treeTable
     */
    private void createTreeTableColumns(final Tree tableTree) {
        TreeColumn column1 = new TreeColumn(tableTree, SWT.LEFT);
        column1.setText(I18NUtils.getString("col1"));
        column1.setWidth(120);
        TreeColumn column2 = new TreeColumn(tableTree, SWT.CENTER);
        column2.setText(I18NUtils.getString("col2"));
        column2.setWidth(120);
        TreeColumn column3 = new TreeColumn(tableTree, SWT.RIGHT);
        column3.setText(I18NUtils.getString("col3"));
        column3.setWidth(120);
    }
    
    /**
     * @param parent parent Composite
     */
    private void createFullSelectionTreeTable(final Composite parent) {
        final Tree tableTree2 = new Tree(parent, SWT.BORDER | SWT.H_SCROLL
                | SWT.FULL_SELECTION | SWT.V_SCROLL);
        tableTree2.setSize(95, 65);
        tableTree2.setLocation(10, 220);
        tableTree2.setHeaderVisible(true);
        CompUtils.setComponentName(tableTree2,
                ComponentNameConstants.TESTPAGE_TREETABLES_TRT02);
        createTreeTableColumns(tableTree2);
        createTreeTableItems(tableTree2);
    }
    
    /**
     * @param parent parent Composite
     */
    private void createSelectionTreeTable(final Composite parent) {
        final Tree tableTree3 = new Tree(parent, SWT.BORDER | SWT.H_SCROLL
                | SWT.FULL_SELECTION | SWT.V_SCROLL);
        tableTree3.setSize(370, 200);
        tableTree3.setLocation(200, 220);
        tableTree3.setHeaderVisible(true);
        CompUtils.setComponentName(tableTree3,
                ComponentNameConstants.TESTPAGE_TREETABLES_TRT03);
        createTreeTableColumns(tableTree3);
        createTreeTableItems(tableTree3);
        addDragAndDrop(tableTree3);
    }
    
    /**
     * @param tableTree
     *                  tableTree
     */
    private void createTreeTableItems(Tree tableTree) {
        fillDataFirstRow(tableTree);
        fillDataSecondRow(tableTree);
        fillDataThirdRow(tableTree);
        fillDataFourthRow(tableTree);
    }

    /**
     * @param tableTree tableTree
     */
    private void fillDataFirstRow(Tree tableTree) {
        TreeItem itemFirstRow = new TreeItem(tableTree, SWT.NONE);
        itemFirstRow.setText(new String[] { I18NUtils.getString("val1"), 
            I18NUtils.getString("text1"), I18NUtils.getString("tab1") });
        TreeItem subItemFirst1 = new TreeItem(itemFirstRow, SWT.NONE);
        subItemFirst1.setText(new String[] { I18NUtils.getString("item_first"), 
            I18NUtils.getString("text4"), I18NUtils.getString("menu") });
        TreeItem subItemFirst2 = new TreeItem(itemFirstRow, SWT.NONE);
        subItemFirst2.setText(new String[] { 
            I18NUtils.getString("item_second"), I18NUtils.getString("text1"), 
            I18NUtils.getString("menubar") }); 
        TreeItem subItemFirst3 = new TreeItem(itemFirstRow, SWT.NONE);
        subItemFirst3.setText(new String[] { I18NUtils.getString("item_third"), 
            I18NUtils.getString("text2"), I18NUtils.getString("menu_more") });
        TreeItem subItemFirst4 = new TreeItem(itemFirstRow, SWT.NONE);
        subItemFirst4.setText(new String[] { 
            I18NUtils.getString("item_fourth"), I18NUtils.getString("text7"), 
            I18NUtils.getString("menu") });
        TreeItem subsubItemFirst1 = new TreeItem(subItemFirst1, SWT.NONE);
        subsubItemFirst1.setText(new String[] { I18NUtils.getString("text1"), 
            I18NUtils.getString("text1"), I18NUtils.getString("text1") });
    }
    
    /**
     * @param tableTree tableTree
     */
    private void fillDataSecondRow(Tree tableTree) {
        TreeItem itemSecondRow = new TreeItem(tableTree, SWT.NONE);
        itemSecondRow.setText(new String[] { I18NUtils.getString("val2"), 
            I18NUtils.getString("text6"), I18NUtils.getString("img_banana") });
        TreeItem subItemSecond1 = new TreeItem(itemSecondRow, SWT.NONE);
        subItemSecond1.setText(new String[] { 
            I18NUtils.getString("item_first"), I18NUtils.getString("text7"), 
            I18NUtils.getString("tooltip") });
        TreeItem subItemSecond2 = new TreeItem(itemSecondRow, SWT.NONE);
        subItemSecond2.setText(new String[] { 
            I18NUtils.getString("item_second"), I18NUtils.getString("text2"), 
            I18NUtils.getString("title_lists") });
        TreeItem subItemSecond3 = new TreeItem(itemSecondRow, SWT.NONE);
        subItemSecond3.setText(new String[] { 
            I18NUtils.getString("item_third"), I18NUtils.getString("text5"), 
            I18NUtils.getString("title_trees") });
        TreeItem subItemSecond4 = new TreeItem(itemSecondRow, SWT.NONE);
        subItemSecond4.setText(new String[] { 
            I18NUtils.getString("item_fourth"), I18NUtils.getString("text4"), 
            I18NUtils.getString("title_menus") });
        TreeItem subsubItemSecond1 = new TreeItem(subItemSecond1, SWT.NONE);
        subsubItemSecond1.setText(new String[] { I18NUtils.getString("val6"), 
            I18NUtils.getString("text3"), I18NUtils.getString("tab1") });
    }
    
    /**
     * @param tableTree tableTree
     */
    private void fillDataThirdRow(Tree tableTree) {
        TreeItem itemThirdRow = new TreeItem(tableTree, SWT.NONE);
        itemThirdRow.setText(new String[] { I18NUtils.getString("val3"), 
            I18NUtils.getString("text3"), I18NUtils.getString("img_kiwi") });
        TreeItem subItemThird1 = new TreeItem(itemThirdRow, SWT.NONE);
        subItemThird1.setText(new String[] { 
            I18NUtils.getString("item_first"), I18NUtils.getString("text6"), 
            I18NUtils.getString("title_tables") });
        TreeItem subItemThird2 = new TreeItem(itemThirdRow, SWT.NONE);
        subItemThird2.setText(new String[] { 
            I18NUtils.getString("item_second"), I18NUtils.getString("text2"), 
            I18NUtils.getString("title_labels") });
        TreeItem subItemThird3 = new TreeItem(itemThirdRow, SWT.NONE);
        subItemThird3.setText(new String[] { 
            I18NUtils.getString("item_third"), I18NUtils.getString("tab1"), 
            I18NUtils.getString("title_buttons") });
        TreeItem subItemThird4 = new TreeItem(itemThirdRow, SWT.NONE);
        subItemThird4.setText(new String[] { 
            I18NUtils.getString("item_fourth"), I18NUtils.getString("tab2"), 
            I18NUtils.getString("title_application") });
        TreeItem subsubItemThird1 = new TreeItem(subItemThird1, SWT.NONE);
        subsubItemThird1.setText(new String[] { I18NUtils.getString("val5"), 
            I18NUtils.getString("tab3"), I18NUtils.getString("img_kiwi") });
    }
    
    /**
     * @param tableTree tableTree
     */
    private void fillDataFourthRow(Tree tableTree) {
        TreeItem itemFourthRow = new TreeItem(tableTree, SWT.NONE);
        itemFourthRow.setText(new String[] { I18NUtils.getString("val2"), 
            I18NUtils.getString("text6"), I18NUtils.getString("img_banana") });
        TreeItem subItemFourth1 = new TreeItem(itemFourthRow, SWT.NONE);
        subItemFourth1.setText(new String[] { 
            I18NUtils.getString("item_first"), I18NUtils.getString("text7"), 
            I18NUtils.getString("tooltip") });
        TreeItem subItemFourth2 = new TreeItem(itemFourthRow, SWT.NONE);
        subItemFourth2.setText(new String[] { 
            I18NUtils.getString("item_second"), I18NUtils.getString("text2"), 
            I18NUtils.getString("title_lists") });
        TreeItem subItemFourth3 = new TreeItem(itemFourthRow, SWT.NONE);
        subItemFourth3.setText(new String[] { 
            I18NUtils.getString("item_third"), I18NUtils.getString("text5"), 
            I18NUtils.getString("title_trees") });
        TreeItem subItemFourth4 = new TreeItem(itemFourthRow, SWT.NONE);
        subItemFourth4.setText(new String[] { 
            I18NUtils.getString("item_fourth"), I18NUtils.getString("text4"), 
            I18NUtils.getString("title_menus") });
        TreeItem subsubItemFourth1 = new TreeItem(subItemFourth1, SWT.NONE);
        subsubItemFourth1.setText(new String[] { I18NUtils.getString("val6"), 
            I18NUtils.getString("text3"), I18NUtils.getString("tab1") });
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_treeTables"); //$NON-NLS-1$
    }
    
    /**
     * adds drag and drop functionality to a tree
     * @param tree the tree
     */
    private void addDragAndDrop(final Tree tree) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance()};
        DragSource source = new DragSource(tree, 
                DND.DROP_COPY | DND.DROP_MOVE);

        source.addDragListener(new DragSourceListener() {

            private TreeItem m_source = null;

            public void dragStart(DragSourceEvent event) {
                TreeItem[] selection = tree.getSelection();
                if (selection.length > 0) {
                    event.doit = true;
                    m_source = selection[0];
                } else {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {
                event.data = m_source.getText();
                //nothing
            }
            public void dragFinished(DragSourceEvent event) {
                //nothing
            }
        });
        
        source.setTransfer(types);
        DropTarget target = new DropTarget(tree,
                DND.DROP_COPY | DND.DROP_MOVE);
        target.setTransfer(types);
        
        target.addDropListener(new DropTargetListener() {

            public void dropAccept(DropTargetEvent event) {
                //nothing
            }

            public void drop(DropTargetEvent event) {
                if (event.data == null) {
                    event.detail = DND.DROP_NONE;
                    return;
                }
                String text = "Drag source: " + event.data.toString() + " - ";
                if (event.item instanceof TreeItem) {
                    text += "Drop target: " + ((TreeItem)event.item).getText();
                    
                }
                getLabel().setText(text);
                getLabel().pack();
            }

            public void dragOver(DropTargetEvent event) {
                event.feedback |= DND.FEEDBACK_SELECT;
            }

            public void dragOperationChanged(DropTargetEvent arg0) {
                //nothing
            }

            public void dragLeave(DropTargetEvent arg0) {
                //nothing
            }

            public void dragEnter(DropTargetEvent arg0) {
                //nothing
            }
        });
    }
}
