/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 7745 $
 *
 * $Date: 2009-01-27 14:34:25 +0100 (Tue, 27 Jan 2009) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.utils;

import org.eclipse.swt.widgets.Widget;

/**
 * CompUtils
 * 
 * @author berndk
 * @created 20.01.2009
 */
public final class CompUtils {
    /** gd key for comp names */
    public static final String TEST_COMP_NAME_KEY = "TEST_COMP_NAME"; //$NON-NLS-1$

    /**
     * Constructor
     */
    private CompUtils() {
    // empty
    }

    /**
     * set component name at given widget, using the default gd key
     * 
     * @param anyWidget
     *            the widget to set a comp name for
     * @param compName
     *            the used comp name
     */
    public static void setComponentName(Widget anyWidget, String compName) {
        anyWidget.setData(TEST_COMP_NAME_KEY, compName);
    }
}
