/*******************************************************************************
 * Copyright (c) 2015 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.autcheck;

import java.util.Date;
import java.util.List;

import org.eclipse.jubula.client.AUTAgent;
import org.eclipse.jubula.client.MakeR;
import org.eclipse.jubula.tools.AUTIdentifier;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;


/** @author BREDEX GmbH */
public class App {
    
    /** the aut agent */
    private static AUTAgent agent;
    
    /** start time */
    private static Date startTime = new Date();
    
    /** aut id */
    private static String autid;
    
    /** state, i.e. "started" or "stopped" */
    private static String state;
    
    /** private constructor */
    private App() {
        //private
    }
    
    /**
     * @param args args
     */
    public static void main(String[] args) throws Exception {
        if ((args.length != 5)
                || (!args[4].equals("started")  //$NON-NLS-1$
                        && !args[4].equals("stopped"))) { //$NON-NLS-1$
            printUsageErrorMessage();
            System.exit(-1);
        }
        try {
            autid = args[0];
            String agenthost = args[1];
            int agentport = Integer.parseInt(args[2]);
            int timeout = Integer.parseInt(args[3]);
            long end = startTime.getTime() + timeout * 1000;
            state = args[4];
            boolean started = (state.equals("started")); //$NON-NLS-1$
            agent = MakeR.createAUTAgent(agenthost, agentport);
            agent.connect();
            List<AUTIdentifier> registeredAUTs;
            do {
                registeredAUTs = agent.getAllRegisteredAUTIdentifier();
                boolean autFound = false;
                for (AUTIdentifier autIdentifier : registeredAUTs) {
                    if (autid.equals(autIdentifier.getID())) {
                        autFound = true;
                    }
                }
                if (autFound == started) {
                    registered(started);
                    exit(0);
                }
                TimeUtil.delay(1000);
            } while (new Date().getTime() <= end);
            System.err.println("AUT '" + autid + "' was not " //$NON-NLS-1$ //$NON-NLS-2$
                    + state + " succesfully."); //$NON-NLS-1$
        } catch (NumberFormatException e) {
            printUsageErrorMessage();
            exit(-1);
        }
        exit(-1);
    }


    /**
     * 
     */
    private static void printUsageErrorMessage() {
        System.err.println("Usage: java -jar autcheck.jar " //$NON-NLS-1$
                + "<autid> <host> <portnumber> " //$NON-NLS-1$
                + "<timeout in seconds> started|stopped"); //$NON-NLS-1$
    }
    
    /**
     * Communicate the status
     * @param availableStatus the status
     */
    private static void registered(boolean availableStatus) {
        Date now = new Date();
        long delay = (now.getTime() - startTime.getTime()) / 1000l;
        System.out.println("AUT '" + autid + "' " + state + " after " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                + delay + " seconds."); //$NON-NLS-1$
    }

    /**
     * Exits with disconnecting from agent
     * @param exitCode exit code
     */
    private static void exit(int exitCode) {
        if (agent.isConnected()) {            
            agent.disconnect();
        }
        System.exit(exitCode);
    }
    
}