package org.eclipse.jubula.qa.dateutil;

import java.util.Date;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Hello world!
 * 
 */
public class App
{
    public static void main(String[] args)
    {
        Options options = new Options();
        OptionGroup result = new OptionGroup();
        result.setRequired(true);
        result.addOption(new Option("s", false, "set the result as exit code"));
        result.addOption(new Option("v", false, "display the result"));
        options.addOptionGroup(result);
        
        OptionGroup tasks = new OptionGroup();
        tasks.setRequired(true);
        tasks.addOption(new Option("dow", false,
                "the result is the day of the week, with Monday == 1"));
        tasks.addOption(new Option("we", false,
                "result is 1 if the current date is defined as weekend (Fr, Sa), 0 otherwise"));
        options.addOptionGroup(tasks);

        int exitCode = 0;
        CommandLineParser parser = new GnuParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            exitCode = process(cmd);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("dateutil", options);
        }
        System.exit(exitCode);
    }

    private static int process(CommandLine cmd) {
        boolean resultAsExitCode = cmd.hasOption("s");
        boolean displayResult = cmd.hasOption("v");
        int exitCode = 0;
        int result = 0;
        
        if (cmd.hasOption("dow")) {
            result = getDOW();
        } else if (cmd.hasOption("we")) {
            int dow = getDOW();
            if (dow==5 || dow == 6) {
                result = 1;
            } else {
                result = 0;
            }
        }
        
        if (resultAsExitCode) {
            exitCode = result;
        }
        if (displayResult) {
            System.out.println(result);
        }
        return exitCode;

    }

    private static int getDOW() {
        Date now = new Date();
        int day = now.getDay();
        if (day == 0) {
            day = 7;
        }
        return day;
    }
}
