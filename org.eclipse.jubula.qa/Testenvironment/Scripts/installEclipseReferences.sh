#!/bin/bash
set -x

refDir=/projects/guidancer/Workspace/EclipseInstallations/Reference
destDir=/projects/guidancer/Workspace/hu_snapshot/current/Testenvironment/EclipseRef
gdDir=/projects/guidancer/Workspace/hu_snapshot/current/platforms/lin.gtk.x86

if [ -d $destDir ]
then
chmod -R u+rw $destDir
rm -rf $destDir
fi
mkdir -p $destDir

for src in $refDir/eclipse*
do
    dest=$destDir/`basename $src`
    mkdir -p $dest
    lndir -silent $src $dest
done

cd $destDir
for e in eclipse*
do
(cd $e/plugins; unzip -xnq $gdDir/development/rcp-support.zip)
done
