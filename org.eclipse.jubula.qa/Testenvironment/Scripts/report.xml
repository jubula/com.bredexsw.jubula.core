<?xml version="1.0"?>
<project name="collectResults" default="countExecutedTests" basedir=".">
    <target name="init">
        <property name="hudson.url.base" value="http://build.dev.bredex.local:8080" />
        <property name="hudson.url.post" value="consoleText" />
        
        <mkdir dir="tmp"/>
        <get src="${hudson.url.base}/jnlpJars/hudson-cli.jar" dest="tmp/hudson-cli.jar"/>
    </target>
    
    <target name="getJobNames" depends="init">
        <!-- get job names by using the Hudson built-in CLI client -->
        <java jar="tmp/hudson-cli.jar" fork="true">
            <arg value="-s"/>
            <arg value="${hudson.url.base}"/>
            <arg value="list-jobs"/>
            <redirector outputproperty="jobNames">
                <outputfilterchain>
                    <containsregex pattern="gdTest.*"/>
                </outputfilterchain>
            </redirector>
        </java>
        
        <delete file="tmp/hudson-cli.jar"/>
    </target>
    
    <target name="countExecutedTests" depends="init, getJobNames">
        <!-- loop through all Hudson job and get results -->
        <script language="javascript">
            <![CDATA[
              var jobNames = project.getProperty("jobNames");
              var a = jobNames.split("\n");
              for (var i = 0; i < a.length; i++) {
                  var jobName = a[i];
                  
                  antcall = project.createTask("antcall");
                  antcall.init();
                  antcall.setTarget("getConsoleLog");
                  antcallParam = antcall.createParam();
                  antcallParam.setName("hudson.job.name");
                  antcallParam.setValue(jobName);
                  antcall.execute();
              }
            ]]>
        </script>
        
        <antcall target="echoNoReport"/>
        <antcall target="clean"/>
    </target>
    
    <target name="getConsoleLog">
        <touch file="tmp/${hudson.job.name}_log.txt"/>
        
        <get src="${hudson.url.base}/job/${hudson.job.name}/lastSuccessfulBuild/${hudson.url.post}" dest="tmp/${hudson.job.name}_log.txt" ignoreerrors="true"/>
        
        <antcall target="checkNoRun"/>
        <antcall target="getTimeAndCheck"/>
        <antcall target="parseTestSuites"/>
        <antcall target="parseTestJobs"/>
        <antcall target="parseJUnitTests"/>
        <antcall target="parseCmdToolErrors"/>
    </target>
    
    <target name="getTimeAndCheck">
        <touch file="tmp/timestamp.txt"/>
        
        <get src="${hudson.url.base}/job/${hudson.job.name}/lastSuccessfulBuild/buildTimestamp" dest="tmp/timestamp.txt" ignoreerrors="true"/>
        <loadfile property="timeStamp" srcfile="tmp/timestamp.txt">
            <filterchain>
                <!-- convert year to four digits -->
                <replaceregex pattern="(\d+/\d+/)(\d+)" replace="\120\2"/>
            </filterchain>
        </loadfile>
        
        <delete file="tmp/timestamp.txt"/>
        
        <touch file="tmp/jobs_no-report.txt"/>
        
        <!-- check whether test runs are from previous night -->
        <script language="javascript">
            <![CDATA[
              var timeStamp = project.getProperty("timeStamp");
              var jobName = project.getProperty("hudson.job.name");
              
              var testDate = new Date(timeStamp);
              var todayDate = new Date();
              
              var testDay = testDate.getDay();
              var testHours = testDate.getHours();
              
              var todayDay = todayDate.getDay();
              
              // the job will be listed as not executed if it didn't run on previous day or before 5 pm (i.e. not during the night)
              if(((todayDay + 6) % 7) != testDay || testHours < 17) {
                  concat = project.createTask("concat");
                  
                  var baseDir = project.getProperty("basedir");
                  var destFile = new java.io.File(basedir + "/tmp/jobs_no-report.txt");
                  var testsFile = new java.io.File(basedir + "/tmp/" + jobName + "_log.txt");
                  
                  concat.setAppend(true);
                  concat.setDestfile(destFile);
                  concat.addText("  " + jobName + "\n");
                  concat.execute();
                  
                  concat.setAppend(false);
                  concat.setDestfile(testsFile);
                  concat.addText("");
                  concat.execute();
              }
            ]]>
        </script>
    </target>
    
    <target name="checkNoRun">
        <!-- look for the string "Dtest.norun=-n" to determine whether the job ran in norun mode -->
        <loadfile property="log.norun" srcfile="tmp/${hudson.job.name}_log.txt">
            <filterchain>
                <containsregex pattern="^(.*)(Dtest.norun=-n)(.*\n)*"/>
            </filterchain>
        </loadfile>
        
        <antcall target="messageNoRunAndClear"/>
    </target>
    
    <target name="messageNoRunAndClear" if="log.norun">
        <echo>Tests were executed in no-run mode; reports will be ignored.</echo>
        
        <echo file="tmp/${hudson.job.name}_log.txt"></echo>
    </target>
    
    <target name="parseTestSuites">
        <!-- search for test suites  -->
        <loadfile property="log.filtered" srcfile="tmp/${hudson.job.name}_log.txt">
            <filterchain>
                <tokenfilter>
                    <containsregex pattern="(^running)|(\[echo\] running)|(Writing Report into file-system)"/>
                    
                    <!-- filter lines out containing output from AUT  -->
                    <replaceregex pattern="^.*(AUT).*$" replace=" " byline="true" />
                    
                    <!-- extract test suite name, e.g. "caa_swing_CONTEXT_MENU_FULLTEST" from "running caa_swing 1.0 en_US caa_swing_aut_hudson_win@localhost caa_swing_CONTEXT_MENU_FULLTEST 18000" -->
                    <replaceregex pattern=".*\s(.*)\s\d+$" replace="    \1" byline="true" />
                    
                    <!-- simplify -->
                    <replaceregex pattern=".*Writing Report into file-system.*" replace="reported successfully" byline="true" />
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <property name="log.filtered" value=""/>
        
        <touch file="tmp/testsuites.txt"/>
        
        <!-- write all test suites to file -->
        <concat append="true" destfile="tmp/testsuites.txt">${log.filtered}</concat>
        <replaceregexp file="tmp/testsuites.txt" match=".*reported successfully" replace="" byline="true" flags="g" />
        
        <touch file="tmp/testsuites_no-report.txt"/>
        
        <!-- write all test suites to file and filter suites which ran successfully -->
        <concat append="true" destfile="tmp/testsuites_no-report.txt">  ${hudson.job.name}:${line.separator}</concat>
        <concat append="true" destfile="tmp/testsuites_no-report.txt">${log.filtered}</concat>
        <replaceregexp file="tmp/testsuites_no-report.txt" match="((\r\n)|(\n))reported successfully" replace=" reported successfully" byline="false" flags="g" />
        <replaceregexp file="tmp/testsuites_no-report.txt" match=".*reported successfully" replace="" byline="true" flags="g" />
    </target>
    
    <target name="parseTestJobs">
        <!-- search for expected and perfomed test suite executions in test jobs -->
        <loadfile property="log.filtered.testJobs" srcfile="tmp/${hudson.job.name}_log.txt">
            <filterchain>
                <tokenfilter>
                    <containsregex pattern="(Test Job.*\d* test suite executions are expected)|(Test Job.*\d* test suite executions performed)"/>
                    
                    <replaceregex pattern="\s*\[(echo|exec)\]\s*(.*)" replace="    \2"/>
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <property name="log.filtered.testJobs" value=""/>
        
        <!-- write expected executions to file -->
        <concat destfile="tmp/testjobs_expected.txt">${log.filtered.testJobs}
            <filterchain>
                <tokenfilter>
                    <containsregex pattern=".*expected.*"/>
                    <ignoreblank/>
                </tokenfilter>
            </filterchain>
        </concat>
        
        <!-- write performed executions to file -->
        <concat destfile="tmp/testjobs_performed.txt">${log.filtered.testJobs}
            <filterchain>
                <tokenfilter>
                    <containsregex pattern=".*performed.*"/>
                    <ignoreblank/>
                </tokenfilter>
            </filterchain>
        </concat>
        
        <loadfile property="log.filtered.testjobs.expected" srcfile="tmp/testjobs_expected.txt"/>
        <loadfile property="log.filtered.testjobs.performed" srcfile="tmp/testjobs_performed.txt"/>
        
        <!-- calculate difference between expected and performed executions -->
        <script language="javascript">
            <![CDATA[
              var diffTests = "";
              
              var expectedTests = project.getProperty("log.filtered.testjobs.expected");
              var performedTests = project.getProperty("log.filtered.testjobs.performed");
              
              if(expectedTests != null && performedTests != null) {
                  var expectedArray = expectedTests.split("\n");
                  var performedArray = performedTests.split("\n");
                  
                  for (var i = 0; i < expectedArray.length - 1; i++) {
                      var jobName = expectedArray[i].match("Test Job \".*\":");
                      
                      // get the number of expected and performed executions
                      var expectedValue = expectedArray[i].match("([0-9]+) test suite executions");
                      var performedValue = performedArray[i].match("([0-9]+) test suite executions");
                      
                      var diffValue = expectedValue[1] - performedValue[1];
                      
                      if(diffValue != 0) {
                          diffTests += "    " + jobName + " " + diffValue + " test suite executions have not been performed...\n";
                      }
                  }
              }
              
              project.setProperty("log.filtered.testJobs.diffs", diffTests);
            ]]>
        </script>
        
        <delete file="tmp/testjobs_expected.txt"/>
        <delete file="tmp/testjobs_performed.txt"/>
        
        <touch file="tmp/testjobs_executed.txt"/>
        
        <concat append="true" destfile="tmp/testjobs_executed.txt">  ${hudson.job.name}:${line.separator}</concat>
        <concat append="true" destfile="tmp/testjobs_executed.txt">${log.filtered.testJobs.diffs}</concat>
    </target>
    
    <target name="parseJUnitTests">
        <!-- search for junit tests -->
        <loadfile property="log.filtered.junit" srcfile="tmp/${hudson.job.name}_log.txt">
            <filterchain>
                <tokenfilter>
                    <containsregex pattern="\[junit\].*"/>
                    
                    <replaceregex pattern="\s*\[junit\]\s(.*)" replace="    \1"/>
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <property name="log.filtered.junit" value=""/>
        
        <touch file="tmp/junit-tests_executed.txt"/>
        
        <concat append="true" destfile="tmp/junit-tests_executed.txt">  ${hudson.job.name}:${line.separator}</concat>
        <concat append="true" destfile="tmp/junit-tests_executed.txt">${log.filtered.junit}</concat>
    </target>
    
    <target name="parseCmdToolErrors">
        <!-- search for commandline tool test error -->
        <loadfile property="log.filtered.cmdlinetools" srcfile="tmp/${hudson.job.name}_log.txt">
            <filterchain>
                <tokenfilter>
                    <containsregex pattern="^(\s)*\[echo\](\s)*Commandline tool test error"/>
                    
                    <replaceregex pattern="\s*\[echo\]\s*Commandline tool test error: (.*)" replace="  \1"/>
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <property name="log.filtered.cmdlinetools" value=""/>
        
        <touch file="tmp/cmdlinetools_errors.txt"/>
        
        <concat append="true" destfile="tmp/cmdlinetools_errors.txt">${log.filtered.cmdlinetools}</concat>
    </target>
    
    <target name="countJobs">
        <loadfile property="jobs.noReport.all" srcfile="tmp/jobs_no-report.txt"/>
        <property name="jobs.noReport.all" value="${line.separator} "/>
        <delete file="tmp/jobs_no-report.txt"/>
        
        <resourcecount property="count.jobs.all">
            <tokens>
                <concat>${jobNames}
                    <filterchain>
                        <ignoreblank/>
                    </filterchain>
                </concat>
            </tokens>
        </resourcecount>
        
        <resourcecount property="count.jobs.noReport">
            <tokens>
                <concat>${jobs.noReport.all}
                    <filterchain>
                        <ignoreblank/>
                    </filterchain>
                </concat>
            </tokens>
        </resourcecount>
    </target>
    
    <target name="countTestSuites">
        <delete file="tmp/overview.txt"/>
        
        <tstamp>
            <format pattern="E" offset="-1" property="previous.day"/>
        </tstamp>
        
        <condition property="executionPlan.day.remove" value="Sunday" else="Friday">
            <or>
                <equals arg1="${previous.day}" arg2="Fri"/>
                <equals arg1="${previous.day}" arg2="Sat"/>
            </or>
        </condition>
        
        <!-- get the execution overview -->
        <get src="http://build.dev.bredex.local:8080/view/GUIdancer/job/gdExecutionOverview/lastSuccessfulBuild/artifact/com.bredexsw.jubula.core/org.eclipse.jubula.qa/Testenvironment/Scripts/tmp/overview.txt" dest="tmp/overview.txt"/>
        
        <!-- just show the execution plan of the previous day -->
        <replaceregexp file="tmp/overview.txt" match="${line.separator}(Execution|expected)" replace=";\1" flags="g"/>
        <replaceregexp file="tmp/overview.txt" match="Tests to execute in regression runs \(${executionPlan.day.remove} -->.*" replace=""/>
        <replaceregexp file="tmp/overview.txt" match=";" replace="${line.separator}" flags="g"/>
        
        <loadfile property="expectedTestsuites" srcfile="tmp/overview.txt">
            <filterchain>
                <tokenfilter>
                    <containsregex pattern=".*'testsuite'.*"/>
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <loadfile property="log.filtered.testsuites.noReport.all" srcfile="tmp/testsuites_no-report.txt">
            <filterchain>
                <tokenfilter>
                    <ignoreblank/>
                </tokenfilter>
            </filterchain>
        </loadfile>
        
        <loadfile property="testSuites" srcfile="tmp/testsuites.txt"/>
        
        <property name="log.filtered.testsuites.noReport.all" value=""/>
        <property name="testSuites" value=""/>
        
        <delete file="tmp/testsuites.txt"/>
        <delete file="tmp/testsuites_no-report.txt"/>
        
        <resourcecount property="count.testsuites.all">
            <tokens>
                <concat>${testSuites}
                    <filterchain>
                        <ignoreblank/>
                    </filterchain>
                </concat>
            </tokens>
        </resourcecount>
        
        <resourcecount property="count.testsuites.noReport">
            <tokens>
                <concat>${log.filtered.testsuites.noReport.all}
                    <filterchain>
                        <tokenfilter>
                            <replaceregex pattern=".*gdTest.*:" replace=""/>
                            <ignoreblank/>
                        </tokenfilter>
                    </filterchain>
                </concat>
            </tokens>
        </resourcecount>
        
        <resourcecount property="count.testsuites.expected">
            <tokens>
                <concat>${expectedTestsuites}
                    <filterchain>
                        <ignoreblank/>
                    </filterchain>
                </concat>
            </tokens>
        </resourcecount>
    </target>
    
    <target name="getTestJobs">
        <loadfile property="log.filtered.testjobs.all" srcfile="tmp/testjobs_executed.txt"/>
        <property name="log.filtered.testjobs.all" value="${line.separator} "/>
        <delete file="tmp/testjobs_executed.txt"/>
    </target>
    
    <target name="getJUnitTests">
        <loadfile property="log.filtered.junit.all" srcfile="tmp/junit-tests_executed.txt"/>
        <property name="log.filtered.junit.all" value="${line.separator} "/>
        <delete file="tmp/junit-tests_executed.txt"/>
    </target>
    
    <target name="getCmdToolErrors">
        <loadfile property="log.filtered.cmdlinetools.all" srcfile="tmp/cmdlinetools_errors.txt"/>
        <property name="log.filtered.cmdlinetools.all" value="${line.separator} "/>
        <delete file="tmp/cmdlinetools_errors.txt"/>
    </target>
    
    <target name="echoNoReport" description="print the calculated result of executed and reported test suites and JUnit tests" depends="countJobs, countTestSuites, getTestJobs, getJUnitTests, getCmdToolErrors">
        <fail unless="count.jobs.all" />
        <fail unless="count.jobs.noReport" />
        <fail unless="count.testsuites.all" />
        <fail unless="count.testsuites.noReport" />
        <echo message="########################################################################################" />
        <echo message="The following ${count.jobs.noReport} out of ${count.jobs.all} CI jobs did not run successfully last night:" />
        <echo message="${jobs.noReport.all}" />
        <echo message="########################################################################################" />
        <echo message="The following ${count.testsuites.noReport} out of the ${count.testsuites.all} started test suites (expected ${count.testsuites.expected}) did not write a report:" />
        <echo message="${log.filtered.testsuites.noReport.all}" />
        <echo message="########################################################################################" />    
        <echo message="Testjob execution failures for the last 24h:" />
        <echo message="${log.filtered.testjobs.all}" />
        <echo message="########################################################################################" />  
        <echo message="JUnit tests execution overview for the last 24h:" />
        <echo message="${log.filtered.junit.all}" />
        <echo message="########################################################################################" />  
        <echo message="Commandline tools test failures overview for the last 24h:" />
        <echo message="${log.filtered.cmdlinetools.all}" />
        <echo message="########################################################################################" />
    </target>
    
    <target name="clean">
        <delete>
            <fileset dir="tmp" includes="gdTest*.txt"/>
        </delete>
    </target>
</project>