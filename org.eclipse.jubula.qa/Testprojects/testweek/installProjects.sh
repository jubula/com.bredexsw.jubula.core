#!/bin/bash
#set -x
USER=$TEST_UDV_DATABASE_USERNAME
PWD=$TEST_UDV_DATABASE_PASSWORD

EXECDIR=/projects/guidancer/Workspace/hu_snapshot/current/platforms/lin.gtk.x86/ite
DBTOOL=$EXECDIR/dbtool
DIR=`dirname $0`

#work around a problem with GUI dependencies
export DISPLAY=unixdev.bredex.de:1

# delete all projects but the test result summary
echo Start deleting all projects at `date`
$DBTOOL -debug -consolelog -deleteall -keepsummary -dburl jdbc:oracle:thin:@//unixsrv7rh.dev.bredex.local:1521/gd -dbuser $USER -dbpw $PWD -data @none
echo Done deleting all projects at `date`

UNBOUND=`ls -1 $DIR/*.jub | grep -i unbound`
BOUND=`ls -1 $DIR/*.jub | grep -iv unbound`
echo Start importing at `date`
# import project 
for IMPORT in $UNBOUND $BOUND
do
echo Importing $IMPORT
$DBTOOL -import $IMPORT \
	-dburl jdbc:oracle:thin:@//unixsrv7rh.dev.bredex.local:1521/gd -dbuser $USER -dbpw $PWD -data @none
done
echo Done importing at `date`
